// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('prestamo_ind');rutaMenu('Mis Préstamos','Préstamos','Mis Préstamos');
}

function listado(){
    var fini 		= $("#fechaInicio").val();
    var ffin 		= $("#fechaFin").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	
	INI_LOAD();
    $.post("modulos/prestamo_ind/prestamo_ind.php?cmd=listar", {
        fini: fini, ffin:ffin, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Solicitud de Préstamo";
    }

    if (vista == 'visualizar') {
        cmd = "loadVisualizar";
        titulo = "Solicitud de Préstamo";
    }
   
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/prestamo_ind/prestamo_ind.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}


function mostrar(){
	var filtro = $("#idFiltro").val();

	if(filtro == 'tema'){
		$("#divCodigo").css("display","none");
		$("#divDescripcion").css("display","none");
		$("#divTema").css("display","block");
	}
	else{
	if(filtro == 'codigo'){
		$("#divCodigo").css("display","block");
		$("#divTema").css("display","none");
		$("#divDescripcion").css("display","none");
	}
	else{
		$("#divCodigo").css("display","none");
		$("#divTema").css("display","none");
		$("#divDescripcion").css("display","block");
	 	}
	}
}

function OpenForm2(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'visualizar') {
        cmd = "loadVer";
        titulo = "Visualizar Libro";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/prestamo_ind/prestamo_ind.php?cmd=" + cmd, {id: id},
        function(data) {
			$("#childModal1").html(data);
			FIN_LOAD();
        });

        $("#childModal1").modal({
        });
    });
}

function buscarLibro(){

	var idFacultad 		= $("#idFacultad").val();
    var idFiltro 		= $("#idFiltro").val();
	var descripcion		= $("#txtDescripcion").val();
	var codigo 	 		= $("#txtCodigo").val();
	var idTemas 		= $("#idTemas").val();
	
	INI_LOAD();
    $.post("modulos/prestamo_ind/prestamo_ind.php?cmd=buscarLibro", {
        idFacultad: idFacultad, idFiltro:idFiltro, descripcion: descripcion, codigo:codigo, idTemas:idTemas
    }, function(data) {
        $("#divTabla").html(data);
		FIN_LOAD();
    });
}

function solicitar(idLibro,nomLibro,disponible,idFacultad,nomFacultad,ejemplares){

	var idBiblioteca 	= $("#txtBiblioteca").val();
	var id 				= '';
	var data 			= '';
	var flag 			= '0';

	if(disponible <= 0){
		bootbox.alert("No se encuentra ejemplares disponibles del registro seleccionado");
		return;
	}

	if(idBiblioteca != ''){

	 if(idBiblioteca != idFacultad){ 
	 	bootbox.alert("No puede agregar ejemplares de distintas bibliotecas en la misma solicitud de préstamo");
		return;
	 }

	}

	 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        if(id == idLibro){ flag = '1'; }
        
	});

	if(flag == '1'){ bootbox.alert("El ejemplar ya fue agregado a la solicitud"); return; }	 

	$("#txtBiblioteca").val(idFacultad);

	$("#divSolicitado").css("display","block");

	var html = '<tr><td><input type="hidden" id=txtlibro_'+idLibro+' value= '+idLibro+' class="detalles" >'+nomFacultad+'</td>'+
				'<td>'+ nomLibro +'</td>'+
				'<td><input type="number" id=txtcant_'+idLibro+' value="1" min="1" max="'+disponible+'" size="5" /></td>'+
				'<td align=center><a class=cursor-point  title=Eliminar onclick="quitarEjemplar(this)"><span class="label label-danger">ELIMINAR</span></a></td></tr>';
      $('#detalle').append(html);

		var filas = $('#detalle tr').length;
		$("#txtSeleccionado").html("Ejemplares Solicitados: " + filas);
		$("#btnregistrar").prop("disabled", false);

}

function quitarEjemplar(objeto){
	$(objeto).parent('td').parent('tr').remove();
	var filas = $('#detalle tr').length;
	if(filas <= 0){
		$("#txtBiblioteca").val('');
		$("#divSolicitado").css("display","none");
		$("#btnregistrar").prop("disabled", true);
	}
	$("#txtSeleccionado").html("Ejemplares Solicitados: " + filas);

}

function registrar(){
	var id 				= '';
	var data 			= '';
	var detLibro		= '';
	var detCant			= '';
	var idBiblioteca 	= $("#txtBiblioteca").val();
	var diasPrestamo 	= $("#txtDiasPrestamo").val();
	var filas 			= $('#detalle tr').length;
	var valor 			= '';

	if(filas <= 0){
		bootbox.alert("Debe agregar ejemplares a solicitar");
		return;
	}

	 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        valor 		= $("#txtlibro_"+id).val();

        if(detLibro == ''){
        	detLibro = valor;
        }
        else{
        	detLibro = detLibro + '|' + valor;
        }

        valor 		= $("#txtcant_"+id).val();

        if(detCant == ''){
        	detCant = valor;
        }
        else{
        	detCant = detCant + '|' + valor;
        }

	});

		INI_LOAD();
    $.post("modulos/prestamo_ind/prestamo_ind.php?cmd=registrar", {
        idBiblioteca:idBiblioteca, detLibro:detLibro, detCant:detCant,diasPrestamo:diasPrestamo
    }, function(data) {
        if (data>=1) {
			$("#childModal1").modal('hide');
			MainForm();
			bootbox.alert("Registro guardado correctamente. Código de solicitud: "+data);
			window.open("modulos/prestamo_ind/vistas/descarga.php");
			setTimeout(function() { window.open('http://localhost:90/imprimir.php','_blank') },3000);
			FIN_LOAD();
            
        } else {
        	   if(data == 'failed'){
        	   		MainForm();
               		bootbox.alert('Error al registrar. Intente nuevamente');
			   		FIN_LOAD();
        	   }
        else{
        	  MainForm();
               bootbox.alert(data);
			   FIN_LOAD();
         }
        }
    });				
}

function imprimirPrestamo(idPrestamo){
	/*
	if(isset($_GET['cmd'])){
$documento=$_GET['cmd'];
	*/
	setTimeout(function() { window.open('vistas/pdfprestamo.php?cmd='+idPrestamo,'_blank') },3000);
}