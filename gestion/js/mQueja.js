// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('quejas');rutaMenu('Quejas','Sugerencias y Quejas','Quejas');
}

function listado(){
    var filtro 		= $("#txtBuscar").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	var idCarrera 	= $("#idFacultad").val();

	INI_LOAD();
    $.post("modulos/quejas/quejas.php?cmd=listar", {
        filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page, idCarrera:idCarrera
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Nueva Queja";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar Sugerencia";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/quejas/quejas.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrar(){
	var idFacultad  	= $("#idFacultad").val();
	var nomSugerencia  	= $("#nomSugerencia").val();
	var detSugerencia   = $("#detSugerencia").val();

		INI_LOAD();
    $.post("modulos/quejas/quejas.php?cmd=registrar", {
        idFacultad:idFacultad, nomSugerencia:nomSugerencia, detSugerencia:detSugerencia
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}

function OpenForm2(vista, id)
{
    var cmd = "loadCreacion";

    if (vista == 'visualizar') {
        cmd = "loadVisualizar";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/quejas/quejas.php?cmd=" + cmd, {id: id},
        function(data) {
			$("#childModal1").html(data);
			FIN_LOAD();
        });

        $("#childModal1").modal({
        });
    });
}

function grabarRpta(){
	var id  		= $("#txtid").val();
	var detRpta  	= $("#detRpta").val();

		INI_LOAD();
    $.post("modulos/quejas/quejas.php?cmd=grabarRpta", {
        id:id, detRpta:detRpta
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			listado();			
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });	
}


function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/sugerencia/sugerencia.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/sugerencia/sugerencia.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}