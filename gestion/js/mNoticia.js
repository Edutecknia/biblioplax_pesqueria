// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	var nomFacultad = $("#nomFacultad").val();

	 loadMenu('noticias');rutaMenu('Noticias','Inicio','Noticias');
}

function listado(){
    var idFacultad	= $("#idFacultad").val();
    var filtro 		= $("#txtBuscar").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	INI_LOAD();
    $.post("modulos/noticias/noticias.php?cmd=listar", {
        idFacultad:idFacultad,filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Nueva Noticia";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar Noticia";
    }

   
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/noticias/noticias.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrarNoticia(){
	var idFacultad 		= $("#idFacultad").val();
	var nombreNoticia  	= $("#nombreNoticia").val();
	var desNoticia  	= $(".fr-view").html();
	
		INI_LOAD();
    $.post("modulos/noticias/noticias.php?cmd=registrar", {
        idFacultad:idFacultad, nombreNoticia:nombreNoticia, desNoticia:desNoticia
    }, function(data) {
        if (data=="passed") {
        	$(".fr-popup").css("display","none");
        	$(".fr-popup fr-desktop fr-ltr").css("display","none"); 
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}


function editarNoticia(){
	var id 				= $("#txtid").val();
	var idFacultad 		= $("#idFacultad").val();
	var nombreNoticia  	= $("#nombreNoticia").val();
	var desNoticia  	= $(".fr-view").html();

		INI_LOAD();
    $.post("modulos/noticias/noticias.php?cmd=editar", {
        id:id,  idFacultad:idFacultad, nombreNoticia:nombreNoticia, desNoticia:desNoticia
    }, function(data) {
        if (data=="passed") {
        	$(".fr-popup").css("display","none");
        	$(".fr-popup fr-desktop fr-ltr").css("display","none"); 
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al editar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/noticias/noticias.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/noticias/noticias.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}