// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('autor');rutaMenu('Autores','Mantenimiento','Autores');
}

function listado(){
    var filtro 		= $("#txtBuscar").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	INI_LOAD();
    $.post("modulos/autor/autor.php?cmd=listar", {
        filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Nueva Autor";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar Autor";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/autor/autor.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrar(){
	var nomAutor  	= $("#nomAutor").val();
	var apeAutor  	= $("#apeAutor").val();
	var seudoAutor  = $("#seudoAutor").val();
	var idPais  	= $("#idPais").val();
	var idAutorTipo = $("#idAutorTipo").val();


		INI_LOAD();
    $.post("modulos/autor/autor.php?cmd=registrar", {
        nomAutor:nomAutor, apeAutor:apeAutor, seudoAutor:seudoAutor, idPais:idPais, idAutorTipo:idAutorTipo
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}


function editar(){
	var id 		= $("#txtid").val();
	var nomAutor  	= $("#nomAutor").val();
	var apeAutor  	= $("#apeAutor").val();
	var seudoAutor  = $("#seudoAutor").val();
	var idPais  	= $("#idPais").val();
	var idAutorTipo = $("#idAutorTipo").val();

		INI_LOAD();
    $.post("modulos/autor/autor.php?cmd=editar", {
        id:id, nomAutor:nomAutor, apeAutor:apeAutor, seudoAutor:seudoAutor, idPais:idPais, idAutorTipo:idAutorTipo
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/autor/autor.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/autor/autor.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}