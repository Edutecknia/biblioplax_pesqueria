// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('tabla_reporte');rutaMenu('Reportes','Reportes','Reportes Disponibles');
}

function listado(){
	INI_LOAD();
    $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=listar", {
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function loadReporte(nro) {
	
    $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=loadVistaReporte", {nro: nro},
    function(data) {
        $("#ContentPrincipal").html(data);
    });
}

function reporte_1(){
    var datos = '';

    var fecIni      = $("#fechaInicio").val();
    var fecFin      = $("#fechaFin").val();
    var idCarrera   = $("#idFacultad").val();
    var biblioteca  =  $("#idFacultad option:selected").text();

    datos = fecIni.concat('|');
    datos = datos.concat(fecFin);
    datos = datos.concat('|');
    datos = datos.concat(idCarrera);
    datos = datos.concat('|');
    datos = datos.concat(biblioteca);

    window.open('exportar/exporta_3.php?exporta='+datos,'Reporte');
}


/*RESULTADO DE REPORTES*/
function listado_1(){
	var idProducto	= $("#cboProducto").val();
    var fecInicio	= $("#fechaInicio").val();
	var fecFin		= $("#fechaFin").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	var idTransaccion    = $("#idTransaccion").val();

	INI_LOAD();
    $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=listado_1", {
        idProducto: idProducto, fecInicio: fecInicio, fecFin:fecFin, orden:orden, direccion:direccion, pagina: page, idTransaccion:idTransaccion
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function listado_2(){
	var idCara				= $("#cboCara").val();
	var idManguera	    	= $("#cboManguera").val();
    var fecInicio			= $("#fechaInicio").val();
	var fecFin				= $("#fechaFin").val();
	var orden 				= $("#orden").attr("value");
	var direccion			= $("#direccion").attr("value");
	var page 				= $("#pag_actual").attr("value");

	INI_LOAD();
    $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=listado_2", {
        idCara: idCara, idManguera:idManguera, fecInicio: fecInicio, fecFin:fecFin, orden:orden, direccion:direccion, pagina: page
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });

}

function listado_3(){
	var idCara				= $("#cboCara").val();
	var idManguera	    	= $("#cboManguera").val();
    var fecInicio			= $("#fechaInicio").val();
	var fecFin				= $("#fechaFin").val();
	var orden 				= $("#orden").attr("value");
	var direccion			= $("#direccion").attr("value");
	var page 				= $("#pag_actual").attr("value");

	INI_LOAD();
    $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=listado_3", {
        idCara: idCara, idManguera:idManguera, fecInicio: fecInicio, fecFin:fecFin, orden:orden, direccion:direccion, pagina: page
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });

}