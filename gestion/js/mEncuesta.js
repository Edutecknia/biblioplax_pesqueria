// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('encuesta');rutaMenu('Encuestas','Inicio','Encuestas');
}

function listado(){
    var filtro 		= $("#txtBuscar").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	var idCarrera 	= $("#idFacultad").val();

	INI_LOAD();
    $.post("modulos/encuesta/encuesta.php?cmd=listar", {
        filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page, idCarrera:idCarrera
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Nueva Encuesta";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar Encuesta";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/encuesta/encuesta.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function habilitar(id){
    var opcion = $("#idTipo_"+id).val();

    if(opcion=='SELECTIVA'){
        $("#opciones_"+id).prop('disabled', false);
    }
    else{
        $("#opciones_"+id).val('');
        $("#opciones_"+id).prop('disabled', true);
    }
}

function registrar(){
	var idFacultad  	= $("#idFacultad").val();
	var nomencuesta  	= $("#nomEncuesta").val();

	var pregunta_1      = $("#pregunta_1").val();
    var tipo_1          = $("#idTipo_1").val();
    var opcion_1        = $("#opciones_1").val();

    var pregunta_2      = $("#pregunta_2").val();
    var tipo_2          = $("#idTipo_2").val();
    var opcion_2        = $("#opciones_2").val();

    var pregunta_3      = $("#pregunta_3").val();
    var tipo_3          = $("#idTipo_3").val();
    var opcion_3        = $("#opciones_3").val();

    var pregunta_4      = $("#pregunta_4").val();
    var tipo_4          = $("#idTipo_4").val();
    var opcion_4        = $("#opciones_4").val();

    var pregunta_5      = $("#pregunta_5").val();
    var tipo_5          = $("#idTipo_5").val();
    var opcion_5        = $("#opciones_5").val();

		INI_LOAD();
    $.post("modulos/encuesta/encuesta.php?cmd=registrar", {
        idFacultad:idFacultad, nomencuesta:nomencuesta, 
        pregunta_1:pregunta_1, tipo_1:tipo_1, opcion_1:opcion_1,
        pregunta_2:pregunta_2, tipo_2:tipo_2, opcion_2:opcion_2,
        pregunta_3:pregunta_3, tipo_3:tipo_3, opcion_3:opcion_3,
        pregunta_4:pregunta_4, tipo_4:tipo_4, opcion_4:opcion_4,
        pregunta_5:pregunta_5, tipo_5:tipo_5, opcion_5:opcion_5,
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}

function Iniciar(id)
{
    bootbox.confirm("¿Seguro que deseas iniciar la encuesta?", function(result) {
        if(result){
        INI_LOAD();     
        $.post("modulos/encuesta/encuesta.php?cmd=iniciar", {
        id:id
        }, function(data) {
          if (data=="passed") {
              listado();        
              QuitarModal();    
              bootbox.alert("Registro iniciado correctamente");
              FIN_LOAD();
            
          } else{
                bootbox.alert("Error al iniciar, vuelva a intentarlo");
                FIN_LOAD();
          }
        });  
                        
        }
    });
}

function Eliminar(id)
{
    bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
        if(result){
        INI_LOAD();     
        $.post("modulos/encuesta/encuesta.php?cmd=eliminar", {
        id:id
        }, function(data) {
          if (data=="passed") {
              listado();        
              QuitarModal();    
              bootbox.alert("Registro eliminado correctamente");
              FIN_LOAD();
            
          } else{
                bootbox.alert("Error al eliminar, vuelva a intentarlo");
                FIN_LOAD();
          }
        });  
                        
        }
    });
}

function Finalizar(id)
{
    bootbox.confirm("¿Seguro que deseas finalizar la encuesta?", function(result) {
        if(result){
        INI_LOAD();     
        $.post("modulos/encuesta/encuesta.php?cmd=finalizar", {
        id:id
        }, function(data) {
          if (data=="passed") {
              listado();        
              QuitarModal();    
              bootbox.alert("Registro finalizado correctamente");
              FIN_LOAD();
            
          } else{
                bootbox.alert("Error al finalizar, vuelva a intentarlo");
                FIN_LOAD();
          }
        });  
                        
        }
    });
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/encuesta/encuesta.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/encuesta/encuesta.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}