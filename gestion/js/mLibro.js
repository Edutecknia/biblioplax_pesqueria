// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('libros');rutaMenu('Libros','Mantenimiento','Libros');
}

function listado(){
    var filtro 		= $("#txtBuscar").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	var idFacultad	= $("#idFacultad").val();
	var codLibro	= $("#txtBuscarCod").val();
	var desEstado	= $("#cboEstadoLibro").val();

	INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=listar", {
        filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page, idFacultad:idFacultad,codLibro:codLibro,desEstado:desEstado
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function ordenarLista(campo, direccion)
{
    $("#orden").attr("value", campo);
    $("#direccion").attr("value", direccion);
    listado();
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Nueva Libro";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar Libro";
    }
    if (vista == 'visualizar') {
        cmd = "loadVer";
        titulo = "Visualizar Libro";
    }
	if (vista == 'adjuntos') {
        cmd = "loadAdjuntos";
        titulo = "Documentos Adjuntos";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/libros/libros.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}


function agregarEjemplar(){
	var isbnLibro	= $("#isbnLibro").val();
	var codLibro  	= $("#codLibro").val();
	var barraLibro 	= $("#barraLibro").val();
	var Patrimonio 	= $("#codPatrimonio").val();

	/*if(isbnLibro == ''){
		bootbox.alert("Ingrese código ISBN del libro");
		return;
	}*/

	if(codLibro == ''){
		bootbox.alert("Ingrese código del libro");
		return;
	}

	/*if(barraLibro == ''){
		bootbox.alert("Ingrese código de barras del libro");
		return;
	}*/
	 
	 $("#divTabla").css("display","block");

	 var html = '<tr><td><input type="hidden" id=txtisbn_'+codLibro+' value= '+isbnLibro+' >'+isbnLibro+'</td><td><input type="hidden" id=txtcodlibro_'+codLibro+' value='+codLibro+'  class="detalles" >'+ codLibro +'</td><input type="hidden" id=txtbarralibro_'+codLibro+' value='+ barraLibro +' /><td><input type="hidden" id=txtpatrimoniolibro_'+codLibro+' value='+Patrimonio+' >'+Patrimonio+'</td><td align=center><a class=cursor-point  title=Eliminar onclick="quitarEjemplar(this)"><i class="fa fa-trash"></i></a></td></tr>';
      $('#detalle').append(html);

      	$("#isbnLibro").val("");
		$("#codLibro").val("");
		$("#barraLibro").val("");
		var filas = $('#detalle tr').length;
		$("#txtEjemplar").html("Ejemplares: " + filas);

}

function quitarEjemplar(objeto){
	$(objeto).parent('td').parent('tr').remove();
	var filas = $('#detalle tr').length;
	if(filas <= 0){
		$("#divTabla").css("display","none");
	}
	$("#txtEjemplar").html("Ejemplares: " + filas);

}

function validarMaterial(){
	var idFacultad		= $("#idFacultad").val();
	var tituloLibro		= $("#tituloLibro").val();
	var idAutor		  	= $("#idAutor").val();
	var idEditorial  	= $("#idEditorial").val();
	var filas 			= $('#detalle tr').length;

	if(filas <= 0){
		bootbox.alert("Debe agregar ejemplares del libro");
		return;
	}

	INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=validarLibro", {
        idFacultad:idFacultad,tituloLibro:tituloLibro, idAutor:idAutor, idEditorial:idEditorial
    }, function(data) {
        if (data=="passed") {
        	FIN_LOAD();
        	registrar();
        } 
        else {
        		//$("#childModal1").html("");
        		mostrarMaterialDuplicado(data); //$("#childModal1").html(data);
			   FIN_LOAD();
        }
    });

    /*$("#childModal1").modal({
    });*/

}

function mostrarMaterialDuplicado(data){

    $("#childModal1").html("");
    $("#childModal1").html(data);
    $("#childModal1").modal({
    });
}

function vincularEjemplar(idLibro){
	
	var id 		= '';
	var data 	= '';
	var detIsbn	= '';
	var detCod	= '';
	var detBar	= '';
	var detPat	= '';
	var valor 	= '';


		INI_LOAD();

		 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        valor 		= $("#txtisbn_"+id).val();

        if(detIsbn == ''){
        	detIsbn = valor;
        }
        else{
        	detIsbn = detIsbn + '|' + valor;
        }

        valor 		= $("#txtcodlibro_"+id).val();

        if(detCod == ''){
        	detCod = valor;
        }
        else{
        	detCod = detCod + '|' + valor;
        }

        valor 		= $("#txtbarralibro_"+id).val();

        if(detBar == ''){
        	detBar = valor;
        }
        else{
        	detBar = detBar + '|' + valor;
        }

        valor 		= $("#txtpatrimoniolibro_"+id).val();

        if(detPat == ''){
        	detPat = valor;
        }
        else{
        	detPat = detPat + '|' + valor;
        }
        

	});

    $.post("modulos/libros/libros.php?cmd=vincularEjemplar", {
        idLibro:idLibro,detIsbn:detIsbn, detCod:detCod, detBar:detBar, detPat:detPat
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
        		$("#childModal1").modal('hide');
				QuitarModal();
				MainForm();
               	bootbox.alert(data);
			   	FIN_LOAD();
        }
    });

}

function registrar(){
	var id 		= '';
	var data 	= '';
	var detIsbn	= '';
	var detCod	= '';
	var detBar	= '';
	var detPat	= '';
	var valor 	= '';
	var filas 	= $('#detalle tr').length;

	if(filas <= 0){
		bootbox.alert("Debe agregar ejemplares del libro");
		return;
	}

	var idFacultad		= $("#idFacultad").val();
	var idLibroTipo  	= $("#idLibroTipo").val();
	var tituloLibro		= $("#tituloLibro").val();
	var idAutor		  	= '';//$("#idAutor").val();
	var idEditorial  	= $("#idEditorial").val();
	var anioLibro  		= $("#anioLibro").val();
	var idPais			= $("#idPais").val();
	var volLibro		= $("#volLibro").val();
	var paginaLibro		= $("#paginaLibro").val();
	var idAdquisicion	= $("#idAdquisicion").val();
	var idProveedor		= $("#idProveedor").val();
	var fechaAdquisicion= $("#fechaAdquisicion").val();
	var precioLibro		= $("#precioLibro").val();
	var temasRelacion	= $("#idTemas").val();

    $(".detallesAutor").each(function(i,x){
        id         = $(this).attr('id');
         if(idAutor == ''){
            idAutor = id;
        }
        else{
            idAutor = idAutor + '|' + id;
        }
    });
    
	 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        valor 		= $("#txtisbn_"+id).val();

        if(detIsbn == ''){
        	detIsbn = valor;
        }
        else{
        	detIsbn = detIsbn + '|' + valor;
        }

        valor 		= $("#txtcodlibro_"+id).val();

        if(detCod == ''){
        	detCod = valor;
        }
        else{
        	detCod = detCod + '|' + valor;
        }

        valor 		= $("#txtbarralibro_"+id).val();

        if(detBar == ''){
        	detBar = valor;
        }
        else{
        	detBar = detBar + '|' + valor;
        }

        valor 		= $("#txtpatrimoniolibro_"+id).val();

        if(detPat == ''){
        	detPat = valor;
        }
        else{
        	detPat = detPat + '|' + valor;
        }
        

	});

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=registrar", {
        idFacultad:idFacultad, idLibroTipo:idLibroTipo, tituloLibro:tituloLibro, idAutor:idAutor, 
        idEditorial:idEditorial, anioLibro:anioLibro, idPais:idPais, volLibro:volLibro,
        paginaLibro:paginaLibro, idAdquisicion:idAdquisicion, idProveedor:idProveedor,
        fechaAdquisicion:fechaAdquisicion, precioLibro:precioLibro, temasRelacion:temasRelacion,
        detIsbn:detIsbn, detCod:detCod, detBar:detBar, detPat:detPat
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
        	   $("#childModal1").modal('hide');
        	   QuitarModal();
        	   MainForm();
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}


function editar(){
	var id 		= '';
	var data 	= '';
	var detIsbn	= '';
	var detCod	= '';
	var detBar	= '';
	var detPat	= '';
	var valor 	= '';
	var filas 	= $('#detalle tr').length;

	if(filas <= 0){
		bootbox.alert("Debe agregar ejemplares del libro");
		return;
	}

	var idLibro 		= $("#txtid").val();
	var idFacultad		= $("#idFacultad").val();
	var idLibroTipo  	= $("#idLibroTipo").val();
	var tituloLibro		= $("#tituloLibro").val();
	var idAutor		  	= '';//$("#idAutor").val();
	var idEditorial  	= $("#idEditorial").val();
	var anioLibro  		= $("#anioLibro").val();
	var idPais			= $("#idPais").val();
	var volLibro		= $("#volLibro").val();
	var paginaLibro		= $("#paginaLibro").val();
	var idAdquisicion	= $("#idAdquisicion").val();
	var idProveedor		= $("#idProveedor").val();
	var fechaAdquisicion= $("#fechaAdquisicion").val();
	var precioLibro		= $("#precioLibro").val();
	var temasRelacion	= $("#idTemas").val();

    $(".detallesAutor").each(function(i,x){
        id         = $(this).attr('id');
         if(idAutor == ''){
            idAutor = id;
        }
        else{
            idAutor = idAutor + '|' + id;
        }
    });

	 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        valor 		= $("#txtisbn_"+id).val();
        
        if(detIsbn == ''){
        	detIsbn = valor;
        }
        else{
        	detIsbn = detIsbn + '|' + valor;
        }

        valor 		= $("#txtcodlibro_"+id).val();

        if(detCod == ''){
        	detCod = valor;
        }
        else{
        	detCod = detCod + '|' + valor;
        }

        valor 		= $("#txtbarralibro_"+id).val();

        if(detBar == ''){
        	detBar = valor;
        }
        else{
        	detBar = detBar + '|' + valor;
        }

        valor 		= $("#txtpatrimoniolibro_"+id).val();

        if(detPat == ''){
        	detPat = valor;
        }
        else{
        	detPat = detPat + '|' + valor;
        }

	});

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=editar", {
        idLibro:idLibro, idFacultad:idFacultad, idLibroTipo:idLibroTipo, tituloLibro:tituloLibro, idAutor:idAutor, 
        idEditorial:idEditorial, anioLibro:anioLibro, idPais:idPais, volLibro:volLibro,
        paginaLibro:paginaLibro, idAdquisicion:idAdquisicion, idProveedor:idProveedor,
        fechaAdquisicion:fechaAdquisicion, precioLibro:precioLibro, temasRelacion:temasRelacion,
        detIsbn:detIsbn, detCod:detCod, detBar:detBar, detPat:detPat
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
        	   MainForm();
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });			
}

function listarAdjuntos(){
	var id = $("#txtid").val();

	INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=listarAdjuntos", {
        id:id
    }, function(data) {
        $("#divTabla").html(data);
		FIN_LOAD();
    });

}

function registrarAdjunto(){
	var id 			= $("#txtid").val();
	var desArchivo 	= $("#desAdjunto").val();
	var tipoArchivo = $("#idLibroTipo").val();

	INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=registrarAdjunto", {
        id:id,desArchivo:desArchivo,tipoArchivo:tipoArchivo
    }, function(data) {
        if (data=="passed") {
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
			listarAdjuntos();
        } else {
        	  bootbox.alert("Error al registrar, vuelva a intentarlo");
			   FIN_LOAD();
        }
    });

}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/libros/libros.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert(data);
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/libros/libros.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function EliminarAdjunto(id){

	bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/libros/libros.php?cmd=eliminarAdjunto", {
        id:id
	    }, function(data) {
          if (data=="passed") {
          	  listarAdjuntos();
			  QuitarModal();			
			  bootbox.alert("Registro eliminado correctamente");
			  FIN_LOAD();
          } else{
                bootbox.alert("Error al eliminar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});

}

function eliminarEjemplar(codLibro){

	var idLibro = $("#txtid").val();
	
	bootbox.confirm("¿Seguro que deseas eliminar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/libros/libros.php?cmd=eliminarEjemplar", {
        idLibro:idLibro, codLibro:codLibro
	    }, function(data) {
          if (data=="passed") {		
          	  $("#fila_"+codLibro).trigger("click");
			  bootbox.alert("Registro eliminado correctamente");
			  FIN_LOAD();
          } else{
                bootbox.alert(data);
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});

}

function myFunction(event) {
    texto 		= $("#codLibro").val();
    var press 	= event.key;

     $.post("modulos/libros/libros.php?cmd=generarBarra", {
        texto:texto, press:press
	    }, function(data) {
          	$("#barraLibro").val(data);
    	});	 
}


function OpenFormModal(vista)
{
    var cmd = "loadCreacion";

    if (vista == 'autor') {
        cmd = "loadAutor";
    }
    if (vista == 'editorial') {
        cmd = "loadEditorial";
    }
    if (vista == 'proveedor') {
        cmd = "loadProveedor";
    }
    if (vista == 'tema') {
        cmd = "loadTema";
    }
     if (vista == 'buscarautor') {
        cmd = "loadBuscarAutor";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/libros/libros.php?cmd=" + cmd, {},
        function(data) {
			$("#childModal1").html(data);
			FIN_LOAD();
        });

        $("#childModal1").modal({
        });
    });
}

function registrarAutor(){
	var nomAutor  	= $("#nomAutor").val();
	var apeAutor  	= $("#apeAutor").val();
	var seudoAutor  = $("#seudoAutor").val();
	var idPais  	= $("#idPaisAutor").val();
	var idAutorTipo = $("#idAutorTipo").val();

	if(nomAutor == ''){bootbox.alert("Debe ingresar el nombre del autor"); return;}
	else{

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=registrarAutor", {
        nomAutor:nomAutor, apeAutor:apeAutor, seudoAutor:seudoAutor, idPais:idPais, idAutorTipo:idAutorTipo
    }, function(data) {
        if (data>=1) {
			$("#childModal1").modal('hide');

            $("#divTablaAutor").css("display","block");
            var html = '<tr><td><input type="hidden" id='+data+' value= '+data+' class="detallesAutor" >'+nomAutor+'</td>'+ '<td align=center><a class=cursor-point  title=Eliminar onclick="quitarAutor(this)"><i class="fa fa-trash"></i></a></td></tr>';
            $('#detalleAutor').append(html);

			QuitarModal();
			bootbox.alert("Registro guardado correctamente");

			/*$.post("modulos/libros/libros.php?cmd=ComboAutor", {
		    }, function(data1) {
				$("#idAutor").html(data1);
				$('#idAutor > option[value='+data+']').attr('selected', 'selected');
			});*/

			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });

	}
			
}

function registrarEditorial(){
	var nomEditorial  	= $("#nomEditorial").val();
	var idPais  		= $("#idPaisEditorial").val();
	var direcEditorial  = $("#direcEditorial").val();
	var fonoEditorial  	= $("#fonoEditorial").val();
	var webEditorial  	= $("#webEditorial").val();
	var mailEditorial	= $("#mailEditorial").val();

	if(nomEditorial == ''){bootbox.alert("Debe ingresar el nombre de la editorial"); return;}
	else{

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=registrarEditorial", {
        nomEditorial:nomEditorial, idPais:idPais, direcEditorial:direcEditorial, fonoEditorial:fonoEditorial, mailEditorial:mailEditorial, webEditorial:webEditorial
    }, function(data) {
        if (data>=1) {
			$("#childModal1").modal('hide');
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");

			$.post("modulos/libros/libros.php?cmd=ComboEditorial", {
		    }, function(data1) {
				$("#idEditorial").html(data1);
				$('#idEditorial > option[value='+data+']').attr('selected', 'selected');
			});

			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });			

    }	
}

function registrarProveedor(){
	var rucproveedor	= $("#rucProveedor").val();
	var nomproveedor  	= $("#nomProveedor").val();
	var idPais  		= $("#idPaisProveedor").val();
	var direcproveedor  = $("#direcProveedor").val();
	var fonoproveedor  	= $("#fonoProveedor").val();
	var webproveedor  	= $("#webProveedor").val();
	var mailproveedor	= $("#mailProveedor").val();
	var contacproveedor	= $("#contactoProveedor").val();

	if(nomproveedor == ''){bootbox.alert("Debe ingresar el nombre del Proveedor"); return;}
	else{

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=registrarProveedor", {
        rucproveedor:rucproveedor, nomproveedor:nomproveedor, idPais:idPais, direcproveedor:direcproveedor, 
        fonoproveedor:fonoproveedor, mailproveedor:mailproveedor, webproveedor:webproveedor, contacproveedor:contacproveedor
    }, function(data) {
        if (data>=1) {
			$("#childModal1").modal('hide');
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");

			$.post("modulos/libros/libros.php?cmd=ComboProveedor", {
		    }, function(data1) {
				$("#idProveedor").html(data1);
				$('#idProveedor > option[value='+data+']').attr('selected', 'selected');
			});

			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });	

    }			
}

function registrarTema(){
	var nombre	= $("#nombreCategoria").val();

	if(nombre == ''){bootbox.alert("Debe ingresar la descripción del tema"); return;}
	else{

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=registrarTema", {
        nombre:nombre
    }, function(data) {
        if (data>=1) {
			$("#childModal1").modal('hide');
			QuitarModal();
			bootbox.alert("Registro guardado correctamente");

			$.post("modulos/libros/libros.php?cmd=ComboTema", {
		    }, function(data1) {
				$("#idTemas").html(data1);
				$('#idTemas > option[value='+data+']').attr('selected', 'selected');
			});

			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });	

    }			
}

function OpenFormEjemplar(vista, id)
{
    var cmd 		= "loadCreacion";
    var titulo 		= "";
    var idLibro 	= $("#txtid").val();

    if (vista == 'edicion') {
        cmd = "loadEdicionEjemplar";
        titulo = "Editar Ejemplar";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/libros/libros.php?cmd=" + cmd, {idLibro:idLibro,id:id},
        function(data) {
			$("#childModal1").html(data);
			FIN_LOAD();
        });

        $("#childModal1").modal({
        });
    });

}

function editarEjemplar(){

	var codLibroOld		= $("#txtCodLibro").val();
	var idLibro 		= $("#txtidLibro").val();
	var codisbn	 		= $("#isbnLibroEdit").val();
	var codLibro 		= $("#codLibroEdit").val();
	var codPatrimonio  	= $("#codPatrimonioEdit").val();

	if(codLibro == ''){bootbox.alert("Ingrese código del libro"); return;}
	else{

		INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=editarEjemplar", {
        codLibroOld:codLibroOld, idLibro:idLibro, codisbn:codisbn, codLibro:codLibro, 
        codPatrimonio:codPatrimonio
    }, function(data) {
        if (data == 'passed') {
			$("#childModal1").modal('hide');
			QuitarModal();
			$("#filaisbn_"+codLibroOld).html(codisbn);
			$("#filacodlibro_"+codLibroOld).html(codLibro);
			$("#filapatrimonial_"+codLibroOld).html(codPatrimonio);
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });	

    }

}


function exportarExcel(){
    var datos = '';

    var filtro      = $("#txtBuscar").val();
    var indActivo   = $("#cboEstado").val();
    var orden       = $("#orden").attr("value");
    var direccion   = $("#direccion").attr("value");
    var idFacultad  = $("#idFacultad").val();
    var biblioteca  = $("#idFacultad option:selected").text();
    var codLibro    = $("#txtBuscarCod").val();
    var desEstado   = $("#cboEstadoLibro").val();

    datos = filtro.concat('|');
    datos = datos.concat(indActivo);
    datos = datos.concat('|');
    datos = datos.concat(orden);
    datos = datos.concat('|');
    datos = datos.concat(direccion);
    datos = datos.concat('|');
    datos = datos.concat(idFacultad);
    datos = datos.concat('|');
    datos = datos.concat(codLibro);
    datos = datos.concat('|');
    datos = datos.concat(desEstado);
    datos = datos.concat('|');
    datos = datos.concat(biblioteca);

    window.open('exportar/exporta_1.php?exporta='+datos,'Reporte');
}


function buscarAutor(){
    var filtro      = $("#nomAutor").val();

    if(filtro == ''){ return;}

    INI_LOAD();
    $.post("modulos/libros/libros.php?cmd=listarAutor", {
        filtro: filtro
    }, function(data) {
        $("#ContenedorListadoAutor").html(data);
        FIN_LOAD();
    });
}

function seleccionarAutor(nombre, idAutor){
    
     $("#divTablaAutor").css("display","block");

     var html = '<tr><td><input type="hidden" id='+idAutor+' value= '+idAutor+' class="detallesAutor" >'+nombre+'</td>'+ '<td align=center><a class=cursor-point  title=Eliminar onclick="quitarAutor(this)"><i class="fa fa-trash"></i></a></td></tr>';
      $('#detalleAutor').append(html);

}

function quitarAutor(objeto){
    $(objeto).parent('td').parent('tr').remove();
    var filas = $('#detalleAutor tr').length;
    if(filas <= 0){
        $("#divTablaAutor").css("display","none");
    }
}