// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('usuario');rutaMenu('Usuarios','Inicio','Usuarios');
}

function listado(){
    var filtro 		= $("#txtBuscar").val();
	var indActivo	= $("#cboEstado").val();
	var indCachimbo = $("#cboCachimbo").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	INI_LOAD();
    $.post("modulos/usuario/usuario.php?cmd=listar", {
        filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page,
        indCachimbo:indCachimbo
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Nuevo Usuario";
    }
    if (vista == 'edicion') {
        cmd = "loadEdicion";
        titulo = "Editar Usuario";
    }
   
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/usuario/usuario.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function generarUsuario(){
	var idRol				= $("#idUsuarioTipo").val();
	var usuario 			= $("#numDocumento").val();
	if(idRol == 3){ //alumno
		usuario 			= $("#codUniversitario").val();
	}
	$("#loginUsuario").val(usuario);
}

function registrarUsuario(){
	var idRol				= $("#idUsuarioTipo").val();
	var idDocumento			= $("#idDocumentoIdentidad").val();
	var numDocumento		= $("#numDocumento").val();
	var codUniversitario	= $("#codUniversitario").val();
	var nombre 				= $("#nombreUsuario").val();
	var apellido 			= $("#apellidoUsuario").val();
	var idFacultad			= $("#idFacultad").val();
	var mail  				= $("#mailUsuario").val();
	var login   			= $("#loginUsuario").val();

		INI_LOAD();
    $.post("modulos/usuario/usuario.php?cmd=registrar", {
        idRol:idRol, idDocumento:idDocumento, numDocumento:numDocumento, codUniversitario:codUniversitario,
        nombre:nombre, apellido:apellido, idFacultad:idFacultad, mail:mail, login:login
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}

function editarUsuario(){
	var id 					= $("#txtid").val();
	var idRol				= $("#idUsuarioTipo").val();
	var idDocumento			= $("#idDocumentoIdentidad").val();
	var numDocumento		= $("#numDocumento").val();
	var codUniversitario	= $("#codUniversitario").val();
	var nombre 				= $("#nombreUsuario").val();
	var apellido 			= $("#apellidoUsuario").val();
	var idFacultad			= $("#idFacultad").val();
	var mail  				= $("#mailUsuario").val();
	var login   			= $("#loginUsuario").val();
	var idEstado   			= $("#idEstado").val();

		INI_LOAD();
    $.post("modulos/usuario/usuario.php?cmd=editar", {
        id:id, idRol:idRol, idDocumento:idDocumento, numDocumento:numDocumento, codUniversitario:codUniversitario,
        nombre:nombre, apellido:apellido, idFacultad:idFacultad, mail:mail, login:login, idEstado:idEstado
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert(data);
			   FIN_LOAD();
        }
    });				
}

function Inactivar(id){
	
	bootbox.confirm("¿Seguro que deseas inactivar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/usuario/usuario.php?cmd=inactivar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();		
			  QuitarModal();	
			  bootbox.alert("Registro inactivado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al inactivar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function Activar(id){
	
	bootbox.confirm("¿Seguro que deseas activar el registro?", function(result) {
		if(result){
		INI_LOAD();		
        $.post("modulos/usuario/usuario.php?cmd=activar", {
        id:id
	    }, function(data) {
          if (data=="passed") {
			  listado();
			  QuitarModal();			
			  bootbox.alert("Registro activado correctamente");
			  FIN_LOAD();
            
          } else{
                bootbox.alert("Error al activar, vuelva a intentarlo");
			    FIN_LOAD();
		  }
    	});	 
			   			
		}
	});
}

function OpenFormPermisos(vista, id,nom)
{
    var cmd = "loadCreacion";
    var titulo = "";

   
    if (vista == 'permisos') {
        cmd = "loadPermisos";
        titulo = "Permisos de Usuario "+nom;
    }

    if (vista == 'clave') {
        cmd = "loadClave";
        titulo = "Cambiar Contraseña de Usuario "+nom;
    }
   
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/usuario/usuario.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function registrarPermisos(){
	var id 		= $("#txtid").val();
	 var contador = 0;
	 var cadena = '';
	 var campo = '';

	
	$(".CkbPermisos").each(function(i,x){
	  campo = $(this).attr('id');
	  if($("#"+campo+"").is(':checked')){
	    if(contador==0){cadena = campo;contador = 1;}
	    else{cadena = cadena+','+campo;}
	    }
	 });

		INI_LOAD();
    $.post("modulos/usuario/usuario.php?cmd=registrarPermiso", {
        id:id,cadena:cadena
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, intente nuevamente");
			   FIN_LOAD();
        }
    });				
}

function editarClave(){
	var id 		= $("#txtid").val();
	var con1	= $("#nuevaClave").val();
	var con2	= $("#confirmarClave").val();

	if(con1 != con2){
		bootbox.alert("Las contraseñas ingresadas no coinciden...");
		return;
	}

		INI_LOAD();
    $.post("modulos/usuario/usuario.php?cmd=editarClave", {
        id:id,con1:con1
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
               bootbox.alert("Error al registrar, intente nuevamente");
			   FIN_LOAD();
        }
    });				
}

function editarClave2(){
	var id 		= $("#txtid").val();
	var con1	= $("#nuevaClave").val();
	var con2	= $("#confirmarClave").val();

	if(con1 != con2){
		bootbox.alert("Las contraseñas ingresadas no coinciden...");
		return;
	}

		INI_LOAD();
    $.post("modulos/usuario/usuario.php?cmd=editarClave", {
        id:id,con1:con1
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			//listado();			
			//QuitarModal();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
			setTimeout(function(){ location.reload(); }, 3000);
            
        } else {
               bootbox.alert("Error al registrar, intente nuevamente");
			   FIN_LOAD();
        }
    });				
}

function OpenFormPermisos2(vista, id,nom)
{
    var cmd = "loadCreacion";
    var titulo = "";

   
    if (vista == 'permisos') {
        cmd = "loadPermisos";
        titulo = "Permisos de Usuario "+nom;
    }

    if (vista == 'clave') {
        cmd = "loadClave";
        titulo = "Cambiar Contraseña de Usuario "+nom;
    }
   
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/clave/clave.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function MainForm2(){
	 location.reload(); 
}


function exportarExcel(){
    var datos = '';

    var filtro      = $("#txtBuscar").val();
    var indActivo   = $("#cboEstado").val();
    var indCachimbo = $("#cboCachimbo").val();

    datos = filtro.concat('|');
    datos = datos.concat(indActivo);
    datos = datos.concat('|');
    datos = datos.concat(indCachimbo);

    window.open('exportar/exporta_2.php?exporta='+datos,'Reporte');
}