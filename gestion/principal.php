<?php

require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

session_start();
if($_SESSION['BTK_USUARIO']==NULL){
session_destroy();
echo "<script>sessionExpire();</script>";   
exit(0);
}

  global $conexion;
  
  $objUsuario   = $_SESSION['BTK_USUARIO'];
  $idUsuario    = $objUsuario->__get('_sess_usu_id');
  $idRol        = $objUsuario->__get('_sess_id_rol');

    //si es administrador, asignar todos los permisos
  if($idRol == 1){

    $array=array();

    for ($i=0; $i <= 100; $i++) { 
      $array[] = $i;  
    }

  }
  else{

        $modelUsuario   = new Data_sgausuario();
        $arrayPermisos  = $modelUsuario->fu_permisoListar($conexion,$idUsuario); 
 
        if(count($arrayPermisos)<=0){ 
          $arrayPermisos  = $modelUsuario->fu_permisoListarRol($conexion,$idRol); 
        }

        $array=array();
        if(count($arrayPermisos)>0){

        foreach ($arrayPermisos as $obj):
    
          $array[] = $obj['ID_SISTEMA_PRIVILEGIO'];
    
        endforeach;
      
        }

      }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Facultad de Pesquería</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   
   <?php
   include 'librerias_css.php';
   ?> 
    
  </head>
  <body class="hold-transition skin-blue-light sidebar-mini "> <!--sidebar-collapse-->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">CEDINPES</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg" id="">CEDINPES</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegaci&oacute;n</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- Notifications: style can be found in dropdown.less -->
              
              <!-- Tasks: style can be found in dropdown.less -->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs" id="divusuario1">UserName</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p id="divusuario2">
                      UserName
                      <!--<small>Member since Nov. 2012</small>-->
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                   <!-- <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Perfil</a>
                    </div>-->
                    <div class="pull-right">
                      <a href="javascript:cerrarSesion();" class="btn btn-default btn-flat">Salir</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p id="divusuario">UserName</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <!--<form class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">OPCIONES DE MENU</li>
            <!--active-->
            <?php /*if(in_array(9,$array)){ */?>
           <!-- <li>
              <a href="javascript:loadMenu('dashboard');rutaMenu('Dashboard','Inicio','Dashboard');">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php /*} */?>

            <?php if(in_array(1,$array) || in_array(2,$array) || in_array(3,$array) || in_array(4,$array) || in_array(5,$array)){ ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gears (alias)"></i> <span>Mantenimiento</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array(1,$array)){ ?>
                <li id="li_categoria" class="opciones"><a href="javascript:loadMenu('categoria');rutaMenu('Temas','Mantenimiento','Temas');"><i class="fa fa-circle-o"></i>Temas</a></li>
                <?php } ?>
                
                <?php if(in_array(2,$array)){ ?>
                <li id="li_autor" class="opciones"><a href="javascript:loadMenu('autor');rutaMenu('Autores','Mantenimiento','Autores');"><i class="fa fa-circle-o"></i>Autores</a></li>
                <?php } ?>
                
                <?php if(in_array(3,$array)){ ?>
                <li id="li_editorial" class="opciones"><a href="javascript:loadMenu('editorial');rutaMenu('Editoriales','Mantenimiento','Editoriales');"><i class="fa fa-circle-o"></i>Editoriales</a></li>
                <?php } ?>

                <?php if(in_array(4,$array)){ ?>
                <li id="li_proveedor" class="opciones"><a href="javascript:loadMenu('proveedor');rutaMenu('Proveedores','Mantenimiento','Proveedores');"><i class="fa fa-circle-o"></i>Proveedores</a></li>
                <?php } ?>
                
                <?php if(in_array(5,$array)){ ?>
                <li id="li_libros" class="opciones"><a href="javascript:loadMenu('libros');rutaMenu('Libros','Mantenimiento','Libros');"><i class="fa fa-circle-o"></i>Libros</a></li>
                <?php } ?>
              </ul>
            </li>
            <?php } ?>

            <?php if(in_array(7,$array) || in_array(8,$array) ){ ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Préstamos</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array(7,$array)){ ?>
                <li id="li_prestamo_ind" class="opciones"><a href="javascript:loadMenu('prestamo_ind');rutaMenu('Mis Préstamos','Préstamos','Mis Prestamos');"><i class="fa fa-circle-o"></i>Mis Préstamos</a></li>
                <?php } ?>
                
                <?php if(in_array(8,$array)){ ?>
                <li id="li_prestamos" class="opciones"><a href="javascript:loadMenu('prestamos');rutaMenu('Solicitudes de Préstamo','Préstamos','Solicitudes');"><i class="fa fa-circle-o"></i>Solicitudes</a></li>
                <?php } ?>
              </ul>
            </li>
            <?php } ?>

            <?php if(in_array(9,$array)){ ?>
          <!--  <li>
              <a href="javascript:loadMenu('visita');rutaMenu('Visitas','Inicio','Visitas');">
                <i class="fa fa-tasks"></i> <span>Visitas</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php } ?>

            <!--<li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Sugerencias y Quejas</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li id="li_sugerencia" class="opciones"><a href="javascript:loadMenu('sugerencia');rutaMenu('Sugerencias','Sugerencias y Quejas','Sugerencias');"><i class="fa fa-circle-o"></i>Sugerencias</a></li>
                <li id="li_quejas" class="opciones"><a href="javascript:loadMenu('quejas');rutaMenu('Quejas','Sugerencias y Quejas','Quejas');"><i class="fa fa-circle-o"></i>Quejas</a></li>
              </ul>
            </li>-->
            
            <?php if(in_array(9,$array)){ ?>
          <!-- <li>
              <a href="javascript:loadMenu('encuesta');rutaMenu('Encuestas','Inicio','Encuestas');">
                <i class="fa fa-tasks"></i> <span>Encuestas</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php } ?>

            <?php/* if(in_array(9,$array)){ */?>
          <!--  <li>
              <a href="javascript:loadMenu('noticias');rutaMenu('Noticias','Inicio','Noticias');">
                <i class="fa fa-newspaper-o"></i> <span>Noticias</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php/*}*/ ?>

            <?php /*if(in_array(9,$array)){*/ ?>
           <!-- <li>
              <a href="javascript:loadMenu('eventos');rutaMenu('Eventos','Inicio','Eventos');">
                <i class="fa fa-calendar"></i> <span>Eventos</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php /*}*/ ?>

            <?php /*if(in_array(9,$array)){ */?>
           <!-- <li>
              <a href="javascript:loadMenu('tabla_reporte');rutaMenu('Reportes','Inicio','Reportes');">
                <i class="fa fa-bar-chart"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>-->
            <?php/* }*/ ?>
            
            <?php if(in_array(6,$array)){ ?>
            <li>
              <a href="javascript:loadMenu('usuario');rutaMenu('Usuarios','Inicio','Usuarios');">
                <i class="fa fa-users"></i> <span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
            <?php } ?>
            
            <li>
              <a href="javascript:loadMenu('clave');rutaMenu('Cambiar Contraseña','Inicio','Cambiar Contraseña');">
                <i class="fa fa-unlock-alt"></i> <span>Cambiar Contraseña</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 id="namemodulo">
            <!--<small>Control panel</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#" id="homeruta"><i class=""></i></a></li>
            <li class="active" id="nameruta"></li>
          </ol>
        </section>

        <!-- CONTENEDOR PRINCIPAL -->
       <section class="content">
       	<div class="row">
         <section class="col-lg-12"> 
         <div id="ContentPrincipal">     
         Bienvenido          
         </div>
         </section><!-- right col -->
         </div>
        </section><!-- /.FIN CONTENEDOR PRINCIPAL -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; <?php echo date('Y').' ' ?> <a href="#" target="_blank"></a>.</strong> Todos los derechos reservados.
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      
    </div><!-- ./wrapper -->

	<?php
    include 'librerias_js.php';
	?>
    
  </body>
  <script>
$(document).ready(function(){
	ObtenerDatosInicio();		
});
</script>
</html>
