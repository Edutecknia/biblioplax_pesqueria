<script>
$(document).ready(function(){
        
       $('#numDocumento').change(function() {
            generarUsuario();
       });

       $('#codUniversitario').change(function() {
            generarUsuario();
       });
});
</script>

<form action="javascript:registrarUsuario();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

                <div class="form-group col-sm-4">
                <label>Rol:</label>
                    <select class="form-control" name="idUsuarioTipo" id="idUsuarioTipo" onchange="javascript:generarUsuario();">
                        <?php foreach ($arrayUsuTipo as $obj): ?>
                        <option value="<?php echo $obj['ID_USUARIO_TIPO']; ?>">
                        <?php echo utf8_encode($obj['NOM_USUARIO_TIPO']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-sm-4">
                <label>Tipo Documento:</label>
                    <select class="form-control" name="idDocumentoIdentidad" id="idDocumentoIdentidad">
                        <?php foreach ($arrayDocTipo as $obj): ?>
                        <option value="<?php echo $obj['ID_DOCUMENTO_IDENTIDAD_TIPO']; ?>">
                        <?php echo utf8_encode($obj['NOM_DOCUMENTO_IDENTIDAD']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-sm-4">
                <label>N° Documento:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="numDocumento" name="numDocumento" 
                                    class="form-control" 
                                    placeholder="Número de documento identidad" 
                                    type="number" 
                                    required="required">
                                </div>
                        </div>

                <div class="form-group col-sm-4">
                <label>Código Universitario:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="codUniversitario" name="codUniversitario" 
                                    class="form-control" 
                                    placeholder="Código Universitario" 
                                    type="number" 
                                    >
                                </div>
                        </div>

  <div class="form-group col-sm-4">
                <label>Nombres:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreUsuario" name="nombreUsuario" 
                                    class="form-control" 
                                    placeholder="Nombres del usuario" 
                                    type="text" 
                                    maxlength="200" 
                                    required="required">
                                </div>
                        </div>

  <div class="form-group col-sm-4">
                <label>Apellidos:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="apellidoUsuario" name="apellidoUsuario" 
                                    class="form-control" 
                                    placeholder="Apellidos del usuario" 
                                    type="text" 
                                    maxlength="200" 
                                    required="required">
                                </div>
                        </div>

  <div class="form-group col-sm-4">
                <label>Acceso a Biblioteca:</label>
                    <select class="form-control" name="idFacultad" id="idFacultad">
                        <option value="0">TODAS</option>
                        <?php foreach ($arrayFacultad as $obj): ?>
                        <option value="<?php echo $obj['ID_CARRERA']; ?>">
                        <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>


  <div class="form-group col-sm-4">
                <label>E-mail:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="mailUsuario" name="mailUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese e-mail del usuario" 
                                    type="email" 
                                    maxlength="200" 
                                    required="required">
                                </div>
                        </div>

  <div class="form-group col-sm-4">
                <label>Usuario:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="loginUsuario" name="loginUsuario" 
                                    class="form-control" 
                                    placeholder="Ingrese login del usuario" 
                                    type="text" 
                                    maxlength="200" 
                                    readonly="readonly">
                                </div>
                        </div>




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					




                   