<script type="text/javascript" src="js/mUsuario.js"></script>

<script>
$(document).ready(function(){
		
		listado();		
        $("#btnBuscar").click(function() {
        listado();
        });

        $('#txtBuscar').change(function() {
        $('#pag_actual').val('1');
        });

        $('#cboEstado').change(function() {
        $('#pag_actual').val('1');
        });

        $('#cboCachimbo').change(function() {
        $('#pag_actual').val('1');
        });
});
</script>
<div class="content" >
             <div class="row">
                <div class="col-md-12">

				<div class="box box-primary">
                <div class="box-header with-border">
                  <h5 class="box-title" id="titulo">Filtro de búsqueda</h5>
                </div>
                <div class="box-body" id="Contenedorform">
                
                <input type="hidden" id="pag_actual" name="pag_actual" value="<?php echo $pagina ?>"/>
                <input type="hidden" id="orden" name="orden" value="<?php echo $orden ?>"/>
                <input type="hidden" id="direccion" name="direccion" value="<?php echo $direccion ?>"/>
                
                <div class="col-md-3">
                <label>Descripción</label>
                  <input class="form-control" type="text" placeholder="Nombre del usuario" name="txtBuscar" id="txtBuscar">
                </div>

                <div class="col-md-2">
                <label>Estado</label>
                      <select class="form-control" name="cboEstado" id="cboEstado">
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                      </select>
                </div>

                <div class="col-md-2">
                <label>Usuarios</label>
                      <select class="form-control" name="cboCachimbo" id="cboCachimbo">
                        <option value="0">TODOS</option>
                      </select>
                </div>

                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Buscar" name="btnBuscar" id="btnBuscar"><i class="fa fa-search"> </i>   Buscar</a>
                </div>

                               
                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Nuevo" onclick="javascript:OpenForm('creacion');"><i class="fa fa-file-o"> </i>   Nuevo</a>
                </div>

                <div class="form-group col-md-1">
                <label></label>
               <!-- <a class="btn btn-block btn-danger" tooltip="Nuevo" onclick="javascript:exportarExcel();"><i class="fa fa-file-excel-o"> </i>   Excel</a>-->
                </div>

                <div style="clear:both"></div>
                
                <div class="col-md-12 table-responsive" id="ContenedorListado" >
                </div>
                
                </div><!-- /.box-body -->
              </div><!-- /.box-primary -->
                  
                </div>
            </div>
            </div>