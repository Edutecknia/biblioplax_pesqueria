<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'NOM_USUARIO';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar($filtro, $indActivo, $orden, $direccion, $pagina, $indCachimbo){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		=  $_SESSION['BTK_USUARIO'];
	
	$modelUsuario 	= new Data_sgausuario();
	$arrayUsuario 	= $modelUsuario->fu_Listar($conexion,$filtro, $indActivo, $orden, $direccion, $pagina, $indCachimbo);

	foreach ($arrayUsuario as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var filtro 		= $("#txtBuscar").val();
							var indActivo	= $("#cboEstado").val();
							var indCachimbo = $("#cboCachimbo").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/usuario/usuario.php?cmd=listar", {
						filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num, indCachimbo:indCachimbo
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,0);

	$modelData		= new Data_sgausuario();
	$arrayDocTipo	= $modelData->fu_listarDocumentoTipoTodo($conexion);
	$arrayUsuTipo	= $modelData->fu_listarUsuarioTipoTodo($conexion);


	include $nombreModulo . 'vistas/crear.php';
}

function registrar($idRol, $idDocumento, $numDocumento, $codUniversitario, $nombre, $apellido, $idFacultad, $mail, $login){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 		= new Logic_Utils();
	$ip				= $objUtils->getIP();

	$objUsuario 	=  $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();

	$rpta =$modelUsuario->fu_registrar($conexion,$idRol, $idDocumento, $numDocumento, $codUniversitario, $nombre, $apellido, $idFacultad, $mail, $login, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0 && $rpta['DES_MENSAJE']== 'VALIDO'){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}



function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,0);

	$modelData		= new Data_sgausuario();
	$arrayDocTipo	= $modelData->fu_listarDocumentoTipoTodo($conexion);
	$arrayUsuTipo	= $modelData->fu_listarUsuarioTipoTodo($conexion);
	$arrayEstado	= $modelData->fu_listarPrestamoUsuarioEstado($conexion);
	
	$modelUsuario 	= new Data_sgausuario();
	$modelUsuario 	= $modelUsuario->fu_encontrar($conexion,$id);
	$id = $id;
	include $nombreModulo . 'vistas/editar.php';

}

function editar($id, $idRol, $idDocumento, $numDocumento, $codUniversitario, $nombre, $apellido, $idFacultad, $mail, $login, $idEstado){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 		= new Logic_Utils();
	$ip				= $objUtils->getIP();
	
	$objUsuario 	=  $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();

	$rpta =$modelUsuario->fu_editar($conexion, $id ,$idRol, $idDocumento, $numDocumento, $codUniversitario, $nombre, $apellido, $idFacultad, $mail, $login, $idEstado, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0 && $rpta['DES_MENSAJE']== 'VALIDO'){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}

}

function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgausuario();

	$rpta = $modelData->fu_inactivar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgausuario();

	$rpta = $modelData->fu_activar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}

function loadPermisos($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelUsuario 	= new Data_sgausuario();
	$arrayPermisos  = $modelUsuario->fu_permisoListar($conexion,$id); 
	$modelUsuario 	= $modelUsuario->fu_encontrar($conexion,$id);
	
	if(count($arrayPermisos)<=0){	
		$modelData	 	= new Data_sgausuario();	
		$arrayPermisos  = $modelData->fu_permisoListarRol($conexion,$modelUsuario['ID_USUARIO_TIPO']); 
	}


	$array=array();
	if(count($arrayPermisos)>0){

		foreach ($arrayPermisos as $obj):

		 $array[] = $obj['ID_SISTEMA_PRIVILEGIO'];

		endforeach;
	}
	else{
		
	}
	
	$id = $id;

	include $nombreModulo . 'vistas/permiso.php';
}

function registrarPermiso($id,$cadena){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	=  $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();
	$rpta_ =$modelUsuario->fu_permisoEliminar($conexion,$id); 

	if($cadena != ''){

		$data = explode(',', $cadena);

		for ($i=0; $i < count($data); $i++) { 
			
			$rpta =$modelUsuario->fu_permisoRegistrar($conexion,$id,$data[$i],$idUsuario, $ip);

		}

		if($rpta > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}

	}
	else{
		if($rpta_ > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}
	}

}

function loadClave($id){
	$id = $id;
	include $nombreModulo . 'vistas/clave.php';
}

function editarClave($id,$clave){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	=  $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();
	$rpta =$modelUsuario->fu_editarClave($conexion, $id, $clave, $idUsuario, $ip);

		if($rpta > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}

}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['filtro'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['indCachimbo']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['idRol'],$_POST['idDocumento'],$_POST['numDocumento'],$_POST['codUniversitario']
					,$_POST['nombre'],$_POST['apellido'],$_POST['idFacultad'],$_POST['mail']
					,$_POST['login']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;	
	case 'editar':
		editar($_POST['id'],$_POST['idRol'],$_POST['idDocumento'],$_POST['numDocumento'],$_POST['codUniversitario']
					,$_POST['nombre'],$_POST['apellido'],$_POST['idFacultad'],$_POST['mail']
					,$_POST['login'],$_POST['idEstado']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
	case 'loadPermisos':
		loadPermisos($_POST['id']);break;
	case 'registrarPermiso':
		registrarPermiso($_POST['id'],$_POST['cadena']);break;
	case 'loadClave':
		loadClave($_POST['id']);break;	
	case 'editarClave':
		editarClave($_POST['id'],$_POST['con1']);break;	
    default:
        vistaPrincipal();
        break;
}
?>