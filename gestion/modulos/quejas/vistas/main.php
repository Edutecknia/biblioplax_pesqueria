<script type="text/javascript" src="js/mQueja.js"></script>

<script>
$(document).ready(function(){
		
		listado();		
        $("#btnBuscar").click(function() {
        listado();
        });

        /* reset paginacion al cambiar texto */
        $('#txtBuscar').change(function() {
        $('#pag_actual').val('1');
        });

        $('#idFacultad').change(function() {
        $('#pag_actual').val('1');
        });
});
</script>
<div class="content" >
             <div class="row">
                <div class="col-md-12">

				<div class="box box-primary">
                <div class="box-header with-border">
                  <h5 class="box-title" id="titulo">Filtro de búsqueda</h5>
                </div>
                <div class="box-body" id="Contenedorform">
                
                <input type="hidden" id="pag_actual" name="pag_actual" value="<?php echo $pagina ?>"/>
                <input type="hidden" id="orden" name="orden" value="<?php echo $orden ?>"/>
                <input type="hidden" id="direccion" name="direccion" value="<?php echo $direccion ?>"/>
                
                <div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php if($idRol == 1){ //si es administrador
                ?>
                <option value="">TODOS</option>
                <?php
                }
                ?>
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>">
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Buscar" name="btnBuscar" id="btnBuscar"><i class="fa fa-search"> </i>   Buscar</a>
                </div>

                <div class="col-md-3">
                </div>                
                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Nuevo" onclick="javascript:OpenForm('creacion');"><i class="fa fa-file-o"> </i>   Nuevo</a>
                </div>
                
                <div class="col-md-12 table-responsive" id="ContenedorListado">
                </div>
                
                </div><!-- /.box-body -->
              </div><!-- /.box-primary -->
                  
                </div>
            </div>
            </div>