<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';


/**
 * Carga la vista principal
 */
function vistaPrincipal() {
   	
	$objConexion = new Data_DB();
	$conexion = $objConexion->conn();

    include $nombreModulo . 'vistas/main.php';
}

function login($usu,$pass){

	global $conexion, $etiquetaTitulo, $nombreModulo;
	$objsga_usuario 	= new Data_sgausuario();
	$objUsuario 		= new Data_Usuario();

	$objUtils = new Logic_Utils();
	$usu = $objUtils->limpiar_cadena($usu);

	$pass = str_replace(
			array("|",
				 "'",
				 "&"),
			'', $pass);

	$idUsuario		 	= $objsga_usuario->fu_login($conexion,$usu,$pass);

	if($idUsuario > 0){
	
	$usuario				= $objsga_usuario->fu_encontrar($conexion,$idUsuario);
	$objUsuario 			= $objUsuario->asignar_datos($usuario['ID_USUARIO'], $usu, $usuario['ID_CARRERA'], $usuario['ID_USUARIO_TIPO']);
	$_SESSION['BTK_USUARIO'] 	= $objUsuario;  
	$rpta = 'passed';  
	}
	else{
	$rpta = 'Error al ingresar';
	}
	echo $rpta;
}

function datosInicio(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$usucre 		= $objUsuario->__get('_sess_usu_name');
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$objsga_usuario = new Data_sgausuario();
	$usuario		= $objsga_usuario->fu_encontrar($conexion,$idUsuario);

	/*$objsga_Sociedad 	= new Data_sgasociedad();
    $sociedad 			= $objsga_Sociedad->fu_encontrar($conexion, $idsociedad);
	
	$nomcorto 			= $sociedad['NOM_CORTO'];*/
	
	echo 'BIBLIOPLAX'.'|'.utf8_encode($usuario['NOM_USUARIO']);
}

function cerrarSesion(){
	$objSession = new Data_Session();
	$objSession->validaSession();
	exit(0);
}

function idFacultad($id){
		$_SESSION['BTK_CARRERA_ID'] = $id;	

	switch ($id) {
		case '1':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'bromatologia';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Bromatología y Nutrición';
			break;
		case '2':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'ciencias';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ciencias';
			break;
		case '3':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'ciencias_sociales';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ciencias Sociales';
			break;
		case '4':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'ciencias_empresariales';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ciencias Empresariales';
			break;
		case '5':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'derecho';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Derecho';
			break;
		case '6':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'agraria_alimentaria';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Agrária,Ind.Alim. Amb.';
			break;
		case '7':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'contabilidad';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Cs. Económicas Cont y Fin.';
			break;
		case '8':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'civil';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ing. Civíl';
			break;
		case '9':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'ing_indust_sistema';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ing. Indus,Sist.,Inform.';
			break;
		case '10':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'pesquera';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ing. Pesquera';
			break;
		case '11':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'quimica';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Ing. Química y Met.';
			break;
		case '12':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'educacion';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Educación';
			break;
		case '13':
			$_SESSION['BTK_DIR_CARPETA_FACULTAD'] = 'medicina';
			$_SESSION['BTK_NOM_FACULTAD'] = 'Medicina Humana';
			break;
	}
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
	case 'main':
		 vistaPrincipal();
		 break;	
	case 'login':
		 login($_POST['usu'],$_POST['pas']);
		 break;		
	case 'datosInicio':
		 datosInicio();
		 break;
	case 'cerrarSesion':
		 cerrarSesion();
		 break;
	case 'idFacultad':
		 idFacultad($_POST['id']);
		 break;	 
    default:
        vistaPrincipal();
        break;
}
?>