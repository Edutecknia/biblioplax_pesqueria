<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'E.ID_NOTICIA_EVENTO';
	$direccion = 'DESC';
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

    include $nombreModulo . 'vistas/main.php';
}


function listar($idFacultad, $filtro, $indActivo, $orden, $direccion, $pagina){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	
	$modelCategoria	= new Data_sganoticia();
	$arrayNoticias	= $modelCategoria->fu_listarEvento($conexion,$filtro, $indActivo, $orden, $direccion, $pagina,$idFacultad);

	foreach ($arrayNoticias as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();		
                            var idFacultad	= $("#idFacultad").val();					
							var filtro 		= $("#txtBuscar").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/noticias/noticias.php?cmd=listar", {
						idFacultad:idFacultad,filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	include $nombreModulo . 'vistas/crear.php';
}

function registrar($idFacultad, $fechaEvento, $nomNoticiaEvento, $desNoticiaEvento){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();
	
	$modelCategoria	= new Data_sganoticia();

	$data 			= @explode('/', $fechaEvento);
	$dia 			= $data[0];
	$mes 			= $data[1];
	$anio 			= $data[2];

	$fechaNoticia 			= $anio.'-'.$mes.'-'.$dia;

	$rpta = $modelCategoria->fu_registrar($conexion,$nomNoticiaEvento,$fechaNoticia, $desNoticiaEvento, 2, $idFacultad ,$idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$obj_Noticia	= new Data_sganoticia();
	$obj_Noticia 	= $obj_Noticia->fu_encontrar($conexion,$id);
	
	$id = $id;
	
	include $nombreModulo . 'vistas/editar.php';
}

function editar($id, $fechaEvento, $idFacultad, $nomNoticiaEvento, $desNoticiaEvento){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$objUtils 		= new Logic_Utils();
	$ip				= $objUtils->getIP();

	$modelCategoria	= new Data_sganoticia();
	
	$data 			= @explode('/', $fechaEvento);
	$dia 			= $data[0];
	$mes 			= $data[1];
	$anio 			= $data[2];

	$fechaNoticia 			= $anio.'-'.$mes.'-'.$dia;

	$rpta = $modelCategoria->fu_editar($conexion,$id, $nomNoticiaEvento,$fechaNoticia, $desNoticiaEvento, 2, $idFacultad ,$idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}


function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$objUtils 		= new Logic_Utils();
	$ip				= $objUtils->getIP();
	
	
	$modelCategoria	= new Data_sganoticia();
	$rpta = $modelCategoria->fu_inactivar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$objUtils 		= new Logic_Utils();
	$ip				= $objUtils->getIP();

	$modelCategoria	= new Data_sganoticia();
	$rpta = $modelCategoria->fu_activar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}


/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['idFacultad'], $_POST['filtro'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['idFacultad'],$_POST['fechaEvento'], $_POST['nombreNoticia'],$_POST['desNoticia']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;		
	case 'editar':
		editar($_POST['id'],$_POST['fechaEvento'],$_POST['idFacultad'],$_POST['nombreNoticia'],$_POST['desNoticia']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>