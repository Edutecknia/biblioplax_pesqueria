 <link rel="stylesheet" href="css/css_editor/froala_editor.css">
  <link rel="stylesheet" href="css/css_editor/froala_style.css">
  <link rel="stylesheet" href="css/css_editor/plugins/code_view.css">
  <link rel="stylesheet" href="css/css_editor/plugins/colors.css">
  <link rel="stylesheet" href="css/css_editor/plugins/emoticons.css">
  <link rel="stylesheet" href="css/css_editor/plugins/image_manager.css">
  <link rel="stylesheet" href="css/css_editor/plugins/image.css">
  <link rel="stylesheet" href="css/css_editor/plugins/line_breaker.css">
  <link rel="stylesheet" href="css/css_editor/plugins/table.css">
  <link rel="stylesheet" href="css/css_editor/plugins/char_counter.css">
  <link rel="stylesheet" href="css/css_editor/plugins/video.css">
  <link rel="stylesheet" href="css/css_editor/plugins/fullscreen.css">
  <link rel="stylesheet" href="css/css_editor/plugins/file.css">
  <link rel="stylesheet" href="css/css_editor/plugins/quick_insert.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

  <script type="text/javascript" src="js/js_editor/froala_editor.min.js" ></script>
  <script type="text/javascript" src="js/js_editor/plugins/align.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/char_counter.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/colors.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/entities.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/file.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/fullscreen.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/image.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/link.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/lists.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/quote.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/table.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/save.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/url.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/video.min.js"></script>
  <script type="text/javascript" src="js/js_editor/languages/es.js"></script>

  <script>
    $(function(){
      $('#edit').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })

    });

        $(document).ready(function(){
        
        $(".fr-hidden").removeClass("fr-hidden")

        $('#dtFechaEvento').datetimepicker(
        {
          format: 'DD/MM/YYYY',
          defaultDate: new Date()
        }
        ).on('changeDate', function(e){
          $(this).datepicker('hide');
        });   
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });

        var allDivs = $('div');
        var topZindex = 0;
        var topDivId;
          allDivs.each(function(){
            var currentZindex = parseInt($(this).css('z-index'));
          if(currentZindex == 9999) {
              $(this).css('z-index',-9999);
          }
      });

});

  </script>


                    


<form action="javascript:editar();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

<input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>  

<div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>" <?php if($obj['ID_CARRERA'] == $obj_Noticia['ID_CARRERA']){echo 'selected';} ?> >
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

 <div class="form-group col-sm-6">
                <label>Nombre de Noticia:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreNoticia" name="nombreNoticia" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre de la noticia institucional" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo $obj_Noticia['NOM_NOTICIA_EVENTO']; ?>"
                                    >
                                </div>
                        </div>


    <div class="form-group col-sm-3">
    <label>Fecha del Evento:</label>
        <div class='input-group date' id='dtFechaEvento'>
        <input type='text' class="form-control" id="fechaEvento" required="required"
         value="<?php 
              $fecha   = strtotime( $obj_Noticia['FECHA_NOTICIA_EVENTO'] ); 
              $fecha   = date('d/m/Y', $fecha); 
              echo $fecha; 
              ?>" />
        <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
        </span>
        </div>
    </div> 


 <div class="form-group col-sm-12">
<div id="editor">
<div id='edit' style="margin-top: 30px; min-height:400px;">
<?php echo $obj_Noticia['DES_NOTICIA_EVENTO']; ?>
</div>
</div>
</div>  




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>                 