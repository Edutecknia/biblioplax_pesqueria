<script type="text/javascript" src="js/mPrestamoind.js"></script>

<script>
$(document).ready(function(){
		
$('#dtFechaIni').datetimepicker(
     {
         format: 'DD/MM/YYYY',
         defaultDate: new Date()
     }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
    });     
    
    $(".timepicker").timepicker({
        showInputs: false
    });


$('#dtFechaFin').datetimepicker(
     {
         format: 'DD/MM/YYYY',
         defaultDate: new Date()
     }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
    });     
    
    $(".timepicker").timepicker({
        showInputs: false
    });


		listado();		
        $("#btnBuscar").click(function() {
        listado();
        });
});
</script>
<div class="content" >
             <div class="row">
                <div class="col-md-12">

				<div class="box box-primary">
                <div class="box-header with-border">
                  <h5 class="box-title" id="titulo">Filtro de búsqueda</h5>
                </div>
                <div class="box-body" id="Contenedorform">
                
                <input type="hidden" id="pag_actual" name="pag_actual" value="<?php echo $pagina ?>"/>
                <input type="hidden" id="orden" name="orden" value="<?php echo $orden ?>"/>
                <input type="hidden" id="direccion" name="direccion" value="<?php echo $direccion ?>"/>
                

                <div class="form-group col-sm-3">
                <label>Fecha Inicio:</label>
                        <div class='input-group date' id='dtFechaIni'>
                        <input type='text' 
                                class="form-control" 
                                id="fechaInicio"
                        />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                </div>

                <div class="form-group col-sm-3">
                <label>Fecha Fin:</label>
                        <div class='input-group date' id='dtFechaFin'>
                        <input type='text' 
                                class="form-control" 
                                id="fechaFin"
                        />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                </div>

                <div class="col-md-2">
                <label>Estado</label>
                      <select class="form-control" name="cboEstado" id="cboEstado">
                        <option value="0">TODOS</option>
                        <?php foreach ($arrayEstados as $obj): ?>
                        <option value="<?php echo $obj['ID_PRESTAMO_ESTADO']; ?>"><?php echo utf8_encode($obj['DES_ESTADO']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Buscar" name="btnBuscar" id="btnBuscar"><i class="fa fa-search"> </i>   Buscar</a>
                </div>

                              
                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Nuevo" onclick="javascript:OpenForm('creacion');"><i class="fa fa-file-o"> </i>   Nuevo Préstamo</a>
                </div>
                
                <div class="col-md-12 table-responsive" id="ContenedorListado">
                </div>
                
                </div><!-- /.box-body -->

                <!--<div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
                </div>-->

              </div><!-- /.box-primary -->
                  
                </div>
            </div>
            </div>