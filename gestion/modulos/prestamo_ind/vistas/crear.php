<script>
$(document).ready(function(){
 
 $(".select2").select2();

 $("#btnBuscar").click(function() {
        buscarLibro();
 });

});
</script>


<form action="javascript:registrar();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">


                <div class="form-group col-sm-10">
                <label>Solicitante:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtSolicitante" name="txtSolicitante" 
                                    class="form-control" 
                                    type="text" 
                                    value="<?php echo utf8_encode($modelUsuario['NOM_USUARIO'].' '.$modelUsuario['APE_USUARIO']);?>" 
                                    readonly
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-2">
                <label>Días de Préstamo:</label>
                        <div class="input-group">
                            <span class="input-group-addon"></span>
                            <input id="txtDiasPrestamo" name="txtDiasPrestamo" 
                                   class="form-control" 
                                   type="number" 
                                   min="0"
                                   max="15" 
                                   value="0" 
                                   >
                        </div>
                </div>


                <div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <option value="">TODAS</option>
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>"><?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

                <div class="form-group col-sm-3">
                <label>Buscar Por:</label>
                <select class="form-control" name="idFiltro" id="idFiltro" onchange="javascript:mostrar();">
                    <option value="titulo">TÍTULO</option>
                    <option value="autor">AUTOR</option>
                    <option value="tema">TEMA</option>
                    <option value="codigo">CÓDIGO</option>
                    <option value="isbn">ISBN</option>
                </select>
                </div>

                <div class="form-group col-sm-4" id="divDescripcion">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtDescripcion" name="txtDescripcion" 
                                    class="form-control" 
                                    placeholder="Descripción de la búsqueda" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-4" style="display:none" id="divCodigo">
                <label>Código:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtCodigo" name="txtCodigo" 
                                    class="form-control" 
                                    placeholder="Código de Libro" 
                                    type="number" 
                                    min="1"
                                    maxlength="6" 
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-4" style="display:none" id="divTema">
                <label>Temas Relacionados:</label>
                               <select class="form-control select2" 
                                        name="idTemas" 
                                        id="idTemas"  
                                        data-placeholder="Seleccione" 
                                        style="width: 100%;"
                                >
                                <?php foreach ($arrayTemas as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_CATEGORIA']; ?>"><?php echo utf8_encode($obj['NOM_CATEGORIA']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Buscar" name="btnBuscar" id="btnBuscar"><i class="fa fa-search"> </i>   Buscar</a>
                </div>

                <input type="hidden" name="txtBiblioteca" id="txtBiblioteca">

                <div class="col-md-12 table-responsive" id="divTabla">
                </div>


                <div class="col-md-12"><br><br></div>

                <div class="col-md-12 table-responsive" id="divSolicitado" style="display:none">

                <div class="form-group col-sm-12">
                <h4 class="text-yellow" id="txtSeleccionado">Ejemplares Solicitados:</h4>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="col-md-12 table-responsive">
                    <table id="ejemplares" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>BIBLIOTECA</th>
                        <th>TITULO</th>
                        <th>CANT. SOLICITADA</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody id="detalle">
                    </tbody>  
                    </table>
                </div>


                </div>

</div>


                      <p>&nbsp;</p>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="submit" id="btnregistrar" class="btn btn-primary pull-left" disabled><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					                   