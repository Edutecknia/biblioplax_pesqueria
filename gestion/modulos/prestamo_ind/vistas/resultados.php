<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<div class="form-group col-sm-12">

                <h4 class="text-green" id="txtResultado">Se encontraron <?php echo count($arrayLibros); ?> resultado(s)</h4>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>

</div>

<script>
  $(function () {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false
    });
  });
</script>

<?php
if(count($arrayLibros) > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>BIBLIOTECA</th>
                        <th>TIPO</th>
                        <th>TITULO</th>
                        <th>EDITORIAL</th>
                        <th>AÑO PUBLICACIÓN</th>
                        <th>VOLUMEN</th>
                        <th>DISPONIBLES</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    
                    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
                    $contItem = $contItem2 = 0;
                    
                    foreach ($arrayLibros as $obj):
                    $contItem = $contItem2 + $item;
                    $contItem2++;

                    ?>
                    <tr>
                    <td><?php echo utf8_encode($obj['NOM_CARRERA']); ?></td>
                    <td><?php echo utf8_encode($obj['DES_LIBRO_TIPO']); ?></td>
                    <td><?php echo utf8_encode($obj['DES_TITULO']);?></td>
                    <td><?php echo utf8_encode($obj['NOM_EDITORIAL']);?></td>
                    <td><?php echo utf8_encode($obj['NUM_ANIO_PUBLICA']);?></td>
                    <td><?php echo $obj['NUM_VOLUMEN'];?></td>
                    <td><?php echo $obj['DISPONIBLES'];?></td>
                    <td class="center" align="center">
                    <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm2('visualizar','<?php echo $obj['ID_LIBRO'] ?>');">
                        <span class='label label-primary'>VER</span>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Solicitar Ejemplar" 
                        onclick="javascript:solicitar('<?php echo $obj['ID_LIBRO'] ?>','<?php echo utf8_encode($obj['DES_TITULO']) ?>','<?php echo $obj['DISPONIBLES'] ?>','<?php echo $obj['ID_CARRERA'] ?>','<?php echo utf8_encode($obj['NOM_CARRERA']) ?>','<?php echo $obj['EJEMPLARES'] ?>');">
                        <span class='label label-success'>SOLICITAR</span>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
<?php
}
?>                       