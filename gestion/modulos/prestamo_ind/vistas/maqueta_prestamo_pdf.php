<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
global $conexion;

$documento=$_GET['cmd'];

$documento = str_replace(
            array("|",
                 "'",
                 "&"),
            '', $documento);

 if(!is_numeric($documento)){
    $documento=0;
 }

 if($documento > 0){

$modelData  = new Data_sgaprestamo();
$objData    = $modelData->fu_Encontrar($conexion,$documento); 

$fechaSolicitado    = strtotime( $objData['FEC_INICIO'] ); 
$fechaSolicitado    = date('d/m/Y H:i:s', $fechaSolicitado);

$fechaEntrega       = strtotime( $objData['FEC_FIN'] ); 
$fechaEntrega       = date('d/m/Y H:i:s', $fechaEntrega); 

$arrayData          = $modelData->fu_EncontrarDetalle($conexion,$documento); 

?>
<style type="text/css">
            table.table1{
                font-family: Arial;
                font-size: 12px;
                font-weight: bold;
                line-height: 0.3em;
                font-style: normal;
                border-collapse:separate;
                /*width: 950px;*/
            }
            .table1 thead th{
                padding:10px;
                color:#fff;
                /*text-shadow:1px 1px 1px #568F23;*/
                border:1px solid #4E4B4B;
                border-bottom:1px solid #4E4B4B;
                background-color:#4E4B4B;
            }
            .table1 thead th:empty{
                background:transparent;
                border:none;
            }
            .table1 tbody th{
                padding:10px;
				text-align:center;
				color:#fff;
                text-shadow:1px 1px 1px;
                background-color:#4E4B4B;
                border:1px solid #4E4B4B;
                border-right:1px solid #4E4B4B;
				font-size:10px;
            }
            .table1 tfoot td{
                color: #9CD009;
                font-size:12px;
                text-align:center;
                padding:10px 0px;
                text-shadow:1px 1px 1px #444;
            }
            .table1 tfoot th{
                color:#666;
            }
            .table1 tbody td{
                padding:10px;
                text-align:center;
                background-color:#F0EFEF;
                border: 2px solid #F0EFEF;
                -moz-border-radius:2px;
                -webkit-border-radius:2px;
                border-radius:2px;
                color:#666;
                text-shadow:1px 1px 1px #fff;
				font-size:10px;
            }
            .table1 tbody span.check::before{
                /*content : url(../images/check0.png)*/
            }
        </style>

<!--SOLICITUD DE PRESTAMO-->
<div style="width: 600px;height: auto;">

<div id="divcabecera1" style="padding-left:20px;" >
<img id = "" src="../../../img/pdf.png" style="text-align: center"/>
</div>

<div id="divcabecera2" style="width:400; padding-left:380px; margin-top:-70px;">
<div style="width:350; text-align:center;">
<label style="font-family:Arial;color:#000; font-size:18px;"><b>CEDINPES</b></label><br />
<label style="font-family:Arial;color:#000; font-size:10px;"><b>SOLICITUD DE PRESTAMO</b></label><br />
<label style="font-family:Arial;color:#F00; font-size:12px;"><b>N° <?php echo $documento; ?></b></label>
</div>
</div>

<div id="datos1" style="width:600; padding-left:20px;">
<table class="table1" style="margin-top:60px; width:600;" >
                                <tbody>
                                        <tr>
                                            <th>SOLICITANTE : </th>
                                            <td width="250" style="text-align: left"><?php  echo utf8_encode($objData['NOM_USUARIO'].' '.$objData['APE_USUARIO']); ?></td>              
                                        </tr>
                                        <tr>
                                            
                                            <th>CÓDIGO : </th>
                                            <td width="150" style="text-align: left"><?php  echo utf8_encode($objData['COD_UNIVERSITARIO']); ?></td>   
                                        </tr>
                                        <tr>
                                            <th width="125">BIBLIOTECA : </th>
                                            <td width="260" style="text-align: left"><?php  echo utf8_encode($objData['NOM_CARRERA']); ?></td>              
                                        </tr>
                                        <tr>
                                            
                                            <th>FECHA PRESTAMO : </th>
                                            <td width="150" style="text-align: left">DEL <?php echo $fechaSolicitado.' AL '.$fechaEntrega; ?></td>   
                                        </tr>
                                  
                                </tbody>
                            </table>                            
</div>


                        <div style="width:600; padding-left:20px;height: 3px;">
                        <table  class="table1" >
                        <tbody>
                            <tr>
                              <th class="green" style="font-size:10px;" width="10" >COD.</th>
                              <th class="green" style="font-size:10px;" width="15" >ISBN</th>
                              <th class="green" style="font-size:10px;" width="300">TITULO</th>         
                              <th class="green" width="50" style="font-size:10px;">CANTIDAD</th>         
                            </tr>

                        <?php foreach ($arrayData as $obj): ?>
                        
                        <tr border="1" height="3" style="height:3 !important; font-size:8px;">
                            <td align="left" style="height:3 !important; font-size:8px;"><?php echo $obj['COD_LIBRO']; ?></td>
                            <td style="height:3 !important; font-size:8px;"><?php echo utf8_encode($obj['COD_ISBM']); ?></td>
                            <td style="height:3 !important; font-size:8px;"><?php  echo utf8_encode($obj['DES_TITULO']); ?></td>
                            <td style="height:3 !important; font-size:8px;">1</td>
                        </tr>

                        <?php endforeach; ?> 

                    
                        </tbody>
    </table>
    



                        </div>

</div>
<!--SOLICITUD DE PRESTAMO-->





<?php
}
?>