<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
include_once 'vistas/funciones_imprimir.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'P.ID_PRESTAMO';
	$direccion = 'DESC';

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgaprestamo();
	$arrayEstados	= $modelData->fu_listarPrestamoEstado($conexion);

    include $nombreModulo . 'vistas/main.php';
}


function listar($fini, $ffin, $indActivo, $orden, $direccion, $pagina){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	if($fini == ''){ $fini = INICIO_SISTEMA; }
	if($ffin == ''){ $ffin = date('d/m/Y');  }

	$fecha          = @explode('/',$fini);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fini 			= $anio.'-'.$mes.'-'.$dia;

    $fecha          = @explode('/',$ffin);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $ffin			= $anio.'-'.$mes.'-'.$dia;
	
	$modelData	= new Data_sgaprestamo();
	$arrayData	= $modelData->fu_listarxUsuario($conexion,$fini, $ffin, $indActivo, $orden, $direccion, $pagina,$idUsuario);

	foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var fini 		= $("#fechaInicio").val();
    						var ffin 		= $("#fechaFin").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/prestamo_ind/prestamo_ind.php?cmd=listar", {
						fini: fini, ffin:ffin, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,0);

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);

	$modelUsuario 	= new Data_sgausuario();
	$modelUsuario 	= $modelUsuario->fu_encontrar($conexion,$idUsuario);

	include $nombreModulo . 'vistas/crear.php';
}

function buscarLibro($idFacultad,$idFiltro,$descripcion,$codigo,$idTemas){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	if($idFiltro == 'tema'){
		$descripcion = $idTemas;
	}

	$modelData		= new Data_sgaprestamo();
	$arrayLibros	= $modelData->fu_buscarLibro($conexion,$idFacultad, $idFiltro, $descripcion, $codigo,'');

	include $nombreModulo . 'vistas/resultados.php';

}

function loadVer($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);
	
	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$modelData		= new Data_sgalibro();
	$arrayLibroTipo	= $modelData->fu_listarLibroTipo($conexion);
	$arrayAdjunto	= $modelData->fu_listarAdjunto($conexion,$id);

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	$modelData		= new Data_sgaautor();
	$arrayAutor		= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaeditorial();
	$arrayEditorial	= $modelData->fu_listarTodo($conexion);
	
	$obj_Data		= new Data_sgalibro();
	$arrayEjemplar  = $obj_Data->fu_EncontrarEjemplares($conexion,$id);
	$arrayDatos  	= $obj_Data->fu_EncontrarAutores($conexion,$id);
	$obj_Data	 	= $obj_Data->fu_Encontrar($conexion,$id);

	$_SESSION['BTK_CARPETA_ID_FACULTAD'] 	= $obj_Data['ID_CARRERA'];

	$arrayAutorLib 	= array();
	foreach ($arrayDatos as $obj) {
		$arrayAutorLib[] = $obj['ID_AUTOR'];
	}

	$arrayTema		= array();
	$arrayTema		= @explode(',', $obj_Data['DES_CATEGORIA_RELACION']);

	$id = $id;
	
	include $nombreModulo . 'vistas/ver.php';

}

function registrar($idBiblioteca, $detLibro, $detCant, $diasPrestamo){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$fecha          = @explode('/',$fecAdquisicion);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecAdquisicion = $anio.'-'.$mes.'-'.$dia;

    $temasRelacion=@implode(',',$temasRelacion);

    $temasRelacion	= '0,'.$temasRelacion;
	
	$modelData	= new Data_sgaprestamo();

	$rpta = $modelData->fu_registrar($conexion,$idUsuario, $idBiblioteca, $diasPrestamo, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0){
		
		$idPrestamo = $rpta['ID_PRESTAMO'];

		$aLibro = @explode('|', $detLibro);
		$aCant  = @explode('|', $detCant);

		for($i = 0; $i < count($aLibro); $i++){

			$idLibro 	= $aLibro[$i];
			$cantidad	= $aCant[$i];

			$rptaDet = $modelData->fu_registrarDetalle($conexion,$idPrestamo, $idLibro, $cantidad, $idUsuario, $ip); 
		}

		@imprime_ticket_prestamo($idPrestamo);

		echo $idPrestamo;
	}
	else{ 
		if($rpta['IND_OPERACION'] == 0){
			echo $rpta['DES_MENSAJE'];
		}
	else{
		echo 'failed';
	 }
	}
	
}


function loadVisualizar($idPrestamo){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData  = new Data_sgaprestamo();
	$objData    = $modelData->fu_Encontrar($conexion,$idPrestamo); 
	$arrayData  = $modelData->fu_EncontrarDetalle($conexion,$idPrestamo); 

	include $nombreModulo . 'vistas/visualizar.php';
}

/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['fini'],$_POST['ffin'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'buscarLibro':
		 buscarLibro($_POST['idFacultad'],$_POST['idFiltro'],$_POST['descripcion'],$_POST['codigo'],$_POST['idTemas']);
		 break;	
	case 'loadVer':
		loadVer($_POST['id']);break;
	case 'registrar':
		registrar($_POST['idBiblioteca'],$_POST['detLibro'], $_POST['detCant'], $_POST['diasPrestamo']);break;
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;	
    default:
        vistaPrincipal();
        break;
}
?>