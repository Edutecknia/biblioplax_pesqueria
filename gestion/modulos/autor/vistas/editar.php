<form action="javascript:editar();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

<input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>  
                
                <div class="form-group col-sm-6">
                <label>Autor:</label>
                               <select class="form-control" name="idAutorTipo" id="idAutorTipo">
                                <?php foreach ($arrayAutorTipo as $obj): ?>
                                <option value="<?php echo $obj['ID_AUTOR_TIPO']; ?>"
                                <?php if($obj['ID_AUTOR_TIPO'] == $obj_Data['ID_AUTOR_TIPO']){ echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['DES_TIPO']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div style="clear:both;"></div>

                <div class="form-group col-sm-12">
                <label>Nombre Completo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomAutor" name="nomAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre completo del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo utf8_encode($obj_Data['NOM_AUTOR']); ?>">
                                </div>
                </div>

                <!--<div class="form-group col-sm-6">
                <label>Apellidos:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="apeAutor" name="apeAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese apellidos del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    value="<?php //echo utf8_encode($obj_Data['APE_AUTOR']); ?>"
                                    >
                                </div>
                </div>-->

                <div class="form-group col-sm-6">
                <label>Seudónimo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="seudoAutor" name="seudoAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese seudónimo del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    value="<?php echo utf8_encode($obj_Data['DES_SEUDONIMO']); ?>"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Pais:</label>
                               <select class="form-control" name="idPais" id="idPais">
                               <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"  
                                <?php if($obj['ID_PAIS'] == $obj_Data['ID_PAIS']){ echo 'selected';} ?>
                                >
                                <?php echo utf8_encode($obj['NOM_PAIS']); ?>
                                </option>
                                <?php endforeach; ?>
                                </select>
                </div>




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>                         




                   