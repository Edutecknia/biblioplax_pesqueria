<form action="javascript:registrar();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

                <div class="form-group col-sm-6">
                <label>Autor:</label>
                               <select class="form-control" name="idAutorTipo" id="idAutorTipo">
                                <?php foreach ($arrayAutorTipo as $obj): ?>
                                <option value="<?php echo $obj['ID_AUTOR_TIPO']; ?>"><?php echo utf8_encode($obj['DES_TIPO']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div style="clear:both;"></div>

                <div class="form-group col-sm-12">
                <label>Nombres:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomAutor" name="nomAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre completo del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                </div>

                <!--<div class="form-group col-sm-6">
                <label>Apellidos:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="apeAutor" name="apeAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese apellidos del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>-->

                <div class="form-group col-sm-6">
                <label>Seudónimo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="seudoAutor" name="seudoAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese seudónimo del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Pais:</label>
                               <select class="form-control" name="idPais" id="idPais">
                               <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): 
                                /*$nomPais = $obj['NOM_PAIS'];
                                $encod = mb_detect_encoding($obj['NOM_PAIS'], 'UTF-8, ISO-8859-1');
                                if($encod=='ISO-8859-1'){
                                    $nomPais = utf8_encode($obj['NOM_PAIS']);
                                }*/
                                ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					




                   