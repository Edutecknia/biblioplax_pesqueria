<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	 
    $objUsuario     = $_SESSION['BTK_USUARIO'];
    $idFacultad     = $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

    $modelData      = new Data_sgacarrera();
    $arrayFacultad  = $modelData->fu_listarTodo($conexion,$idFacultad);

    include $nombreModulo . 'vistas/main.php';
}


function grafico_1($idFacultad){
    global $conexion, $etiquetaTitulo, $nombreModulo;
    
    if($cantDias == ''){$cantDias = 7; }

    $modelData  = new Data_sgadashboard();
    $arrayData  = $modelData->fu_grafico_1($conexion,$idFacultad);

    $arrayConcepto      = array();
    $arrayCantidad      = array();

  foreach ($arrayData as $obj) {

    $arrayConcepto[]       = $obj['NOM_CAMPO'];
    $arrayCantidad[]    = $obj['CANTIDAD'];
  }
  
  $jsonCantidad     = json_encode($arrayCantidad);
  $jsonConcepto     = json_encode($arrayConcepto);
  
	include $nombreModulo . 'vistas/1.php';	
}


function grafico_2(){
    global $conexion, $etiquetaTitulo, $nombreModulo;

    $modelData  = new Data_sgadashboard();
    $arrayData  = $modelData->fu_grafico_2($conexion);

    $arrayProducto      = array();
    $arrayCantidad      = array();

  foreach ($arrayData as $obj) {

    $arrayProducto[]    = $obj['NOM_PRODUCTO'];
    $arrayCantidad[]    = number_format($obj['IMP_TOTAL'], 2, '.', '');
  }
  
  $jsonCantidad     = json_encode($arrayCantidad);
  $jsonProducto     = json_encode($arrayProducto);
  
  include $nombreModulo . 'vistas/2.php'; 
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'grafico_1':
		 grafico_1($_POST['idFacultad']);
		 break;	
  case 'grafico_2':
     grafico_2();
     break;     
    default:
        vistaPrincipal();
        break;
}
?>