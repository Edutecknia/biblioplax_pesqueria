<script>
$(document).ready(function(){
		
    var barChartData = {
                  labels : <?php echo $jsonProducto; ?>,
                  datasets : [
                  {
                    type: 'bar',
                    backgroundColor: "#00C66C",
                    label: 'Monto en S/ ',
                    data: <?php echo $jsonCantidad ?>,
                    borderColor: '#00C66C',
                    borderWidth: 2
                  }
                  ]
                }                  

                  
            var ctx = document.getElementById("chart_grafico_2").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                    animation: {
                        onComplete: function () {
                            var chartInstance = this.chart;
                            var ctx = chartInstance.ctx;
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = "center";
                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
                                }),this)
                            }),this);
                        }
                    }
                }
            });
    
});
</script>

<div class="box box-warning">
    <div class="box-header with-border">
    <h3 class="box-title">Venta del día por producto</h3>
    </div>

 <div class="box-body table-responsive no-padding" id="Contenedorform">
 <canvas id="chart_grafico_2"></canvas>
 </div>

</div>
