<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	
    include $nombreModulo . 'vistas/main.php';
}


function listar(){

	include $nombreModulo . 'vistas/listar.php';	
}

/********MOSTRANDO MAIN DE REPORTE*********/
function loadVistaReporte($nro){
    
    switch ($nro) {
        case '1': vista_1();break;
        case '2': vista_2();break;
        case '3': vista_3();break;
    }
}

function vista_1(){
    global $conexion, $etiquetaTitulo, $nombreModulo;
    
    $objUsuario     = $_SESSION['BTK_USUARIO'];
    $idFacultad     = $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

    $modelData      = new Data_sgacarrera();
    $arrayFacultad  = $modelData->fu_listarTodo($conexion,$idFacultad);

    include $nombreModulo . 'vistas/1.php';    
}

function vista_2(){
    global $conexion, $etiquetaTitulo, $nombreModulo;

    include $nombreModulo . 'vistas/2.php';    
}

function vista_3(){
    global $conexion, $etiquetaTitulo, $nombreModulo;
    $pagina = 1;
    $orden = 'T.ID_CARA,T.ID_MANGUERA';
    $direccion = 'DESC';

    $modelData  = new Data_sgacara();
    $arrayData  = $modelData->fu_listarTodo($conexion);

    include $nombreModulo . 'vistas/3.php';    
}

/*************************************RESULTADO DE REPORTES*************************+*/

function listado_1($idProducto, $fecInicio, $fecFin, $orden, $direccion, $pagina, $idTransaccion){
    global $conexion, $etiquetaTitulo, $nombreModulo;
    
    $objUsuario     = $_SESSION['FUS_USUARIO'];

    if($fecInicio != '' && $fecFin != ''){
    $fecha          = @explode('/',$fecInicio);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecInicio      = $anio.'-'.$mes.'-'.$dia;

    $fecha          = @explode('/',$fecFin);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecFin         = $anio.'-'.$mes.'-'.$dia;
    }
    
    $modelData  = new Data_sgareporte();
    $arrayData  = $modelData->fu_listar_1($conexion,$idProducto, $fecInicio, $fecFin, $orden, $direccion, $pagina, $idTransaccion);

    foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

    include $nombreModulo . 'resultados/1.php';

    if($numPaginas > 1){
     echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
                            loop:true,
                            firstClass: "first"                         
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();                           
                            var idProducto  = $("#cboProducto").val();
                            var fecInicio   = $("#fechaInicio").val();
                            var fecFin      = $("#fechaFin").val();
                            var orden       = $("#orden").attr("value");
                            var direccion        = $("#direccion").attr("value");
                            var page             = $("#pag_actual").attr("value");
                            var idTransaccion    = $("#idTransaccion").val();
                        INI_LOAD();   
                        $("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
                        $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=listado_1", {
                        idProducto: idProducto, fecInicio: fecInicio, fecFin:fecFin, orden:orden, direccion:direccion, pagina: num, idTransaccion:idTransaccion
                        }, function(data){
                            $("#cont_resultado_main").empty();
                            $("#cont_resultado_main").append(data);
                            FIN_LOAD();
                        });
                            
                        }).find(".pagination");
                    </script>'; 
     }
    
}

function listarMangueraxCara($idCara){
    global $conexion, $etiquetaTitulo, $nombreModulo;

    $combo = '<option value="">TODOS</option>';

    if($idCara != ''){
    
    $modelData      = new Data_sgacara();
    $arrayData      = $modelData->fu_listarManguera($conexion, $idCara);

    if (count($arrayData) > 0) {

        foreach ($arrayData as $obj): 
                 $combo.='<option value="' . $obj['ID_MANGUERA'] . '">' . utf8_encode($obj['NOM_PRODUCTO']). '</option>';
         endforeach; 
    }

    }
    
    echo $combo;

}

function listado_2($idCara, $idManguera, $fecInicio, $fecFin, $orden, $direccion, $pagina){
    global $conexion, $etiquetaTitulo, $nombreModulo;
    
    $objUsuario     = $_SESSION['FUS_USUARIO'];

    if($fecInicio != '' && $fecFin != ''){
    $fecha          = @explode('/',$fecInicio);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecInicio      = $anio.'-'.$mes.'-'.$dia;

    $fecha          = @explode('/',$fecFin);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecFin         = $anio.'-'.$mes.'-'.$dia;
    }
    
    $modelData  = new Data_sgareporte();
    $arrayData  = $modelData->fu_listar_2($conexion,$idCara, $idManguera, $fecInicio, $fecFin, $orden, $direccion, $pagina);

    foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

    include $nombreModulo . 'resultados/2.php';

    if($numPaginas > 1){
     echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
                            loop:true,
                            firstClass: "first"                         
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();                           
                            var idCara      = $("#cboCara").val();
                            var idManguera  = $("#cboManguera").val();
                            var fecInicio   = $("#fechaInicio").val();
                            var fecFin      = $("#fechaFin").val();
                            var orden       = $("#orden").attr("value");
                            var direccion        = $("#direccion").attr("value");
                            var page             = $("#pag_actual").attr("value");

                        INI_LOAD();   
                        $("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
                        $.post("modulos/tabla_reporte/tabla_reporte.php?cmd=listado_2", {
                        idCara: idCara, idManguera:idManguera, fecInicio: fecInicio, fecFin:fecFin, orden:orden, direccion:direccion, pagina: num
                        }, function(data){
                            $("#cont_resultado_main").empty();
                            $("#cont_resultado_main").append(data);
                            FIN_LOAD();
                        });
                            
                        }).find(".pagination");
                    </script>'; 
     }
    
}

function listado_3($idCara, $idManguera, $fecInicio, $fecFin, $orden, $direccion, $pagina){
    global $conexion, $etiquetaTitulo, $nombreModulo;
    
    $objUsuario     = $_SESSION['FUS_USUARIO'];

    if($fecInicio != ''){
    $fecha          = @explode('/',$fecInicio);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecInicio      = $anio.'-'.$mes.'-'.$dia;
    }
    
    $modelData  = new Data_sgareporte();
    $arrayData  = $modelData->fu_listar_3($conexion,$idCara, $idManguera, $fecInicio, $fecFin, $orden, $direccion, $pagina);

    include $nombreModulo . 'resultados/3.php';
        
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar();
		 break;	
    case 'loadVistaReporte':
         loadVistaReporte($_POST['nro']);
         break;     
    case 'listado_1':
         listado_1($_POST['idProducto'],$_POST['fecInicio'],$_POST['fecFin'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['idTransaccion']);
         break;
    case 'listarMangueraxCara':
         listarMangueraxCara($_POST['idCara']);
         break;
    case 'listado_2':
         listado_2($_POST['idCara'],$_POST['idManguera'],$_POST['fecInicio'],$_POST['fecFin'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
         break;
    case 'listado_3':
         listado_3($_POST['idCara'],$_POST['idManguera'],$_POST['fecInicio'],$_POST['fecFin'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
         break;
    default:
        vistaPrincipal();
        break;
}
?>