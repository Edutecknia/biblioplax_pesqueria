<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div> 
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>REPORTE</th>
                        <th>DESCRIPCIÓN</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>1</td>
                    <td>REPORTE DE PRESTAMOS</td>
                    <td>MUESTRA LOS PRESTAMOS REALIZADOS SEGÚN RANGO DE FECHAS</td>
                    <td class="center">
                    <a  style="cursor:pointer;" 
                        title="Abrir" 
                        onclick="javascript:loadReporte('1');">
                        <i class="fa fa-sign-in"></i>
                    </a>    
                    </td>
                    </tr>

                    <tr>
                    <td>2</td>
                    <td>REPORTE DE MOVIMIENTOS DE LIBRO</td>
                    <td>MUESTRA LOS MOVIMIENTOS REALIZADOS POR CADA MATERIAL, PUEDE SER FILTRADO SEGÚN RANGO DE FECHAS</td>
                    <td class="center">
                    <a  style="cursor:pointer;" 
                        title="Abrir" 
                        onclick="javascript:loadReporte('2');">
                        <i class="fa fa-sign-in"></i>
                    </a>    
                    </td>
                    </tr>

                    

                    </tbody>
                </table>                
</div>                  