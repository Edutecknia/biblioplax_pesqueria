<script type="text/javascript" src="js/mReporte.js"></script>

<script>
$(document).ready(function(){
		

    $('#dtFechaIni').datetimepicker(
     {
         format: 'DD/MM/YYYY',
         defaultDate: new Date()
     }
    ).on('changeDate', function(e){
     $(this).datepicker('hide');
    });     
    
    $(".timepicker").timepicker({
        showInputs: false
    });


    $('#dtFechaFin').datetimepicker(
     {
         format: 'DD/MM/YYYY',
         defaultDate: new Date()
     }
    ).on('changeDate', function(e){
     $(this).datepicker('hide');
    });     
    
    $(".timepicker").timepicker({
        showInputs: false
    });

        $("#btnBuscar").click(function() {
        listado_1();
        });

        /* reset paginacion al cambiar texto */
        $('#fechaInicio').change(function() {
        $('#pag_actual').val('1');
        });

        $('#fechaFin').change(function() {
        $('#pag_actual').val('1');
        });

        $('#cboProducto').change(function() {
        $('#pag_actual').val('1');
        });

        $('#idTransaccion').change(function() {
        $('#pag_actual').val('1');
        });

      

});
</script>
<div class="content" >
             <div class="row">
                <div class="col-md-12">

				<div class="box box-primary">
                <div class="box-header with-border">
                  <h5 class="box-title" id="titulo">Reporte de Préstamos</h5>
                </div>
                <div class="box-body" id="Contenedorform">
                
                <input type="hidden" id="pag_actual" name="pag_actual" value="<?php echo $pagina ?>"/>
                <input type="hidden" id="orden" name="orden" value="<?php echo $orden ?>"/>
                <input type="hidden" id="direccion" name="direccion" value="<?php echo $direccion ?>"/>
                
                <div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>">
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

                <div class="form-group col-sm-2">
                <label>Fecha Inicio:</label>
                        <div class='input-group date' id='dtFechaIni'>
                        <input type='text' 
                                class="form-control" 
                                id="fechaInicio"
                        />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                </div>

                <div class="form-group col-sm-2">
                <label>Fecha Fin:</label>
                        <div class='input-group date' id='dtFechaFin'>
                        <input type='text' 
                                class="form-control" 
                                id="fechaFin"
                        />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                </div>

                 <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Exportar" onclick="javascript:reporte_1();"><i class="fa fa-file-excel-o"> </i>   Exportar</a>
                </div>

                <div class="form-group col-md-1">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Buscar" name="btnBuscar" id="btnBuscar" onclick="javascript:MainForm();" title="Volver"><i class="fa fa-arrow-circle-left" title="Volver"> </i></a>
                </div>
                
                <div class="col-md-12 table-responsive" id="ContenedorListado">
                </div>
                
                </div><!-- /.box-body -->
              </div><!-- /.box-primary -->
                  
                </div>
            </div>
            </div>