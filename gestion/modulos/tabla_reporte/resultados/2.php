<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>
<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>ID VENTA</th>
                        <th>FECHA</th>
                        <th>CARA</th>
                        <th>MANGUERA</th>
                        <th>PRODUCTO</th>
                        <th>VOLUMEN</th>
                        <th>PRECIO</th>
                        <th>MONTO</th>
                        <th>VOLUMEN FINAL</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
                    $idCara     = '';
                    $idmanguera = '';
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayData as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;

                    $fecha   = strtotime( $obj['FEC_VENTA'] ); 
                    $fecha   = date('d/m/Y H:i:s', $fecha); 

                    if($idCara != $obj['ID_CARA']){

                        echo '<tr><td colspan="10"><h5><strong>CARA '.$obj['ID_CARA'].'</strong></h5></td></tr>';
                        $idCara     = $obj['ID_CARA'];
                        $idmanguera = $obj['ID_MANGUERA'];
                    }
                    else{
                        if($idmanguera != $obj['ID_MANGUERA']){ 
                            $idmanguera = $obj['ID_MANGUERA']; 
                            echo '<tr><td colspan="10">&nbsp;</td></tr>';
                        }
                    }
					?>
                    <tr>
                    <td><?php echo $contItem;?></td>
                    <td><?php echo $obj['ID_TRANSACCION'];?></td>
                    <td><?php echo $fecha; ?></td>
                    <td><?php echo $obj['ID_CARA'];?></td>
                    <td><?php echo $obj['ID_MANGUERA'];?></td>
                    <td><?php echo utf8_encode($obj['NOM_PRODUCTO']);?></td>
                    <td align="right"><?php if($obj['CANT_VOLUMEN'] < 1){ echo '0'.$obj['CANT_VOLUMEN']; } else{echo $obj['CANT_VOLUMEN'];} ?></td>
                     <td align="right"><?php echo number_format($obj['IMP_PRECIO'], 2, '.', ','); ?></td>
                    <td align="right"><?php echo number_format($obj['IMP_TOTAL'], 2, '.', ','); ?></td>
                    <td align="right"><?php echo $obj['VOLFIN'];?></td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  