<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>
<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if(count($arrayData) > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>CARA</th>
                        <th>MANGUERA</th>
                        <th>PRODUCTO</th>
                        <th>VOLUMEN FINAL</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
                    $idCara     = '';
                    $idmanguera = '';
					$contItem2  = 0;
					
                    foreach ($arrayData as $obj):
                    $contItem2++;

                    if($idCara != $obj['ID_CARA']){

                        echo '<tr><td colspan="4"><h5><strong>CARA '.$obj['ID_CARA'].'</strong></h5></td></tr>';
                        $idCara     = $obj['ID_CARA'];
                        $idmanguera = $obj['ID_MANGUERA'];
                    }
					?>
                    <tr>
                    <td><?php echo $obj['ID_CARA'];?></td>
                    <td><?php echo $obj['ID_MANGUERA'];?></td>
                    <td><?php echo utf8_encode($obj['NOM_PRODUCTO']);?></td>
                    <td align="right"><?php echo $obj['VOLFIN'];?></td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  