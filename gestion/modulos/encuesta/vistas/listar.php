<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false
    });
  });
</script>
<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>BIBLIOTECA</th>
                        <th>DESCRIPCIÓN</th>
                        <th>FECHA REGISTRO</th>
                        <th>FECHA INICIO</th>
                        <th>FECHA FIN</th>
                        <th>ESTADO</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayData as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;
					?>
                    <tr>
                    <td><?php echo $contItem;?></td>
                    <td><?php echo utf8_encode($obj['NOM_CARRERA']); ?></td>
                    <td><?php echo utf8_encode($obj['NOM_ENCUESTA']);?></td>
                    <td>
                    <?php 
                    $fecha   = strtotime( $obj['FEC_REGISTRO'] ); 
                    $fecha   = date('d/m/Y H:i:s', $fecha); 
                    echo $fecha;
                    ?>
                    </td>
                    <td>
                    <?php 
                    $fecha = '';
                    if($obj['FEC_INICIO'] != null){
                    $fecha   = strtotime( $obj['FEC_INICIO'] ); 
                    $fecha   = date('d/m/Y H:i:s', $fecha); 
                    }
                    echo $fecha;
                    ?>
                    </td>
                    <td>
                    <?php 
                     $fecha = '';
                    if($obj['FEC_FIN'] != null){
                    $fecha   = strtotime( $obj['FEC_FIN'] ); 
                    $fecha   = date('d/m/Y H:i:s', $fecha); 
                    }
                    echo $fecha;
                    ?>
                    </td>
                    <td class="center"><?php echo utf8_encode($obj['DES_ESTADO']); ?></td>
                    <td class="center" align="center">
                    <?php if($obj['DES_ESTADO']=='SIN INICIAR'){ ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Iniciar" 
                        onclick="javascript:Iniciar('<?php echo $obj['ID_ENCUESTA'] ?>');">
                        <i class="fa fa-play"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Eliminar" 
                        onclick="javascript:Eliminar('<?php echo $obj['ID_ENCUESTA'] ?>');">
                        <i class="fa fa-trash"></i>
                    </a>
                     <?php } ?>

                     <?php if($obj['DES_ESTADO']=='INICIADO'){ ?>
                     <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Finalizar" 
                        onclick="javascript:Finalizar('<?php echo $obj['ID_ENCUESTA'] ?>');">
                        <i class="fa fa-stop"></i>
                    </a>&nbsp;&nbsp;
                     <?php } ?>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  