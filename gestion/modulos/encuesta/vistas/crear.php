<form action="javascript:registrar();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">



                <div class="form-group col-sm-6">
                <label>Registrar en Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>">
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>


                <div class="form-group col-sm-6">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomEncuesta" name="nomEncuesta" 
                                    class="form-control" 
                                    placeholder="Descripción o titulo de la encuesta" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                </div>

                
               <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">PREGUNTA 1</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="form-group col-sm-12">
                <label>Pregunta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="pregunta_1" name="pregunta_1" 
                                    class="form-control" 
                                    placeholder="Descripción de la pregunta 1" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Tipo de Pregunta:</label>
                <select class="form-control" name="idTipo_1" id="idTipo_1" onchange="javascript:habilitar('1');">
                <option value="DESCRIPTIVA">DESCRIPTIVA</option>
                <option value="SELECTIVA">SELECTIVA</option>
                </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Opciones:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="opciones_1" name="opciones_1" 
                                    class="form-control" 
                                    placeholder="Para una pregunta selectiva separe las opciones por coma (,)" 
                                    type="text" 
                                    maxlength="300"
                                    disabled 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">PREGUNTA 2</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="form-group col-sm-12">
                <label>Pregunta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="pregunta_2" name="pregunta_2" 
                                    class="form-control" 
                                    placeholder="Descripción de la pregunta 2" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Tipo de Pregunta:</label>
                <select class="form-control" name="idTipo_2" id="idTipo_2" onchange="javascript:habilitar('2');">
                <option value="DESCRIPTIVA">DESCRIPTIVA</option>
                <option value="SELECTIVA">SELECTIVA</option>
                </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Opciones:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="opciones_2" name="opciones_2" 
                                    class="form-control" 
                                    placeholder="Para una pregunta selectiva separe las opciones por coma (,)" 
                                    type="text" 
                                    maxlength="300"
                                    disabled
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">PREGUNTA 3</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="form-group col-sm-12">
                <label>Pregunta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="pregunta_3" name="pregunta_3" 
                                    class="form-control" 
                                    placeholder="Descripción de la pregunta 3" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Tipo de Pregunta:</label>
                <select class="form-control" name="idTipo_3" id="idTipo_3" onchange="javascript:habilitar('3');">
                <option value="DESCRIPTIVA">DESCRIPTIVA</option>
                <option value="SELECTIVA">SELECTIVA</option>
                </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Opciones:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="opciones_3" name="opciones_3" 
                                    class="form-control" 
                                    placeholder="Para una pregunta selectiva separe las opciones por coma (,)" 
                                    type="text" 
                                    maxlength="300"
                                    disabled
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">PREGUNTA 4</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="form-group col-sm-12">
                <label>Pregunta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="pregunta_4" name="pregunta_4" 
                                    class="form-control" 
                                    placeholder="Descripción de la pregunta 4" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Tipo de Pregunta:</label>
                <select class="form-control" name="idTipo_4" id="idTipo_4" onchange="javascript:habilitar('4');">
                <option value="DESCRIPTIVA">DESCRIPTIVA</option>
                <option value="SELECTIVA">SELECTIVA</option>
                </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Opciones:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="opciones_4" name="opciones_4" 
                                    class="form-control" 
                                    placeholder="Para una pregunta selectiva separe las opciones por coma (,)" 
                                    type="text" 
                                    maxlength="300"
                                    disabled
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">PREGUNTA 5</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="form-group col-sm-12">
                <label>Pregunta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="pregunta_5" name="pregunta_5" 
                                    class="form-control" 
                                    placeholder="Descripción de la pregunta 5" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Tipo de Pregunta:</label>
                <select class="form-control" name="idTipo_5" id="idTipo_5" onchange="javascript:habilitar('5');">
                <option value="DESCRIPTIVA">DESCRIPTIVA</option>
                <option value="SELECTIVA">SELECTIVA</option>
                </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Opciones:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="opciones_5" name="opciones_5" 
                                    class="form-control" 
                                    placeholder="Para una pregunta selectiva separe las opciones por coma (,)" 
                                    type="text" 
                                    maxlength="300"
                                    disabled
                                    >
                                </div>
                </div>


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					




                   