<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'C.NOM_PROVEEDOR';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar($filtro, $indActivo, $orden, $direccion, $pagina){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	
	$modelData	= new Data_sgaproveedor();
	$arrayData	= $modelData->fu_listar($conexion,$filtro, $indActivo, $orden, $direccion, $pagina);

	foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var filtro 		= $("#txtBuscar").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/proveedor/proveedor.php?cmd=listar", {
						filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	include $nombreModulo . 'vistas/crear.php';
}

function registrar($rucproveedor,$nomproveedor, $idPais, $direcproveedor, $fonoproveedor, $webproveedor, $mailproveedor, $contacproveedor){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaproveedor();

	$rpta = $modelData->fu_registrar($conexion, $rucproveedor,$nomproveedor, $idPais, $direcproveedor, $fonoproveedor, $mailproveedor, $webproveedor, $contacproveedor, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);
	
	$obj_Data		= new Data_sgaproveedor();
	$obj_Data	 	= $obj_Data->fu_Encontrar($conexion,$id);
	$id = $id;
	include $nombreModulo . 'vistas/editar.php';

}

function editar($id,$rucproveedor,$nomproveedor, $idPais, $direcproveedor, $fonoproveedor, $webproveedor, $mailproveedor, $contacproveedor){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaproveedor();

	$rpta = $modelData->fu_editar($conexion, $id, $rucproveedor,$nomproveedor, $idPais, $direcproveedor, $fonoproveedor, $mailproveedor, $webproveedor, $contacproveedor, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}


function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaproveedor();

	$rpta = $modelData->fu_inactivar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaproveedor();

	$rpta = $modelData->fu_activar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}


/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['filtro'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['rucproveedor'],$_POST['nomproveedor'], $_POST['idPais'], $_POST['direcproveedor'], $_POST['fonoproveedor'], $_POST['webproveedor'], $_POST['mailproveedor'], $_POST['contacproveedor']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;		
	case 'editar':
		editar($_POST['id'],$_POST['rucproveedor'],$_POST['nomproveedor'], $_POST['idPais'], $_POST['direcproveedor'], $_POST['fonoproveedor'], $_POST['webproveedor'], $_POST['mailproveedor'], $_POST['contacproveedor']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>