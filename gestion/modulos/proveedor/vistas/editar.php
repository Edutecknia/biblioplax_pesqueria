<form action="javascript:editar();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

                <input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>  

                <div class="form-group col-sm-6">
                <label>RUC:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="rucProveedor" name="rucProveedor" 
                                    class="form-control" 
                                    placeholder="RUC del Proveedor" 
                                    type="number" 
                                    max="99999999999" 
                                    required 
                                    value="<?php echo $obj_Data['NUM_RUC']; ?>"
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-6">
                <label>Nombre:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomProveedor" name="nomProveedor" 
                                    class="form-control" 
                                    placeholder="Nombre del Proveedor" 
                                    type="text" 
                                    maxlength="200" 
                                    required="required"
                                    value="<?php echo utf8_encode($obj_Data['NOM_PROVEEDOR']); ?>">
                                </div>
                </div>

                
                <div class="form-group col-sm-6">
                <label>Pais:</label>
                               <select class="form-control" name="idPais" id="idPais">
                               <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"
                                <?php if($obj['ID_PAIS'] == $obj_Data['ID_PAIS']){ echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>


                <div class="form-group col-sm-6">
                <label>Dirección:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="direcProveedor" name="direcProveedor" 
                                    class="form-control" 
                                    placeholder="Dirección del Proveedor" 
                                    type="text" 
                                    maxlength="200" 
                                    value="<?php echo utf8_encode($obj_Data['DES_DIRECCION']); ?>"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Teléfono:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="fonoProveedor" name="fonoProveedor" 
                                    class="form-control" 
                                    placeholder="Teléfono del Proveedor" 
                                    type="number" 
                                    value="<?php echo $obj_Data['NUM_TELEFONO']; ?>"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>E-mail:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="mailProveedor" name="mailProveedor" 
                                    class="form-control" 
                                    placeholder="E-Mail del Proveedor" 
                                    type="email" 
                                    maxlength="200" 
                                    value="<?php echo $obj_Data['DES_MAIL']; ?>"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Sitio Web:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="webProveedor" name="webProveedor" 
                                    class="form-control" 
                                    placeholder="Sitio Web del Proveedor" 
                                    type="text" 
                                    maxlength="200" 
                                    value="<?php echo utf8_encode($obj_Data['DES_WEB']); ?>"
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-6">
                <label>Contacto:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="contactoProveedor" name="contactoProveedor" 
                                    class="form-control" 
                                    placeholder="Nombre del Contacto" 
                                    type="text" 
                                    maxlength="200" 
                                    value="<?php echo utf8_encode($obj_Data['DES_CONTACTO']); ?>"
                                    >
                                </div>
                </div>


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>                 