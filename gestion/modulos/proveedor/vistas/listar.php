<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false
    });
  });
</script>
<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>RUC</th>
                        <th>NOMBRE</th>
                        <th>PAIS</th>
                        <th>DIRECCIÓN</th>
                        <th>TELÉFONO</th>
                        <th>E-MAIL</th>
                        <th>SITIO WEB</th>
                        <th>CONTACTO</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayData as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;
					?>
                    <tr>
                    <td><?php echo $contItem;?></td>
                    <td><?php echo $obj['NUM_RUC']; ?></td>
                    <td><?php echo utf8_encode($obj['NOM_PROVEEDOR']); ?></td>
                    <td><?php echo utf8_encode($obj['NOM_PAIS']);?></td>
                    <td><?php echo utf8_encode($obj['DES_DIRECCION']);?></td>
                    <td><?php echo $obj['NUM_TELEFONO'];?></td>
                    <td><?php echo $obj['DES_MAIL'];?></td>
                    <td><?php echo utf8_encode($obj['DES_WEB']);?></td>
                    <td><?php echo utf8_encode($obj['DES_CONTACTO']);?></td>
                    <td class="center" align="center">
                    <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Editar" 
                        onclick="javascript:OpenForm('edicion','<?php echo $obj['ID_PROVEEDOR'] ?>');">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Activo - Click para inactivar" 
                        onclick="javascript:Inactivar('<?php echo $obj['ID_PROVEEDOR'] ?>');">
                        <i class="fa fa-thumbs-up"></i>
                    </a>
                    <?php
                    }
                    else{
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Inactivo - Click para activar" 
                        onclick="javascript:Activar('<?php echo $obj['ID_PROVEEDOR'] ?>');">
                        <i class="fa fa-thumbs-down"></i>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  