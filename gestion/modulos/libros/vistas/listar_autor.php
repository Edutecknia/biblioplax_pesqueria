<?php if(count($arrayAutor) > 0){  ?>               
               <table id="example1" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th class="center">NOMBRE</th>
                        <th class="center">SEUDONIMO</th>
                        <th class="center">SELEC.</th>
                      </tr>
                    </thead>
                    <tbody >
                    <?php
                    $contador = 0;

                    foreach ($arrayAutor as $obj) {
                    ?>
                    <tr>

                    <td><?php echo utf8_encode($obj['DATOS']);?></td>
                    <td><?php echo utf8_encode($obj['DES_SEUDONIMO']);?></td>

                    <td class="center" align="center">
                    <a class="cursor-point"  
                        title="Seleccionar" 
                        onclick="javascript:seleccionarAutor('<?php echo utf8_encode($obj['DATOS']) ?>','<?php echo $obj['ID_AUTOR'] ?>');"
                        >
                        <i class="fa fa-check-square-o"></i>
                    </a> 
                    </td>
                    
                


                    </tr>
                    <?php
                       }
                    ?>
                    </tbody>
                </table>
<?php 
}
else{
        echo 'No se encontraron resultados...';
}
 ?>                
