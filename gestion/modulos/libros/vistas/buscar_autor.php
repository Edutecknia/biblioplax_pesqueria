<script>
$(document).ready(function(){
        
        $("#btnBuscar").click(function() {
            
            buscarAutor();

        });
});
</script>

<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Buscar Autor</h4>
        </div>

<form id="frmAutor">
 <div class="modal-body">
<div class="container col-sm-12">


                <div class="form-group col-sm-10">
                <label>Nombre:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomAutor" name="nomAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre o seudónimo del autor" 
                                    type="text" 
                                    maxlength="100">
                                </div>
                </div>

              
                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-primary" tooltip="Buscar" name="btnBuscar" id="btnBuscar"><i class="fa fa-search"> </i>   Buscar</a>
                </div>

                <div class="col-md-12 table-responsive" id="ContenedorListadoAutor" style="height: 400px !important; overflow-y:auto; ">
                </div>

</div>

                        <p>&nbsp;</p>
                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cerrar</button>
                        </div>

 </div>
</form>					

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->



                   