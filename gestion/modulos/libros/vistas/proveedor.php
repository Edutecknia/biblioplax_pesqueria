<script>
$(document).ready(function(){
        
        $("#btnregistrarProveedor").click(function() {
            
            registrarProveedor();

        });
});
</script>

<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Nuevo Proveedor</h4>
        </div>


<form id="frmProveedor">
 <div class="modal-body">
<div class="container col-sm-12">


                <div class="form-group col-sm-6">
                <label>RUC:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="rucProveedor" name="rucProveedor" 
                                    class="form-control" 
                                    placeholder="RUC del Proveedor" 
                                    type="text" 
                                    maxlength="11" 
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-6">
                <label>Nombre:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomProveedor" name="nomProveedor" 
                                    class="form-control" 
                                    placeholder="Nombre del Proveedor" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                </div>

                
                <div class="form-group col-sm-6">
                <label>Pais:</label>
                               <select class="form-control" name="idPaisProveedor" id="idPaisProveedor">
                               <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>


                <div class="form-group col-sm-6">
                <label>Dirección:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="direcProveedor" name="direcProveedor" 
                                    class="form-control" 
                                    placeholder="Dirección del Proveedor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Teléfono:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="fonoProveedor" name="fonoProveedor" 
                                    class="form-control" 
                                    placeholder="Teléfono del Proveedor" 
                                    type="text" 
                                    maxlength="20" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>E-mail:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="mailProveedor" name="mailProveedor" 
                                    class="form-control" 
                                    placeholder="E-Mail del Proveedor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Sitio Web:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="webProveedor" name="webProveedor" 
                                    class="form-control" 
                                    placeholder="Sitio Web del Proveedor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-6">
                <label>Contacto:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="contactoProveedor" name="contactoProveedor" 
                                    class="form-control" 
                                    placeholder="Nombre del Contacto" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="button" id="btnregistrarProveedor" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


                   