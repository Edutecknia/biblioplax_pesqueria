<script>
$(document).ready(function(){
 
 $(".select2").select2();
  
 $('#dtFechaAdq').datetimepicker(
     {
         format: 'DD/MM/YYYY'
     }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
    });     
    
    //Timepicker
    $(".timepicker").timepicker({
        showInputs: false
    });

});
</script>


 <div class="modal-body">
<div class="container col-sm-12">

                <div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                    <select class="form-control" 
                            name="idFacultad" 
                            id="idFacultad">
                        <?php foreach ($arrayFacultad as $obj): ?>
                        <option value="<?php echo $obj['ID_CARRERA']; ?>"
                        <?php if($obj['ID_CARRERA'] == $obj_Data['ID_CARRERA']){echo 'selected';} ?>
                        ><?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-sm-3">
                <label>Tipo:</label>
                        <select class="form-control select2" 
                                name="idLibroTipo" 
                                id="idLibroTipo"
                                style="width: 100%;">
                            <?php foreach ($arrayLibroTipo as $obj): ?>
                            <option value="<?php echo $obj['ID_LIBRO_TIPO']; ?>"
                            <?php if($obj['ID_LIBRO_TIPO'] == $obj_Data['ID_LIBRO_TIPO']){echo 'selected';}?>
                            ><?php echo utf8_encode($obj['DES_LIBRO_TIPO']); ?></option>
                            <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Título:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="tituloLibro" name="tituloLibro" 
                                    class="form-control" 
                                    placeholder="Título del Libro" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo utf8_encode($obj_Data['DES_TITULO']); ?>" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Autor:</label>

                 <div style="display:block" id="divTablaAutor">
                <table id="autores" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>NOMBRE</th>
                      </tr>
                    </thead>
                    <tbody id="detalleAutor">
                    <?php foreach ($arrayDatos as $obj): ?>
                    <tr>
                    <td><input type="hidden" id="<?php echo $obj['ID_AUTOR']; ?>" class="detallesAutor" ><?php echo utf8_encode($obj['NOM_AUTOR']); ?></td>
                    </tr>
                    <?php endforeach; ?>    
                    </tbody>  
                    </table>
                </div>


                                <!--<select class="form-control select2" 
                                        name="idAutor" 
                                        id="idAutor"
                                        multiple="multiple" data-placeholder="Seleccione Autor" 
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php //foreach ($arrayAutor as $obj): ?>
                                <option value="<?php //echo $obj['ID_AUTOR']; ?>"
                                <?php //if(@in_array($obj['ID_AUTOR'],$arrayAutorLib)){ echo 'selected';} ?>
                                ><?php //echo utf8_encode($obj['DATOS']); ?></option>
                                <?php// endforeach; ?>
                                </select>-->
                </div>

                <div class="form-group col-sm-6">
                <label>Editorial:</label>
                                <select class="form-control select2" 
                                        name="idEditorial" 
                                        id="idEditorial"
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php foreach ($arrayEditorial as $obj): ?>
                                <option value="<?php echo $obj['ID_EDITORIAL']; ?>"
                                <?php if($obj['ID_EDITORIAL'] == $obj_Data['ID_EDITORIAL']){echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['NOM_EDITORIAL']); ?></option>
                                <?php endforeach; ?>
                                </select>    
                </div>

                <div class="form-group col-sm-12">
                </div>

                <div class="form-group col-sm-3">
                <label>Año Publicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="anioLibro" name="anioLibro" 
                                    class="form-control" 
                                    placeholder="Año de Publicación" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    value="<?php echo $obj_Data['NUM_ANIO_PUBLICA']; ?>"
                                    >
                                </div>
                </div>
                

                <div class="form-group col-sm-3">
                <label>Pais Publicación:</label>
                                <select class="form-control select2" 
                                        name="idPais" 
                                        id="idPais"
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"
                                <?php if($obj['ID_PAIS'] == $obj_Data['ID_PAIS']){echo 'selected';}?>
                                ><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-3">
                <label>Volumen:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="volLibro" name="volLibro" 
                                    class="form-control" 
                                    placeholder="Volumen del Libro" 
                                    type="text" 
                                    maxlength="5" 
                                    value="<?php echo $obj_Data['NUM_VOLUMEN']; ?>"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-3">
                <label>Nro. Páginas:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="paginaLibro" name="paginaLibro" 
                                    class="form-control" 
                                    placeholder="Nro. de Páginas del Libro" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    value="<?php echo $obj_Data['NUM_PAGINA']; ?>"
                                    >
                                </div>
                </div>


                 <div class="form-group col-sm-3">
                <label>Adquisición:</label>
                               <select class="form-control select2" 
                                        name="idAdquisicion" 
                                        id="idAdquisicion"
                                        style="width: 100%;"
                                        >
                                <?php foreach ($arrayLibroAdq as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_LIBRO_ADQUISICION']; ?>"
                                <?php if($obj['ID_LIBRO_ADQUISICION'] == $obj_Data['ID_LIBRO_ADQUISICION']){echo 'selected';}?>
                                ><?php echo utf8_encode($obj['DES_LIBRO_ADQUISICION']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-3">
                <label>Proveedor:</label>
                                <select class="form-control select2" 
                                        name="idProveedor" 
                                        id="idProveedor"
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php foreach ($arrayProveedor as $obj): ?>
                                <option value="<?php echo $obj['ID_PROVEEDOR']; ?>"
                                <?php if($obj['ID_PROVEEDOR'] == $obj_Data['ID_PROVEEDOR']){echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['NOM_PROVEEDOR']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>


                <div class="form-group col-sm-3">
                <?php
                $fecha   = strtotime( $obj_Data['FEC_ADQUISICION'] ); 
                $fecha   = date('d/m/Y', $fecha);
                ?>
                <label>Fecha Adquisición:</label>
                        <div class='input-group date' id='dtFechaAdq'>
                        <input type='text' 
                                class="form-control" 
                                id="fechaAdquisicion"
                                value="<?php echo $fecha; ?>" 
                        />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                </div>



                <div class="form-group col-sm-3">
                <label>Precio:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="precioLibro" name="precioLibro" 
                                    class="form-control" 
                                    placeholder="Precio del Libro" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    step="0.01"
                                    value="<?php echo $obj_Data['PRE_LIBRO']; ?>"
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-12">
                <label>Temas Relacionados:</label>
                               <select class="form-control select2" 
                                        name="idTemas" 
                                        id="idTemas"  
                                        multiple="multiple" data-placeholder="Seleccione un tema" 
                                        style="width: 100%;"
                                >
                                <?php foreach ($arrayTemas as $obj): ?>
                                <option value="<?php echo $obj['ID_CATEGORIA']; ?>"
                                <?php if(@in_array($obj['ID_CATEGORIA'],$arrayTema)){ echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['NOM_CATEGORIA']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">EJEMPLARES: <?php echo count($arrayEjemplar); ?></h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="col-md-12 table-responsive" style="display:block" id="divTabla">
                    <table id="ejemplares" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ISBN</th>
                        <th>COD. LIBRO</th>
                        <th>COD. BARRAS</th>
                        <th>COD. PATRIMONIO</th>
                        <th class="center">ESTADO</th>
                      </tr>
                    </thead>
                    <tbody id="detalle">
                    <?php foreach ($arrayEjemplar as $obj): ?>
                    <tr>
                    <td><?php echo $obj['COD_ISBM']; ?></td>
                    <td><?php echo $obj['COD_LIBRO']; ?></td>
                    <td><?php echo $obj['COD_BARRA']; ?></td>
                    <td><?php echo $obj['COD_PATRIMONIAL']; ?></td>
                    <td align="center">
                    <?php 
                    switch ($obj['DES_ESTADO']) {
                        case 'DISPONIBLE':
                            echo "<span class='label label-success'>".$obj['DES_ESTADO']."</span>";
                            break;
                        case 'PRESTADO':
                            echo "<span class='label label-warning'>".$obj['DES_ESTADO']."</span>";
                            break;
                         default:
                            echo "<span class='label label-danger'>".$obj['DES_ESTADO']."</span>";
                            break;
                    }
                    ?>
                    </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>  
                    </table>
                </div>

</div>


                      <p>&nbsp;</p>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
                        </div>

 </div>		