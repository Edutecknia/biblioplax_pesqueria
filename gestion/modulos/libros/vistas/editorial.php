<script>
$(document).ready(function(){
        
        $("#btnregistrarEditorial").click(function() {
            
            registrarEditorial();

        });
});
</script>

<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Nueva Editorial</h4>
        </div>

<form id="frmEditorial">
 <div class="modal-body">
<div class="container col-sm-12">



                <div class="form-group col-sm-12">
                <label>Nombre:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomEditorial" name="nomEditorial" 
                                    class="form-control" 
                                    placeholder="Nombre de la Editorial" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                </div>

                
                <div class="form-group col-sm-6">
                <label>Pais:</label>
                               <select class="form-control" name="idPaisEditorial" id="idPaisEditorial">
                               <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>


                <div class="form-group col-sm-6">
                <label>Dirección:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="direcEditorial" name="direcEditorial" 
                                    class="form-control" 
                                    placeholder="Dirección de la Editorial" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-4">
                <label>Teléfono:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="fonoEditorial" name="fonoEditorial" 
                                    class="form-control" 
                                    placeholder="Teléfono de la Editorial" 
                                    type="text" 
                                    maxlength="20" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-4">
                <label>E-mail:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="mailEditorial" name="mailEditorial" 
                                    class="form-control" 
                                    placeholder="E-Mail de la Editorial" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-4">
                <label>Sitio Web:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="webEditorial" name="webEditorial" 
                                    class="form-control" 
                                    placeholder="Sitio Web de la Editorial" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="button" id="btnregistrarEditorial" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					


</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


                   