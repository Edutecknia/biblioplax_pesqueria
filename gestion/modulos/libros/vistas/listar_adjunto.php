<?php
if(count($arrayAdjunto) > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>DESCRIPCIÓN</th>
                        <th>TIPO DE ARCHIVO</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
				
                    foreach ($arrayAdjunto as $obj):
					$contItem++;
					?>
                    <tr>
                    <td><?php echo $contItem;?></td>
                    <td><?php echo utf8_encode($obj['DES_ARCHIVO']); ?></td>
                    <td><?php echo $obj['NOM_ARCHIVO_TIPO']; ?></td>
                    <td class="center" align="center">
                    
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Descargar" 
                        href="../archivos/<?php echo $_SESSION['BTK_CARPETA_ID_FACULTAD'] ?>/<?php echo $obj['NOM_ARCHIVO']; ?>" 
                        download="<?php echo $obj['DES_ARCHIVO']; ?>"
                    >
                        <i class="fa fa-cloud-download"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Eliminar" 
                        onclick="javascript:EliminarAdjunto('<?php echo $obj['ID_LIBRO_ARCHIVO'] ?>');">
                        <i class="fa fa-trash"></i>
                    </a>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
?>                  
</div>                  