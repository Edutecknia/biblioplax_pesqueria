<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>

<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
<?php 
/*
  foreach ($arrayLibro as $obj):
        echo $obj->ID_LIBRO;
    
        foreach ($obj->DETALLES as $detalle):
            echo $detalle->DES_ESTADO.'>>'.$detalle->DES_TITULO.'<br>';
        endforeach;

    endforeach;
*/
?>

                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th class="center">OPCIONES</th>
                        <th>BIBLIOTECA</th>
                        <th>TIPO</th>
                        <th><a style="color:#333;cursor:pointer" 
                        onclick="javascript:ordenarLista('C.DES_TITULO','<?php if ($direccion == 'DESC') echo "ASC"; else echo "DESC";?>');">TITULO
                        <i class="fa fa-sort<?php if($orden == 'C.DES_TITULO'){ echo '-'.strtolower($direccion);} ?>"></i></a></th>
                        <th>EDITORIAL</th>
                        <th>FECHA ADQ.</th>
                        <th><a style="color:#333;cursor:pointer" 
                        onclick="javascript:ordenarLista('LE.COD_BARRA','<?php if ($direccion == 'DESC') echo "ASC"; else echo "DESC";?>');">COD.BARRAS
                        <i class="fa fa-sort<?php if($orden == 'LE.COD_BARRA'){ echo '-'.strtolower($direccion);} ?>"></i></a></th>
                        <th>COD. PATRIMONIAL</th>
                        <th>ESTADO</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;

                    foreach ($arrayLibro as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;

                    $trClass            = (($contItem % 2) ? 'background-color:#f9f9f9 !important' : 'background-color:#fff !important');

                    $fecha   = strtotime( $obj->FEC_ADQUISICION ); 
                    $fecha   = date('d/m/Y', $fecha); 

                    $rowsPan = count($obj->DETALLES) > 1 ? count($obj->DETALLES) + 1 : 1;
					?>
                    <tr style="<?php echo $trClass; ?>">
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo $contItem;?></td>
                    <td rowspan="<?php echo $rowsPan ?>" class="center" align="center" style="vertical-align:middle;">
                    <?php
                    if($obj->IND_ACTIVO == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm('visualizar','<?php echo $obj->ID_LIBRO ?>');">
                        <i class="fa fa-eye"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Editar" 
                        onclick="javascript:OpenForm('edicion','<?php echo $obj->ID_LIBRO ?>');">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Documentos Adjuntos" 
                        onclick="javascript:OpenForm('adjuntos','<?php echo $obj->ID_LIBRO ?>');">
                        <i class="fa fa-file-text-o"></i>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Activo - Click para inactivar" 
                        onclick="javascript:Inactivar('<?php echo $obj->ID_LIBRO ?>');">
                        <i class="fa fa-thumbs-up"></i>
                    </a>
                    <?php
                    }
                    else{
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Inactivo - Click para activar" 
                        onclick="javascript:Activar('<?php echo $obj->ID_LIBRO ?>');">
                        <i class="fa fa-thumbs-down"></i>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo utf8_encode($obj->NOM_CARRERA); ?></td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo utf8_encode($obj->DES_LIBRO_TIPO); ?></td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo utf8_encode($obj->DES_TITULO);?></td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo utf8_encode($obj->NOM_EDITORIAL);?></td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo $fecha;?></td>
                    
                    <?php 
                    if(count($obj->DETALLES) > 1){
                    $arrayDetalle = array();
                    $arrayDetalle   = $obj->DETALLES;
                    rsort($arrayDetalle);

                    foreach ($arrayDetalle as $detalle): 
                    ?>
                    <tr style="<?php echo $trClass; ?>">
                    <?php //} ?>
                    <td><?php echo $detalle->COD_BARRA;?></td>
                    <td><?php echo $detalle->COD_PATRIMONIAL;?></td>
                    <td>
                    <?php 
                    switch ($detalle->DES_ESTADO) {
                        case 'DISPONIBLE':
                            echo "<span class='label label-success'>".$detalle->DES_ESTADO."</span>";
                            break;
                        case 'PRESTADO':
                            echo "<span class='label label-warning'>".$detalle->DES_ESTADO."</span>";
                            break;
                         default:
                            echo "<span class='label label-danger'>".$detalle->DES_ESTADO."</span>";
                            break;
                    }
                    ?>
                    </td>
                    </tr>
                    
                    <?php 
                    endforeach; 
                    }
                    else{
                    foreach ($obj->DETALLES as $detalle): 
                    ?>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo $detalle->COD_BARRA;?></td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;"><?php echo $detalle->COD_PATRIMONIAL;?></td>
                    <td rowspan="<?php echo $rowsPan ?>" style="vertical-align:middle;">
                    <?php 
                    switch ($detalle->DES_ESTADO) {
                        case 'DISPONIBLE':
                            echo "<span class='label label-success'>".$detalle->DES_ESTADO."</span>";
                            break;
                        case 'PRESTADO':
                            echo "<span class='label label-warning'>".$detalle->DES_ESTADO."</span>";
                            break;
                         default:
                            echo "<span class='label label-danger'>".$detalle->DES_ESTADO."</span>";
                            break;
                    }
                    ?>
                    </td>
                    <?php
                    endforeach;
                    }
                    ?>
                    

                    
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  