<script>
$(document).ready(function(){
        
        $("#btnregistrarAutor").click(function() {
            
            registrarAutor();

        });
});
</script>

<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Nuevo Autor</h4>
        </div>

<form id="frmAutor">
 <div class="modal-body">
<div class="container col-sm-12">

                <div class="form-group col-sm-6">
                <label>Autor:</label>
                               <select class="form-control" name="idAutorTipo" id="idAutorTipo">
                                <?php foreach ($arrayAutorTipo as $obj): ?>
                                <option value="<?php echo $obj['ID_AUTOR_TIPO']; ?>"><?php echo utf8_encode($obj['DES_TIPO']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>


                <div class="form-group col-sm-12">
                <label>Nombre Completo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomAutor" name="nomAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre completo del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                </div>

                <!--<div class="form-group col-sm-6">
                <label>Apellidos:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="apeAutor" name="apeAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese apellidos del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>-->

                <div class="form-group col-sm-6">
                <label>Seudónimo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="seudoAutor" name="seudoAutor" 
                                    class="form-control" 
                                    placeholder="Ingrese seudónimo del autor" 
                                    type="text" 
                                    maxlength="100" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Pais:</label>
                               <select class="form-control" name="idPaisAutor" id="idPaisAutor">
                               <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): 
                                /*$nomPais = $obj['NOM_PAIS'];
                                $encod = mb_detect_encoding($obj['NOM_PAIS'], 'UTF-8, ISO-8859-1');
                                if($encod=='ISO-8859-1'){
                                    $nomPais = utf8_encode($obj['NOM_PAIS']);
                                }*/
                                ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="button" id="btnregistrarAutor" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->



                   