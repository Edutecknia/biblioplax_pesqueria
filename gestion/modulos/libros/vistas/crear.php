<script type="text/javascript" src="js/jquery.numeric.js"></script>

<script>
$(document).ready(function(){
 
 $(".select2").select2();

 $('#dtFechaAdq').datetimepicker(
     {
         format: 'DD/MM/YYYY',
         defaultDate: new Date()
     }
 ).on('changeDate', function(e){
     $(this).datepicker('hide');
    });     
    
    //Timepicker
    $(".timepicker").timepicker({
        showInputs: false
    });

    $('#codLibro').numeric();
    /*var i = 0;
    var texto = '';
    $("#codLibro").keypress(function(){

        texto = $("#codLibro").val();
        var h=("00000" + texto).slice (-6);
        $("#barraLibro").val(h);
    });*/

});
</script>

<div class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
</div>

<form action="javascript:validarMaterial();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
                <div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>">
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

                <div class="form-group col-sm-3">
                <label>Tipo:</label>
                <select class="form-control select2" name="idLibroTipo" id="idLibroTipo" style="width: 100%;">
                <?php foreach ($arrayLibroTipo as $obj): ?>
                <option value="<?php echo $obj['ID_LIBRO_TIPO']; ?>"
                <?php if($obj['ID_LIBRO_TIPO'] == 6 /*LIBRO*/){echo 'selected';} ?>><?php echo utf8_encode($obj['DES_LIBRO_TIPO']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

                <div class="form-group col-sm-6">
                <label>Título:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="tituloLibro" name="tituloLibro" 
                                    class="form-control" 
                                    placeholder="Título del Libro" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-4">
                <label>Autor:</label>
                
                <div style="display:none" id="divTablaAutor">
                <table id="autores" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>NOMBRE</th>
                        <th class="center">ELIMINAR</th>
                      </tr>
                    </thead>
                    <tbody id="detalleAutor">
                    </tbody>  
                    </table>
                </div>

                                <!--<select class="form-control select2" 
                                        name="idAutor" 
                                        id="idAutor"
                                        style="width: 100%;"
                                        multiple="multiple"
                                >

                                </select>-->

                </div>

                <div class="form-group col-sm-1">
                <label>&nbsp;</label>
                <div class="input-group">
                <button type="button" id="btnbuscarAutor" 
                        class="btn btn-warning pull-right" onclick="javascript:OpenFormModal('buscarautor');" title="Buscar Autor"><i class="fa fa-search"></i></button>
                </div>
                </div>

                <div class="form-group col-sm-1">
                <label>&nbsp;</label>
                <div class="input-group">
                <button type="button" id="btnagregarAutor" 
                        class="btn btn-warning pull-left" onclick="javascript:OpenFormModal('autor');" title="Agregar Autor"><i class="fa fa-plus"></i></button>
                </div>
                </div>


                <div class="form-group col-sm-5">
                <label>Editorial:</label>
                                <select class="form-control select2" 
                                        name="idEditorial" 
                                        id="idEditorial"
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php foreach ($arrayEditorial as $obj): ?>
                                <option value="<?php echo $obj['ID_EDITORIAL']; ?>"><?php echo utf8_encode($obj['NOM_EDITORIAL']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-1">
                <label>&nbsp;</label>
                <div class="input-group">
                <button type="button" id="btnagregarEditorial" 
                        class="btn btn-warning pull-left" onclick="javascript:OpenFormModal('editorial');" title="Agregar Editorial"><i class="fa fa-plus"></i></button>
                </div>
                </div>

                <div class="form-group col-sm-12">
                </div>

                <div class="form-group col-sm-3">
                <label>Año Publicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="anioLibro" name="anioLibro" 
                                    class="form-control" 
                                    placeholder="Año de Publicación" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    >
                                </div>
                </div>
                
                <div class="form-group col-sm-3">
                <label>Pais Publicación:</label>
                                <select class="form-control select2" 
                                        name="idPais" 
                                        id="idPais"
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php foreach ($arrayPais as $obj): ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>


                

                <div class="form-group col-sm-3">
                <label>Volumen:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="volLibro" name="volLibro" 
                                    class="form-control" 
                                    placeholder="Volumen del Libro" 
                                    type="text" 
                                    maxlength="5" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-3">
                <label>Nro. Páginas:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="paginaLibro" name="paginaLibro" 
                                    class="form-control" 
                                    placeholder="Nro. de Páginas del Libro" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    >
                                </div>
                </div>


                 <div class="form-group col-sm-2">
                <label>Adquisición:</label>
                               <select class="form-control select2" name="idAdquisicion" id="idAdquisicion" style="width: 100%;">
                                <?php foreach ($arrayLibroAdq as $obj): 
                                ?><option value="<?php echo $obj['ID_LIBRO_ADQUISICION']; ?>" <?php if($obj['ID_LIBRO_ADQUISICION']==2){echo 'selected';} ?> ><?php echo utf8_encode($obj['DES_LIBRO_ADQUISICION']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-3">
                <label>Proveedor:</label>
                                <select class="form-control select2" 
                                        name="idProveedor" 
                                        id="idProveedor"
                                        style="width: 100%;"
                                >
                                <option value="">SELECCIONE</option>
                                <?php foreach ($arrayProveedor as $obj): ?>
                                <option value="<?php echo $obj['ID_PROVEEDOR']; ?>"<?php if($obj['ID_PROVEEDOR']==50){echo 'selected';} ?> ><?php echo utf8_encode($obj['NOM_PROVEEDOR']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-1">
                <label>&nbsp;</label>
                <div class="input-group">
                <button type="button" id="btnagregarProveedor" 
                        class="btn btn-warning pull-left" onclick="javascript:OpenFormModal('proveedor');" title="Agregar Proveedor"><i class="fa fa-plus"></i></button>
                </div>
                </div>


                <div class="form-group col-sm-3">
                <label>Fecha Adquisición:</label>
                        <div class='input-group date' id='dtFechaAdq'>
                        <input type='text' 
                                class="form-control" 
                                id="fechaAdquisicion"
                                <?php if($idUsuario=='1001'){ ?> value="14/08/2014" <?php } ?>
                        />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                </div>



                <div class="form-group col-sm-3">
                <label>Precio:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="precioLibro" name="precioLibro" 
                                    class="form-control" 
                                    placeholder="Precio del Libro" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    step="0.01"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-11">
                <label>Temas Relacionados:</label>
                               <select class="form-control select2" 
                                        name="idTemas" 
                                        id="idTemas"  
                                        multiple="multiple" data-placeholder="Seleccione un tema" 
                                        style="width: 100%;"
                                >
                                <?php foreach ($arrayTemas as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_CATEGORIA']; ?>"><?php echo utf8_encode($obj['NOM_CATEGORIA']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-1">
                <label>&nbsp;</label>
                <div class="input-group">
                <button type="button" id="btnagregarTema" 
                        class="btn btn-warning pull-left" onclick="javascript:OpenFormModal('tema');" title="Agregar Tema"><i class="fa fa-plus"></i></button>
                </div>
                </div>


                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">EJEMPLARES: 0</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>
               

                <div class="form-group col-sm-3">
                <label>ISBN:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="isbnLibro" name="isbnLibro" 
                                    class="form-control" 
                                    placeholder="Código ISBN" 
                                    type="text" 
                                    maxlength="20" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-2">
                <label>Cod. Libro:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="codLibro" name="codLibro" 
                                    class="form-control" 
                                    placeholder="Código Interno" 
                                    type="number" 
                                    min="1"
                                    max="99999"
                                    maxlength="6" 
                                    >
                                    <!--onkeydown="myFunction(event)"-->
                                </div>
                </div>

                <!--<div class="form-group col-sm-2">
                <label>Cod. Barras:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>-->
                                    <input id="barraLibro" name="barraLibro" 
                                    class="form-control" 
                                    placeholder="Código de Barras" 
                                    type="hidden" 
                                    maxlength="20" 
                                    readonly="readonly" 
                                    >
                                <!--</div>
                </div>-->

                <div class="form-group col-sm-3">
                <label>Cod. Patrimonio:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="codPatrimonio" name="codPatrimonio" 
                                    class="form-control" 
                                    placeholder="Código de Patrimonio" 
                                    type="text" 
                                    maxlength="20" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-2">
                <label>&nbsp;</label>
                <div class="input-group">
                <button type="button" id="btnagregar" class="btn btn-warning pull-left" onclick="javascript:agregarEjemplar();"><i class="fa fa-plus"></i> Agregar</button>
                </div>
                </div>


                <div class="col-md-12 table-responsive" style="display:none" id="divTabla">
                    <table id="ejemplares" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ISBN</th>
                        <th>COD. LIBRO</th>
                        <!--<th>COD. BARRAS</th>-->
                        <th>COD. PATRIMONIO</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody id="detalle">
                    </tbody>  
                    </table>
                </div>

</div>


                      <p>&nbsp;</p>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					                   