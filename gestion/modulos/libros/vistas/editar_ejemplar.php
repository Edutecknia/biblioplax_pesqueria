<script>
$(document).ready(function(){
        
        $('#codLibroEdit').numeric();

        $("#btnregistrarEjemplarEdit").click(function() {
            
            editarEjemplar();

        });
});
</script>

<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Editar Ejemplar</h4>
        </div>


<form id="frmProveedor">
 <div class="modal-body">
<div class="container col-sm-12">

                <input type="hidden" name="txtCodLibro" id="txtCodLibro" value="<?php echo $codLibro; ?>">
                <input type="hidden" name="txtidLibro" id="txtidLibro" value="<?php echo $idLibro; ?>">

                <div class="form-group col-sm-4">
                <label>ISBN:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="isbnLibroEdit" name="isbnLibroEdit" 
                                    class="form-control" 
                                    placeholder="Código ISBN" 
                                    type="text" 
                                    maxlength="20"  
                                    value="<?php echo $obj_Data['COD_ISBM']; ?>" 
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-4">
                <label>COD.LIBRO:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="codLibroEdit" name="codLibroEdit" 
                                    class="form-control" 
                                    placeholder="Código Interno" 
                                    type="number" 
                                    min="1"
                                    max="99999"
                                    maxlength="6"
                                    value="<?php echo $obj_Data['COD_LIBRO']; ?>"
                                    >
                                </div>
                </div>

                
                <div class="form-group col-sm-4">
                <label>COD.PATRIMONIO:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="codPatrimonioEdit" name="codPatrimonioEdit" 
                                    class="form-control" 
                                    placeholder="Código de Patrimonio" 
                                    type="text" 
                                    maxlength="20" 
                                    value="<?php echo $obj_Data['COD_PATRIMONIAL']; ?>"
                                    >
                                </div>
                </div>


                


</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="button" id="btnregistrarEjemplarEdit" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


                   