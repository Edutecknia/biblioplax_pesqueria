<script>
$(document).ready(function(){
        
        $("#btnregistrarTema").click(function() {
            
            registrarTema();

        });
});
</script>

<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Nuevo Tema</h4>
        </div>

<form  id="frmTema">
 <div class="modal-body">
<div class="container col-sm-12">



  <div class="form-group col-sm-12">
                <label>Descripción del Tema:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreCategoria" name="nombreCategoria" 
                                    class="form-control" 
                                    placeholder="Ingrese descripción del tema" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required">
                                </div>
                        </div>
</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="button" id="btnregistrarTema" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


                   