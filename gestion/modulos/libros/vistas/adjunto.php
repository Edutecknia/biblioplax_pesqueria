<script>
$(document).ready(function(){
 $(".select2").select2();

 listarAdjuntos();

});
</script>

<link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="js/jquery.filer.js?v=1.0.5"></script>
<script type="text/javascript" src="js/customLibro.js?v=1.0.5"></script>


<form action="javascript:registrarAdjunto();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

                <input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>


                <div class="form-group col-sm-12">
                <label>Título:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="tituloLibro" name="tituloLibro" 
                                    class="form-control" 
                                    placeholder="Título del Libro" 
                                    type="text" 
                                    maxlength="100" 
                                    value="<?php echo utf8_encode($obj_Data['DES_TITULO']); ?>" 
                                    readonly
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-6">
                <label>Archivo:</label>
                        <select class="form-control select2" 
                                name="idLibroTipo" 
                                id="idLibroTipo" 
                                style="width: 100%;"
                        >
                        <?php foreach ($arrayArchivoTipo as $obj): ?>
                        <option value="<?php echo $obj['ID_ARCHIVO_TIPO']; ?>"><?php echo utf8_encode($obj['NOM_ARCHIVO_TIPO']); ?></option>
                        <?php endforeach; ?>
                        </select>
                </div>


                <div class="form-group col-sm-6">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="desAdjunto" name="desAdjunto" 
                                    class="form-control" 
                                    placeholder="Descripción, Nombre del archivo" 
                                    type="text" 
                                    maxlength="200" 
                                    required
                                    >
                                </div>
                </div>

                <div class="tab-content col-sm-12" style="background-color:#FFF;">
                        <div class="tab-pane fade active in" id="subirfoto">
    
                            <div class="form-group col-sm-12">
                            <center>
                            <label>Archivo Adjunto</label>
                            <div class="input-group">
                            <input type="file" name="files[]" id="filer_input2" class="form-control">
                            </div>
                            </center>
                            </div>

                        </div>
                </div>                     


                <div class="form-group col-sm-12">

                <h3 class="text-yellow" id="txtEjemplar">DOCUMENTOS ADJUNTOS REGISTRADOS</h3>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>
               


                <div class="col-md-12 table-responsive" id="divTabla">
                    
                </div>

</div>


                      <p>&nbsp;</p>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>					                   