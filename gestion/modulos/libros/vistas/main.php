<script type="text/javascript" src="js/mLibro.js"></script>

<script>
$(document).ready(function(){
		
		listado();		
        $("#btnBuscar").click(function() {
        listado();
        });

        /* reset paginacion al cambiar texto */
        $('#txtBuscar').change(function() {
        $('#pag_actual').val('1');
        $("#orden").attr("value", 'C.FEC_REGISTRO');
        $("#direccion").attr("value", 'DESC');
        });

        $('#cboEstado').change(function() {
        $('#pag_actual').val('1');
        $("#orden").attr("value", 'C.FEC_REGISTRO');
        $("#direccion").attr("value", 'DESC');
        });

        $('#idFacultad').change(function() {
        $('#pag_actual').val('1');
        $("#orden").attr("value", 'C.FEC_REGISTRO');
        $("#direccion").attr("value", 'DESC');
        });

        $('#txtBuscarCod').change(function() {
        $('#pag_actual').val('1');
        $("#orden").attr("value", 'C.FEC_REGISTRO');
        $("#direccion").attr("value", 'DESC');
        });

        $('#cboEstadoLibro').change(function() {
        $('#pag_actual').val('1');
        $("#orden").attr("value", 'C.FEC_REGISTRO');
        $("#direccion").attr("value", 'DESC');
        });

});
</script>
<div class="content" >
             <div class="row">
                <div class="col-md-12">

				<div class="box box-primary">
                <div class="box-header with-border">
                  <h5 class="box-title" id="titulo">Filtro de búsqueda</h5>
                </div>
                <div class="box-body" id="Contenedorform">
                
                <input type="hidden" id="pag_actual" name="pag_actual" value="<?php echo $pagina ?>"/>
                <input type="hidden" id="orden" name="orden" value="<?php echo $orden ?>"/>
                <input type="hidden" id="direccion" name="direccion" value="<?php echo $direccion ?>"/>
                
                <div class="form-group col-sm-3">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>">
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>


                <div class="col-md-3">
                <label>Titulo</label>
                  <input class="form-control" type="text" placeholder="Titulo del Libro" name="txtBuscar" id="txtBuscar">
                </div>

                <div class="col-md-3">
                <label>Código</label>
                  <input class="form-control" type="text" placeholder="Código del libro" name="txtBuscarCod" id="txtBuscarCod">
                </div>

                <div class="col-md-3">
                <label>Estado</label>
                      <select class="form-control" name="cboEstadoLibro" id="cboEstadoLibro">
                      <option value="">TODOS</option>
                      <?php foreach ($arrayEstado as $obj): ?>
                        <option value="<?php echo $obj['DES_ESTADO']; ?>">
                        <?php echo utf8_encode($obj['DES_ESTADO']); ?></option>
                        <?php endforeach; ?>
                      </select>
                </div>

                <div style="clear:both;"></div>

                <div class="col-md-3">
                <label>Activo</label>
                      <select class="form-control" name="cboEstado" id="cboEstado">
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                      </select>
                </div>

                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Buscar" name="btnBuscar" id="btnBuscar"><i class="fa fa-search"> </i>   Buscar</a>
                </div>

                              
                <div class="form-group col-md-2">
                <label></label>
                <a class="btn btn-block btn-danger" tooltip="Nuevo" onclick="javascript:OpenForm('creacion');"><i class="fa fa-file-o"> </i>   Nuevo</a>
                </div>

                <div class="form-group col-md-2">
                <label></label>
               <!-- <a class="btn btn-block btn-danger" tooltip="Exportar" onclick="javascript:exportarExcel();"><i class="fa fa-file-excel-o"> </i>   Exportar</a>-->
                </div>
                
                <div class="col-md-12 table-responsive" id="ContenedorListado">
                </div>
                
                </div><!-- /.box-body -->

                <!--<div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
                </div>-->

              </div><!-- /.box-primary -->
                  
                </div>
            </div>
            </div>