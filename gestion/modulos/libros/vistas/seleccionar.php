<div class="modal-dialog custom-class">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">AVISO</h4>
        </div>

<?php
if(count($arrayData2) > 0){
?>    
                    <br>
                    <div class="col-sm-12">
                    El material que intenta guardar ya se encuentra registrado.<br> 
                    Si desea agregar los nuevos ejemplares a algún registro existente debe dar clic en en el botón SELECCIONAR<br>
                    Si desea crear como un nuevo registro debe dar clic en el botón REGISTRAR COMO NUEVO
                    </div>
                    <p>&nbsp;</p>

                <table id="example1" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>TITULO</th>
                        <th>EDITORIAL</th>
                        <th>AUTOR</th>
                        <th>AÑO</th>
                        <th>VOLUMEN</th>
                        <th class="center">OPCION</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $idLibro = '';
                    $autor = '';
                    foreach ($arrayData2 as $obj):

                    if($idLibro != $obj['ID_LIBRO']) {

                        $autor      = utf8_encode($obj['NOM_AUTOR']);
                        $idLibro    = $obj['ID_LIBRO'];
                        $autor      = '';

                        foreach ($arrayData2 as $obj2):

                        if($idLibro == $obj2['ID_LIBRO']) {
                            if($autor == ''){ $autor = utf8_encode($obj2['NOM_AUTOR']); }
                            else{ $autor.= ', '.utf8_encode($obj2['NOM_AUTOR']); }
                        }
                        endforeach;

					?>
                    <tr>
                    <td><?php echo utf8_encode($obj['DES_TITULO']);?></td>
                    <td><?php echo utf8_encode($obj['NOM_EDITORIAL']);?></td>
                    <td><?php echo $autor;?></td>
                    <td><?php echo utf8_encode($obj['NUM_ANIO_PUBLICA']);?></td>
                    <td><?php echo utf8_encode($obj['NUM_VOLUMEN']);?></td>
                    <td class="center" align="center">
                    <a onclick="javascript:vincularEjemplar('<?php echo $obj['ID_LIBRO'] ?>');" style="cursor:pointer;"><span class='label label-success'>SELECCIONAR</span></a>
                    </td>
                    </tr>
                    <?php
                     }
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
?>                  

                        <div class="modal-footer clearfix">
                        <button type="submit" id="btnregistrarNuevo" class="btn btn-primary pull-left" onclick="javascript:registrar();"><i class="fa fa-save"></i> REGISTRAR COMO NUEVO</button>
                        </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->