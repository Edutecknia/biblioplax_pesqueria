<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'C.FEC_REGISTRO';
	$direccion = 'DESC';

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	//$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,0);

	$modelData		= new Data_sgalibro();
	$arrayEstado	= $modelData->fu_listarLibroEstado($conexion);

    include $nombreModulo . 'vistas/main.php';
}


function listar($filtro, $indActivo, $orden, $direccion, $pagina, $idFacultad, $codLibro, $desEstado){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	
	$modelData	= new Data_sgalibro();
	$arrayData	= $modelData->fu_listar($conexion,$filtro, $indActivo, $orden, $direccion, $pagina,$idFacultad, $codLibro, $desEstado);

	foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;

    $arrayLibro 	= array();
    $arrayDetalle 	= array();

    foreach ($arrayData as $obj):

    	$arrayDetalle = null;
               
                $arrayDetalle = (object) array (
                    'ID_LIBRO'       		=> $obj['ID_LIBRO'],
                    'NOM_CARRERA'           => $obj['NOM_CARRERA'],
                    'DES_LIBRO_TIPO'        => $obj['DES_LIBRO_TIPO'],
                    'DES_LIBRO_ADQUISICION' => $obj['DES_LIBRO_ADQUISICION'],
                    'DES_TITULO'            => $obj['DES_TITULO'],
                    'NOM_EDITORIAL'         => $obj['NOM_EDITORIAL'],
                    'NUM_ANIO_PUBLICA'      => $obj['NUM_ANIO_PUBLICA'],
                    'NUM_PAGINA'            => $obj['NUM_PAGINA'],
                    'NUM_VOLUMEN'    		=> $obj['NUM_VOLUMEN'],
                    'FEC_ADQUISICION'     	=> $obj['FEC_ADQUISICION'],
                    'IND_ACTIVO'            => $obj['IND_ACTIVO'],
                    'FEC_REGISTRO'          => $obj['FEC_REGISTRO'],
                    'NOM_PAIS'              => $obj['NOM_PAIS'],
                    'COD_ISBM'              => $obj['COD_ISBM'],
                    'COD_LIBRO'             => $obj['COD_LIBRO'],
                    'COD_BARRA'             => $obj['COD_BARRA'],
                    'COD_PATRIMONIAL'       => $obj['COD_PATRIMONIAL'],
                    'DES_ESTADO'            => $obj['DES_ESTADO']
                );

				@$arrayLibro[$obj['ID_LIBRO']]->ID_LIBRO   				= $obj['ID_LIBRO'];
				@$arrayLibro[$obj['ID_LIBRO']]->NOM_CARRERA   			= $obj['NOM_CARRERA'];
				@$arrayLibro[$obj['ID_LIBRO']]->DES_LIBRO_TIPO   		= $obj['DES_LIBRO_TIPO'];
				@$arrayLibro[$obj['ID_LIBRO']]->DES_LIBRO_ADQUISICION   	= $obj['DES_LIBRO_ADQUISICION'];
				@$arrayLibro[$obj['ID_LIBRO']]->DES_TITULO   			= $obj['DES_TITULO'];
				@$arrayLibro[$obj['ID_LIBRO']]->NOM_EDITORIAL   			= $obj['NOM_EDITORIAL'];
				@$arrayLibro[$obj['ID_LIBRO']]->NUM_ANIO_PUBLICA   		= $obj['NUM_ANIO_PUBLICA'];
				@$arrayLibro[$obj['ID_LIBRO']]->NUM_PAGINA   			= $obj['NUM_PAGINA'];
				@$arrayLibro[$obj['ID_LIBRO']]->NUM_VOLUMEN   			= $obj['NUM_VOLUMEN'];
				@$arrayLibro[$obj['ID_LIBRO']]->FEC_ADQUISICION   		= $obj['FEC_ADQUISICION'];
				@$arrayLibro[$obj['ID_LIBRO']]->IND_ACTIVO   			= $obj['IND_ACTIVO'];
				@$arrayLibro[$obj['ID_LIBRO']]->FEC_REGISTRO   			= $obj['FEC_REGISTRO'];
				@$arrayLibro[$obj['ID_LIBRO']]->NOM_PAIS   				= $obj['NOM_PAIS'];
				@$arrayLibro[$obj['ID_LIBRO']]->DETALLES[] = $arrayDetalle;
    endforeach;

    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var filtro 		= $("#txtBuscar").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							var idFacultad	= $("#idFacultad").val();
							var codLibro	= $("#txtBuscarCod").val();
							var desEstado	= $("#cboEstadoLibro").val();
						INI_LOAD();	
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/libros/libros.php?cmd=listar", {
						filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num, idFacultad:idFacultad,codLibro:codLibro,desEstado:desEstado
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
							FIN_LOAD();
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$modelData		= new Data_sgalibro();
	$arrayLibroTipo	= $modelData->fu_listarLibroTipo($conexion);
	$arrayLibroAdq	= $modelData->fu_listarLibroAdquisicion($conexion);

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	//$modelData		= new Data_sgaautor();
	//$arrayAutor		= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaeditorial();
	$arrayEditorial	= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaproveedor();
	$arrayProveedor	= $modelData->fu_listarTodo($conexion);

	include $nombreModulo . 'vistas/crear.php';
}

function registrar($idFacultad, $idLibroTipo, $desTitulo, $idAutor, $idEditorial, $anioPublica, $idPais, 
					$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, 
					$temasRelacion, $detIsbn, $detCod, $detBar, $detPat){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$fecha          = @explode('/',$fecAdquisicion);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecAdquisicion = $anio.'-'.$mes.'-'.$dia;

    $temasRelacion=@implode(',',$temasRelacion);

    $temasRelacion	= '0,'.$temasRelacion;
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_registrar($conexion,$idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, 
							$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, $temasRelacion, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0){
		
		$idLibro = $rpta['ID_LIBRO'];

		$aIsbn = @explode('|', $detIsbn);
		$aCod  = @explode('|', $detCod);
		$aBar  = @explode('|', $detBar);
		$aPat  = @explode('|', $detPat);

		$cadenaMsje = '';

		for($i = 0; $i < count($aCod); $i++){

			$isbn 	= $aIsbn[$i];
			$cod 	= $aCod[$i];
			$bar 	= ''; //$aBar[$i];
			$pat 	= $aPat[$i];

			$rptaDet = $modelData->fu_registrarEjemplar($conexion,$idLibro, $isbn, $cod, $bar, $pat, $idUsuario, $ip); 

			if($rptaDet['IND_OPERACION'] == 0){
				$cadenaMsje	 = $cadenaMsje.'<br>'.$rptaDet['DES_MENSAJE'];
			}

		}

		if($idAutor != ''){
			
			$idAutor = @explode('|', $idAutor);

			for($i = 0; $i < count($idAutor); $i++){
				$autorId 	= $idAutor[$i];
				$rptaAut 	= $modelData->fu_registrarAutor($conexion,$idLibro, $autorId, $idUsuario, $ip); 
			}
		}

		if($cadenaMsje == '' || $cadenaMsje==null){ echo 'passed';}
		else{echo $cadenaMsje;}

	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}

}

function loadVer($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);
	
	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$modelData		= new Data_sgalibro();
	$arrayLibroTipo	= $modelData->fu_listarLibroTipo($conexion);
	$arrayLibroAdq	= $modelData->fu_listarLibroAdquisicion($conexion);

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	//$modelData		= new Data_sgaautor();
	//$arrayAutor		= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaeditorial();
	$arrayEditorial	= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaproveedor();
	$arrayProveedor	= $modelData->fu_listarTodo($conexion);
	
	$obj_Data		= new Data_sgalibro();
	$arrayEjemplar  = $obj_Data->fu_EncontrarEjemplares($conexion,$id);
	$arrayDatos  	= $obj_Data->fu_EncontrarAutores($conexion,$id);
	$obj_Data	 	= $obj_Data->fu_Encontrar($conexion,$id);

	/*$arrayAutorLib 	= array();
	foreach ($arrayDatos as $obj) {
		$arrayAutorLib[] = $obj['ID_AUTOR'];
	}*/

	$arrayTema		= array();
	$arrayTema		= @explode(',', $obj_Data['DES_CATEGORIA_RELACION']);

	$id = $id;
	
	include $nombreModulo . 'vistas/ver.php';

}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);
	
	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$modelData		= new Data_sgalibro();
	$arrayLibroTipo	= $modelData->fu_listarLibroTipo($conexion);
	$arrayLibroAdq	= $modelData->fu_listarLibroAdquisicion($conexion);

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	$modelData		= new Data_sgaautor();
	$arrayAutor		= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaeditorial();
	$arrayEditorial	= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaproveedor();
	$arrayProveedor	= $modelData->fu_listarTodo($conexion);
	
	$obj_Data		= new Data_sgalibro();
	$arrayEjemplar  = $obj_Data->fu_EncontrarEjemplares($conexion,$id);
	$arrayDatos     = $obj_Data->fu_EncontrarAutores($conexion,$id);

	/*$arrayAutorLib 	= array();
	foreach ($arrayDatos as $obj) {
		$arrayAutorLib[] = $obj['ID_AUTOR'];
	}*/

	$obj_Data	 	= $obj_Data->fu_Encontrar($conexion,$id);

	$arrayTema		= array();
	$arrayTema		= @explode(',', $obj_Data['DES_CATEGORIA_RELACION']);

	$id = $id;

	include $nombreModulo . 'vistas/editar.php';

}

function editar($idLibro, $idFacultad, $idLibroTipo, $desTitulo, $idAutor, $idEditorial, $anioPublica, $idPais, 
				$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, 
				$temasRelacion, $detIsbn, $detCod, $detBar, $detPat){

	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$fecha          = @explode('/',$fecAdquisicion);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fecAdquisicion = $anio.'-'.$mes.'-'.$dia;

    $temasRelacion=@implode(',',$temasRelacion);

    $temasRelacion	= '0,'.$temasRelacion;
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_editar($conexion,$idLibro, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, 
							$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, $temasRelacion, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		
		$aIsbn = @explode('|', $detIsbn);
		$aCod  = @explode('|', $detCod);
		$aBar  = @explode('|', $detBar);
		$aPat  = @explode('|', $detPat);

		$cadenaMsje = '';

		if(count($aCod) > 0 && $detCod != ''){
		
		for($i = 0; $i < count($aCod); $i++){

			$isbn 	= $aIsbn[$i];
			$cod 	= $aCod[$i];
			$bar 	= ''; //$aBar[$i];
			$pat 	= $aPat[$i];

			$rptaDet = $modelData->fu_registrarEjemplar($conexion,$idLibro, $isbn, $cod, $bar, $pat, $idUsuario, $ip); 

			if($rptaDet['IND_OPERACION'] == 0){
				$cadenaMsje	 = $cadenaMsje.'<br>'.$rptaDet['DES_MENSAJE'];
			}

		}
		
		}

		if($idAutor != ''){

			$idAutor = @explode('|', $idAutor);

			for($i = 0; $i < count($idAutor); $i++){

				$autorId 	= $idAutor[$i];
				$rptaAut 	= $modelData->fu_registrarAutor($conexion,$idLibro, $autorId, $idUsuario, $ip); 
			}
		}

		if($cadenaMsje == '' || $cadenaMsje != null){ echo 'passed';}
		else{echo $cadenaMsje;}

	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function loadAdjuntos($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData			= new Data_sgalibro();
	$arrayArchivoTipo	= $modelData->fu_listarArchivoTipo($conexion);
	
	$obj_Data			= new Data_sgalibro();
	$obj_Data	 		= $obj_Data->fu_Encontrar($conexion,$id);
	
	$_SESSION['BTK_CARPETA_ID_FACULTAD'] 	= $obj_Data['ID_CARRERA'];
	$id = $id;

	include $nombreModulo . 'vistas/adjunto.php';

}

function listarAdjuntos($idLibro){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData			= new Data_sgalibro();
	$arrayAdjunto		= $modelData->fu_listarAdjunto($conexion,$idLibro);

	include $nombreModulo . 'vistas/listar_adjunto.php';
}

function registrarAdjunto($idLibro, $idArchivoTipo, $desArchivo){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_registrarAdjunto($conexion,$idLibro, $idArchivoTipo, $desArchivo, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){

		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}

function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_inactivar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_activar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}

function eliminarAdjunto($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_eliminarAdjunto($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}

function eliminarEjemplar($idLibro, $codLibro){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgalibro();

	$rpta = $modelData->fu_eliminarEjemplar($conexion, $idLibro, $codLibro, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
}

function generarBarra($texto,$press){

	if(!is_numeric($press)){
		if($press=='Backspace' || $press=='Delete'){  
			
			$texto = substr($texto, 0, strlen($texto)-1 );
			$numero = str_repeat("0", 6 - strlen($texto));
		}
		else{
			$numero = str_repeat("0", 6 - strlen($texto));
		}
	}
	else{
		$texto = $texto.$press;
		$numero = str_repeat("0", 6 - strlen($texto));
	}
	//ArrowLeft ArrowRight ArrowUp ArrowDown Delete
	/*if($press=='Backspace'){  }
	else{
	}*/
	echo $numero.$texto;
}

function loadAutor(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgaautor();
	$arrayAutorTipo	= $modelData->fu_listarAutorTipo($conexion);

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	include $nombreModulo . 'vistas/autor.php';
}

function registrarAutor($nomAutor, $apeAutor, $seudoAutor, $idPais, $idAutorTipo){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaautor();

	$rpta = $modelData->fu_registrar($conexion, $nomAutor, $apeAutor, $seudoAutor, $idPais, $idAutorTipo, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo $rpta['ID_REGISTRO'];
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function ComboAutor(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgaautor();
	$arrayAutor		= $modelData->fu_listarTodo($conexion);

	$combo = '';

    if (count($arrayAutor) > 0) {

		foreach ($arrayAutor as $obj): 
				 $combo.='<option value="' . $obj['ID_AUTOR'] . '">' . utf8_encode($obj['DATOS']). '</option>';
		 endforeach; 
    }
    
    echo $combo;

}

function loadEditorial(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	include $nombreModulo . 'vistas/editorial.php';
}

function registrarEditorial($nomEditorial, $idPais, $direcEditorial, $fonoEditorial, $mailEditorial, $webEditorial){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaeditorial();

	$rpta = $modelData->fu_registrar($conexion, $nomEditorial, $idPais, $direcEditorial, $fonoEditorial, $mailEditorial, $webEditorial, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo $rpta['ID_REGISTRO'];
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function ComboEditorial(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgaeditorial();
	$arrayEditorial	= $modelData->fu_listarTodo($conexion);

	$combo = '';

    if (count($arrayEditorial) > 0) {

    	$combo = '<option value="">SELECCIONE</option>';

		foreach ($arrayEditorial as $obj): 
				 $combo.='<option value="' . $obj['ID_EDITORIAL'] . '">' . utf8_encode($obj['NOM_EDITORIAL']). '</option>';
		 endforeach; 
    }
    
    echo $combo;

}

function loadProveedor(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	include $nombreModulo . 'vistas/proveedor.php';
}

function registrarProveedor($rucproveedor,$nomproveedor, $idPais, $direcproveedor, $fonoproveedor, $webproveedor, $mailproveedor, $contacproveedor){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgaproveedor();

	$rpta = $modelData->fu_registrar($conexion, $rucproveedor,$nomproveedor, $idPais, $direcproveedor, $fonoproveedor, $mailproveedor, $webproveedor, $contacproveedor, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo $rpta['ID_REGISTRO'];
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function ComboProveedor(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgaproveedor();
	$arrayProveedor	= $modelData->fu_listarTodo($conexion);

	$combo = '';

    if (count($arrayProveedor) > 0) {

    	$combo = '<option value="">SELECCIONE</option>';

		foreach ($arrayProveedor as $obj): 
				 $combo.='<option value="' . $obj['ID_PROVEEDOR'] . '">' . utf8_encode($obj['NOM_PROVEEDOR']). '</option>';
		 endforeach; 
    }
    
    echo $combo;
}

function loadTema(){

	include $nombreModulo . 'vistas/tema.php';
}

function registrarTema($nombre){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelCategoria	= new Data_sgacategoria();

	$rpta = $modelCategoria->fu_registrarCategoria($conexion,$nombre, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo $rpta['ID_REGISTRO'];
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function ComboTema(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);

	$combo = '';

    if (count($arrayTemas) > 0) {

		foreach ($arrayTemas as $obj): 
				 $combo.='<option value="' . $obj['ID_CATEGORIA'] . '">' . utf8_encode($obj['NOM_CATEGORIA']). '</option>';
		 endforeach; 
    }
    
    echo $combo;
}

function validarLibro($idFacultad, $tituloLibro, $idAutor, $idEditorial){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgalibro();
	$rpta			= $modelData->fu_validarLibro($conexion,$idFacultad, $tituloLibro, $idAutor, $idEditorial);

	if($rpta['CANTIDAD'] > 0){
		$modelData2		= new Data_sgalibro();
		$arrayData2		= $modelData2->fu_listarLibroValidar($conexion,$idFacultad, $tituloLibro, $idAutor, $idEditorial);

		include $nombreModulo . 'vistas/seleccionar.php';
	}
	else{
		echo 'passed';
	}

}

function vincularEjemplar($idLibro, $detIsbn, $detCod, $detBar, $detPat){
	global $conexion, $etiquetaTitulo, $nombreModulo;

		$objUtils 		= new Logic_Utils();
		$ip				= $objUtils->getIP();

		$objUsuario 	= $_SESSION['BTK_USUARIO'];
		$idUsuario		= $objUsuario->__get('_sess_usu_id');

		$aIsbn = @explode('|', $detIsbn);
		$aCod  = @explode('|', $detCod);
		$aBar  = @explode('|', $detBar);
		$aPat  = @explode('|', $detPat);

		$cadenaMsje = '';

		for($i = 0; $i < count($aCod); $i++){

			$isbn 	= $aIsbn[$i];
			$cod 	= $aCod[$i];
			$bar 	= ''; //$aBar[$i];
			$pat 	= $aPat[$i];

			$modelData		= new Data_sgalibro();
			$rptaDet = $modelData->fu_registrarEjemplar($conexion,$idLibro, $isbn, $cod, $bar, $pat, $idUsuario, $ip); 

			if($rptaDet['IND_OPERACION'] == 0){
				$cadenaMsje	 = $cadenaMsje.'<br>'.$rptaDet['DES_MENSAJE'];
			}

		}

		if($cadenaMsje == '' || $cadenaMsje != null){ echo 'passed';}
		else{echo $cadenaMsje;}

}

function loadEdicionEjemplar($idLibro, $codLibro){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$obj_Data		= new Data_sgalibro();
	$obj_Data  		= $obj_Data->fu_EncontrarEjemplarCodLibro($conexion,$idLibro, $codLibro);

	include $nombreModulo . 'vistas/editar_ejemplar.php';
}

function editarEjemplar($codLibroOld, $idLibro, $codisbn, $codLibro, $codPatrimonio){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 		= new Logic_Utils();
	$ip				= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelData		= new Data_sgalibro();
	$rpta 			= $modelData->fu_editarEjemplar($conexion, $codLibroOld, $idLibro, $codisbn, $codLibro, $codPatrimonio, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0){
			echo 'passed';
	}
	else{
		echo $rpta['DES_MENSAJE'];
	}

}

function loadBuscarAutor(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	include $nombreModulo . 'vistas/buscar_autor.php';
}

function listarAutor($nomAutor){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData		= new Data_sgaautor();
	$arrayAutor		= $modelData->fu_listarTodoFiltro($conexion, $nomAutor);

	include $nombreModulo . 'vistas/listar_autor.php';
}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['filtro'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'],$_POST['idFacultad'],$_POST['codLibro'],$_POST['desEstado']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['idFacultad'],$_POST['idLibroTipo'], $_POST['tituloLibro'], $_POST['idAutor'], $_POST['idEditorial'], $_POST['anioLibro'], 
				  $_POST['idPais'], $_POST['paginaLibro'],  $_POST['volLibro'], $_POST['idAdquisicion'], $_POST['idProveedor'], $_POST['fechaAdquisicion'],
				  $_POST['precioLibro'], $_POST['temasRelacion'], $_POST['detIsbn'], $_POST['detCod'], $_POST['detBar'], $_POST['detPat']);break;
	case 'loadVer':
		loadVer($_POST['id']);break;		
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;		
	case 'editar':
		editar($_POST['idLibro'],$_POST['idFacultad'],$_POST['idLibroTipo'], $_POST['tituloLibro'], $_POST['idAutor'], $_POST['idEditorial'], $_POST['anioLibro'], 
			   $_POST['idPais'], $_POST['paginaLibro'],  $_POST['volLibro'], $_POST['idAdquisicion'], $_POST['idProveedor'], $_POST['fechaAdquisicion'],
			   $_POST['precioLibro'], $_POST['temasRelacion'], $_POST['detIsbn'], $_POST['detCod'], $_POST['detBar'], $_POST['detPat']);break;
	case 'loadAdjuntos':
		loadAdjuntos($_POST['id']);break;	
	case 'listarAdjuntos':
		listarAdjuntos($_POST['id']);break;	
	case 'registrarAdjunto':
		registrarAdjunto($_POST['id'],$_POST['tipoArchivo'],$_POST['desArchivo']);break;	
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
	case 'eliminarAdjunto':
		eliminarAdjunto($_POST['id']);break;
	case 'eliminarEjemplar':
		eliminarEjemplar($_POST['idLibro'],$_POST['codLibro']);break;
	case 'generarBarra':
		generarBarra($_POST['texto'],$_POST['press']);break;
	case 'loadAutor':
		loadAutor();break;
	case 'registrarAutor':
		registrarAutor($_POST['nomAutor'], $_POST['apeAutor'], $_POST['seudoAutor'], $_POST['idPais'], $_POST['idAutorTipo']);break;
	case 'ComboAutor':
		ComboAutor();break;
	case 'loadEditorial':
		loadEditorial();break;
	case 'registrarEditorial':
		registrarEditorial($_POST['nomEditorial'], $_POST['idPais'], $_POST['direcEditorial'], $_POST['fonoEditorial'], $_POST['mailEditorial'], $_POST['webEditorial']);break;
	case 'ComboEditorial':
		ComboEditorial();break;
	case 'loadProveedor':
		loadProveedor();break;
	case 'registrarProveedor':
		registrarProveedor($_POST['rucproveedor'],$_POST['nomproveedor'], $_POST['idPais'], $_POST['direcproveedor'], $_POST['fonoproveedor'], $_POST['webproveedor'], $_POST['mailproveedor'], $_POST['contacproveedor']);break;			
	case 'ComboProveedor':
		ComboProveedor();break;
	case 'loadTema':
		loadTema();break;
	case 'registrarTema':
		registrarTema($_POST['nombre']);break;
	case 'ComboTema':
		ComboTema();break;
	case 'validarLibro':
		validarLibro($_POST['idFacultad'],$_POST['tituloLibro'],$_POST['idAutor'],$_POST['idEditorial']);break;
	case 'vincularEjemplar':
		vincularEjemplar($_POST['idLibro'], $_POST['detIsbn'], $_POST['detCod'], $_POST['detBar'], $_POST['detPat']);break;
	case 'loadEdicionEjemplar':
		loadEdicionEjemplar($_POST['idLibro'],$_POST['id']);break;
	case 'editarEjemplar':
		editarEjemplar($_POST['codLibroOld'],$_POST['idLibro'],$_POST['codisbn'],$_POST['codLibro'],$_POST['codPatrimonio']);break;
	case 'loadBuscarAutor':
		loadBuscarAutor();break;
	case 'listarAutor':
		listarAutor($_POST['filtro']);break;
    default:
        vistaPrincipal();
        break;
}
?>