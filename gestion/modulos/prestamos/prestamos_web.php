<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'P.ID_PRESTAMO';
	$direccion = 'DESC';

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$modelData		= new Data_sgaprestamo();
	$arrayEstados	= $modelData->fu_listarPrestamoEstado($conexion);

    include $nombreModulo . 'vistas/main.php';
}


function listar($idFacultad, $idPrestamo, $codUniversitario, $fini, $ffin, $indActivo, $orden, $direccion, $pagina){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	if($fini == ''){ $fini = INICIO_SISTEMA; }
	if($ffin == ''){ $ffin = date('d/m/Y');  }

	$fecha          = @explode('/',$fini);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $fini 			= $anio.'-'.$mes.'-'.$dia;

    $fecha          = @explode('/',$ffin);
    $dia            = $fecha[0];
    $mes            = $fecha[1];
    $anio           = $fecha[2];
    $ffin			= $anio.'-'.$mes.'-'.$dia;
	
	$modelData	= new Data_sgaprestamo();
	$arrayData	= $modelData->fu_listarPrestamos($conexion,$idFacultad, $indActivo, $orden, $direccion, $pagina,$idPrestamo, $codUniversitario, $fini, $ffin);

	foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var idFacultad	= $("#idFacultad").val();
							var idPrestamo 	= $("#txtCodigo").val();
							var codigoU		= $("#txtCodigoU").val();
    						var fini 		= $("#fechaInicio").val();
    						var ffin 		= $("#fechaFin").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/prestamos/prestamos.php?cmd=listar", {
						idFacultad:idFacultad, idPrestamo:idPrestamo, codigoU:codigoU, fini: fini, ffin:ffin, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);

	include $nombreModulo . 'vistas/crear.php';
}

function buscarUsuario($codUniversitario){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelUsuario 	= new Data_sgausuario();
	$modelUsuario 	= $modelUsuario->fu_encontrarxCodUniv($conexion,$codUniversitario);

	if($modelUsuario == '' || $modelUsuario == null){
		echo 'failed';
	}
	else{
		echo utf8_encode($modelUsuario['NOM_USUARIO'].' '.$modelUsuario['APE_USUARIO']).'|'.$modelUsuario['DES_ESTADO'].'|'.$modelUsuario['ID_USUARIO'];
	}
}

function buscarLibro($idFacultad,$idFiltro,$descripcion,$codigo,$idTemas,$idCatalogo){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	if($idFiltro == 'tema'){
		$descripcion = $idTemas;
	}

	$modelData		= new Data_sgaprestamo();
	$arrayLibros	= $modelData->fu_buscarLibro($conexion,$idFacultad, $idFiltro, $descripcion, $codigo, $idCatalogo);

	include $nombreModulo . 'vistas/resultados2.php';

}

function loadVer($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	//$objUsuario		= $_SESSION['BTK_USUARIO'];
	//$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacategoria();
	$arrayTemas		= $modelData->fu_listarTodoCategoria($conexion);
	
	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,0);

	$modelData		= new Data_sgalibro();
	$arrayLibroTipo	= $modelData->fu_listarLibroTipo($conexion);
	$arrayAdjunto	= $modelData->fu_listarAdjunto($conexion,$id);

	$modelData		= new Data_sgapais();
	$arrayPais		= $modelData->fu_listarTodoPais($conexion);

	$modelData		= new Data_sgaautor();
	$arrayAutor		= $modelData->fu_listarTodo($conexion);

	$modelData		= new Data_sgaeditorial();
	$arrayEditorial	= $modelData->fu_listarTodo($conexion);
	
	$obj_Data		= new Data_sgalibro();
	$arrayEjemplar  = $obj_Data->fu_EncontrarEjemplares($conexion,$id);
	$arrayDatos  	= $obj_Data->fu_EncontrarAutores($conexion,$id);
	$obj_Data	 	= $obj_Data->fu_Encontrar($conexion,$id);

	$_SESSION['BTK_CARPETA_ID_FACULTAD'] 	= $obj_Data['ID_CARRERA'];

	$arrayAutorLib 	= array();
	foreach ($arrayDatos as $obj) {
		$arrayAutorLib[] = $obj['ID_AUTOR'];
	}

	$arrayTema		= array();
	$arrayTema		= @explode(',', $obj_Data['DES_CATEGORIA_RELACION']);

	$id = $id;
	
	include $nombreModulo . 'vistas/ver2.php';

}

function registrar($idSolicita, $idBiblioteca, $detLibro, $detCant, $diasPrestamo){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
		
	$modelData	= new Data_sgaprestamo();

	$rpta = $modelData->fu_registrar($conexion,$idSolicita, $idBiblioteca, $diasPrestamo, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0){
		
		$idPrestamo = $rpta['ID_PRESTAMO'];

		$aLibro = @explode('|', $detLibro);
		$aCant  = @explode('|', $detCant);

		for($i = 0; $i < count($aLibro); $i++){

			$idLibro 	= $aLibro[$i];
			$cantidad	= $aCant[$i];

			$rptaDet = $modelData->fu_registrarDetalle($conexion,$idPrestamo, $idLibro, $cantidad, $idUsuario, $ip); 
		}

		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
	
}


function loadVisualizar($idPrestamo){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData  = new Data_sgaprestamo();
	$objData    = $modelData->fu_Encontrar($conexion,$idPrestamo); 
	$arrayData  = $modelData->fu_EncontrarDetalle($conexion,$idPrestamo); 

	include $nombreModulo . 'vistas/visualizar.php';
}

function loadAtender($idPrestamo){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData  = new Data_sgaprestamo();
	$objData    = $modelData->fu_Encontrar($conexion,$idPrestamo); 
	$arrayData  = $modelData->fu_EncontrarDetalle($conexion,$idPrestamo); 

	$idPrestamo  = $idPrestamo;

	include $nombreModulo . 'vistas/atender.php';

}

function registrarAtender($idPrestamo, $cadena ,$comentario){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelData  	= new Data_sgaprestamo();

	$data 			= @explode(',', $cadena);

	if($cadena != ''){

		for ($i=0; $i < count($data); $i++) { 
			
			$data2 = $data[$i];
			$dataL = @explode('_', $data2);

			$rptaDet = $modelData->fu_cambiarEstadoLibro($conexion,$idPrestamo, $dataL[0], $dataL[1], $idUsuario, $ip); 
		}
	}

	$rpta = $modelData->fu_prestamoAtender($conexion,$idPrestamo, $comentario, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{
		echo 'failed';
	}

}

function loadFinalizar($idPrestamo){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$modelData  = new Data_sgaprestamo();
	$objData    = $modelData->fu_Encontrar($conexion,$idPrestamo); 
	$arrayData  = $modelData->fu_EncontrarDetalle($conexion,$idPrestamo); 

	$idPrestamo  = $idPrestamo;

	include $nombreModulo . 'vistas/finalizar.php';

}

function registrarFinalizar($idPrestamo, $cadena ,$comentario){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelData  	= new Data_sgaprestamo();

	$data 			= @explode(',', $cadena);

	if($cadena != ''){

		/*for ($i=0; $i < count($data); $i++) { 
			
			$data2 = $data[$i];
			$dataL = @explode('_', $data2);

			$rptaDet = $modelData->fu_cambiarEstadoLibro($conexion,$idPrestamo, $dataL[0], $dataL[1], $idUsuario, $ip); 
		}*/
	}

	$rpta = $modelData->fu_prestamoFinalizar($conexion,$idPrestamo, $comentario, $idUsuario, $ip); 

	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{
		echo 'failed';
	}

}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['idFacultad'],$_POST['idPrestamo'],$_POST['codigoU'],$_POST['fini'],$_POST['ffin'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'buscarUsuario':
		buscarUsuario($_POST['codigo']);break;	
	case 'buscarLibro':
		 buscarLibro($_POST['idFacultad'],$_POST['idFiltro'],$_POST['descripcion'],$_POST['codigo'],$_POST['idTemas'],$_POST['idCatalogo']);
		 break;	
	case 'loadVer':
		loadVer($_POST['id']);break;
	case 'registrar':
		registrar($_POST['idUsuario'],$_POST['idBiblioteca'],$_POST['detLibro'], $_POST['detCant'], $_POST['diasPrestamo']);break;
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;	
	case 'loadAtender':
		loadAtender($_POST['id']);break;	
	case 'registrarAtender':
		registrarAtender($_POST['idPrestamo'],$_POST['cadena'],$_POST['comentario']);break;	
	case 'loadFinalizar':
		loadFinalizar($_POST['id']);break;	
	case 'registrarFinalizar':
		registrarFinalizar($_POST['idPrestamo'],$_POST['cadena'],$_POST['comentario']);break;	
    default:
        vistaPrincipal();
        break;
}
?>