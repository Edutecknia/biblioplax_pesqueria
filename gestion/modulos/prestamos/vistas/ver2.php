<script>
$(document).ready(function(){

    $(".select2").select2();
 
});
</script>
<style type="text/css">
 
 @media screen and (min-width: 768px) {
    .custom-class {
        width: 700px; /* either % (e.g. 60%) or px (400px) */
    }
}   

</style>

    <div class="modal-dialog custom-class">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="titulo" style="color:#333"><?php echo utf8_encode($obj_Data['DES_TITULO']); ?></h4>
            </div>


 <div class="modal-body">
<div class="container col-sm-12">

<?php
    $caratula   = '';
    $indice     = '';
    $img        = '';

    foreach ($arrayAdjunto as $obj):

    switch ($obj['ID_ARCHIVO_TIPO']) {
        case '1':
            $caratula   = 'archivos/'.$_SESSION['BTK_CARPETA_ID_FACULTAD'].'/'.$obj['NOM_ARCHIVO'];
            break;
        case '2':
            $indice     = 'archivos/'.$_SESSION['BTK_CARPETA_ID_FACULTAD'].'/'.$obj['NOM_ARCHIVO'];
            break;
        case '3':
            $img        = 'archivos/'.$_SESSION['BTK_CARPETA_ID_FACULTAD'].'/'.$obj['NOM_ARCHIVO'];
            break;
    }

    endforeach;
?>
                <?php if($caratula != ''){?>
                <div class="col-sm-12">
                <center><a href="<?php echo $caratula; ?>" target="_blank"><img src="<?php echo $caratula; ?>" class="img-responsive" width="150px" height="150px"></a></center>
                </div>
                <?php } ?>

                <div class="form-group col-sm-6">
                <label style="color:#333">Biblioteca:</label>
                    <select class="form-control" 
                            name="idFacultad" 
                            id="idFacultad" disabled="disabled">
                        <?php foreach ($arrayFacultad as $obj): ?>
                        <option value="<?php echo $obj['ID_CARRERA']; ?>"
                        <?php if($obj['ID_CARRERA'] == $obj_Data['ID_CARRERA']){echo 'selected';} ?>
                        ><?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                <label style="color:#333">Tipo:</label>
                        <select class="form-control select2" 
                                name="idLibroTipo" 
                                id="idLibroTipo"
                                style="width: 100%;" disabled="disabled">
                            <?php foreach ($arrayLibroTipo as $obj): ?>
                            <option value="<?php echo $obj['ID_LIBRO_TIPO']; ?>"
                            <?php if($obj['ID_LIBRO_TIPO'] == $obj_Data['ID_LIBRO_TIPO']){echo 'selected';}?>
                            ><?php echo utf8_encode($obj['DES_LIBRO_TIPO']); ?></option>
                            <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-sm-12">
                <label style="color:#333">Título:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="tituloLibro" name="tituloLibro" 
                                    class="form-control" 
                                    placeholder="Título del Libro" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo utf8_encode($obj_Data['DES_TITULO']); ?>" 
                                    disabled="disabled"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-12">
                <label style="color:#333">Autor:</label>
                                <select class="form-control select2" 
                                        name="idAutor" 
                                        id="idAutor"
                                        multiple="multiple" data-placeholder="NO ESPECÍFICO" 
                                        style="width: 100%;" 
                                        disabled="disabled"
                                        style="color:#333"
                                >
                                <option>NO ESPECÍFICO</option>
                                <?php foreach ($arrayAutor as $obj): ?>
                                <option value="<?php echo $obj['ID_AUTOR']; ?>"
                                <?php if(@in_array($obj['ID_AUTOR'],$arrayAutorLib)){ echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['DATOS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-12">
                <label style="color:#333">Editorial:</label>
                                <select class="form-control select2" 
                                        name="idEditorial" 
                                        id="idEditorial"
                                        style="width: 100%;"
                                        disabled="disabled"
                                        data-placeholder="NO ESPECÍFICO" 
                                >
                                <option>NO ESPECÍFICO</option>
                                <?php foreach ($arrayEditorial as $obj): ?>
                                <option value="<?php echo $obj['ID_EDITORIAL']; ?>"
                                <?php if($obj['ID_EDITORIAL'] == $obj_Data['ID_EDITORIAL']){echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['NOM_EDITORIAL']); ?></option>
                                <?php endforeach; ?>
                                </select>    
                </div>

                <div class="form-group col-sm-12">
                </div>

                <div class="form-group col-sm-6">
                <label style="color:#333">Año Publicación:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="anioLibro" name="anioLibro" 
                                    class="form-control" 
                                    placeholder="NO ESPECÍFICO" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    value="<?php echo $obj_Data['NUM_ANIO_PUBLICA']; ?>"
                                    disabled="disabled"
                                    >
                                </div>
                </div>
                

                <div class="form-group col-sm-6">
                <label style="color:#333">Pais Publicación:</label>
                                <select class="form-control select2" 
                                        name="idPais" 
                                        id="idPais"
                                        style="width: 100%;"
                                        disabled="disabled"
                                        placeholder="NO ESPECÍFICO" 
                                >
                                <option>NO ESPECÍFICO</option>
                                <?php foreach ($arrayPais as $obj): ?>
                                <option value="<?php echo $obj['ID_PAIS']; ?>"
                                <?php if($obj['ID_PAIS'] == $obj_Data['ID_PAIS']){echo 'selected';}?>
                                ><?php echo utf8_encode($obj['NOM_PAIS']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div style="clear:both;"></div>

                <div class="form-group col-sm-6">
                <label style="color:#333">Volumen:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="volLibro" name="volLibro" 
                                    class="form-control" 
                                    placeholder="NO ESPECÍFICO" 
                                    type="text" 
                                    maxlength="5" 
                                    value="<?php echo $obj_Data['NUM_VOLUMEN']; ?>"
                                    disabled="disabled"
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label style="color:#333">Nro. Páginas:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="paginaLibro" name="paginaLibro" 
                                    class="form-control" 
                                    placeholder="NO ESPECÍFICO" 
                                    type="number" 
                                    min="1"
                                    max ="9999"
                                    value="<?php echo $obj_Data['NUM_PAGINA']; ?>"
                                    disabled="disabled"
                                    >
                                </div>
                </div>

         
                <div class="form-group col-sm-12">
                <label style="color:#333">Temas Relacionados:</label>
                               <select class="form-control select2" 
                                        name="idTemas" 
                                        id="idTemas"  
                                        multiple="multiple" data-placeholder="NO ESPECÍFICO" 
                                        style="width: 100%;"
                                        disabled="disabled"
                                        style="color:#333 !important"
                                >
                                <?php foreach ($arrayTemas as $obj): ?>
                                <option value="<?php echo $obj['ID_CATEGORIA']; ?>"
                                <?php if(@in_array($obj['ID_CATEGORIA'],$arrayTema)){ echo 'selected';} ?>
                                ><?php echo utf8_encode($obj['NOM_CATEGORIA']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <?php if($indice != ''){?>
                <div class="col-sm-12">
                <center><a href="<?php echo $indice; ?>" target="_blank">VER INDICE</a></center>
                </div>
                <?php } ?>

</div>


                      <p>&nbsp;</p>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cerrar</button>
                        </div>

 </div>		

 </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->