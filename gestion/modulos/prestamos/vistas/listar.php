<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false
    });
  });
</script>
<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>CÓDIGO PRESTAMO</th>
                        <th>SOLICITANTE</th>
                        <th>COD. SOLICITANTE</th>
                        <th>FECHA SOLICITADO</th>
                        <th>FECHA ENTREGA</th>
                        <th>FECHA DEVOLUCIÓN</th>
                        <th>DIAS ATRASO</th>
                        <th>ESTADO</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayData as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;

                    $fechaSolicitado    = strtotime( $obj['FEC_INICIO'] ); 
                    $fechaSolicitado    = date('d/m/Y H:i:s', $fechaSolicitado);

                    $fechaEntrega       = strtotime( $obj['FEC_FIN'] ); 
                    $fechaEntrega       = date('d/m/Y H:i:s', $fechaEntrega); 

                    $fechaDevolucion    = '';

                    if( $obj['FEC_ENTREGA'] != '' &&  $obj['FEC_ENTREGA'] != null){

                    $fechaDevolucion       = strtotime( $obj['FEC_ENTREGA'] ); 
                    $fechaDevolucion       = date('d/m/Y H:i:s', $fechaDevolucion);

                    }

					?>
                    <tr>
                    <td><?php echo $contItem;?></td>
                    <td><?php echo utf8_encode($obj['ID_PRESTAMO']);?></td>
                    <td><?php echo utf8_encode($obj['NOM_USUARIO'].' '.$obj['APE_USUARIO']); ?></td>
                    <td><?php echo utf8_encode($obj['COD_UNIVERSITARIO']);?></td>
                    <td><?php echo $fechaSolicitado; ?></td>
                    <td><?php echo $fechaEntrega; ?></td>
                    <td><?php echo $fechaDevolucion; ?></td>
                    <td align="center"><?php echo $obj['ATRASO']; ?></td>
                    <td><?php echo utf8_encode($obj['DES_ESTADO']);?></td>
                    <td class="center" align="center">
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm('visualizar','<?php echo $obj['ID_PRESTAMO'] ?>');">
                        <i class="fa fa-eye"></i>
                    </a>&nbsp;&nbsp;
                     <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Imprimir Solicitud" 
                        href="modulos/prestamo_ind/vistas/pdfprestamo.php?cmd=<?php echo $obj['ID_PRESTAMO'] ?>" 
                        target="_blank"
                        >
                        <i class="fa fa-print"></i>
                    </a>&nbsp;&nbsp;
                    <?php
                        if($obj['DES_ESTADO'] == 'SOLICITADO'){
                        ?>
                        <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="ATENDER" 
                        onclick="javascript:OpenForm('atender','<?php echo $obj['ID_PRESTAMO'] ?>');">
                        <i class="fa fa-check-square-o"></i>
                        </a>&nbsp;&nbsp;
                        <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="ANULAR" 
                        onclick="javascript:inactivar('<?php echo $obj['ID_PRESTAMO'] ?>');">
                        <i class="fa fa-trash"></i>
                        </a>&nbsp;&nbsp;
                        <?php
                        }
                        if($obj['DES_ESTADO'] == 'ATENDIDO'){
                        ?>
                        <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Finalizar Préstamo" 
                        onclick="javascript:OpenForm('finalizar','<?php echo $obj['ID_PRESTAMO'] ?>');">
                        <i class="fa fa-check-square-o"></i>
                        </a>&nbsp;&nbsp;
                        <?php
                        }

                    }
                    ?>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  