<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<div class="form-group col-sm-12">

<h5 class="text-green" id="txtResultado">Se encontraron <?php echo count($arrayLibros); ?> resultado(s)</h5>
                

</div>

<script>
  $(function () {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false
    });
  });
</script>

<?php
if(count($arrayLibros) > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th style="color:#333">BIBLIOTECA</th>
                        <th style="color:#333">TIPO</th>
                        <th style="color:#333">TITULO</th>
                        <th style="color:#333">EDITORIAL</th>
                        <th style="color:#333">AÑO PUBLICACIÓN</th>
                        <th style="color:#333">VOLUMEN</th>
                        <th style="color:#333">DISPONIBLES</th>
                        <th style="color:#333" class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    
                    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
                    $contItem = $contItem2 = 0;
                    
                    foreach ($arrayLibros as $obj):
                    $contItem = $contItem2 + $item;
                    $contItem2++;

                    ?>
                    <tr style="color:#333">
                    <td><?php echo utf8_encode($obj['NOM_CARRERA']); ?></td>
                    <td><?php echo utf8_encode($obj['DES_LIBRO_TIPO']); ?></td>
                    <td><?php echo utf8_encode($obj['DES_TITULO']);?></td>
                    <td><?php echo utf8_encode($obj['NOM_EDITORIAL']);?></td>
                    <td><?php echo utf8_encode($obj['NUM_ANIO_PUBLICA']);?></td>
                    <td><?php echo $obj['NUM_VOLUMEN'];?></td>
                    <td><?php echo $obj['DISPONIBLES'];?></td>
                    <td class="center" align="center">
                    <?php
                    if($obj['IND_ACTIVO'] == 1){
                    ?>
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm2('visualizar','<?php echo $obj['ID_LIBRO'] ?>');">
                        <span class='label label-primary'>VER</span>
                    </a>&nbsp;&nbsp;
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Solicitar Ejemplar" 
                        onclick="javascript:solicitar('<?php echo $obj['ID_LIBRO'] ?>','<?php echo utf8_encode($obj['DES_TITULO']) ?>','<?php echo $obj['DISPONIBLES'] ?>','<?php echo $obj['ID_CARRERA'] ?>','<?php echo utf8_encode($obj['NOM_CARRERA']) ?>','<?php echo $obj['EJEMPLARES'] ?>');">
                        <span class='label label-success'>SOLICITAR</span>
                    </a>
                    <?php
                    }
                    ?>
                    </td>
                    </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
<?php
}
?>                       