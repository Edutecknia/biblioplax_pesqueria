<?php
function imprime_ticket_prestamo($idPrestamo){
global $conexion;

$modelData  = new Data_sgaprestamo();
$objData    = $modelData->fu_Encontrar($conexion,$idPrestamo); 

$fechaSolicitado    = strtotime( $objData['FEC_INICIO'] ); 
$fechaSolicitado    = date('d/m/Y H:i:s', $fechaSolicitado);

$fechaEntrega       = strtotime( $objData['FEC_FIN'] ); 
$fechaEntrega       = date('d/m/Y H:i:s', $fechaEntrega); 

$arrayData          = $modelData->fu_EncontrarDetalle($conexion,$idPrestamo); 


$GDOC='';
//$ABRIRGABETA=(chr(27) . chr(112) . chr(0) . chr(75) . chr(50));//ABRIR EL CAJON
//$GDOC.=$ABRIRGABETA;
$CORTE=(chr(10) . chr(10) . chr(10) . chr(10) . chr(29) . chr(86) . chr(49) . chr(12));
$SALTO="\r\n";
$BARRASIM1='========================================'.$SALTO;
$BARRASIM='---------------------------------------'.$SALTO;
$TAB='	';
$ESP=' '; 
$GDOC.=$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.'BIBLIOPLAX'.$SALTO;
$GDOC.=$ESP.$ESP.$ESP.'SOLICITUD DE PRESTAMO'.$SALTO;
$GDOC.=$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.'N°:'.$TAB.$idPrestamo.$SALTO; 
$GDOC.=$BARRASIM;
$GDOC.='SOLICITANTE:'.$ESP.utf8_encode($objData['NOM_USUARIO'].' '.$objData['APE_USUARIO']).$SALTO; 
$GDOC.='CÓDIGO:'.$ESP.utf8_encode($objData['COD_UNIVERSITARIO']).$SALTO; 
$GDOC.='BIBLIOTECA:'.$ESP.utf8_encode($objData['NOM_CARRERA']).$SALTO; 
$GDOC.='F.PRESTAMO'.$ESP.$ESP.':'.$ESP.$fechaSolicitado.$SALTO; 
$GDOC.='F.DEVOLUCION:'.$ESP.$fechaEntrega.$SALTO; 
$GDOC.=$BARRASIM;
$GDOC.='COD'.$TAB.'TITULO'.$TAB.$TAB.$TAB.'CANT'.$SALTO;

foreach ($arrayData as $obj):
$cantd=0;
$difd=0;
$cadenad='';
$vacio='';
$cantd=strlen(trim(utf8_encode($obj['DES_TITULO'])));
if($cantd<18){
	$difd = 18 - $cantd;
	for ($id = 0; $id < $difd; $id++) {
		$vacio = $vacio.' ';
	}
}
$cadenad = trim(utf8_encode($obj['DES_TITULO']));
$cadenad = $cadenad.$vacio;
$GDOC.=$obj['COD_LIBRO'].$TAB.strtoupper(substr($cadenad,0,18)).$TAB.'1'.$SALTO;
endforeach;

$GDOC.=$BARRASIM;
$GDOC.='TOTAL ITEMS '.$TAB.$TAB.$TAB.count($arrayData).$SALTO;
$GDOC.=$BARRASIM;
$GDOC.=$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.$ESP.'UNJFSC'.$SALTO;
$GDOC.=$SALTO.$SALTO;
$GDOC.=$CORTE;

//CREAR ARCHIVO 
$sourcefile='vistas/ticket.txt';
$file=fopen($sourcefile,'w');
fwrite($file,$GDOC);
fclose($file);	 
//shell_exec("cat ".$sourcefile."> /dev/ttyS0");

}
?>