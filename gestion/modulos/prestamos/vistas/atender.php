<script>
$(document).ready(function(){
 
 $(".select2").select2();

});
</script>

<form action="javascript:registrarAtender();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

                <?php 
                    $fechaSolicitado    = strtotime( $objData['FEC_INICIO'] ); 
                    $fechaSolicitado    = date('d/m/Y H:i:s', $fechaSolicitado);

                    $fechaEntrega       = strtotime( $objData['FEC_FIN'] ); 
                    $fechaEntrega       = date('d/m/Y H:i:s', $fechaEntrega); 
                ?>

                <input type="hidden" id="txtid" value="<?php echo $idPrestamo; ?>">
                <div class="form-group col-sm-12">
                <label>Solicitante:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtSolicitante" name="txtSolicitante" 
                                    class="form-control" 
                                    type="text" 
                                    value="<?php echo utf8_encode($objData['NOM_USUARIO'].' '.$objData['APE_USUARIO']); ?>" 
                                    readonly
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-6">
                <label>Biblioteca:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtSolicitante" name="txtSolicitante" 
                                    class="form-control" 
                                    type="text" 
                                    value="<?php echo utf8_encode($objData['NOM_CARRERA']); ?>" 
                                    readonly
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-3">
                <label>Fecha Solicitado:</label>
                        <div class="input-group">
                            <span class="input-group-addon"></span>
                            <input id="txtDiasPrestamo" name="txtDiasPrestamo" 
                                   class="form-control" 
                                   type="text" 
                                   value="<?php echo $fechaSolicitado; ?>"
                                   readonly
                                   >
                        </div>
                </div>

                <div class="form-group col-sm-3">
                <label>Fecha Entrega:</label>
                        <div class="input-group">
                            <span class="input-group-addon"></span>
                            <input id="txtDiasPrestamo" name="txtDiasPrestamo" 
                                   class="form-control" 
                                   type="text" 
                                   value="<?php echo $fechaEntrega; ?>"
                                   readonly
                                   >
                        </div>
                </div>


                <div class="col-md-12"><br><br></div>

                <div class="col-md-12 table-responsive" id="divSolicitado">

                <div class="form-group col-sm-12">
                <h4 class="text-yellow" id="txtSeleccionado">Ejemplares Solicitados: <?php echo count($arrayData)?></h4>
                <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
                </div>
                </div>

                <div class="col-md-12 table-responsive">
                    <table id="ejemplares" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>CÓDIGO</th>
                        <th>COD.BARRAS</th>
                        <th>COD.PATRIMONIAL</th>
                        <th>ISBN</th>
                        <th>TITULO</th>
                        <th>CANT. SOLICITADA</th>
                        <th align="center" class="center">ENTREGAR</th>
                      </tr>
                    </thead>
                    <tbody id="detalle">
                    <?php foreach ($arrayData as $obj): ?>
                        <tr>
                            <td><?php echo $obj['COD_LIBRO']; ?></td>
                            <td><?php echo $obj['COD_BARRA']; ?></td>
                            <td><?php echo $obj['COD_PATRIMONIAL']; ?></td>
                            <td><?php echo utf8_encode($obj['COD_ISBM']); ?></td>
                            <td><?php  echo utf8_encode($obj['DES_TITULO']); ?></td>
                            <td>1</td>
                            <td class="center"><input type="checkbox" id="<?php echo $obj['ID_LIBRO'].'_'.$obj['COD_LIBRO'] ?>" type="checkbox" class="CkbLibros" checked></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>  
                    </table>
                </div>


                </div>


                <div class="form-group col-sm-12">
                <label>Comentario:</label>
                        <div class="input-group">
                            <span class="input-group-addon"></span>
                           <textarea id="txtComentario" placeholder="Comentario u Observación de la solicitud" class="form-control" ></textarea>
                        </div>
                </div>

</div>


                      <p>&nbsp;</p>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
                            <button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
 </form>