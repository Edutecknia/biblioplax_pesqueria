<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'C.FEC_REGISTRO';
	$direccion = 'DESC';

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS
	$idRol        	= $objUsuario->__get('_sess_id_rol');

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

    include $nombreModulo . 'vistas/main.php';
}


function listar($filtro, $indActivo, $orden, $direccion, $pagina, $idCarrera){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idRol        	= $objUsuario->__get('_sess_id_rol');
	
	if($indActivo == '' || $indActivo == null){ $indActivo = 1; }

	$modelData	= new Data_sgasugerencia();
	$arrayData	= $modelData->fu_listar($conexion,$filtro, $indActivo, $orden, $direccion, $pagina, $idUsuario, $idCarrera, 'SUGERENCIA');

	foreach ($arrayData as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

    include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var filtro 		= $("#txtBuscar").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
							var idCarrera 	= $("#idFacultad").val();
							
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/sugerencia/sugerencia.php?cmd=listar", {
						filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num, idCarrera:idCarrera
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

	include $nombreModulo . 'vistas/crear.php';
}

function registrar($idFacultad, $nomSugerencia, $detSugerencia){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgasugerencia();

	$rpta = $modelData->fu_registrar($conexion, $idFacultad, $nomSugerencia, $detSugerencia, 'SUGERENCIA',$idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function loadVisualizar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$idRol        	= $objUsuario->__get('_sess_id_rol');

	$obj_Data		= new Data_sgasugerencia();

	if($idRol != 3){
	$rpta	 		= $obj_Data->fu_visualizarSugerencia($conexion,$id, $idUsuario, $ip);		
	}

	$obj_Data	 	= $obj_Data->fu_Encontrar($conexion,$id);
	
	$id = $id;
	
	include $nombreModulo . 'vistas/visualizar.php';

}

function grabarRpta($id, $detRpta){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgasugerencia();

	$rpta = $modelData->fu_grabarRpta($conexion, $id, $detRpta, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}


function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgasugerencia();

	$rpta = $modelData->fu_inactivar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelData	= new Data_sgasugerencia();

	$rpta = $modelData->fu_activar($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}


/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['filtro'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina'], $_POST['idCarrera']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['idFacultad'], $_POST['nomSugerencia'], $_POST['detSugerencia']);break;			
	case 'loadVisualizar':
		loadVisualizar($_POST['id']);break;		
	case 'grabarRpta':
		grabarRpta($_POST['id'],$_POST['detRpta']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>