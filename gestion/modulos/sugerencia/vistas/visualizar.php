<div class="modal-dialog">
<div class="modal-content">
        <div class="modal-header">
        <button type="button" id="btncerrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="titulo">Visualizar Sugerencia</h4>
        </div>


<form action="javascript:grabarRpta();" id="frm">
<div class="modal-body">
<div class="container col-sm-12">

               <input type="hidden" name="txtid" id="txtid" value="<?php echo $id; ?>">
                
                <div class="form-group col-sm-12">
                <label>Biblioteca:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomSugerencia" name="nomSugerencia" 
                                    class="form-control" 
                                    placeholder="Titulo de la sugerencia" 
                                    type="text" 
                                    value="<?php echo $obj_Data['NOM_CARRERA']; ?>" 
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-12">
                <label>Titulo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nomSugerencia" name="nomSugerencia" 
                                    class="form-control" 
                                    placeholder="Titulo de la sugerencia" 
                                    type="text" 
                                    maxlength="200" 
                                    required="required"
                                    value="<?php echo $obj_Data['NOM_SUGERENCIA_QUEJA']; ?>" >
                                </div>
                </div>

                
              

                <div class="form-group col-sm-12">
                <label>Descripción:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control"  maxlength="400" required="required" placeholder="Detalle de la sugerencia" id="detSugerencia" name="detSugerencia"><?php echo $obj_Data['DES_SUGERENCIA_QUEJA']; ?></textarea>
                                </div>
                </div>

                <div class="form-group col-sm-12">
                <label>Respuesta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <textarea class="form-control"  maxlength="400" required="required" placeholder="" id="detRpta" name="detRpta"><?php echo $obj_Data['DES_RPTA_SUGERENCIA_QUEJA']; ?></textarea>
                                </div>
                </div>

</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> Cerrar</button>
                            <?php if($idRol != 3) { ?>
                            <button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                            <?php } ?>
                        </div>

 </div>	
 </form>

</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


                   