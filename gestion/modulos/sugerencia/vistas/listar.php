<div id="content-paginar" class="col-md-12">
    <div id="paginacion" class="col-md-12">
    </div>
</div>
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false
    });
  });
</script>
<div id="cont_resultado_main" class="col-md-12">
<div class="modal fade" id="childModal1" tabindex="-1" role="dialog" aria-hidden="true">
</div>
<?php
if($numTotalElementos > 0){
?>    
                <table id="example2" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>BIBLIOTECA</th>
                        <th>TITULO</th>
                        <th>FECHA</th>
                        <th>ESTADO</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
					
					$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
		            $contItem = $contItem2 = 0;
					
                    foreach ($arrayData as $obj):
					$contItem = $contItem2 + $item;
                    $contItem2++;
					?>
                    <tr>
                    <td><?php echo $contItem;?></td>
                    <td><?php echo utf8_encode($obj['NOM_CARRERA']); ?></td>
                    <td><?php echo utf8_encode($obj['NOM_SUGERENCIA_QUEJA']);?></td>
                    <td>
                    <?php 
                    $fecha   = strtotime( $obj['FEC_REGISTRO'] ); 
                    $fecha   = date('d/m/Y H:i:s', $fecha); 
                    echo $fecha;
                    ?>
                    </td>
                    <td class="center">
                    <?php 
                    if($obj['IND_VISTO']== 0 && $obj['ID_USUARIO_RESPONDE']==''){
                        echo "<span class='label label-warning'>".'NO LEIDO'."</span>";
                    }
                    else{
                        if($obj['IND_VISTO']== 1 && $obj['ID_USUARIO_RESPONDE'] ==''){
                            echo "<span class='label label-success'>".'LEIDO'."</span>";
                        }
                    else{
                        if($obj['IND_VISTO']== 0 && $obj['ID_USUARIO_RESPONDE'] !=''){
                            echo "<span class='label label-success'>".'RESPUESTA'."</span>";
                        }
                    }
                    }
                    ?>
                    </td>
                    <td class="center" align="center">
                    <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Visualizar" 
                        onclick="javascript:OpenForm2('visualizar','<?php echo $obj['ID_SUGERENCIA'] ?>');">
                        <i class="fa fa-eye"></i>
                    </a>
                    </td>
                    </tr>
                    <?php
					endforeach;
					?>
                    </tbody>
                </table>
<?php
}
else{
	echo 'No se encontraron resultados';
}
?>                  
</div>                  