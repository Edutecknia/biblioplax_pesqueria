<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'C.NOM_CATEGORIA';
	$direccion = 'ASC';
    include $nombreModulo . 'vistas/main.php';
}


function listar($filtro, $indActivo, $orden, $direccion, $pagina){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	$modelCategoria	= new Data_sgacategoria();
	$arrayCategoria	= $modelCategoria->fu_listarCategoria($conexion,$filtro, $indActivo, $orden, $direccion, $pagina);

	foreach ($arrayCategoria as $obj):
        $numTotalElementos = $obj['TOTAL_FILAS'];
        break;
    endforeach;
    $numPaginas = ceil($numTotalElementos / TAM_PAG_LISTADO); 

	include $nombreModulo . 'vistas/listar.php';

	if($numPaginas > 1){
	 echo '<script type="text/javascript">
                        $("#paginacion").bootpag({
                            total: ' . $numPaginas . ',
                            page: ' . $pagina . ',
                            maxVisible: 10,
                            leaps: true,
                            firstLastUse: true,
                            first: "<span aria-hidden=true>inicio</span>",
                            last: "<span aria-hidden=true>fin</span>",
                            wrapClass: "pagination",
                            activeClass: "active",
                            disabledClass: "disabled",
                            nextClass: "next",
                            prevClass: "prev",
                            lastClass: "last",
							loop:true,
                            firstClass: "first"							
                        }).on("page", function(event, num){
                            event.stopImmediatePropagation();							
							var filtro 		= $("#txtBuscar").val();
							var indActivo	= $("#cboEstado").val();
							var orden 		= $("#orden").attr("value");
							var direccion	= $("#direccion").attr("value");
							var page 		= $("#pag_actual").attr("value");
						INI_LOAD();
						$("#pag_actual").attr("value", num);
                        $(".content4").html("Page " + num); 
						$.post("modulos/categoria/categoria.php?cmd=listar", {
						filtro: filtro, indActivo: indActivo, orden:orden, direccion:direccion, pagina: num
						}, function(data){
							$("#cont_resultado_main").empty();
							$("#cont_resultado_main").append(data);
							FIN_LOAD();
						});
							
                        }).find(".pagination");
                    </script>';	
	 }
	
}

function loadCreacion(){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	include $nombreModulo . 'vistas/crear.php';
}

function registrar($nombre){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelCategoria	= new Data_sgacategoria();

	$rpta = $modelCategoria->fu_registrarCategoria($conexion,$nombre, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}

function loadEdicion($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$obj_Categoria	= new Data_sgacategoria();
	$obj_Categoria 	= $obj_Categoria->fu_EncontrarCategoria($conexion,$id);
	$id = $id;
	include $nombreModulo . 'vistas/editar.php';

}

function editar($id, $nombre){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelCategoria	= new Data_sgacategoria();

	$rpta = $modelCategoria->fu_editarCategoria($conexion, $id,$nombre, $idUsuario, $ip); 
	
	if($rpta['IND_OPERACION'] > 0){
		echo 'passed';
	}
	else{ 
		echo $rpta['DES_MENSAJE'];
	}
	
}


function inactivar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelCategoria	= new Data_sgacategoria();

	$rpta = $modelCategoria->fu_inactivarCategoria($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}

}

function activar($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	= $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	
	
	$modelCategoria	= new Data_sgacategoria();

	$rpta = $modelCategoria->fu_activarCategoria($conexion, $id, $idUsuario, $ip); 
	
	if($rpta > 0){
		echo 'passed';
	}
	else{ 
		echo 'failed';
	}
}


/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'listar':
		 listar($_POST['filtro'],$_POST['indActivo'],$_POST['orden'],$_POST['direccion'],$_POST['pagina']);
		 break;	
	case 'loadCreacion':
		loadCreacion();break;	
	case 'registrar':
		registrar($_POST['nombre']);break;			
	case 'loadEdicion':
		loadEdicion($_POST['id']);break;		
	case 'editar':
		editar($_POST['id'],$_POST['nombre']);break;
	case 'inactivar':
		inactivar($_POST['id']);break;
	case 'activar':
		activar($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>