 <link rel="stylesheet" href="css/css_editor/froala_editor.css">
  <link rel="stylesheet" href="css/css_editor/froala_style.css">
  <link rel="stylesheet" href="css/css_editor/plugins/code_view.css">
  <link rel="stylesheet" href="css/css_editor/plugins/colors.css">
  <link rel="stylesheet" href="css/css_editor/plugins/emoticons.css">
  <link rel="stylesheet" href="css/css_editor/plugins/image_manager.css">
  <link rel="stylesheet" href="css/css_editor/plugins/image.css">
  <link rel="stylesheet" href="css/css_editor/plugins/line_breaker.css">
  <link rel="stylesheet" href="css/css_editor/plugins/table.css">
  <link rel="stylesheet" href="css/css_editor/plugins/char_counter.css">
  <link rel="stylesheet" href="css/css_editor/plugins/video.css">
  <link rel="stylesheet" href="css/css_editor/plugins/fullscreen.css">
  <link rel="stylesheet" href="css/css_editor/plugins/file.css">
  <link rel="stylesheet" href="css/css_editor/plugins/quick_insert.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

  <script type="text/javascript" src="js/js_editor/froala_editor.min.js" ></script>
  <script type="text/javascript" src="js/js_editor/plugins/align.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/char_counter.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/colors.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/entities.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/file.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/fullscreen.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/image.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/link.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/lists.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/quote.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/table.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/save.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/url.min.js"></script>
  <script type="text/javascript" src="js/js_editor/plugins/video.min.js"></script>
  <script type="text/javascript" src="js/js_editor/languages/es.js"></script>


  <link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="js/jquery.filer.js?v=1.0.5"></script>
<script type="text/javascript" src="js/customNoticia.js?v=1.0.5"></script>


  <script>
    $(function(){
      $('#edit').froalaEditor({
          language: 'es',
          imageUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
          fileUploadURL: '<?php echo APP_UPLOAD_PHP; ?>',
            imageUploadParams: {
            id: 'my_editor'
            }
      })

      

    });

    $(document).ready(function(){
        
        $(".fr-hidden").removeClass("fr-hidden")

        var allDivs = $('div');
        var topZindex = 0;
        var topDivId;
          allDivs.each(function(){
            var currentZindex = parseInt($(this).css('z-index'));
          if(currentZindex == 9999) {
              $(this).css('z-index',-9999);
          }
      });
});

  </script>


                    


<form action="javascript:editarNoticia();" id="frm">
 <div class="modal-body">
<div class="container col-sm-12">

<input type="hidden" name="txtid" id="txtid" value="<?php echo $id;?>"/>  

<div class="form-group col-sm-6">
                <label>Biblioteca:</label>
                <select class="form-control" name="idFacultad" id="idFacultad">
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['ID_CARRERA']; ?>" <?php if($obj['ID_CARRERA'] == $obj_Noticia['ID_CARRERA']){echo 'selected';} ?> >
                <?php echo utf8_encode($obj['NOM_CARRERA']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>

 <div class="form-group col-sm-6">
                <label>Nombre de Noticia:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="nombreNoticia" name="nombreNoticia" 
                                    class="form-control" 
                                    placeholder="Ingrese nombre de la noticia institucional" 
                                    type="text" 
                                    maxlength="100" 
                                    required="required"
                                    value="<?php echo $obj_Noticia['NOM_NOTICIA_EVENTO']; ?>"
                                    >
                                </div>
                        </div>


<div class="tab-content col-sm-12" style="background-color:#FFF;">
      <div class="tab-pane fade active in" id="subirfoto">
      

         <div class="form-group col-sm-12">
         <center><label>Imagen principal (458 x 300 píxeles)</label>
                                <div class="input-group">
<input type="file" name="files[]" id="filer_input2" multiple>
                                </div>
                                </center>
                      </div>



            <div class="jFiler-items jFiler-row"><ul class="jFiler-items-list jFiler-items-grid">
            <li class="jFiler-item" data-jfiler-index="0" style="">                        
            <div class="jFiler-item-container">                            
            <div class="jFiler-item-inner">                                
            <div class="jFiler-item-thumb">                                    
            <div class="jFiler-item-status"></div>                                    
            <div class="jFiler-item-info">                                        
           <span class="jFiler-item-title"><b title="<?php echo $obj_Noticia['NOM_IMG_PRINCIPAL'] ?>"><?php echo $obj_Noticia['NOM_IMG_PRINCIPAL'] ?></b></span> 
            <span class="jFiler-item-others">KB</span>                                 
            </div>                                    
            <div class="jFiler-item-thumb-image"><img src="../archivos/noticias_eventos/<?php echo $obj_Noticia['NOM_IMG_PRINCIPAL'] ?>" draggable="false"></div>                                
            </div>                                
            <div class="jFiler-item-assets jFiler-row">                                    
            <ul class="list-inline pull-left">                                        
            <li><div class="jFiler-jProgressBar" style="display: none;"><div class="bar" style="width: 100%;">
            </div></div><div class="jFiler-item-others text-success"><i class="icon-jfi-check-circle"></i> Correcto</div></li>                                    
            </ul>                                    
            <ul class="list-inline pull-right">                                        
            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>                                    
            </ul>                                
            </div>                            
            </div>                        
            </div>                    
            </li></ul></div>

                      
                      
      </div>
      </div>


 <div class="form-group col-sm-12">
<div id="editor">
<div id='edit' style="margin-top: 30px; min-height:400px;">
<?php echo $obj_Noticia['DES_NOTICIA_EVENTO']; ?>
</div>
</div>
</div>  




</div>


                      
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-danger" onclick="javascript:MainForm();"><i class="fa fa-times"></i> Cancelar</button>
<button type="submit" id="btnregistrar" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Grabar</button>
                        </div>

 </div>
</form>                 