<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	$pagina = 1;
	$orden = 'E.ID_NOTICIA_EVENTO';
	$direccion = 'DESC';
	
	$objUsuario		= $_SESSION['BTK_USUARIO'];
	$idFacultad		= $objUsuario->__get('_sess_id_facultad'); //0; //TODAS LAS BIBLIOTECAS

	$modelData		= new Data_sgacarrera();
	$arrayFacultad	= $modelData->fu_listarTodo($conexion,$idFacultad);

    include $nombreModulo . 'vistas/main.php';
}

function detalleNoticia($id){
	global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$obj_Noticia	= new Data_sganoticia();
	$obj_Noticia 	= $obj_Noticia->fu_encontrar($conexion,$id);

	include $nombreModulo . 'vistas/detalle.php';
}


/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'detalleNoticia':
		detalleNoticia($_POST['id']);break;
    default:
        vistaPrincipal();
        break;
}
?>