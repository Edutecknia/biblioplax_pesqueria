<?php

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
//
$nombreModulo = '';

if($_SESSION['BTK_USUARIO']==NULL){
$objSession = new Data_Session();
$objSession->validaSession();
exit(0);
}

/**
 * Carga la vista principal
 */
function vistaPrincipal() {
    global $conexion, $etiquetaTitulo, $nombreModulo;
	
	$objUsuario 	=  $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');
	$nomUsuario 	= $objUsuario->__get('_sess_usu_name');

    include $nombreModulo . 'vistas/main.php';
}

function loadClave($id){
	$id = $id;
	include $nombreModulo . 'vistas/clave.php';
}

function editarClave($id,$clave){
	global $conexion, $etiquetaTitulo, $nombreModulo;

	$objUtils 	= new Logic_Utils();
	$ip			= $objUtils->getIP();

	$objUsuario 	=  $_SESSION['BTK_USUARIO'];
	$idUsuario		= $objUsuario->__get('_sess_usu_id');

	$modelUsuario 	= new Data_sgausuario();
	$rpta =$modelUsuario->fu_editarClave($conexion, $id, $clave, $idUsuario, $ip);

		if($rpta > 0){
			echo 'passed';
		}
		else{ 
			echo 'failed';
		}

}
/*
 * Opciones de recepcion de variables
 */
if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';
switch ($_GET['cmd']) {
	case 'loadClave':
		loadClave($_POST['id']);break;	
	case 'editarClave':
		editarClave($_POST['id'],$_POST['con1']);break;	
    default:
        vistaPrincipal();
        break;
}
?>