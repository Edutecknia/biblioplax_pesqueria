<?php
require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

session_start();
if($_SESSION['BTK_USUARIO'] !=NULL){
echo "<script>location.href ='principal.php';</script>";   
}
 
?>
<!DOCTYPE html>
<html class="bg-black">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Facultad de Pesqueria</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <!--<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />-->
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="css/validate.css">    
	<link rel="stylesheet" href="css/Login.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini bg-black" onLoad="javascript:contenlogin();">
   
   <div id="ControlFormlogin">
   	
   </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="js/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    
    <!--JS SISTEMA INICIO-->
    <script src="js/mLogin.js"></script>
    <!--JS SISTEMA FIN-->
    
  </body>
</html>
