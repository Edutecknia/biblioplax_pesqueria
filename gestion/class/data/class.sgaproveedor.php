<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgaproveedor.php
 * */
class Data_sgaproveedor{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_PROVEEDOR_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_registrar($conexion, $numRuc, $nomProveedor, $idPais, $direcProveedor, $fonoProveedor, $mailProveedor, $webProveedor, $contactoProveedor, $idUsuario, $ip) {
		try {
			
			//$nomProveedor = strtoupper($nomProveedor);
			//$direcProveedor = strtoupper($direcProveedor);
			//$webProveedor = strtolower($webProveedor);
			//$contactoProveedor = strtoupper($contactoProveedor);

			$numRuc= "'" . trim($numRuc) . "'";

			$nomProveedor 	= utf8_decode($nomProveedor);
			$direcProveedor = utf8_decode($direcProveedor);
			$webProveedor 	= utf8_decode($webProveedor);
			$contactoProveedor = utf8_decode($contactoProveedor);
			//if(trim($numRuc)==""){$numRuc='NULL';}else{ $numRuc= "'" . trim($numRuc) . "'"  ;}
			if(trim($nomProveedor)==""){$nomProveedor='NULL';}else{ $nomProveedor= "'" . trim($nomProveedor) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($direcProveedor)==""){$direcProveedor='NULL';}else{ $direcProveedor= "'" . trim($direcProveedor) . "'"  ;}
			if(trim($fonoProveedor)==""){$fonoProveedor='NULL';}else{ $fonoProveedor= "'" . trim($fonoProveedor) . "'"  ;}
			if(trim($mailProveedor)==""){$mailProveedor='NULL';}else{ $mailProveedor= "'" . trim($mailProveedor) . "'"  ;}
			if(trim($webProveedor)==""){$webProveedor='NULL';}else{ $webProveedor= "'" . trim($webProveedor) . "'"  ;}
			if(trim($contactoProveedor)==""){$contactoProveedor='NULL';}else{ $contactoProveedor= "'" . trim($contactoProveedor) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_PROVEEDOR_REGISTRAR ($numRuc, $nomProveedor, $idPais, $direcProveedor, $fonoProveedor, $mailProveedor, $webProveedor, $contactoProveedor, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_Encontrar($conexion,$id) {
		try {
						
			$sql = "SELECT ID_PROVEEDOR,NUM_RUC,NOM_PROVEEDOR,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB,DES_CONTACTO FROM BTK_PROVEEDOR WHERE ID_PROVEEDOR = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_editar($conexion, $id, $numRuc, $nomProveedor, $idPais, $direcProveedor, $fonoProveedor, $mailProveedor, $webProveedor, $contactoProveedor, $idUsuario, $ip) {
		try {
			
			/*$nomProveedor = strtoupper($nomProveedor);
			$direcProveedor = strtoupper($direcProveedor);
			$webProveedor = strtolower($webProveedor);
			$contactoProveedor = strtoupper($contactoProveedor);*/

			$nomProveedor 	= utf8_decode($nomProveedor);
			$direcProveedor = utf8_decode($direcProveedor);
			$webProveedor 	= utf8_decode($webProveedor);
			$contactoProveedor = utf8_decode($contactoProveedor);

			if(trim($numRuc)==""){$numRuc='NULL';}else{ $numRuc= "'" . trim($numRuc) . "'"  ;}
			if(trim($nomProveedor)==""){$nomProveedor='NULL';}else{ $nomProveedor= "'" . trim($nomProveedor) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($direcProveedor)==""){$direcProveedor='NULL';}else{ $direcProveedor= "'" . trim($direcProveedor) . "'"  ;}
			if(trim($fonoProveedor)==""){$fonoProveedor='NULL';}else{ $fonoProveedor= "'" . trim($fonoProveedor) . "'"  ;}
			if(trim($mailProveedor)==""){$mailProveedor='NULL';}else{ $mailProveedor= "'" . trim($mailProveedor) . "'"  ;}
			if(trim($webProveedor)==""){$webProveedor='NULL';}else{ $webProveedor= "'" . trim($webProveedor) . "'"  ;}
			if(trim($contactoProveedor)==""){$contactoProveedor='NULL';}else{ $contactoProveedor= "'" . trim($contactoProveedor) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_PROVEEDOR_EDITAR ($id, $numRuc, $nomProveedor, $idPais, $direcProveedor, $fonoProveedor, $mailProveedor, $webProveedor, $contactoProveedor, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_PROVEEDOR_INACTIVAR '
              . '(:ID,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      			$id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_PROVEEDOR_ACTIVAR '
              . '(:ID,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      			$id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_PROVEEDOR,NUM_RUC,NOM_PROVEEDOR,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB,DES_CONTACTO FROM BTK_PROVEEDOR WHERE IND_ACTIVO = 1 ORDER BY 3";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>