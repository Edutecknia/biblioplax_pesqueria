<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sganoticia.php
 * */
class Data_sganoticia{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listarNoticia($conexion,$filtro,$indActivo,$orden,$direccion,$pagina,$idCarrera) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
			if(trim($idCarrera)==""){$idCarrera='NULL';}else{ $idCarrera= "'" . trim($idCarrera) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_NOTICIA_BIBLIOTECA_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina,$idCarrera)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarEvento($conexion,$filtro,$indActivo,$orden,$direccion,$pagina,$idCarrera) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
			if(trim($idCarrera)==""){$idCarrera='NULL';}else{ $idCarrera= "'" . trim($idCarrera) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_EVENTO_BIBLIOTECA_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina,$idCarrera)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrar($conexion, $nomNoticiaEvento, $fechaEvento, $descripcion, $indTipo, $idFacultad, $idUsuario, $ip) {
		try {
			
			$nomNoticiaEvento = utf8_decode($nomNoticiaEvento);
			$descripcion = utf8_decode($descripcion);

			if(trim($nomNoticiaEvento)==""){$nomNoticiaEvento='NULL';}else{ $nomNoticiaEvento= "'" . trim($nomNoticiaEvento) . "'"  ;}
			if(trim($fechaEvento)==""){$fechaEvento='NULL';}else{ $fechaEvento= "'" . trim($fechaEvento) . "'"  ;}
			
			if(trim($descripcion)==""){$descripcion='NULL';}else{ $descripcion= "'" . trim($descripcion) . "'"  ;}
			if(trim($indTipo)==""){$indTipo='NULL';}else{ $indTipo= "'" . trim($indTipo) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_NOTICIA_EVENTO_BIBLIOTECA_REGISTRAR ($nomNoticiaEvento, $fechaEvento, $descripcion, $indTipo, $idFacultad, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_encontrar($conexion,$id) {
		try {
							
			$sql 	= "SELECT N.ID_NOTICIA_EVENTO, N.cod_escuela, N.NOM_NOTICIA_EVENTO, N.FECHA_NOTICIA_EVENTO, N.DES_NOTICIA_EVENTO, N.NOM_IMG_PRINCIPAL, N.IND_ACTIVO, ".
						" N.FEC_REGISTRO, U.NOM_USUARIO, U.APE_USUARIO ".
						" FROM BTK_NOTICIA_EVENTO_BIBLIOTECA N INNER JOIN BTK_USUARIO U ON N.ID_USUARIO_REGISTRO = U.ID_USUARIO ".
						" WHERE N.ID_NOTICIA_EVENTO = ".$id;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetch();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_editar($conexion, $id, $nomNoticiaEvento, $fechaEvento, $descripcion, $indTipo, $idFacultad, $idUsuario, $ip) {
		try {
			
			$nomNoticiaEvento = utf8_decode($nomNoticiaEvento);
			$descripcion = utf8_decode($descripcion);

			if(trim($nomNoticiaEvento)==""){$nomNoticiaEvento='NULL';}else{ $nomNoticiaEvento= "'" . trim($nomNoticiaEvento) . "'"  ;}
			if(trim($fechaEvento)==""){$fechaEvento='NULL';}else{ $fechaEvento= "'" . trim($fechaEvento) . "'"  ;}
			
			if(trim($descripcion)==""){$descripcion='NULL';}else{ $descripcion= "'" . trim($descripcion) . "'"  ;}
			if(trim($indTipo)==""){$indTipo='NULL';}else{ $indTipo= "'" . trim($indTipo) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_NOTICIA_EVENTO_BIBLIOTECA_EDITAR ($id, $nomNoticiaEvento, $fechaEvento, $descripcion, $indTipo, $idFacultad, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_inactivar($conexion, $id, $idUsuario, $ip) {
		try {
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_NOTICIA_EVENTO_BIBLIOTECA_INACTIVAR ($id, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_activar($conexion, $id, $idUsuario, $ip) {
		try {
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_NOTICIA_EVENTO_BIBLIOTECA_ACTIVAR ($id, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarTodoNoticia($conexion,$idCarrera) {
		try {
							
			$sql 	= "SELECT N.ID_NOTICIA_EVENTO, N.cod_escuela, N.NOM_NOTICIA_EVENTO, N.FECHA_NOTICIA_EVENTO, N.DES_NOTICIA_EVENTO, N.NOM_IMG_PRINCIPAL, N.IND_ACTIVO, ".
						" N.FEC_REGISTRO, U.NOM_USUARIO, U.APE_USUARIO ".
						" FROM BTK_NOTICIA_EVENTO_BIBLIOTECA N INNER JOIN BTK_USUARIO U ON N.ID_USUARIO_REGISTRO = U.ID_USUARIO ".
						" WHERE N.cod_escuela = ".$idCarrera." AND N.IND_TIPO = 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarTodoEventos($conexion,$idCarrera) {
		try {
							
			$sql 	= "SELECT N.ID_NOTICIA_EVENTO, N.cod_escuela, N.NOM_NOTICIA_EVENTO, N.FECHA_NOTICIA_EVENTO, N.DES_NOTICIA_EVENTO, N.NOM_IMG_PRINCIPAL, N.IND_ACTIVO, ".
						" N.FEC_REGISTRO, U.NOM_USUARIO, U.APE_USUARIO ".
						" FROM BTK_NOTICIA_EVENTO_BIBLIOTECA N INNER JOIN BTK_USUARIO U ON N.ID_USUARIO_REGISTRO = U.ID_USUARIO ".
						" WHERE N.cod_escuela = ".$idCarrera." AND N.IND_TIPO = 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

}
?>