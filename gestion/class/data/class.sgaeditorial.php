<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgaeditorial.php
 * */
class Data_sgaeditorial{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_EDITORIAL_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrar($conexion, $nomEditorial, $idPais, $direcEditorial, $fonoEditorial, $mailEditorial, $webEditorial, $idUsuario, $ip) {
		try {
			
			/*$nomEditorial = strtoupper($nomEditorial);
			$direcEditorial = strtoupper($direcEditorial);
			$webEditorial = strtolower($webEditorial);*/

			$nomEditorial = utf8_decode($nomEditorial);
			$direcEditorial = utf8_decode($direcEditorial);
			$webEditorial = utf8_decode($webEditorial);

			if(trim($nomEditorial)==""){$nomEditorial='NULL';}else{ $nomEditorial= "'" . trim($nomEditorial) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($direcEditorial)==""){$direcEditorial='NULL';}else{ $direcEditorial= "'" . trim($direcEditorial) . "'"  ;}
			if(trim($fonoEditorial)==""){$fonoEditorial='NULL';}else{ $fonoEditorial= "'" . trim($fonoEditorial) . "'"  ;}
			if(trim($mailEditorial)==""){$mailEditorial='NULL';}else{ $mailEditorial= "'" . trim($mailEditorial) . "'"  ;}
			if(trim($webEditorial)==""){$webEditorial='NULL';}else{ $webEditorial= "'" . trim($webEditorial) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_EDITORIAL_REGISTRAR ($nomEditorial, $idPais, $direcEditorial, $fonoEditorial, $mailEditorial, $webEditorial, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Encontrar($conexion,$id) {
		try {
						
			$sql = "SELECT ID_EDITORIAL,NOM_EDITORIAL,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB FROM BTK_EDITORIAL WHERE ID_EDITORIAL = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_editar($conexion, $id, $nomEditorial, $idPais, $direcEditorial, $fonoEditorial, $mailEditorial, $webEditorial, $idUsuario, $ip) {
		try {
			
			/*$nomEditorial = strtoupper($nomEditorial);
			$direcEditorial = strtoupper($direcEditorial);
			$webEditorial = strtolower($webEditorial);*/

			$nomEditorial = utf8_decode($nomEditorial);
			$direcEditorial = utf8_decode($direcEditorial);
			$webEditorial = utf8_decode($webEditorial);

			if(trim($nomEditorial)==""){$nomEditorial='NULL';}else{ $nomEditorial= "'" . trim($nomEditorial) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($direcEditorial)==""){$direcEditorial='NULL';}else{ $direcEditorial= "'" . trim($direcEditorial) . "'"  ;}
			if(trim($fonoEditorial)==""){$fonoEditorial='NULL';}else{ $fonoEditorial= "'" . trim($fonoEditorial) . "'"  ;}
			if(trim($mailEditorial)==""){$mailEditorial='NULL';}else{ $mailEditorial= "'" . trim($mailEditorial) . "'"  ;}
			if(trim($webEditorial)==""){$webEditorial='NULL';}else{ $webEditorial= "'" . trim($webEditorial) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_EDITORIAL_EDITAR ($id, $nomEditorial, $idPais, $direcEditorial, $fonoEditorial, $mailEditorial, $webEditorial, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_EDITORIAL_INACTIVAR '
              . '(:ID_EDITORIAL,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_EDITORIAL',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_EDITORIAL_ACTIVAR '
              . '(:ID_EDITORIAL,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_EDITORIAL',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_EDITORIAL,NOM_EDITORIAL,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB FROM BTK_EDITORIAL WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>