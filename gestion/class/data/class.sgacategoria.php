<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgacategoria.php
 * */
class Data_sgacategoria{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listarCategoria($conexion,$filtro,$indActivo,$orden,$direccion,$pagina) {
		try {
			
			$paginado = TAM_PAG_LISTADO;
			
			$filtro = utf8_decode($filtro);

			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_CATEGORIA_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

/*public function fu_registrarCategoriaXX($conexion,$nombre, $idUsuario, $ip) {
		try {
			
			$nombre = strtoupper($nombre);

			$stmt = $conexion->prepare(
                'EXECUTE USP_BTK_CATEGORIA_REGISTRAR '
              . ':NOM_CATEGORIA, :ID_USUARIO, :IP '
       		 );
        	
        	$stmt->bindParam(':NOM_CATEGORIA',      $nombre,     PDO::PARAM_STR);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
			
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}*/

public function fu_registrarCategoria($conexion,$nombre, $idUsuario, $ip) {
		try {

			/*$nombre = strtoupper($nombre);*/
			
			$nombre = utf8_decode($nombre);

			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
			
			$sql = "CALL USP_BTK_CATEGORIA_REGISTRAR ($nombre,$idUsuario,$ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_EncontrarCategoria($conexion,$idCategoria) {
		try {
						
			$sql = "SELECT ID_CATEGORIA,NOM_CATEGORIA FROM BTK_CATEGORIA WHERE ID_CATEGORIA = ".$idCategoria;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_editarCategoria($conexion,$id, $nombre, $idUsuario, $ip) {
		try {
			//$nombre = strtoupper($nombre);

			$nombre = utf8_decode($nombre);
			/*$encod = mb_detect_encoding($nombre, 'UTF-8, ISO-8859-1');
					if($encod=='ISO-8859-1'){
					   $nombre = utf8_encode($nombre);
				}
				else{
					$nombre = utf8_decode($nombre);
				}
			*/

			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
			
			$sql = "CALL USP_BTK_CATEGORIA_EDITAR ($id,$nombre,$idUsuario,$ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

			/*$nombre = strtoupper($nombre);

			$stmt = $conexion->prepare(
                'EXECUTE USP_BTK_CATEGORIA_EDITAR '
              . ':ID_CATEGORIA, :NOM_CATEGORIA, :ID_USUARIO, :IP '
       		 );
        	
        	$stmt->bindParam(':ID_CATEGORIA',      $id,     	PDO::PARAM_INT);
        	$stmt->bindParam(':NOM_CATEGORIA',      $nombre,       PDO::PARAM_STR);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
	
		return $var;*/
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_inactivarCategoria($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_CATEGORIA_INACTIVAR '
              . '(:ID_CATEGORIA,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_CATEGORIA',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activarCategoria($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_CATEGORIA_ACTIVAR '
              . '(:ID_CATEGORIA,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_CATEGORIA',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodoCategoria($conexion) {
		try {
			
			$sql 	= "SELECT ID_CATEGORIA,NOM_CATEGORIA FROM BTK_CATEGORIA WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>