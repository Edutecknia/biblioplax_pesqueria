<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgadashboard.php
 * */
class Data_sgadashboard{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_grafico_1($conexion, $idFacultad) {
		try {
			
			$sql 	= "CALL USP_BTK_GRAFICO_1 $idFacultad";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_grafico_2($conexion) {
		try {
			
			$sql 	= "SELECT ID_PRODUCTO,NOM_PRODUCTO,SUM(IMP_TOTAL) AS IMP_TOTAL ".
						" FROM TRANSACCION ".
						" WHERE CONVERT(FEC_VENTA,DATE) = CONVERT(NOW(),DATE) ".
						" GROUP BY ID_PRODUCTO,NOM_PRODUCTO ".
						" ORDER BY IMP_TOTAL ASC ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

}
?>