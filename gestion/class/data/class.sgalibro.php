<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgalibro.php
 * */
class Data_sgalibro{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina,$idFacultad, $codLibro, $desEstado) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			if(trim($desEstado)==""){$desEstado='NULL';}else{ $desEstado= "'" . trim($desEstado) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_LIBRO_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina,$idFacultad, $codLibro, $desEstado)";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_registrar($conexion, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, 
							$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro,$temasRelacion, $idUsuario, $ip) {
		try {
			
			//$desTitulo = strtoupper($desTitulo);

			$desTitulo = utf8_decode($desTitulo);

			if(trim($idLibroTipo)==""){$idLibroTipo='NULL';}else{ $idLibroTipo= "'" . trim($idLibroTipo) . "'"  ;}
			if(trim($desTitulo)==""){$desTitulo='NULL';}else{ $desTitulo= "'" . trim($desTitulo) . "'"  ;}
			if(trim($idAutor)==""){$idAutor='NULL';}else{ $idAutor= "'" . trim($idAutor) . "'"  ;}
			if(trim($idEditorial)==""){$idEditorial='NULL';}else{ $idEditorial= "'" . trim($idEditorial) . "'"  ;}
			if(trim($anioPublica)==""){$anioPublica='NULL';}else{ $anioPublica= "'" . trim($anioPublica) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($numPagina)==""){$numPagina='NULL';}else{ $numPagina= "'" . trim($numPagina) . "'"  ;}
			if(trim($numVolumen)==""){$numVolumen='NULL';}else{ $numVolumen= "'" . trim($numVolumen) . "'"  ;}
			if(trim($idAdquisicion)==""){$idAdquisicion='NULL';}else{ $idAdquisicion= "'" . trim($idAdquisicion) . "'"  ;}
			if(trim($idProveedor)==""){$idProveedor='NULL';}else{ $idProveedor= "'" . trim($idProveedor) . "'"  ;}
			if(trim($fecAdquisicion)==""){$fecAdquisicion='NULL';}else{ $fecAdquisicion= "'" . trim($fecAdquisicion) . "'"  ;}
			if(trim($preLibro)==""){$preLibro='NULL';}else{ $preLibro= "'" . trim($preLibro) . "'"  ;}
			if(trim($temasRelacion)==""){$temasRelacion='NULL';}else{ $temasRelacion= "'" . trim($temasRelacion) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_REGISTRAR ($idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, ".
					"$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, $temasRelacion, $idUsuario, $ip)";
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_registrarEjemplar($conexion, $idLibro, $isbn, $codLibro, $codBarra, $codPatrimonio, $idUsuario, $ip) {
		try {
			
			if($isbn == 'undefined'){ $isbn = '';}

			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($isbn)==""){$isbn='NULL';}else{ $isbn= "'" . trim($isbn) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			if(trim($codBarra)==""){$codBarra='NULL';}else{ $codBarra= "'" . trim($codBarra) . "'"  ;}
			if(trim($codPatrimonio)==""){$codPatrimonio='NULL';}else{ $codPatrimonio= "'" . trim($codPatrimonio) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_EJEMPLAR_REGISTRAR ($idLibro, $isbn, $codLibro, $codBarra, $codPatrimonio, $idUsuario, $ip )";
		
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_eliminarEjemplar($conexion, $idLibro, $codLibro, $idUsuario, $ip) {
		try {
			
			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_EJEMPLAR_ELIMINAR ($idLibro, $codLibro, $idUsuario, $ip )";
		
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrarAutor($conexion, $idLibro, $idAutor, $idUsuario, $ip) {
		try {
					
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_AUTOR_REGISTRAR ($idLibro, $idAutor, $idUsuario, $ip) ";

			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Encontrar($conexion,$id) {
		try {
						
			$sql = "SELECT ID_LIBRO,ID_CARRERA,ID_LIBRO_TIPO,DES_TITULO,ID_EDITORIAL,NUM_ANIO_PUBLICA,ID_PAIS,NUM_PAGINA,NUM_VOLUMEN,".
					" ID_LIBRO_ADQUISICION,ID_PROVEEDOR,FEC_ADQUISICION,PRE_LIBRO,DES_CATEGORIA_RELACION FROM BTK_LIBRO WHERE ID_LIBRO = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_EncontrarEjemplares($conexion,$id) {
		try {
						
			$sql = "SELECT COD_ISBM,COD_LIBRO,COD_BARRA,DES_ESTADO,COD_PATRIMONIAL FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO = ".$id." AND IND_ACTIVO = 1";
			$stm = $conexion->query($sql);
			$result = $stm->fetchAll();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarAutores($conexion,$id) {
		try {
						
			$sql = "SELECT L.ID_AUTOR,NOM_AUTOR FROM BTK_LIBRO_AUTOR L INNER JOIN BTK_AUTOR A ON L.ID_AUTOR = A.ID_AUTOR WHERE L.ID_LIBRO = ".$id." AND L.IND_ACTIVO = 1";

			$stm = $conexion->query($sql);
			$result = $stm->fetchAll();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_editar($conexion, $idLibro, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, 
							$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro,$temasRelacion, $idUsuario, $ip) {
		try {
			
			//$desTitulo = strtoupper($desTitulo);
			$desTitulo = utf8_decode($desTitulo);

			if(trim($idLibroTipo)==""){$idLibroTipo='NULL';}else{ $idLibroTipo= "'" . trim($idLibroTipo) . "'"  ;}
			if(trim($desTitulo)==""){$desTitulo='NULL';}else{ $desTitulo= "'" . trim($desTitulo) . "'"  ;}
			if(trim($idAutor)==""){$idAutor='NULL';}else{ $idAutor= "'" . trim($idAutor) . "'"  ;}
			if(trim($idEditorial)==""){$idEditorial='NULL';}else{ $idEditorial= "'" . trim($idEditorial) . "'"  ;}
			if(trim($anioPublica)==""){$anioPublica='NULL';}else{ $anioPublica= "'" . trim($anioPublica) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($numPagina)==""){$numPagina='NULL';}else{ $numPagina= "'" . trim($numPagina) . "'"  ;}
			if(trim($numVolumen)==""){$numVolumen='NULL';}else{ $numVolumen= "'" . trim($numVolumen) . "'"  ;}
			if(trim($idAdquisicion)==""){$idAdquisicion='NULL';}else{ $idAdquisicion= "'" . trim($idAdquisicion) . "'"  ;}
			if(trim($idProveedor)==""){$idProveedor='NULL';}else{ $idProveedor= "'" . trim($idProveedor) . "'"  ;}
			if(trim($fecAdquisicion)==""){$fecAdquisicion='NULL';}else{ $fecAdquisicion= "'" . trim($fecAdquisicion) . "'"  ;}
			if(trim($preLibro)==""){$preLibro='NULL';}else{ $preLibro= "'" . trim($preLibro) . "'"  ;}
			if(trim($temasRelacion)==""){$temasRelacion='NULL';}else{ $temasRelacion= "'" . trim($temasRelacion) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_EDITAR ($idLibro, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, ".
					"$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, $temasRelacion, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrarAdjunto($conexion, $idLibro, $idArchivoTipo, $desArchivo, $idUsuario, $ip) {
		try {
			
			$desArchivo = utf8_decode($desArchivo);

			if(trim($desArchivo)==""){$desArchivo='NULL';}else{ $desArchivo= "'" . trim($desArchivo) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_ARCHIVO_REGISTRAR ($idLibro, $idArchivoTipo, $desArchivo, $idUsuario, $ip )";
            echo $sql;
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarAdjunto($conexion,$idLibro) {
		try {
			
			
			$sql 	= "SELECT A.ID_LIBRO_ARCHIVO,A.DES_ARCHIVO,T.NOM_ARCHIVO_TIPO,A.NOM_ARCHIVO,A.ID_ARCHIVO_TIPO ".
					 " FROM BTK_LIBRO_ARCHIVO A INNER JOIN BTK_ARCHIVO_TIPO T ON A.ID_ARCHIVO_TIPO = T.ID_ARCHIVO_TIPO WHERE A.ID_LIBRO = ".$idLibro." AND A.IND_ACTIVO = 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}

			$sql = "CALL USP_BTK_LIBRO_INACTIVAR ($id, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();

			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_LIBRO_ACTIVAR '
              . '(:ID,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      			$id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

		public function fu_eliminarAdjunto($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_LIBRO_ARCHIVO_ELIMINAR '
              . '(:ID,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      			$id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_PROVEEDOR,NUM_RUC,NOM_PROVEEDOR,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB,DES_CONTACTO FROM BTK_PROVEEDOR WHERE IND_ACTIVO = 1 ORDER BY 3";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarLibroTipo($conexion) {
		try {
			
			$sql 	= "SELECT ID_LIBRO_TIPO,DES_LIBRO_TIPO FROM BTK_LIBRO_TIPO WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarLibroAdquisicion($conexion) {
		try {
			
			$sql 	= "SELECT ID_LIBRO_ADQUISICION,DES_LIBRO_ADQUISICION FROM BTK_LIBRO_ADQUISICION WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarArchivoTipo($conexion) {
		try {
			
			$sql 	= "SELECT ID_ARCHIVO_TIPO,NOM_ARCHIVO_TIPO FROM BTK_ARCHIVO_TIPO WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_validarLibro($conexion, $idFacultad, $nomLibro, $idAutor, $idEditorial) {
		try {
			
			$nomLibro 			= utf8_decode($nomLibro);						
			$cadena_autor 		= '';
			$cadena_autor2 		= '';
			$cadena_editorial 	= '';

			if($idAutor != ''){

				$cadena_autor2 	= '0';

				for($i = 0; $i < count($idAutor); $i++){
					
					$cadena_autor2 	.= ','.$idAutor[$i];
				}

				$cadena_autor = ' AND A.ID_AUTOR IN('.$cadena_autor2.') ';
			}

			if($idEditorial != ''){
				$cadena_editorial = ' AND L.ID_EDITORIAL = '.$idEditorial;
			}
			else{
				$cadena_editorial = ' AND L.ID_EDITORIAL IS NULL ';	
			}

			$sql = "SELECT COUNT(*) AS CANTIDAD FROM BTK_LIBRO L LEFT JOIN BTK_LIBRO_AUTOR A ON L.ID_LIBRO = A.ID_LIBRO ".
					" WHERE L.ID_CARRERA = ".$idFacultad." AND L.DES_TITULO  = "."'".$nomLibro."'".
					$cadena_editorial.$cadena_autor." AND L.IND_ACTIVO = 1 ";
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarLibroValidar($conexion, $idFacultad, $nomLibro, $idAutor, $idEditorial) {
		try {
			
			$nomLibro 			= utf8_decode($nomLibro);
			$cadena_autor 		= '';
			$cadena_autor2 		= '';
			$cadena_editorial 	= '';

			if($idAutor != ''){

				$cadena_autor2 	= '0';

				for($i = 0; $i < count($idAutor); $i++){
					
					$cadena_autor2 	.= ','.$idAutor[$i];
				}

				$cadena_autor = ' AND A.ID_AUTOR IN('.$cadena_autor2.') ';
			}

			if($idEditorial != ''){
				$cadena_editorial = ' AND L.ID_EDITORIAL = '.$idEditorial;
			}
			else{
				$cadena_editorial = ' AND L.ID_EDITORIAL IS NULL ';	
			}

			$sql = "SELECT L.ID_LIBRO, L.DES_TITULO, L.NUM_ANIO_PUBLICA,L.NUM_PAGINA, L.NUM_VOLUMEN, E.NOM_EDITORIAL, T.NOM_AUTOR  ".
					" FROM BTK_LIBRO L ".
					" LEFT JOIN BTK_EDITORIAL E ON L.ID_EDITORIAL = E.ID_EDITORIAL ".
					" LEFT JOIN BTK_LIBRO_AUTOR A ON L.ID_LIBRO = A.ID_LIBRO ".
					" LEFT JOIN BTK_AUTOR T ON T.ID_AUTOR = A.ID_AUTOR ".
					" WHERE L.ID_CARRERA = ".$idFacultad." AND L.DES_TITULO  = "."'".$nomLibro."'".
					$cadena_editorial.$cadena_autor." AND L.IND_ACTIVO = 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_EncontrarEjemplarCodLibro($conexion,$idLibro, $codLibro) {
		try {
						
			$sql = "SELECT COD_ISBM,COD_LIBRO,COD_BARRA,DES_ESTADO,COD_PATRIMONIAL FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO = ".$idLibro." AND COD_LIBRO = ".$codLibro." AND IND_ACTIVO = 1";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_editarEjemplar($conexion, $codLibroOld, $idLibro, $codisbn, $codLibro, $codPatrimonio, $idUsuario, $ip) {
		try {
			
			if(trim($codLibroOld)==""){$codLibroOld='NULL';}else{ $codLibroOld= "'" . trim($codLibroOld) . "'"  ;}
			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($codisbn)==""){$codisbn='NULL';}else{ $codisbn= "'" . trim($codisbn) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			if(trim($codPatrimonio)==""){$codPatrimonio='NULL';}else{ $codPatrimonio= "'" . trim($codPatrimonio) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


$sql = "CALL USP_BTK_LIBRO_EJEMPLAR_EDITAR  ($idLibro, $codLibroOld, $codisbn, $codLibro, $codPatrimonio, $idUsuario, $ip )";
		
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarLibroEstado($conexion) {
		try {
			
			$sql 	= "SELECT ID_LIBRO_ESTADO, DES_ESTADO  FROM BTK_LIBRO_ESTADO WHERE IND_ACTIVO = 1 ORDER BY 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

}
?>