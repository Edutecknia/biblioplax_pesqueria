<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgasugerencia.php
 * */
class Data_sgasugerencia{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina, $idUsuario, $idCarrera, $tipo) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($idCarrera)==""){$idCarrera='NULL';}else{ $idCarrera= "'" . trim($idCarrera) . "'"  ;}
			if(trim($tipo)==""){$tipo='NULL';}else{ $tipo= "'" . trim($tipo) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_SUGERENCIA_QUEJA_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina, $idUsuario, $idCarrera, $tipo)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrar($conexion, $idFacultad, $nomSugerencia, $detSugerencia, $tipo, $idUsuario, $ip) {
		try {
			
			$nomSugerencia = utf8_decode($nomSugerencia);
			$detSugerencia = utf8_decode($detSugerencia);

			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($nomSugerencia)==""){$nomSugerencia='NULL';}else{ $nomSugerencia= "'" . trim($nomSugerencia) . "'"  ;}
			if(trim($detSugerencia)==""){$detSugerencia='NULL';}else{ $detSugerencia= "'" . trim($detSugerencia) . "'"  ;}
			if(trim($tipo)==""){$tipo='NULL';}else{ $tipo= "'" . trim($tipo) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_SUGERENCIA_QUEJA_REGISTRAR  ($idFacultad, $nomSugerencia, $detSugerencia, $tipo, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Encontrar($conexion,$id) {
		try {

			$sql = "SELECT C.ID_SUGERENCIA, C.DES_TIPO,	C.cod_escuela, C.NOM_SUGERENCIA_QUEJA, C.DES_SUGERENCIA_QUEJA, C.ID_USUARIO_RESPONDE, ".
					" C.DES_RPTA_SUGERENCIA_QUEJA, C.FEC_RESPUESTA, C.IND_VISTO, A.nom_escuela, C.FEC_REGISTRO ".
					" FROM BTK_SUGERENCIA_QUEJA C ".
					" LEFT JOIN escuela A ON C.cod_escuela = A.cod_escuela WHERE C.ID_SUGERENCIA = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_visualizarSugerencia($conexion, $id, $idUsuario, $ip) {
		try {
			
			$detRpta = utf8_decode($detRpta);

			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_SUGERENCIA_QUEJA_RPTA_VISUALIZAR  ($id, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_grabarRpta($conexion, $id, $detRpta, $idUsuario, $ip) {
		try {
			
			$detRpta = utf8_decode($detRpta);

			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($detRpta)==""){$detRpta='NULL';}else{ $detRpta= "'" . trim($detRpta) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_SUGERENCIA_QUEJA_RPTA_REGISTRAR  ($id, $detRpta, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_EDITORIAL_INACTIVAR '
              . '(:ID_EDITORIAL,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_EDITORIAL',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_EDITORIAL_ACTIVAR '
              . '(:ID_EDITORIAL,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_EDITORIAL',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_EDITORIAL,NOM_EDITORIAL,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB FROM BTK_EDITORIAL WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>