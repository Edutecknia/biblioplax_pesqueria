<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgaencuesta.php
 * */
class Data_sgaencuesta{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina, $idCarrera) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
			if(trim($idCarrera)==""){$idCarrera='NULL';}else{ $idCarrera= "'" . trim($idCarrera) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_ENCUESTA_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina, $idCarrera)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrar($conexion, $idFacultad, $nomEncuesta, $pregunta_1, $tipo_1, $opcion_1, 
							$pregunta_2, $tipo_2, $opcion_2, 
							$pregunta_3, $tipo_3, $opcion_3,
							$pregunta_4, $tipo_4, $opcion_4,
							$pregunta_5, $tipo_5, $opcion_5,
							$idUsuario, $ip) {
		try {
			
			$nomEncuesta = utf8_decode($nomEncuesta);
			
			$pregunta_1 = utf8_decode($pregunta_1);
			$opcion_1 = utf8_decode($opcion_1);

			$pregunta_2 = utf8_decode($pregunta_2);
			$opcion_2 = utf8_decode($opcion_2);

			$pregunta_3 = utf8_decode($pregunta_3);
			$opcion_3 = utf8_decode($opcion_3);

			$pregunta_4 = utf8_decode($pregunta_4);
			$opcion_4 = utf8_decode($opcion_4);

			$pregunta_5 = utf8_decode($pregunta_5);
			$opcion_5 = utf8_decode($opcion_5);

			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($nomEncuesta)==""){$nomEncuesta='NULL';}else{ $nomEncuesta= "'" . trim($nomEncuesta) . "'"  ;}
			
			if(trim($pregunta_1)==""){$pregunta_1='NULL';}else{ $pregunta_1= "'" . trim($pregunta_1) . "'"  ;}
			if(trim($tipo_1)==""){$tipo_1='NULL';}else{ $tipo_1= "'" . trim($tipo_1) . "'"  ;}
			if(trim($opcion_1)==""){$opcion_1='NULL';}else{ $opcion_1= "'" . trim($opcion_1) . "'"  ;}

			if(trim($pregunta_2)==""){$pregunta_2='NULL';}else{ $pregunta_2= "'" . trim($pregunta_2) . "'"  ;}
			if(trim($tipo_2)==""){$tipo_2='NULL';}else{ $tipo_2= "'" . trim($tipo_2) . "'"  ;}
			if(trim($opcion_2)==""){$opcion_2='NULL';}else{ $opcion_2= "'" . trim($opcion_2) . "'"  ;}

			if(trim($pregunta_3)==""){$pregunta_3='NULL';}else{ $pregunta_3= "'" . trim($pregunta_3) . "'"  ;}
			if(trim($tipo_3)==""){$tipo_3='NULL';}else{ $tipo_3= "'" . trim($tipo_3) . "'"  ;}
			if(trim($opcion_3)==""){$opcion_3='NULL';}else{ $opcion_3= "'" . trim($opcion_3) . "'"  ;}

			if(trim($pregunta_4)==""){$pregunta_4='NULL';}else{ $pregunta_4= "'" . trim($pregunta_4) . "'"  ;}
			if(trim($tipo_4)==""){$tipo_4='NULL';}else{ $tipo_4= "'" . trim($tipo_4) . "'"  ;}
			if(trim($opcion_4)==""){$opcion_4='NULL';}else{ $opcion_4= "'" . trim($opcion_4) . "'"  ;}

			if(trim($pregunta_5)==""){$pregunta_5='NULL';}else{ $pregunta_5= "'" . trim($pregunta_5) . "'"  ;}
			if(trim($tipo_5)==""){$tipo_5='NULL';}else{ $tipo_5= "'" . trim($tipo_5) . "'"  ;}
			if(trim($opcion_5)==""){$opcion_5='NULL';}else{ $opcion_5= "'" . trim($opcion_5) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_ENCUESTA_REGISTRAR  ($idFacultad, $nomEncuesta, $pregunta_1, $tipo_1, $opcion_1, $pregunta_2, $tipo_2, $opcion_2, $pregunta_3, $tipo_3, $opcion_3,$pregunta_4, $tipo_4, $opcion_4,$pregunta_5, $tipo_5, $opcion_5, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Encontrar($conexion,$id) {
		try {

			$sql = "SELECT C.ID_SUGERENCIA, C.DES_TIPO,	C.cod_escuela, C.NOM_SUGERENCIA_QUEJA, C.DES_SUGERENCIA_QUEJA, C.ID_USUARIO_RESPONDE, ".
					" C.DES_RPTA_SUGERENCIA_QUEJA, C.FEC_RESPUESTA, C.IND_VISTO, A.cod_escuela, C.FEC_REGISTRO ".
					" FROM BTK_SUGERENCIA_QUEJA C ".
					" LEFT JOIN escuela A ON C.cod_escuela = A.cod_escuela WHERE C.ID_SUGERENCIA = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_visualizarSugerencia($conexion, $id, $idUsuario, $ip) {
		try {
			
			$detRpta = utf8_decode($detRpta);

			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_SUGERENCIA_QUEJA_RPTA_VISUALIZAR  ($id, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_grabarRpta($conexion, $id, $detRpta, $idUsuario, $ip) {
		try {
			
			$detRpta = utf8_decode($detRpta);

			if(trim($id)==""){$id='NULL';}else{ $id= "'" . trim($id) . "'"  ;}
			if(trim($detRpta)==""){$detRpta='NULL';}else{ $detRpta= "'" . trim($detRpta) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_SUGERENCIA_QUEJA_RPTA_REGISTRAR  ($id, $detRpta, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_EDITORIAL_INACTIVAR '
              . ':ID_EDITORIAL,  :ID_USUARIO, :IP '
       		 );
        	
        	$stmt->bindParam(':ID_EDITORIAL',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_EDITORIAL_ACTIVAR '
              . ':ID_EDITORIAL,  :ID_USUARIO, :IP '
       		 );
        	
        	$stmt->bindParam(':ID_EDITORIAL',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_EDITORIAL,NOM_EDITORIAL,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB FROM BTK_EDITORIAL WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>