<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgaexportar.php
 * */
class Data_sgaexportar{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_reporte_1($conexion, $filtro, $indActivo, $orden, $direccion, $idFacultad, $codLibro, $desEstado) {
		try {
			
			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			if(trim($desEstado)==""){$desEstado='NULL';}else{ $desEstado= "'" . trim($desEstado) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_EXPORTA_1 ($filtro,$indActivo,$orden,$direccion,$idFacultad, $codLibro, $desEstado)";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_libroAutorListar($conexion) {
		try {
						
			$sql 	= "SELECT LA.ID_LIBRO, A.ID_AUTOR, A.NOM_AUTOR, L.DES_TITULO ".
						" FROM BTK_LIBRO L ".
						" INNER JOIN BTK_LIBRO_AUTOR LA ON L.ID_LIBRO = LA.ID_LIBRO ".
						" INNER JOIN BTK_AUTOR A ON A.ID_AUTOR = LA.ID_AUTOR ".
						" WHERE LA.IND_ACTIVO = 1 ".
						" ORDER BY LA.ID_LIBRO,A.ID_AUTOR ";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_libroAutorxIdLibro($conexion, $idLibro) {
		try {
						
			$sql 	= "SELECT LA.ID_LIBRO, A.ID_AUTOR, A.NOM_AUTOR ".
						" FROM  ".
						" BTK_LIBRO_AUTOR LA ".
						" INNER JOIN BTK_AUTOR A ON A.ID_AUTOR = LA.ID_AUTOR ".
						" WHERE LA.ID_LIBRO = ".$idLibro." AND LA.IND_ACTIVO = 1 ";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_categoriaListar($conexion, $idRelacion) {
		try {
						
			$sql 	= "SELECT ID_CATEGORIA, NOM_CATEGORIA FROM BTK_CATEGORIA WHERE ID_CATEGORIA IN (".$idRelacion.") AND IND_ACTIVO = 1 ";
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_reporte_2($conexion, $filtro, $indActivo, $indCachimbo) {
		try {
			
			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
						
			$sql 	= "CALL USP_BTK_EXPORTA_2 ($filtro,$indActivo,$indCachimbo)";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_reporte_3($conexion, $fec_inicio, $fec_fin, $idCarrera) {
		try {
			
			$fecha          = @explode('/',$fec_inicio);
    		$dia            = $fecha[0];
    		$mes            = $fecha[1];
    		$anio           = $fecha[2];
    		$fec_inicio     = $anio.'-'.$mes.'-'.$dia;

    		$fecha          = @explode('/',$fec_fin);
    		$dia            = $fecha[0];
    		$mes            = $fecha[1];
    		$anio           = $fecha[2];
    		$fec_fin        = $anio.'-'.$mes.'-'.$dia;

    		$fec_inicio		= "'" . trim($fec_inicio) . "'"  ;
    		$fec_fin		= "'" . trim($fec_fin) . "'"  ;
						
			$sql 	= "CALL USP_BTK_REPORTE_PRESTAMO_MATERIAL ($idCarrera,$fec_inicio,$fec_fin)";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_reporte_3_cantidad($conexion, $fec_inicio, $fec_fin, $idCarrera) {
		try {
			
			$fecha          = @explode('/',$fec_inicio);
    		$dia            = $fecha[0];
    		$mes            = $fecha[1];
    		$anio           = $fecha[2];
    		$fec_inicio     = $anio.'-'.$mes.'-'.$dia;

    		$fecha          = @explode('/',$fec_fin);
    		$dia            = $fecha[0];
    		$mes            = $fecha[1];
    		$anio           = $fecha[2];
    		$fec_fin        = $anio.'-'.$mes.'-'.$dia;

    		$fec_inicio		= "'" . trim($fec_inicio) . "'"  ;
    		$fec_fin		= "'" . trim($fec_fin) . "'"  ;
						
			$sql 	= "CALL USP_BTK_REPORTE_PRESTAMO_MATERIAL_CANTIDAD_FACULTDAD ($idCarrera,$fec_inicio,$fec_fin)";

			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>