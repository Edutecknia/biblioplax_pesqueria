<?php

class Data_DB {

    public static function conn($dbMotor='', $hostname='', $userBd='', $passwordBd='', $nameBd='')
    {
      
        $dbMotor    = $_SESSION['pDB']['dbType'];
        $hostname   = $_SESSION['pDB']['dbHost'];
        $userBd     = $_SESSION['pDB']['dbUser'];
        $passwordBd = $_SESSION['pDB']['dbPass'];
        $nameBd     = $_SESSION['pDB']['dbName'];
        
        $return_value = null;
        try{
             $host = '';
            if ($dbMotor == 'MSSQL'):
                $host .= "mssql:host=" . $hostname . ";";
                $host .= "dbname=" . $nameBd;
            endif;

            $db = new PDO('odbc:Driver={SQL Server}; Server=' . $hostname . '; Database=' . $nameBd . '; Uid=' . $userBd . '; Pwd=' . $passwordBd . ';');
            //$db -> exec("set names utf8");
            return $db;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

	
    public static function closedb($cxn) {
        try {
            unset($cxn);
            //echo "\nConexi�n PDO cerrada.";
        } catch (PDOException $ex) {
             echo "Error al cerrar conexión." .$ex->getMessage();
        }
    }

}

?>
