<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2015 © 
* @package       - - -
 * */
class Data_sgausuario{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_login($conexion,$nomusu,$passwd) {
		try {

			$nomusu 	= utf8_decode($nomusu);

			if(trim($nomusu)==""){$nomusu='NULL';}else{ $nomusu= "'" . trim($nomusu) . "'"  ;}
			if(trim($passwd)==""){$passwd='NULL';}else{ $passwd= "'" . trim(md5($passwd)) . "'"  ;}
						
			$sql = "CALL USP_BTK_USUARIO_LOGIN ($nomusu,$passwd)";
			
			foreach ($conexion->query($sql) as $row):
					$var = $row["CHECK_CONTRASENA"];
			endforeach;
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_encontrar($conexion,$idUsuario) {
		try {
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
						
			$sql = "CALL USP_BTK_USUARIO_ENCONTRAR ($idUsuario)";

			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			//fetchAll
			/*foreach ($conexion->query($sql) as $row):
					$objsga_usuario = new Data_sgausuario();
					$objsga_usuario->__set("_empresa_id", $row["empresa_id"]);
			endforeach;
		return $objsga_usuario;*/
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_encontrarxCodUniv($conexion,$codUniversitario) {
		try {
			if(trim($codUniversitario)==""){$codUniversitario='NULL';}else{ $codUniversitario= "'" . trim($codUniversitario) . "'"  ;}
						
			$sql = "CALL USP_BTK_USUARIO_COD_UNIVERSITARIO_ENCONTRAR ($codUniversitario)";
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			//fetchAll
			/*foreach ($conexion->query($sql) as $row):
					$objsga_usuario = new Data_sgausuario();
					$objsga_usuario->__set("_empresa_id", $row["empresa_id"]);
			endforeach;
		return $objsga_usuario;*/
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina, $indCachimbo) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$nombre 	= utf8_decode($nombre);

			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_USUARIO_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina, $indCachimbo)";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_registrar($conexion,$idRol, $idDocumento, $numDocumento, $codUniversitario, $nombre, $apellido, $idFacultad, $mail, $login, $idUsuario, $ip) {
		try {
			
			
			$nombre 	= utf8_decode($nombre);
			$apellido 	= utf8_decode($apellido);
			$mail 		= utf8_decode($mail);
			$login 		= utf8_decode($login);

			if(trim($idRol)==""){$idRol='NULL';}else{ $idRol= "'" . trim($idRol) . "'"  ;}
			if(trim($idDocumento)==""){$idDocumento='NULL';}else{ $idDocumento= "'" . trim($idDocumento) . "'"  ;}
			if(trim($numDocumento)==""){$numDocumento='NULL';}else{ $numDocumento= "'" . trim($numDocumento) . "'"  ;}
			if(trim($codUniversitario)==""){$codUniversitario='NULL';}else{ $codUniversitario= "'" . trim($codUniversitario) . "'"  ;}
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($apellido)==""){$apellido='NULL';}else{ $apellido= "'" . trim($apellido) . "'"  ;}
			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($mail)==""){$mail='NULL';}else{ $mail= "'" . trim($mail) . "'"  ;}
			if(trim($login)==""){$login='NULL';}else{ $login= "'" . trim($login) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_USUARIO_REGISTRAR ($idRol, $idFacultad, $codUniversitario, $idDocumento, $numDocumento, $nombre, $apellido, $mail, $login, $idUsuario, $ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_editar($conexion,$id, $idRol, $idDocumento, $numDocumento, $codUniversitario, $nombre, $apellido, $idFacultad, $mail, $login, $idEstado, $idUsuario, $ip) {
		try {
			
			
			$nombre 	= utf8_decode($nombre);
			$apellido 	= utf8_decode($apellido);
			$mail 		= utf8_decode($mail);
			$login 		= utf8_decode($login);

			if(trim($idRol)==""){$idRol='NULL';}else{ $idRol= "'" . trim($idRol) . "'"  ;}
			if(trim($idDocumento)==""){$idDocumento='NULL';}else{ $idDocumento= "'" . trim($idDocumento) . "'"  ;}
			if(trim($numDocumento)==""){$numDocumento='NULL';}else{ $numDocumento= "'" . trim($numDocumento) . "'"  ;}
			if(trim($codUniversitario)==""){$codUniversitario='NULL';}else{ $codUniversitario= "'" . trim($codUniversitario) . "'"  ;}
			if(trim($nombre)==""){$nombre='NULL';}else{ $nombre= "'" . trim($nombre) . "'"  ;}
			if(trim($apellido)==""){$apellido='NULL';}else{ $apellido= "'" . trim($apellido) . "'"  ;}
			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($mail)==""){$mail='NULL';}else{ $mail= "'" . trim($mail) . "'"  ;}
			if(trim($login)==""){$login='NULL';}else{ $login= "'" . trim($login) . "'"  ;}
			if(trim($idEstado)==""){$idEstado='NULL';}else{ $idEstado= "'" . trim($idEstado) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_USUARIO_EDITAR ($id, $idRol, $idFacultad, $codUniversitario, $idDocumento, $numDocumento, $nombre, $apellido, $mail, $login, $idEstado, $idUsuario, $ip)";
		
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_USUARIO_INACTIVAR '
              . '(:ID,  :ID_USUARIO, :IP )'
       		 );
        
        	$stmt->bindParam(':ID',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_USUARIO_ACTIVAR '
              . '(:ID,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_permisoEliminar($conexion,$id) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_USUARIO_PERMISO_ELIMINAR '
              . ' (:ID_USR) '
       		 );
        	
        	$stmt->bindParam(':ID_USR',      $id,     PDO::PARAM_INT);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_permisoRegistrar($conexion,$id,$idPermiso,$idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_USUARIO_PERMISO_REGISTRAR '
              . '(:ID,  :ID_PRIVILEGIO, :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_PRIVILEGIO',      $idPermiso,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',        	$ip,  PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_permisoListar($conexion,$id) {
		try {
						
			$sql 	= "SELECT ID_SISTEMA_PRIVILEGIO from BTK_USUARIO_SISTEMA_PRIVILEGIO WHERE ID_USUARIO = ".$id;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_editarClave($conexion,$id,$clave,$idUsuario,$ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_USUARIO_EDITAR_CLAVE '
              . '(:ID,  :CLAVE, :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':CLAVE',      $clave,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',        	$ip,  PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarDocumentoTipoTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_DOCUMENTO_IDENTIDAD_TIPO,NOM_DOCUMENTO_IDENTIDAD FROM BTK_DOCUMENTO_IDENTIDAD_TIPO WHERE IND_ACTIVO = 1 ORDER BY NUM_ORDENAMIENTO";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarUsuarioTipoTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_USUARIO_TIPO,NOM_USUARIO_TIPO FROM BTK_USUARIO_TIPO WHERE IND_ACTIVO = 1 AND ID_USUARIO_TIPO <> 1 ORDER BY NOM_USUARIO_TIPO";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarPrestamoUsuarioEstado($conexion) {
		try {
			
			$sql 	= "SELECT ID_USUARIO_ESTADO,DES_ESTADO FROM BTK_PRESTAMO_USUARIO_ESTADO WHERE IND_ACTIVO = 1 ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_permisoListarRol($conexion,$id) {
		try {
						
			$sql 	= "SELECT ID_SISTEMA_PRIVILEGIO from BTK_USUARIO_TIPO_PRIVILEGIO WHERE ID_USUARIO_TIPO = ".$id;
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}
	
}
?>
