<?php
require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';

global $conexion;
  
  $modelData  = new Data_sgacategoria();
  $arrayData  = $modelData->fu_docenteDedicacion($conexion,'DEDICACION EXCLUSIVA');

  $arrayFacultad       = array();
  $arrayCantidad       = array();

  foreach ($arrayData as $obj) {
    $arrayFacultad[]   = utf8_encode($obj['NOM_CARRERA']);
    $arrayCantidad[]   = $obj['CANTIDAD'];
  }
  
  $jsonCantidadExc     = json_encode($arrayCantidad);
  $jsonFacultadExc     = json_encode($arrayFacultad);


  $modelData  = new Data_sgacategoria();
  $arrayData  = $modelData->fu_docenteDedicacion($conexion,'TIEMPO COMPLETO');

  $arrayFacultad       = array();
  $arrayCantidad       = array();

  foreach ($arrayData as $obj) {
    $arrayFacultad[]   = utf8_encode($obj['NOM_CARRERA']);
    $arrayCantidad[]   = $obj['CANTIDAD'];
  }
  
  $jsonCantidadCom     = json_encode($arrayCantidad);
  $jsonFacultadCom     = json_encode($arrayFacultad);


  $modelData  = new Data_sgacategoria();
  $arrayData  = $modelData->fu_docenteDedicacion($conexion,'TIEMPO PARCIAL');

  $arrayFacultad       = array();
  $arrayCantidad       = array();

  foreach ($arrayData as $obj) {
    $arrayFacultad[]   = utf8_encode($obj['NOM_CARRERA']);
    $arrayCantidad[]   = $obj['CANTIDAD'];
  }
  
  $jsonCantidadPar     = json_encode($arrayCantidad);
  $jsonFacultadPar     = json_encode($arrayFacultad);


  $modelData  = new Data_sgacategoria();
  $arrayData  = $modelData->fu_docenteDedicacionGeneral($conexion,'TIEMPO PARCIAL');

  $exclusiva = 0;
  $completo  = 0;
  $parcial   = 0;

  foreach ($arrayData as $obj) {
    if(utf8_encode($obj['DES_MAIL']) == 'DEDICACION EXCLUSIVA'){ $exclusiva = $obj['CANTIDAD']; }
    if(utf8_encode($obj['DES_MAIL']) == 'TIEMPO COMPLETO'){ $completo = $obj['CANTIDAD']; }
    if(utf8_encode($obj['DES_MAIL']) == 'TIEMPO PARCIAL'){ $parcial = $obj['CANTIDAD']; }
  }

  //indicador de planes de estudio
  $modelData  = new Data_sgacategoria();
  $arrayData  = $modelData->fu_indicadorPlanEstudio($conexion); 

  $arrayEscuela               = array();
  $arrayCantAsignatura        = array();
  $arrayCantSilabus           = array();

  foreach ($arrayData as $obj) {

  	$nombre = $obj['NOM_CARRERA'];
  	$encod = mb_detect_encoding($obj['NOM_CARRERA'], 'UTF-8, ISO-8859-1');
	if($encod=='ISO-8859-1'){
		$nombre = utf8_encode($obj['NOM_CARRERA']);
	}

	$nomPlan = $obj['NOM_PLAN_ESTUDIO'];
	$encod = mb_detect_encoding($obj['NOM_PLAN_ESTUDIO'], 'UTF-8, ISO-8859-1');
	if($encod=='ISO-8859-1'){
		$nomPlan = utf8_encode($obj['NOM_PLAN_ESTUDIO']);
	}

    $nomPlan = str_replace(' ',"\n",$nomPlan);
    $nombre = str_replace('y',"\n",$nombre);

    $arrayEscuela[]           = $nombre."\n".$nomPlan;
    $arrayCantAsignatura[]    = $obj['CANT_ASIGNATURA'];
    $arrayCantSilabus[]       = $obj['CANT_ASIGNATURA_SILABU'];
  }

  $jsonCantidadAsignatura     = json_encode($arrayCantAsignatura);
  $jsonCantidadSilabus        = json_encode($arrayCantSilabus);
  $jsonEscuelaPlan            = json_encode($arrayEscuela);

  //indicador docentes hoja de vida
  $arrayData  = $modelData->fu_indicadorDocenteHojaVida($conexion); 

  $arrayFacultad_1            = array();
  $arrayCantDocente           = array();
  $arrayCantHojaVida          = array();

  foreach ($arrayData as $obj) {
    $arrayFacultad_1[]            = utf8_encode($obj['NOM_CARRERA']);
    $arrayCantDocente[]           = $obj['CANT_DOCENTE'];
    $arrayCantHojaVida[]          = $obj['CANT_DOCENTE_CV'];
  }

  $jsonCantidadFacultad_1     = json_encode($arrayFacultad_1);
  $jsonCantidadDocente        = json_encode($arrayCantDocente);
  $jsonHojaVida               = json_encode($arrayCantHojaVida);

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="img/logo.png">
    <title>UNJFSC - Universidad Nacional José Faustino Sánchez Carrión</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <?php
   include 'librerias_css.php';
   ?> 

    <?php
    include 'librerias_jsrpt.php';
  ?>

  </head>
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition skin-blue layout-top-nav">

    <div class="wrapper">

      <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="#" class="navbar-brand"><b>Universidad Nacional </b>José Faustino Sánchez Carrión</a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            
           
          </div><!-- /.container-fluid -->
        </nav>
      </header>
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Indicadores de Gestión Académica
              <!--<small>Example 2.0</small>-->
            </h1>
            <!--<ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a href="#">Layout</a></li>
              <li class="active">Top Navigation</li>
            </ol>-->
          </section>

          <!-- Main content -->
          <section class="content">
              <div class="row">

        <!--DEDICACION-->
        <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                    <h3 class="box-title">Docentes por Dedicación Exclusiva</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive no-padding" id="Contenedorform">

                    <canvas id="chart-area" width="300" height="300"></canvas>

                    <script>

                var barChartData = {
                  labels : <?php echo $jsonFacultadExc; ?>,
                  datasets : [
                  {
                    type: 'bar',
                    backgroundColor: "#0070c6",
                    label: 'Dedicación Exclusiva',
                    data: <?php echo $jsonCantidadExc ?>,
                    borderColor: '#0070c6',
                    borderWidth: 2
                  }
                  ]
                }                  

                  
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                    animation: {
                        onComplete: function () {
                            var chartInstance = this.chart;
                            var ctx = chartInstance.ctx;
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = "center";
                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
                                }),this)
                            }),this);
                        }
                    }
                }
            });  


                  </script>

                    </div>

                </div>
                </div>


        <!--COMPLETO-->
                <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                    <h3 class="box-title">Docentes por Tiempo Completo</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive no-padding" id="Contenedorform">

                    <canvas id="chart-area2" width="300" height="300"></canvas>

                    <script>


                  var barChartData2 = {
                  labels : <?php echo $jsonFacultadCom; ?>,
                  datasets : [
                  {
                    type: 'bar',
                    backgroundColor: "#E15E4F",
                    label: 'Tiempo Completo',
                    data: <?php echo $jsonCantidadCom ?>,
                    borderColor: '#E15E4F',
                    borderWidth: 2
                  }
                  ]
                }                  

                  
                var ctx2 = document.getElementById("chart-area2").getContext("2d");
                window.myBar = new Chart(ctx2, {
                                            type: 'bar',
                                            data: barChartData2,
                                            options: {
                                            responsive: true,
                                            title: {
                                                    display: true,
                                                    text: ''
                                            },
                                            animation: {
                                            onComplete: function () {
                                            var chartInstance = this.chart;
                                            var ctx = chartInstance.ctx;
                                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                                            ctx.textAlign = "center";
                                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                            var meta = chartInstance.controller.getDatasetMeta(i);
                                            Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                            ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
                                            }),this)
                                            }),this);
                                            }
                                            }
                                            }
            });

                  </script>

                    </div>

                </div>
                </div>

        <!--PARCIAL-->
                <div class="col-md-7">
                <div class="box">
                    <div class="box-header with-border">
                    <h3 class="box-title">Docentes por Tiempo Parcial</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive no-padding" id="Contenedorform">

                    <canvas id="chart-area3" width="300" height="300"></canvas>

                    <script>

                  var barChartData3 = {
                  labels : <?php echo $jsonFacultadPar; ?>,
                  datasets : [
                  {
                    type: 'bar',
                    backgroundColor: "#00C66C",
                    label: 'Tiempo Parcial',
                    data: <?php echo $jsonCantidadPar ?>,
                    borderColor: '#00C66C',
                    borderWidth: 2
                  }
                  ]
                }                  

                  
                var ctx3 = document.getElementById("chart-area3").getContext("2d");
                window.myBar = new Chart(ctx3, {
                                            type: 'bar',
                                            data: barChartData3,
                                            options: {
                                            responsive: true,
                                            title: {
                                                    display: true,
                                                    text: ''
                                            },
                                            animation: {
                                            onComplete: function () {
                                            var chartInstance = this.chart;
                                            var ctx = chartInstance.ctx;
                                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                                            ctx.textAlign = "center";
                                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                            var meta = chartInstance.controller.getDatasetMeta(i);
                                            Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                            ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
                                            }),this)
                                            }),this);
                                            }
                                            }
                                            }
            });

                  </script>

                    </div>

                </div>
                </div>

        <!--GENERAL-->
                <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                    <h3 class="box-title">Dedicación General de Docentes</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive no-padding" id="Contenedorform">

                    
                    <canvas id="chart-area4" style="height: 300px; width: 515px;" width="768" height="300"></canvas>

                    <script>

        var pieData = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    <?php echo $exclusiva; ?>,
                    <?php echo $completo;  ?>,
                    <?php echo $parcial;   ?>,
                ],
                backgroundColor: [
                    "#0070c6",
                    "#E15E4F",
                    "#00C66C",
                ],
                label: 'Docentes'
            }],
            labels: [
                "Dedicación Exclusiva",
                "Tiempo Completo",
                "Tiempo Parcial"
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: ''
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    }; 

                  var ctx4 = document.getElementById("chart-area4").getContext("2d");
                  window.myDoughnut = new Chart(ctx4, pieData);

                  </script>

                    </div>

                </div>
                </div>

        <!--DEDICACION-->

         <section class="col-lg-12"> 
         <div id="ContentPrincipal">     
         Bienvenido          
         </div>
         </section><!-- right col -->


         <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                    <h3 class="box-title">Planes de Estudio por Escuela Profesional</h3><br>
                    <div class="col-md-4"></div>
                    <!--<div style="background-color:rgba(252,147,65,0.5)" class="col-md-2">Asignaturas Registradas</div>
                    <div style="background-color:rgba(151,249,190,0.5)" class="col-md-2">Asignaturas con Syllabus</div>-->
                    </div><!-- /.box-header -->


                    <div class="box-body table-responsive no-padding" id="Contenedorform">

                    <canvas id="chart-area5" width="300" height="120"></canvas>

                    <script>

            Chart.pluginService.register({
  					beforeInit: function(chart) {
    				var hasWrappedTicks = chart.config.data.labels.some(function(label) {
      				return label.indexOf('\n') !== -1;
    				});

    				if (hasWrappedTicks) {
      				// figure out how many lines we need - use fontsize as the height of one line
      				var tickFontSize = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontSize, Chart.defaults.global.defaultFontSize);
      				var maxLines = chart.config.data.labels.reduce(function(maxLines, label) {
        			return Math.max(maxLines, label.split('\n').length);
      				}, 0);
      				var height = (tickFontSize + 2) * maxLines + (chart.options.scales.xAxes[0].ticks.padding || 0);

      				// insert a dummy box at the bottom - to reserve space for the labels
      				Chart.layoutService.addBox(chart, {
        				draw: Chart.helpers.noop,
        			isHorizontal: function() {
          			return true;
        			},
        			update: function() {
          			return {
            		height: this.height
          			};
        			},
        			height: height,
        			options: {
          			position: 'bottom',
          			fullWidth: 1,
        			}
      			});

      			// turn off x axis ticks since we are managing it ourselves
      			chart.options = Chart.helpers.configMerge(chart.options, {
        		scales: {
          		xAxes: [{
            	ticks: {
              	display: false,
              	// set the fontSize to 0 so that extra labels are not forced on the right side
              	fontSize: 0
            	}
          		}]
        	}
      		});

      	chart.hasWrappedTicks = {
        	tickFontSize: tickFontSize
      	};
    	}
  	},
  		afterDraw: function(chart) {
    	if (chart.hasWrappedTicks) {
      // draw the labels and we are done!
      chart.chart.ctx.save();
      var tickFontSize = chart.hasWrappedTicks.tickFontSize;
      var tickFontStyle = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontStyle, Chart.defaults.global.defaultFontStyle);
      var tickFontFamily = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontFamily, Chart.defaults.global.defaultFontFamily);
      var tickLabelFont = Chart.helpers.fontString(tickFontSize, tickFontStyle, tickFontFamily);
      chart.chart.ctx.font = tickLabelFont;
      chart.chart.ctx.textAlign = 'center';
      var tickFontColor = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].fontColor, Chart.defaults.global.defaultFontColor);
      chart.chart.ctx.fillStyle = tickFontColor;

      var meta = chart.getDatasetMeta(0);
      var xScale = chart.scales[meta.xAxisID];
      var yScale = chart.scales[meta.yAxisID];

      chart.config.data.labels.forEach(function(label, i) {
        label.split('\n').forEach(function(line, j) {
          chart.chart.ctx.fillText(line, xScale.getPixelForTick(i + 0.5), (chart.options.scales.xAxes[0].ticks.padding || 0) + yScale.getPixelForValue(yScale.min) +
            // move j lines down
            j * (chart.hasWrappedTicks.tickFontSize + 2));
        });
      });

      chart.chart.ctx.restore();
    }
  }
});
                  

      var barChartData = {
        labels: <?php echo $jsonEscuelaPlan; ?>,
        datasets: [{
            label: 'Asignaturas Registradas',
            backgroundColor: "rgba(252,147,65,0.5)",
            yAxisID: "y-axis-1",
            data: <?php echo $jsonCantidadAsignatura; ?>
        }, {
            label: 'Asignaturas con Syllabus',
            backgroundColor: "rgba(151,249,190,0.5)",
            yAxisID: "y-axis-2",
            data: <?php echo $jsonCantidadSilabus; ?>
        }]

    };

    var ctx = document.getElementById("chart-area5").getContext("2d");

        window.myPie = Chart.Bar(ctx, {
            data: barChartData, 
            options: {
                responsive: true,
                hoverMode: 'label',
                hoverAnimationDuration: 400,
                stacked: false,
                title:{
                    display:true,
                    text:""
                },
                scales: {
                    yAxes: [{
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "left",
                        id: "y-axis-1",

                        ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 10  // if i use this it always set it '1', which look very awkward if it have high value  e.g. '100'.
                                }


                    }, {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "right",
                        id: "y-axis-2",
                        gridLines: {
                            drawOnChartArea: false
                        },

                        ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 10  // if i use this it always set it '1', which look very awkward if it have high value  e.g. '100'.
                                }

                    }],
                }
            }
        });

                

                  </script>

                    </div>

                </div>
                </div>

                <!--HOJAS DE VIDA-->
                <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                    <h3 class="box-title">Docentes y Hojas de Vida por Facultad</h3><br>
                    <div class="col-md-4"></div>
                    <!--<div style="background-color:#0070c6;color:#fff" class="col-md-2">Docentes Registrados</div>
                    <div style="background-color:#dd4b39;color:#fff" class="col-md-2">Hojas de Vida Registrados</div>-->
                    </div><!-- /.box-header -->


                    <div class="box-body table-responsive no-padding" id="Contenedorform">

                    <canvas id="chart-area6" width="300" height="120"></canvas>
                    <div id="legenda"></div>

                    <script>

        var barChartData6 = {
        labels: <?php echo $jsonCantidadFacultad_1; ?>,
        datasets: [{
            label: 'Docentes Registrados',
            backgroundColor: "#1864f2",
            yAxisID: "y-axis-1",
            data: <?php echo $jsonCantidadDocente; ?>
        }, {
            label: 'Hojas de Vida Registrados',
            backgroundColor: "#E15E4F",
            yAxisID: "y-axis-2",
            data: <?php echo $jsonHojaVida; ?>
        }]

    };

    var ctx6 = document.getElementById("chart-area6").getContext("2d");

        window.myPie = Chart.Bar(ctx6, {
            data: barChartData6, 
            options: {
                responsive: true,
                hoverMode: 'label',
                hoverAnimationDuration: 400,
                stacked: false,
                title:{
                    display:true,
                    text:""
                },
                scales: {
                    yAxes: [{
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "left",
                        id: "y-axis-1",

                        ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 5  // if i use this it always set it '1', which look very awkward if it have high value  e.g. '100'.
                                }


                    }, {
                        type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: "right",
                        id: "y-axis-2",
                        gridLines: {
                            drawOnChartArea: false
                        },

                        ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 5 // if i use this it always set it '1', which look very awkward if it have high value  e.g. '100'.
                                }

                    }],
                }
            }
        });

                  </script>

                    </div>

                </div>
                </div>
                <!--HOJAS DE VIDA-->




         </div>
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="container">
          <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
          </div>
          <strong>Copyright &copy; <?php echo date('Y').' ' ?><a href="http://www.unjfsc.edu.pe">Universidad Nacional José Faustino Sánchez Carrión</a>.</strong> Todos los derechos reservados.
        </div><!-- /.container -->
      </footer>
    </div><!-- ./wrapper -->

  </body>

 <script>
$(document).ready(function(){
  loadMenu('dashboard');   
});
</script>

</html>
