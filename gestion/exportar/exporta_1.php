<?php
set_time_limit(0);
error_reporting(0);
require_once "../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
require_once APP_DIR . 'PHPExcel.php';

global $conexion, $etiquetaTitulo, $nombreModulo;

$VARIABLES=$_GET['exporta'];

$data = @explode('|', $VARIABLES);
$filtro       = $data[0];
$indActivo    = $data[1];
$orden	      = $data[2];
$direccion    = $data[3];
$idFacultad   = $data[4];
$codLibro     = $data[5];
$desEstado    = $data[6];
$biblioteca   = $data[7];

date_default_timezone_set('America/Lima');
$hoy   = date("Y-m-d");
$fecha = @explode('-', $hoy);
$anio   = $fecha[0];
$mes   = $fecha[1];
$dia  = $fecha[2];

switch ($mes) {
  case '01':
    $nmes = 'ENE';
    break;
  case '02':
    $nmes = 'FEB';
    break;
  case '03':
    $nmes = 'MAR';
    break;
  case '04':
    $nmes = 'ABR';
    break;
  case '05':
    $nmes = 'MAY';
    break;
  case '06':
    $nmes = 'JUN';
    break;
  case '07':
    $nmes = 'JUL';
    break;
  case '08':
    $nmes = 'AGO';
    break;
  case '09':
    $nmes = 'SET';
    break;
  case '10':
    $nmes = 'OCT';
    break;
  case '11':
    $nmes = 'NOV';
    break;
  case '12':
    $nmes = 'DIC';
    break;
}

$fecha = $dia.'/'.$nmes.'/'.$anio;

$objData      = new Data_sgaexportar();
$arrayData    = $objData->fu_reporte_1($conexion,$filtro, $indActivo, $orden, $direccion, $idFacultad, $codLibro, $desEstado);
//$arrayAutor   = $objData->fu_libroAutorListar($conexion);

date_default_timezone_set('America/Lima');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("BIBLIOPLAX")
							 ->setLastModifiedBy("BIBLIOPLAX")
							 ->setTitle("Reporte de Materiales")
							 ->setSubject("Reporte de Materiales")
							 ->setDescription("Reporte de Materiales")
							 ->setKeywords("BIBLIOPLAX")
							 ->setCategory("REPORTES");


$styleArray = array(
    	'font'  => array(
        'bold'  => true /*,
        'color' => array('rgb' => 'FF0000'),
        'size'  => 15,
        'name'  => 'Verdana'*/
    ));

$styleArrayCab = array(
    	'font'  => array(
        'bold'  => true ,
        'color' => array('rgb' => 'FFFFFF')/*,
        'size'  => 15,
        'name'  => 'Verdana'*/
    ));

$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getDefaultStyle()->applyFromArray(
    array(
        'fill' => array(
            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
             'rgb' => 'ffffff'
        	)
        ),
    )
);


// PESTAÑA CONSOLIDADA
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:D2');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:D3');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:D5');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:D6');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:D7');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'BIBLIOPLAX')
            ->setCellValue('A2', 'REPORTE DE MATERIAL EN BIBLIOTECA')
            ->setCellValue('A3', 'FECHA REPORTE: '.$fecha)
            ->setCellValue('A4', 'BIBLIOTECA: '.trim($biblioteca))
            ->setCellValue('A5', 'TITULO: '.trim($filtro))
            ->setCellValue('A6', 'CODIGO '.$codLibro)
            ->setCellValue('A7', 'ESTADO: '.$desEstado);

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1')->applyFromArray(
$styleArray
);

        
//INSERTANDO IMAGEN
$gdImage = imagecreatefromjpeg(APP_DIR .'PHPExcel/pdf.jpg');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');
$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setHeight(70);

$objDrawing->setCoordinates('E1');

$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//FIN IMAGEN

//TODOS LOS INVENTARIOS
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A10:B10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C10:F10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G10:H10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I10:J10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K10:L10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M10:N10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O10:P10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q10:R10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('S10:T10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('U10:V10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('W10:X10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Y10:Z10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AA10:AB10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AC10:AE10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF10:AH10');

// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A10', 'TIPO')
            ->setCellValue('C10', 'TITULO')
            ->setCellValue('G10',  'EDITORIAL')
            ->setCellValue('I10',  'AÑO')
            ->setCellValue('K10',  'PAIS')
            ->setCellValue('M10',  'VOLUMEN')
            ->setCellValue('O10',  'PAGINAS')
            ->setCellValue('Q10',  'ADQUISICION')
            ->setCellValue('S10',  'PROVEEDOR')
            ->setCellValue('U10',  'COD.LIBRO')
            ->setCellValue('W10',  'COD.BARRA')
            ->setCellValue('Y10',  'ISBN')
            ->setCellValue('AA10',  'COD.PATRIMONIAL')
            ->setCellValue('AC10',  'AUTOR')
            ->setCellValue('AF10',  'TEMAS/AREAS RELACIONADAS');

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:AH10')->applyFromArray(
$styleArrayCab
); 

$objPHPExcel->getActiveSheet()->getStyle('A10:AH10')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '000000'
        )
    ));

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:AH10')->applyFromArray(
$BStyle
); 

//detalle del reporte
 $item      = 11;
 $idLibro_  = '';
 $idLibro__ = '';

 foreach ($arrayData as $obj):

 $celda_combina1  = 'A'.$item.':'.'B'.$item;
 $celda_combina2  = 'C'.$item.':'.'F'.$item;
 $celda_combina3  = 'G'.$item.':'.'H'.$item;
 $celda_combina4  = 'I'.$item.':'.'J'.$item;
 $celda_combina5  = 'K'.$item.':'.'L'.$item;
 $celda_combina6  = 'M'.$item.':'.'N'.$item;
 $celda_combina7  = 'O'.$item.':'.'P'.$item;
 $celda_combina8  = 'Q'.$item.':'.'R'.$item;
 $celda_combina9  = 'S'.$item.':'.'T'.$item;
 $celda_combina10 = 'U'.$item.':'.'V'.$item;
 $celda_combina11 = 'W'.$item.':'.'X'.$item;
 $celda_combina12 = 'Y'.$item.':'.'Z'.$item;
 $celda_combina13 = 'AA'.$item.':'.'AB'.$item;
 $celda_combina14 = 'AC'.$item.':'.'AE'.$item;
 $celda_combina15 = 'AF'.$item.':'.'AH'.$item;

 $celda_1    = 'A'.$item;
 $celda_2    = 'C'.$item;
 $celda_3    = 'G'.$item;
 $celda_4    = 'I'.$item;
 $celda_5    = 'K'.$item;
 $celda_6    = 'M'.$item;
 $celda_7    = 'O'.$item;
 $celda_8    = 'Q'.$item;
 $celda_9    = 'S'.$item;
 $celda_10    = 'U'.$item;
 $celda_11    = 'W'.$item;
 $celda_12    = 'Y'.$item;
 $celda_13    = 'AA'.$item;
 $celda_14    = 'AC'.$item;
 $celda_15    = 'AF'.$item;
 $celda_borde    = 'A'.$item.':'.'AH'.$item;

 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina1);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina2);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina3);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina4);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina5);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina6);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina7);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina8);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina9);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina10);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina11);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina12);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina13);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina14);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina15);

 $valor_1 = utf8_encode($obj['DES_LIBRO_TIPO']);
 $valor_2 = utf8_encode($obj['DES_TITULO']);
 $valor_3 = utf8_encode($obj['NOM_EDITORIAL']);
 $valor_4 = utf8_encode($obj['NUM_ANIO_PUBLICA']);
 $valor_5 = utf8_encode($obj['NOM_PAIS']);
 $valor_6 = utf8_encode($obj['NUM_VOLUMEN']);
 $valor_7 = utf8_encode($obj['NUM_PAGINA']);
 $valor_8 = utf8_encode($obj['DES_LIBRO_ADQUISICION']);
 $valor_9 = utf8_encode($obj['NOM_PROVEEDOR']);
 $valor_10 = utf8_encode($obj['COD_LIBRO']);
 $valor_11 = utf8_encode($obj['COD_BARRA']);
 $valor_12 = utf8_encode($obj['COD_ISBM']);
 $valor_13 = utf8_encode($obj['COD_PATRIMONIAL']);

 //AUTORES
if($obj['ID_LIBRO'] != $idLibro_){

  $cadenaAutor = '';
  $idLibro_ = $obj['ID_LIBRO'];

  $arrayAutor   = $objData->fu_libroAutorxIdLibro($conexion, $idLibro_);

  foreach ($arrayAutor as $objAutor) {

    if($cadenaAutor == ''){ $cadenaAutor = utf8_encode($objAutor['NOM_AUTOR']); }
      else{
        $cadenaAutor.= '/'.utf8_encode($objAutor['NOM_AUTOR']);
      }

  }

}
 //

 //OBTENER AUTOR(ES) DEL LIBRO
 /*$cadenaAutor = '';
 $flag        = '';

 foreach ($arrayAutor as $objAutor) {
  
    if($obj['ID_LIBRO'] == $objAutor['ID_LIBRO']){

      if($cadenaAutor == ''){ $cadenaAutor = utf8_encode($objAutor['NOM_AUTOR']); }
      else{
        $cadenaAutor.= '/'.utf8_encode($objAutor['NOM_AUTOR']);
      }
      $flag        = 1;
    }
    else{
        if($flag == 1){
          break;
        }
    }

 }*/
 //

 //TEMA RELACIONADO
 $dataRelacion = @explode(',', $obj['DES_CATEGORIA_RELACION']);


 if($obj['ID_LIBRO'] != $idLibro__){

  $temaRel   = '';
  $idRelacion= '';
  $idLibro__ = $obj['ID_LIBRO'];

  
  if(count($dataRelacion) > 1){

    for ($rel=1; $rel < count($dataRelacion); $rel++) { 

            if($idRelacion == ''){ $idRelacion = $dataRelacion[$rel]; }
            else{
            $idRelacion.= ','.$dataRelacion[$rel];
            }

    }

    if($idRelacion != ',' && $idRelacion != ''){
    $arrayCategoria = $objData->fu_categoriaListar($conexion, $idRelacion);
    foreach ($arrayCategoria as $objCat) {
           if($temaRel == ''){ $temaRel = utf8_encode($objCat['NOM_CATEGORIA']); }
            else{$temaRel.= '/'.utf8_encode($objCat['NOM_CATEGORIA']);}
    }
    }

  }

 }


 if($valor_1 == null){ $valor_1 = '';}
 if($valor_2 == null){ $valor_2 = '';}
 if($valor_3 == null){ $valor_3 = '';}
 if($valor_4 == null){ $valor_4 = '';}
 if($valor_5 == null){ $valor_5 = '';}
 if($valor_6 == null){ $valor_6 = '';}
 if($valor_7 == null){ $valor_7 = '';}
 if($valor_8 == null){ $valor_8 = '';}
 if($valor_9 == null){ $valor_9 = '';}
 if($valor_10 == null){ $valor_10 = '';}
 if($valor_11 == null){ $valor_11 = '';}
 if($valor_12 == null){ $valor_12 = '';}
 if($valor_13 == null){ $valor_13 = '';}

 $objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($celda_1  ,  $valor_1)
             ->setCellValue($celda_2  ,  $valor_2)
             ->setCellValue($celda_3   , $valor_3)
             ->setCellValue($celda_4   , $valor_4)
             ->setCellValue($celda_5   , $valor_5)
             ->setCellValue($celda_6   , $valor_6)
             ->setCellValue($celda_7   , $valor_7)
             ->setCellValue($celda_8   , $valor_8)
             ->setCellValue($celda_9   , $valor_9)
             ->setCellValue($celda_10   , $valor_10)
             ->setCellValueExplicit($celda_11, $valor_11,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_12, $valor_12,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_13, $valor_13,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_14, $cadenaAutor,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_15, $temaRel,PHPExcel_Cell_DataType::TYPE_STRING);
 
 $objPHPExcel->setActiveSheetIndex(0)->getStyle($celda_borde)->applyFromArray(
 $BStyle
 ); 

 $item++;

 endforeach;  
//fin detalle

///////////////////////////////////////////////////////////////////////////////////////////////////

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('MATERIAL');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE_BIBLIOPLAX.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


?>