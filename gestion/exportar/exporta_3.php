<?php
set_time_limit(0);
//error_reporting(0);
require_once "../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
require_once APP_DIR . 'PHPExcel.php';

global $conexion, $etiquetaTitulo, $nombreModulo;

$VARIABLES=$_GET['exporta'];

$data = @explode('|', $VARIABLES);
$fec_inicio   = $data[0];
$fec_fin      = $data[1];
$idCarrera    = $data[2];
$biblioteca   = $data[3];

date_default_timezone_set('America/Lima');
$hoy   = date("Y-m-d");
$fecha = @explode('-', $hoy);
$anio   = $fecha[0];
$mes   = $fecha[1];
$dia  = $fecha[2];

switch ($mes) {
  case '01':
    $nmes = 'ENE';
    break;
  case '02':
    $nmes = 'FEB';
    break;
  case '03':
    $nmes = 'MAR';
    break;
  case '04':
    $nmes = 'ABR';
    break;
  case '05':
    $nmes = 'MAY';
    break;
  case '06':
    $nmes = 'JUN';
    break;
  case '07':
    $nmes = 'JUL';
    break;
  case '08':
    $nmes = 'AGO';
    break;
  case '09':
    $nmes = 'SET';
    break;
  case '10':
    $nmes = 'OCT';
    break;
  case '11':
    $nmes = 'NOV';
    break;
  case '12':
    $nmes = 'DIC';
    break;
}

$fecha = $dia.'/'.$nmes.'/'.$anio;

$objData      = new Data_sgaexportar();
$arrayData    = $objData->fu_reporte_3($conexion,$fec_inicio, $fec_fin, $idCarrera);
$arrayCant    = $objData->fu_reporte_3_cantidad($conexion,$fec_inicio, $fec_fin, $idCarrera);

date_default_timezone_set('America/Lima');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("BIBLIOPLAX")
							 ->setLastModifiedBy("BIBLIOPLAX")
							 ->setTitle("Reporte de Prestamos")
							 ->setSubject("Reporte de Prestamos")
							 ->setDescription("Reporte de Prestamos")
							 ->setKeywords("BIBLIOPLAX")
							 ->setCategory("REPORTES");


$styleArray = array(
    	'font'  => array(
        'bold'  => true /*,
        'color' => array('rgb' => 'FF0000'),
        'size'  => 15,
        'name'  => 'Verdana'*/
    ));

$styleArrayCab = array(
    	'font'  => array(
        'bold'  => true ,
        'color' => array('rgb' => 'FFFFFF')/*,
        'size'  => 15,
        'name'  => 'Verdana'*/
    ));

$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getDefaultStyle()->applyFromArray(
    array(
        'fill' => array(
            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
             'rgb' => 'ffffff'
        	)
        ),
    )
);


// PESTAÑA CONSOLIDADA
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:D2');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:D3');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:D5');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:D6');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'BIBLIOPLAX')
            ->setCellValue('A2', 'REPORTE DE PRESTAMOS EN BIBLIOTECA')
            ->setCellValue('A3', 'FECHA REPORTE: '.$fecha)
            ->setCellValue('A4', 'BIBLIOTECA: '.trim($biblioteca))
            ->setCellValue('A5', 'DESDE: '.$data[0])
            ->setCellValue('A6', 'HASTA '.$data[1]);

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1')->applyFromArray(
$styleArray
);

        
//INSERTANDO IMAGEN
$gdImage = imagecreatefromjpeg(APP_DIR .'PHPExcel/pdf.jpg');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');
$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setHeight(70);

$objDrawing->setCoordinates('E1');

$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//FIN IMAGEN

//
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B10:D10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F10:G10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L10:N10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O10:P10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q10:R10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('S10:T10');


// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A10', 'CODIGO')
            ->setCellValue('B10', 'APELLIDOS Y NOMBRES')
            ->setCellValue('E10',  'DNI')
            ->setCellValue('F10',  'EP')
            ->setCellValue('H10',  'TURNO')
            ->setCellValue('I10',  'FECHA')
            ->setCellValue('J10',  'AÑO')
            ->setCellValue('K10',  'SEXO')
            ->setCellValue('L10',  'MATERIAL SOLICITADO')
            ->setCellValue('O10',  'FECHA PRESTAMO')
            ->setCellValue('Q10',  'FECHA DEVOLUCIÓN ESTIMADA')
            ->setCellValue('S10',  'FECHA DEVOLUCIÓN REAL')
            ->setCellValue('U10',  'DIAS ATRASO')
            ->setCellValue('V10',  'ESTADO PRESTAMO')
            ->setCellValue('W10',  'ESTADO LIBRO');

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:W10')->applyFromArray(
$styleArrayCab
); 

$objPHPExcel->getActiveSheet()->getStyle('A10:W10')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '000000'
        )
    ));

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:W10')->applyFromArray(
$BStyle
); 

//detalle del reporte
 $item      = 11;

 foreach ($arrayData as $obj):

 $celda_combina1  = 'B'.$item.':'.'D'.$item;
 $celda_combina2  = 'F'.$item.':'.'G'.$item;
 $celda_combina3  = 'L'.$item.':'.'N'.$item;
 $celda_combina4  = 'O'.$item.':'.'P'.$item;
 $celda_combina5  = 'Q'.$item.':'.'R'.$item;
 $celda_combina6  = 'S'.$item.':'.'T'.$item;

 $celda_1    = 'A'.$item;
 $celda_2    = 'B'.$item;
 $celda_3    = 'E'.$item;
 $celda_4    = 'F'.$item;
 $celda_5    = 'H'.$item;
 $celda_6    = 'I'.$item;
 $celda_7    = 'J'.$item;
 $celda_8    = 'K'.$item;
 $celda_9    = 'L'.$item;
 $celda_10    = 'O'.$item;
 $celda_11    = 'Q'.$item;
 $celda_12    = 'S'.$item;
 $celda_13    = 'U'.$item;
 $celda_14    = 'V'.$item;
 $celda_15    = 'W'.$item;
 $celda_borde    = 'A'.$item.':'.'W'.$item;

 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina1);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina2);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina3);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina4);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina5);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina6);


 $valor_1 = utf8_encode($obj['COD_UNIVERSITARIO']);
 $valor_2 = utf8_encode($obj['APE_USUARIO'].' '.$obj['NOM_USUARIO']);

 $valor_3 = utf8_encode($obj['NUM_DOCUMENTO_IDENTIDAD']);
 $valor_4 = utf8_encode($obj['NOM_CARRERA']);

 $turno = 'MAÑANA';
 if($obj['HORA'] > 12){ $turno = 'TARDE'; }

 $valor_5 = $turno;

 $fecha       = strtotime( $obj['FEC_INICIO'] ); 
 $fecha       = date('d/m/Y H:i:s', $fecha);

 $valor_6 = utf8_encode($fecha);
 $valor_7 = utf8_encode($obj['ANIO']);
 $valor_8 = utf8_encode($obj['DES_SEXO']);
 $valor_9 = utf8_encode($obj['DES_TITULO']);
 $valor_10 = utf8_encode($fecha);

 $fecha       = strtotime( $obj['FEC_FIN'] ); 
 $fecha       = date('d/m/Y H:i:s', $fecha);

 $valor_11 = utf8_encode($fecha);

 $fecha       = strtotime( $obj['FEC_ENTREGA'] ); 
 $fecha       = date('d/m/Y H:i:s', $fecha);
 $valor_12 = utf8_encode($fecha);
 $valor_13 = utf8_encode($obj['ATRASO']);
 $valor_14 = utf8_encode($obj['DES_ESTADO']);
 $valor_15 = utf8_encode($obj['LIBRO_ESTADO_ENTREGA']);


 if($valor_1 == null){ $valor_1 = '';}
 if($valor_2 == null){ $valor_2 = '';}
 if($valor_3 == null){ $valor_3 = '';}
 if($valor_4 == null){ $valor_4 = '';}
 if($valor_5 == null){ $valor_5 = '';}
 if($valor_6 == null){ $valor_6 = '';}
 if($valor_7 == null){ $valor_7 = '';}
 if($valor_8 == null){ $valor_8 = '';}
 if($valor_9 == null){ $valor_9 = '';}
 if($valor_10 == null){ $valor_10 = '';}
 if($valor_11 == null){ $valor_11 = '';}
 if($valor_12 == null){ $valor_12 = '';}
 if($valor_13 == null){ $valor_13 = '';}
 if($valor_14 == null){ $valor_14 = '';}
 if($valor_15 == null){ $valor_15 = '';}

 $objPHPExcel->setActiveSheetIndex(0)
             ->setCellValueExplicit($celda_1  ,  $valor_1,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_2  ,  $valor_2,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_3   , $valor_3,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_4   , $valor_4,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValue($celda_5   , $valor_5)
             ->setCellValue($celda_6   , $valor_6)
             ->setCellValue($celda_7   , $valor_7)
             ->setCellValue($celda_8   , $valor_8)
             ->setCellValueExplicit($celda_9   , $valor_9,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValue($celda_10   , $valor_10)
             ->setCellValue($celda_11, $valor_11)
             ->setCellValue($celda_12, $valor_12)
             ->setCellValue($celda_13, $valor_13)
             ->setCellValue($celda_14, $valor_14)
             ->setCellValue($celda_15, $valor_15);
 
 $objPHPExcel->setActiveSheetIndex(0)->getStyle($celda_borde)->applyFromArray(
 $BStyle
 ); 

 $item++;

 endforeach;  
//fin detalle

///////////////////////////////////////////////////////////////////////////////////////////////////

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('PRESTAMOS');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


//HOJA 2

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('CANTIDADES');

$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A1:D1');  
$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A2:D2');
$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A3:D3');
$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A4:D4');
$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A5:D5');
$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A6:D6');

$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A1', 'BIBLIOPLAX')
            ->setCellValue('A2', 'REPORTE DE PRESTAMOS EN BIBLIOTECA')
            ->setCellValue('A3', 'FECHA REPORTE: '.$fecha)
            ->setCellValue('A4', 'BIBLIOTECA: '.trim($biblioteca))
            ->setCellValue('A5', 'DESDE: '.$data[0])
            ->setCellValue('A6', 'HASTA '.$data[1]);

$objPHPExcel->setActiveSheetIndex(1)->getStyle('A1')->applyFromArray(
$styleArray
);

        
//INSERTANDO IMAGEN
$gdImage = imagecreatefromjpeg(APP_DIR .'PHPExcel/pdf.jpg');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');
$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setHeight(70);

$objDrawing->setCoordinates('E1');

$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//FIN IMAGEN

$objPHPExcel->setActiveSheetIndex(1)->mergeCells('A10:B10');

// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A10', 'ESCUELA')
            ->setCellValue('C10', 'MASCULINO')
            ->setCellValue('D10',  'FEMENINO')
            ->setCellValue('E10',  'TOTAL')
            ->setCellValue('F10',  'MAÑANA')
            ->setCellValue('G10',  'TARDE')
            ->setCellValue('H10',  'TOTAL');

$objPHPExcel->setActiveSheetIndex(1)->getStyle('A10:H10')->applyFromArray(
$styleArrayCab
); 

$objPHPExcel->getActiveSheet()->getStyle('A10:H10')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '000000'
        )
    ));

$objPHPExcel->setActiveSheetIndex(1)->getStyle('A10:H10')->applyFromArray(
$BStyle
); 

//detalle del reporte
 $item          = 11;
 $total_masc    = 0;
 $total_feme    = 0;
 $total_man     = 0;
 $total_tar     = 0;

foreach ($arrayCant as $obj):

 $celda_combina1  = 'A'.$item.':'.'B'.$item;

 $celda_1    = 'A'.$item;
 $celda_2    = 'C'.$item;
 $celda_3    = 'D'.$item;
 $celda_4    = 'E'.$item;
 $celda_5    = 'F'.$item;
 $celda_6    = 'G'.$item;
 $celda_7    = 'H'.$item;
 $celda_borde    = 'A'.$item.':'.'H'.$item;

 $objPHPExcel->setActiveSheetIndex(1)->mergeCells($celda_combina1);


 $valor_1 = utf8_encode($obj['NOM_CARRERA']);

 $masculino     = $obj['MASCULINO']; 
 if($masculino == null){ $masculino = 0;}
 
 $total_masc = $total_masc + $masculino;

 $valor_2 = $masculino;

 $femenino     = $obj['FEMENINO']; 
 if($femenino == null){ $femenino = 0;}

 $total_feme = $total_feme + $femenino;

 $valor_3 = $femenino;

 $valor_4 = $masculino + $femenino;

 $manana     = $obj['MANANA']; 
 if($manana == null){ $manana = 0;}

 $total_man = $total_man + $manana;

 $valor_5 = $manana;

 $tarde     = $obj['TARDE']; 
 if($tarde == null){ $tarde = 0;}

 $total_tar = $total_tar + $tarde;

 $valor_6 = $tarde;
 $valor_7 = $manana + $tarde;


 if($valor_1 == null){ $valor_1 = '';}
 if($valor_2 == null){ $valor_2 = '';}
 if($valor_3 == null){ $valor_3 = '';}
 if($valor_4 == null){ $valor_4 = '';}
 if($valor_5 == null){ $valor_5 = '';}
 if($valor_6 == null){ $valor_6 = '';}
 if($valor_7 == null){ $valor_7 = '';}

 $objPHPExcel->setActiveSheetIndex(1)
             ->setCellValueExplicit($celda_1  ,  $valor_1,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_2  ,  $valor_2,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_3   , $valor_3,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_4   , $valor_4,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_5   , $valor_5,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_6   , $valor_6,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_7   , $valor_7,PHPExcel_Cell_DataType::TYPE_STRING);
 
 $objPHPExcel->setActiveSheetIndex(1)->getStyle($celda_borde)->applyFromArray(
 $BStyle
 ); 

 $item++;

 endforeach;  

 //totales
 $celda_combina1  = 'A'.$item.':'.'B'.$item;

 $celda_1    = 'A'.$item;
 $celda_2    = 'C'.$item;
 $celda_3    = 'D'.$item;
 $celda_4    = 'E'.$item;
 $celda_5    = 'F'.$item;
 $celda_6    = 'G'.$item;
 $celda_7    = 'H'.$item;
 $celda_borde    = 'A'.$item.':'.'H'.$item;

 $total_sexo = $total_masc + $total_feme;
 $total_turno= $total_man  + $total_tar;

 $objPHPExcel->setActiveSheetIndex(1)->mergeCells($celda_combina1);

 $objPHPExcel->setActiveSheetIndex(1)
             ->setCellValueExplicit($celda_1  ,  'TOTALES',PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_2  ,  $total_masc,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_3   , $total_feme,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_4   , $total_sexo,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_5   , $total_man,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_6   , $total_tar,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_7   , $total_turno,PHPExcel_Cell_DataType::TYPE_STRING);


 $objPHPExcel->setActiveSheetIndex(1)->getStyle($celda_borde)->applyFromArray(
 $BStyle
 ); 
 
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE_BIBLIOPLAX.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


?>