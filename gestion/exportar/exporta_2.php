<?php
set_time_limit(0);
//error_reporting(0);
require_once "../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
require_once APP_DIR . 'PHPExcel.php';

global $conexion, $etiquetaTitulo, $nombreModulo;

$VARIABLES=$_GET['exporta'];

$data = @explode('|', $VARIABLES);
$filtro       = $data[0];
$indActivo    = $data[1];
$indCachimbo  = $data[2];

$nomEstado  = 'ACTIVO';
$cachimbo   = 'TODOS';

if($indActivo == 0){ $nomEstado = 'INACTIVO';}
if($indCachimbo == 1){ $cachimbo = 'CACHIMBOS';}



date_default_timezone_set('America/Lima');
$hoy   = date("Y-m-d");
$fecha = @explode('-', $hoy);
$anio   = $fecha[0];
$mes   = $fecha[1];
$dia  = $fecha[2];

switch ($mes) {
  case '01':
    $nmes = 'ENE';
    break;
  case '02':
    $nmes = 'FEB';
    break;
  case '03':
    $nmes = 'MAR';
    break;
  case '04':
    $nmes = 'ABR';
    break;
  case '05':
    $nmes = 'MAY';
    break;
  case '06':
    $nmes = 'JUN';
    break;
  case '07':
    $nmes = 'JUL';
    break;
  case '08':
    $nmes = 'AGO';
    break;
  case '09':
    $nmes = 'SET';
    break;
  case '10':
    $nmes = 'OCT';
    break;
  case '11':
    $nmes = 'NOV';
    break;
  case '12':
    $nmes = 'DIC';
    break;
}

$fecha = $dia.'/'.$nmes.'/'.$anio;

$objData      = new Data_sgaexportar();
$arrayData    = $objData->fu_reporte_2($conexion,$filtro, $indActivo, $indCachimbo);

date_default_timezone_set('America/Lima');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("BIBLIOPLAX")
							 ->setLastModifiedBy("BIBLIOPLAX")
							 ->setTitle("Reporte de Usuarios")
							 ->setSubject("Reporte de Usuarios")
							 ->setDescription("Reporte de Usuarios")
							 ->setKeywords("BIBLIOPLAX")
							 ->setCategory("REPORTES");


$styleArray = array(
    	'font'  => array(
        'bold'  => true /*,
        'color' => array('rgb' => 'FF0000'),
        'size'  => 15,
        'name'  => 'Verdana'*/
    ));

$styleArrayCab = array(
    	'font'  => array(
        'bold'  => true ,
        'color' => array('rgb' => 'FFFFFF')/*,
        'size'  => 15,
        'name'  => 'Verdana'*/
    ));

$BStyle = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getDefaultStyle()->applyFromArray(
    array(
        'fill' => array(
            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
             'rgb' => 'ffffff'
        	)
        ),
    )
);


// PESTAÑA CONSOLIDADA
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:D2');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:D3');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:D4');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:D5');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'BIBLIOPLAX')
            ->setCellValue('A2', 'REPORTE DE USUARIOS')
            ->setCellValue('A3', 'FECHA REPORTE: '.$fecha)
            ->setCellValue('A4', 'ESTADO: '.trim($nomEstado))
            ->setCellValue('A5', 'USUARIOS: '.trim($cachimbo));

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1')->applyFromArray(
$styleArray
);

        
//INSERTANDO IMAGEN
$gdImage = imagecreatefromjpeg(APP_DIR .'PHPExcel/pdf.jpg');
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');
$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setHeight(70);

$objDrawing->setCoordinates('E1');

$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
//FIN IMAGEN

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A10:B10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C10:E10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F10:H10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I10:J10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K10:L10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M10:N10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O10:P10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q10:R10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('S10:T10');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('U10:V10');

// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A10', 'COD. UNIVERSITARIO')
            ->setCellValue('C10', 'NOMBRES')
            ->setCellValue('F10',  'APELLIDOS')
            ->setCellValue('I10',  'DOCUMENTO')
            ->setCellValue('K10',  'NUM. DOCUMENTO')
            ->setCellValue('M10',  'CORREO')
            ->setCellValue('O10',  'SEXO')
            ->setCellValue('Q10',  'TIPO USUARIO')
            ->setCellValue('S10',  'FACULTAD')
            ->setCellValue('U10',  'ESTADO');

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:U10')->applyFromArray(
$styleArrayCab
); 

$objPHPExcel->getActiveSheet()->getStyle('A10:U10')->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => '000000'
        )
    ));

$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:U10')->applyFromArray(
$BStyle
); 

//detalle del reporte
 $item      = 11;

 foreach ($arrayData as $obj):

 $celda_combina1  = 'A'.$item.':'.'B'.$item;
 $celda_combina2  = 'C'.$item.':'.'E'.$item;
 $celda_combina3  = 'F'.$item.':'.'H'.$item;
 $celda_combina4  = 'I'.$item.':'.'J'.$item;
 $celda_combina5  = 'K'.$item.':'.'L'.$item;
 $celda_combina6  = 'M'.$item.':'.'N'.$item;
 $celda_combina7  = 'O'.$item.':'.'P'.$item;
 $celda_combina8  = 'Q'.$item.':'.'R'.$item;
 $celda_combina9  = 'S'.$item.':'.'T'.$item;
 $celda_combina10 = 'U'.$item.':'.'V'.$item;

 $celda_1    = 'A'.$item;
 $celda_2    = 'C'.$item;
 $celda_3    = 'F'.$item;
 $celda_4    = 'I'.$item;
 $celda_5    = 'K'.$item;
 $celda_6    = 'M'.$item;
 $celda_7    = 'O'.$item;
 $celda_8    = 'Q'.$item;
 $celda_9    = 'S'.$item;
 $celda_10    = 'U'.$item;

 $celda_borde    = 'A'.$item.':'.'V'.$item;

 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina1);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina2);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina3);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina4);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina5);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina6);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina7);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina8);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina9);
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells($celda_combina10);

 $valor_1 = utf8_encode($obj['COD_UNIVERSITARIO']);
 $valor_2 = utf8_encode($obj['NOM_USUARIO']);
 $valor_3 = utf8_encode($obj['APE_USUARIO']);
 $valor_4 = utf8_encode($obj['ABR_DOCUMENTO_IDENTIDAD']);
 $valor_5 = utf8_encode($obj['NUM_DOCUMENTO_IDENTIDAD']);
 $valor_6 = utf8_encode($obj['DES_MAIL']);
 $valor_7 = utf8_encode($obj['DES_SEXO']);
 $valor_8 = utf8_encode($obj['NOM_USUARIO_TIPO']);
 $valor_9 = utf8_encode($obj['NOM_CARRERA']);
 $valor_10 = utf8_encode($obj['DES_ESTADO']);



 if($valor_1 == null){ $valor_1 = '';}
 if($valor_2 == null){ $valor_2 = '';}
 if($valor_3 == null){ $valor_3 = '';}
 if($valor_4 == null){ $valor_4 = '';}
 if($valor_5 == null){ $valor_5 = '';}
 if($valor_6 == null){ $valor_6 = '';}
 if($valor_7 == null){ $valor_7 = '';}
 if($valor_8 == null){ $valor_8 = '';}
 if($valor_9 == null){ $valor_9 = '';}
 if($valor_10 == null){ $valor_10 = '';}


 $objPHPExcel->setActiveSheetIndex(0)
             ->setCellValueExplicit($celda_1  , $valor_1,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_2  , $valor_2,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_3  , $valor_3,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_4  , $valor_4,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValueExplicit($celda_5  , $valor_5,PHPExcel_Cell_DataType::TYPE_STRING)
             ->setCellValue($celda_6  , $valor_6)
             ->setCellValue($celda_7  , $valor_7)
             ->setCellValue($celda_8  , $valor_8)
             ->setCellValue($celda_9  , $valor_9)
             ->setCellValue($celda_10 , $valor_10);

 $objPHPExcel->setActiveSheetIndex(0)->getStyle($celda_borde)->applyFromArray(
 $BStyle
 ); 

 $item++;

 endforeach;  
//fin detalle

///////////////////////////////////////////////////////////////////////////////////////////////////

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('USUARIOS');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="USUARIOS_BIBLIOPLAX.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


?>