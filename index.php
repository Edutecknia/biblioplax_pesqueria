<?php
require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

global $conexion;

$idFacultad          = 48; 
$modelData           = new Data_sganoticia();
$arrayNoticias       = $modelData->fu_listarTodoNoticia($conexion, $idFacultad);
$arrayEventos        = $modelData->fu_listarTodoEventos($conexion, $idFacultad);

$modelData           = new Data_sgalibro();
//$arrayUltimas        = $modelData->fu_listarUltimasAdquisiciones($conexion, $idFacultad);
//$objCatalogo         = $modelData->fu_cantidadCatalogo($conexion, $idFacultad);


session_start();

$flagLogedo = 1;

if($_SESSION['BTK_USUARIO']==NULL){
session_destroy();
$flagLogedo = 0;
}
else{
    $objUsuario   = $_SESSION['BTK_USUARIO'];
    $idUsuario    = $objUsuario->__get('_sess_usu_id');

    $objsga_usuario = new Data_sgausuario();
    $usuario        = $objsga_usuario->fu_encontrar($conexion,$idUsuario);    
}

?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="http://pesqueria.lamolina.edu.pe/pesqueria/img/escudo2.png">
    <title>Facultad de Pesqueria</title>

    <?php include 'css.php' ?>

</head>

<body id="page-top" class="index">

<div class="barraPrincipal" id="colores">
   </div>

   <!--<div class="barraLogoUniversidad" id="logouniversidad">
   </div>-->

   <?php include 'menu.php' ?>

    
    <!--<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">

        <div class='item active'>
        <img src="img/slider/1.jpg" alt="UNJFSC">        
        </div>

        <div class='item'>
        <img src="img/slider/2.jpg" alt="UNJFSC">        
        </div>

        <div class='item'>
        <img src="img/slider/3.jpg" alt="UNJFSC">        
        </div>

    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>-->



  
     <header>
        <div class="container">
            <div class="intro-text">
                
                <div class="intro-lead-in">Centro de Documentación e Información Pesquera</div>

                <form action="busqueda.php" method="post">
                <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input class="form-control input-lg" placeholder="Buscar Libros, Revistas, Autores..." type="text" id="txtBusqueda" name="txtBusqueda">
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
                </div>
                </form>

                <div class="intro-heading">&nbsp;</div>
                
            </div>
        </div>
    </header>
  
        <!-- Services Section -->

   

    <!-- Services Section -->
    <?php
    $texto_libros   = "Más de ".$objCatalogo['EJEMPLARES']." libros a tú disposición";
    
    if($objCatalogo['REVISTAS'] > 0){ $texto_revista = "Más de ".$objCatalogo['REVISTAS']." revistas digitalizadas"; } else {$texto_revista = 'Revistas Digitales';}
    if($objCatalogo['VIDEO'] > 0){ $texto_video      = "Más de ".$objCatalogo['VIDEO']." Tesis digitales"; } else {$texto_video = 'Videos Digitales';}
    
    if($objCatalogo['AUDIO'] > 0){ $texto_audio = "Más de ".$objCatalogo['AUDIO']." Audios "; } else {$texto_audio = 'Audios';}
    if($objCatalogo['CARTA'] > 0){ $texto_carta = "Más de ".$objCatalogo['CARTA']." manuscritos"; } else {$texto_carta = 'Manuscritos de importantes autores';}
    
    $texto_imagen   = "Miles de imagenes a tú disposición";

    ?>
    <section id="catalogo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#464646">CATÁLOGO</h2>
                    <!--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
                </div>
            </div>

                <div class="row text-center">
                <div class="col-md-4">
                    <a >
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-book fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading" style="color:#464646">Libros</h4>
                    <p class="text-muted"><?php echo $texto_libros; ?></p>
                    </a>
                </div>
                
                <div class="col-md-4">
                    <a>
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading" style="color:#464646">Revistas</h4>
                    <p class="text-muted"><?php echo $texto_revista; ?></p>
                    </a>
                </div>
                
                <div class="col-md-4">
                    <a >
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-book fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading" style="color:#464646">Tesis</h4>
                    <p class="text-muted"><?php echo $texto_video; ?></p>
                    </a>
                </div>



            </div>


            </div>
        </div>
    </section>


    <!-- About Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#464646">BASES DE DATOS</h2>
                    <!--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
                </div>
            </div>
             <div class="row">
               <center>
                <div class="col-md-3 col-sm-6">
                    <a href="https://www.ebscohost.com/" target="_blank">
                        <img src="img/bases/1.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Ebsco Research Database</h4></center>
                </div>


                <div class="col-md-3 col-sm-6">
                    <a href="http://alicia.concytec.gob.pe/vufind/" target="_blank">
                        <img src="img/bases/2.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Alicia</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://www.scopus.com/sources?zone=&origin=NO%20ORIGIN%20DEFINED" target="_blank">
                        <img src="img/bases/3.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Scopus</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://login.webofknowledge.com/error/Error?Error=IPError&PathInfo=%2F&RouterURL=https%3A%2F%2Fwww.webofknowledge.com%2F&Domain=.webofknowledge.com&Src=IP&Alias=WOK5"
                    target="_blank" >
                        <img src="img/bases/4.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Web of Science</h4></center>
                </div>

                <div class="col-md-12"><br><br></div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.sciencemag.org/" target="_blank">
                        <img src="img/bases/5.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Science Magazine</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.proquest.com/LATAM-ES/" target="_blank">
                        <img src="img/bases/6.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>ProQuest</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://portal.concytec.gob.pe/" target="_blank">
                        <img src="img/bases/concytec.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Concytec</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.sciencedirect.com/" target="_blank">
                        <img src="img/bases/science.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Science Direct</h4></center>
                </div>
                 </center>
            </div>
        </div>
    </section>

   

    <img src="img/Linea_dorado.png" class="img-responsive" />

   <?php include 'footer.php' ?>




       <!--MODAL LOGIN-->
    <?php if($flagLogedo == 0){ ?>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <img class="img-circle" id="img_logo" src="img/logo.png">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                </div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <form id="login-form">
                        <div class="modal-body">
                            <div id="div-login-msg">
                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-login-msg" style="color:#000;">Ingrese su usuario y contraseña.</span>
                            </div>
                            <input id="login_username" class="form-control" type="text" placeholder="Usuario" required>
                            <input id="login_password" class="form-control" type="password" placeholder="Contraseña" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="button" onclick="javascript:loginUsuario()" class="btn btn-primary btn-lg btn-block">Ingresar</button>
                            </div>
                            <div>
                            </div>
                        </div>
                    </form>
                    <!-- End # Login Form -->
                    
                    <!-- Begin | Lost Password Form -->
                    <form id="lost-form" style="display:none;">
                        <div class="modal-body">
                            <div id="div-lost-msg">
                                <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-lost-msg">Ingrese su e-mail</span>
                            </div>
                            <input id="lost_email" class="form-control" type="text" placeholder="E-Mail" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
                            </div>
                            <div>
                                <button id="lost_login_btn" type="button" class="btn btn-link">Ingresar</button>
                                <button id="lost_register_btn" type="button" class="btn btn-link">Registrarse</button>
                            </div>
                        </div>
                    </form>
                    <!-- End | Lost Password Form -->
                    
                    <!-- Begin | Register Form -->
                    <form id="register-form" style="display:none;">
                        <div class="modal-body">
                            <div id="div-register-msg">
                                <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-register-msg">Registrarse como evaluador</span>
                            </div>
                            <input id="register_username" class="form-control" type="text" placeholder="Usuario" required>
                            <input id="register_email" class="form-control" type="text" placeholder="E-Mail" required>
                            <input id="register_password" class="form-control" type="password" placeholder="Contraseña" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Registrarse</button>
                            </div>
                            <div>
                                <button id="register_login_btn" type="button" class="btn btn-link">Ingresar</button>
                                <button id="register_lost_btn" type="button" class="btn btn-link">Olvidaste la contraseña?</button>
                            </div>
                        </div>
                    </form>
                    <!-- End | Register Form -->
                    
                </div>
                <!-- End # DIV Form -->
                
            </div>
        </div>
    </div>
    <!-- END # MODAL LOGIN -->
    <?php } ?>
    <!--MODAL LOGIN FIN-->






    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->
        <!--DETALLE DE NOTICIAS-->

<div class="portfolio-modal modal fade" id="ModalNoticias" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>-->
                <div class="container">

                  <div id="ContenedorModalNoticias"></div>
                    
                </div>
            </div>
        </div>
    </div>
  

 
    <!-- jQuery -->
    <?php include 'js.php'; ?>


 <script>
$(document).ready(function(){

  funcionScroll();

  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });

  $('#CarouselNoticias').bxSlider({
    slideWidth: 500,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 10,
    auto: true  
    });

  $('#CarouselEventos').bxSlider({
    slideWidth: 500,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 10,
    auto: true /*,
    mode: 'vertical' */
    });

  /*$("#linkemergente").trigger("click");*/

});
</script>

</body>

</html>
