 <footer id="footer">
        <div class="container">
            <div class="row" >

                 <div class="col-md-4" style="text-align:left">
                    <h4><span class="letrablanca">CONTÁCTENOS:</span></h4>
                    <ul class="nospace linklist">
          <li style="line-height:10px;"><input type="text" class="form-control" placeholder="Ingrese su nombre"></li>
          <li style="line-height:10px;"><input type="text" class="form-control" placeholder="Ingrese su e-mail"></li>
          <li style="line-height:10px;"><textarea class="form-control" placeholder="Ingrese su mensaje"></textarea></li>
          <li style="line-height:10px;"><button type="button" class="btn btn-primary" style="color:#0070C6;background-color:#fff;border-color:#fff;">Enviar mensaje</button></li>
          <li style="font-size:12px;line-height:10px;">&nbsp;</li>
                </ul>

                </div>

                <!--<div class="col-md-4" style="text-align:left">
                    <span class="letrablanca">ORGANOS DE APOYO ACADEMICO:</span><p>&nbsp;</p>
                    <ul class="nospace linklist">
          <li style="line-height:10px;"><a style="font-size:12px" 
                                        href="#portfolioModal1" class="portfolio-link" data-toggle="modal">- Oficina de Registro y Asuntos Académicos.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal2" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Oficina Central de Admisión.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal3" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Instituto de Idiomas.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal4" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Biblioteca Central.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal5" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Centro de Proyección Social y Ext. Universitaria.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal6" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Centro Universitario de Arte y Cultura.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal7" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Museo Arqueológico.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal8" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Centro Médico.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal9" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Oficina de Bienestar Universitario.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal10" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Oficina de Relaciones Internacionales.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal11" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Unidad de Grados y Títulos.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal12" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Defensoria Universitaria.</a></li>
          <li style="line-height:10px;"><a href="#portfolioModal13" class="portfolio-link" data-toggle="modal" style="font-size:12px">- Secretaria Académica.</a></li>
          <li style="font-size:12px;line-height:10px;">&nbsp;</li>
                </ul>

                </div>
                <div class="col-md-4" style="text-align:left">
                    <span class="letrablanca">ORGANOS DE APOYO ADMINISTRATIVO:</span><p>&nbsp;</p>
                    <ul class="nospace linklist">
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Dirección General Administrativa.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Recursos Humanos.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Logística.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Servicios Generales.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Economia y Contabilidad.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Imagen Institucional.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Servicios Informática.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Asesoria Jurídica.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Planificación y Presupuestos.</a></li>
          <li style="line-height:10px;"><a href="#" style="font-size:12px">- Oficina de Proyección y Formulación de Proytectos.</a></li>
          <li style="font-size:12px;line-height:10px;">&nbsp;</li>
                </ul>

                </div>-->
                <div class="col-md-3" style="text-align:left"></div>
                <div class="col-md-5" style="text-align:left">
                   <span class="letrablanca">CONTACTO:</span><p>&nbsp;</p>
                   <ul class="nospace linklist">
          <li style="font-size:12px;line-height:10px;"> Av. Mercedes Indacochea N° 609</li>
          <li style="font-size:12px;line-height:10px;"> Puerta N°1, Huacho - Perú</li>
          <li style="font-size:12px;line-height:10px;"> rectorado@unijfsc.edu.pe</li>
          <li style="font-size:12px;line-height:10px;"> 232 2118 / 232 6097</li>
          <li style="line-height:10px;"> <a href="#" style="font-size:12px" class="link_a"> Mapa de sitio</a></li>
          <li style="line-height:10px;"> <a href="https://www.facebook.com/unjfschuacho/?rf=111692585530889" target="_blank"><img src="img/redes/facebook.png"/></a>&nbsp;
          <a href="#"><img src="img/redes/twiter.png"/></a>&nbsp;
          <a href="https://www.youtube.com/channel/UC5dyZCRqxozu7gI6Lfb8XrA" target="_blank"><img src="img/redes/youtube.png"/></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <div class="col-md-10" style="background-color:#0070c6;">
      <p style="font-size:12px">&nbsp;</p>
    </div>
 
    <div class="col-md-2" style="background-color:#0070c6;color:#fff;text-align:right;">
    <img src="img/edutecknia.png" class="img-responsive" style="width:200px;height:30px">
    </div>