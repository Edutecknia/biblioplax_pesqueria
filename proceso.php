<?php
//error_reporting(0);
require_once("/config/web.config.php");
//
function __autoload($className)
{
    $className = 'class_'.$className;
    $arrayClassName= explode("_", $className);
    $elemUlt = $arrayClassName[(count($arrayClassName)-1)];
    unset($arrayClassName[(count($arrayClassName)-1)]);

    $base = strtolower(implode("/", $arrayClassName));

    $ruta = APP_DIR . $base . "/class.".$elemUlt . '.php';

    if(file_exists($ruta))
        require_once  APP_DIR . $base ."/class.".$elemUlt . '.php';
}

	$_SESSION['pDB'] = array(
    'dbType'=>$dbType,
	'dbHost'=>$dbHost,
	'dbUser'=>$dbUser,
	'dbPass'=>$dbPass,
	'dbName'=>$dbName
	);

$conexion = conn();

procesarTransaccion();

function procesarTransaccion(){

	global $conexion, $etiquetaTitulo, $nombreModulo;

try {
		
		$id = '';
		$arrayTemporal 	= traerTemporal($conexion);

		foreach ($arrayTemporal as $obj):
		
		if($id != $obj['ID_LIBRO']){

			$id = $obj['ID_LIBRO'];
		
		//registrando libro
		$autores = $obj['AUTOR'];
		$idLibro = $obj['ID_LIBRO'];
		$codLibro = $obj['COD_LIBRO'];
		$idFacultad = $obj['ID_FACULTAD'];
		$idLibroTipo = 6;
		$desTitulo = utf8_encode($obj['NOMBRE']);
		$idEditorial = $obj['ID_EDITORIAL'];;
		$anioPublica = $obj['ANIO'];
		$idPais 	 = $obj['ID_PAIS'];
		$numPagina   = $obj['PAGINAS'];
		$numVolumen  = $obj['VOLUMEN_EDICION'];
		$idAdquisicion = '2';
		$idProveedor  = '';
		$fecAdquisicion = '2017-06-28';
		$preLibro = '';
		$temasRelacion = '0,';
		$idUsuario = '9999';
		$ip = '127.0.0.1';
		$isbn = $obj['ISBN'];
		$codBarra = '';
		$codPatrimonio = $obj['COD_PATRIMONIO']; 

		if(!is_numeric($numPagina)){ $numPagina = ''; }


		$desTitulo = str_replace(
			array("'",'"'),
			'', $desTitulo);
		
		$desTitulo = utf8_decode($desTitulo);

			if(trim($idLibroTipo)==""){$idLibroTipo='NULL';}else{ $idLibroTipo= "'" . trim($idLibroTipo) . "'"  ;}
			if(trim($desTitulo)==""){$desTitulo='NULL';}else{ $desTitulo= "'" . trim($desTitulo) . "'"  ;}
			if(trim($idEditorial)==""){$idEditorial='NULL';}else{ $idEditorial= "'" . trim($idEditorial) . "'"  ;}
			if(trim($anioPublica)==""){$anioPublica='NULL';}else{ $anioPublica= "'" . trim($anioPublica) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($numPagina)==""){$numPagina='NULL';}else{ $numPagina= "'" . trim($numPagina) . "'"  ;}
			if(trim($numVolumen)==""){$numVolumen='NULL';}else{ $numVolumen= "'" . trim($numVolumen) . "'"  ;}
			if(trim($idAdquisicion)==""){$idAdquisicion='NULL';}else{ $idAdquisicion= "'" . trim($idAdquisicion) . "'"  ;}
			if(trim($idProveedor)==""){$idProveedor='NULL';}else{ $idProveedor= "'" . trim($idProveedor) . "'"  ;}
			if(trim($fecAdquisicion)==""){$fecAdquisicion='NULL';}else{ $fecAdquisicion= "'" . trim($fecAdquisicion) . "'"  ;}
			if(trim($preLibro)==""){$preLibro='NULL';}else{ $preLibro= "'" . trim($preLibro) . "'"  ;}
			if(trim($temasRelacion)==""){$temasRelacion='NULL';}else{ $temasRelacion= "'" . trim($temasRelacion) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}

		$sql = "INSERT INTO BTK_LIBRO". 
		"(ID_LIBRO,			cod_escuela,			ID_LIBRO_TIPO,			DES_TITULO, ".
		"ID_EDITORIAL,		NUM_ANIO_PUBLICA,	ID_PAIS,".
		"NUM_PAGINA,			NUM_VOLUMEN,		ID_LIBRO_ADQUISICION,	ID_PROVEEDOR,".
		"FEC_ADQUISICION,	PRE_LIBRO,			DES_CATEGORIA_RELACION,".
		"IND_ACTIVO,			FEC_REGISTRO,		ID_USUARIO_REGISTRO,	IP_REGISTRO".
		")".
		"VALUES($idLibro,$idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, $numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro".
		",$temasRelacion, 1, getdate(), $idUsuario, $ip);";

			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();

		$posicion_coincidencia = strpos($autores, '/');
		if($posicion_coincidencia === false){

				$posicion_coincidencia = strpos($autores, '-');
			
				if($posicion_coincidencia === false){

					$autores = str_replace(
					array(",",';',"."),
					' ', $autores);

					$autores = utf8_encode($autores);

					if($autores != '' && $autores != null){
					$rpta = fu_registrarAutor($conexion, $idLibro, $autores);
					}

				}
				else{
					$data = @explode('-', $autores);

					for ($i=0; $i < count($data); $i++) { 

						$nombre = utf8_encode($data[$i]);
						if($nombre != '' && $nombre != null){
						$rpta = fu_registrarAutor($conexion, $idLibro, $nombre);
						}
					}
				}


			

		}
		else{
			$data = @explode('/', $autores);

			for ($i=0; $i < count($data); $i++) { 

				$nombre = utf8_encode($data[$i]);
				if($nombre != '' && $nombre != null){
				$rpta = fu_registrarAutor($conexion, $idLibro, $nombre);
				}
			}

		}

		}

		
		////////////////// FIN REGISTRAR LIBRO		

		//ejemplares
		$rpta = fu_registrarEjemplar($conexion, $obj['ID_LIBRO'], $obj['ISBN'], $obj['COD_LIBRO'], $codBarra, $obj['COD_PATRIMONIO'], '9999', '127.0.0.1');
		//fin ejemplares

		endforeach;
		
		echo 'termine';
		
	} catch (Exception $e) {
		$result = array(
                    'error' => $e->getMessage(),
        );
	}

}

function traerTemporal($conexion){

	try {
			
			$sql 	= "SELECT FILA,ID_LIBRO,COD_LIBRO,NOMBRE,AUTOR,ANIO,EDITORIAL,PAIS,PAGINAS,COD_PATRIMONIO,ID_PAIS,ID_EDITORIAL,ISBN,VOLUMEN_EDICION FROM BTK_TEMP_MATERIAL ORDER BY ID_LIBRO";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }

}

 function fu_registrarEjemplar($conexion, $idLibro, $isbn, $codLibro, $codBarra, $codPatrimonio, $idUsuario, $ip) {
		try {
			
			if($isbn == 'undefined'){ $isbn = '';}

			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($isbn)==""){$isbn='NULL';}else{ $isbn= "'" . trim($isbn) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			if(trim($codBarra)==""){$codBarra='NULL';}else{ $codBarra= "'" . trim($codBarra) . "'"  ;}
			if(trim($codPatrimonio)==""){$codPatrimonio='NULL';}else{ $codPatrimonio= "'" . trim($codPatrimonio) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "CALL USP_BTK_LIBRO_EJEMPLAR_REGISTRAR ($idLibro, $isbn, $codLibro, $codBarra, $codPatrimonio, $idUsuario, $ip) ";
			//echo $sql.'<br>';
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

 function fu_registrarAutor($conexion, $idLibro, $nomAutor) {
		try {
			
			$nomAutor = utf8_decode($nomAutor);

			if(trim($nomAutor)!=""){

				if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
				if(trim($nomAutor)==""){$nomAutor='NULL';}else{ $nomAutor= "'" . trim($nomAutor) . "'"  ;}
		
				$sql = "CALL USP_BTK__TEMP_LIBRO_AUTOR_REGISTRAR ($idLibro, $nomAutor)";
				
				$stm = $conexion->query($sql);
				$result = $stm->fetch();
				return $result;
			}
			else{
				return 'nulo';
			}
			

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}
/*********************************************************************/
		
/**
* @param PDOStatement $oStm
*/
function closeCursor($oStm) {
    echo "close";
	do $oStm->fetchAll();
	while ($oStm->nextRowSet());
} 

    function conn()
    {
      
        $dbMotor    = $_SESSION['pDB']['dbType'];
        $hostname   = $_SESSION['pDB']['dbHost'];
        $userBd     = $_SESSION['pDB']['dbUser'];
        $passwordBd = $_SESSION['pDB']['dbPass'];
        $nameBd     = $_SESSION['pDB']['dbName'];
        
        $return_value = null;
        try{
             $host = '';
            if ($dbMotor == 'MSSQL'):
                $host .= "mssql:host=" . $hostname . ";";
                $host .= "dbname=" . $nameBd;
            endif;

            $db = new PDO('odbc:Driver={SQL Server}; Server=' . $hostname . '; Database=' . $nameBd . '; Uid=' . $userBd . '; Pwd=' . $passwordBd . ';');
            //$db -> exec("set names utf8");
            return $db;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

	
    function closedb($cxn) {
        try {
            unset($cxn);
            //echo "\nConexi�n PDO cerrada.";
        } catch (PDOException $ex) {
             echo "Error al cerrar conexión." .$ex->getMessage();
        }
    }



?>