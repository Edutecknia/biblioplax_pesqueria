<?php
require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';

global $conexion;

$idFacultad          = 48; 
$modelData           = new Data_sganoticia();
$arrayNoticias       = $modelData->fu_listarTodoNoticia($conexion, $idFacultad);
$arrayEventos        = $modelData->fu_listarTodoEventos($conexion, $idFacultad);

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="img/logo.png">
    <title>Facultad de Pesquería</title>

    <?php include 'css.php' ?>
    <link rel="stylesheet" href="gestion/plugins/select2/select2.min.css">
</head>

<body id="page-top" class="index">

<div class="barraPrincipal" id="colores">
   </div>

   <!--<div class="barraLogoUniversidad" id="logouniversidad">
   </div>-->

   <?php include 'menu.php' ?>

    
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">

        <div class='item active'>
        <img src="img/slider/bienvenida.jpg" alt="UNJFSC">        
        </div>

    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>


        <section id="busqueda">
            <div class="container">
            <div class="row">

                <input type="hidden" name="idFacultad" id="idFacultad" value="<?php echo $idFacultad; ?>">
                <input type="hidden" name="idFiltro" id="idFiltro" value="catalogo">
                <input type="hidden" name="txtDescripcion" id="txtDescripcion" value="">
                <input type="hidden" name="txtCodigo" id="txtCodigo" value="">
                <input type="hidden" name="idTemas" id="idTemas" value="">
                <input type="hidden" name="idCatalogo" id="idCatalogo" value="6">

                   <div class="col-md-12 table-responsive" id="divResultado">
                   </div>         

            </div>
            </div>
        </section>

                

    <!-- About Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#464646">BASES DE DATOS</h2>
                    <!--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
                </div>
            </div>
             <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="https://www.ebscohost.com/" target="_blank">
                        <img src="img/bases/1.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Ebsco Research Database</h4></center>
                </div>


                <div class="col-md-3 col-sm-6">
                    <a href="http://alicia.concytec.gob.pe/vufind/" target="_blank">
                        <img src="img/bases/2.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Alicia</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://www.scopus.com/sources?zone=&origin=NO%20ORIGIN%20DEFINED" target="_blank">
                        <img src="img/bases/3.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Scopus</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://login.webofknowledge.com/error/Error?Error=IPError&PathInfo=%2F&RouterURL=https%3A%2F%2Fwww.webofknowledge.com%2F&Domain=.webofknowledge.com&Src=IP&Alias=WOK5"
                    target="_blank" >
                        <img src="img/bases/4.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Web of Science</h4></center>
                </div>

                <div class="col-md-12"><br></div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.sciencemag.org/" target="_blank">
                        <img src="img/bases/5.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Science Magazine</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.proquest.com/LATAM-ES/" target="_blank">
                        <img src="img/bases/6.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>ProQuest</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://portal.concytec.gob.pe/" target="_blank">
                        <img src="img/bases/concytec.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Concytec</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.sciencedirect.com/" target="_blank">
                        <img src="img/bases/science.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Science Direct</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://repositorio.unjfsc.edu.pe/" target="_blank">
                        <img src="img/bases/logo.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Dspace</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://revistas.unjfsc.edu.pe/" target="_blank">
                        <img src="img/bases/logo.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>OJS</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://datos.unjfsc.edu.pe/" target="_blank">
                        <img src="img/bases/logo.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Dkan</h4></center>
                </div>
               
            </div>
        </div>
    </section>

   

    <img src="img/Linea_dorado.png" class="img-responsive" />

   <?php include 'footer.php' ?>

    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->
            <!--DETALLE DE NOTICIAS-->

<div class="portfolio-modal modal fade" id="ModalNoticias" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>-->
                <div class="container">

                  <div id="ContenedorModalNoticias"></div>
                    
                </div>
            </div>
        </div>
    </div>
 
    <!-- jQuery -->
    <?php include 'js.php'; ?>
    <script src="gestion/plugins/select2/select2.full.min.js"></script>


 <script>
$(document).ready(function(){

  funcionScroll();

  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });

  $('#CarouselNoticias').bxSlider({
    slideWidth: 500,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 10,
    auto: true  
    });

  $('#CarouselEventos').bxSlider({
    slideWidth: 500,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 10,
    auto: true /*,
    mode: 'vertical' */
    });

  buscarLibro();
  /*$("#linkemergente").trigger("click");*/

});
</script>

</body>

</html>
