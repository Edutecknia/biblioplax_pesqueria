// Agency Theme JavaScript

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });


    // Closes the Responsive Menu on Menu Item Click
    $('.cerrarmenu').click(function(){ 
            $('.navbar-toggle:visible').click();
    });

    /*navbar-collapse ul li a*/
    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

})(jQuery); // End of use strict

function funcionScroll(){

    $(window).on('scroll', function(e){
  
  
    var screenTop = $(document).scrollTop();
    var ancho = $(window).width();
  
    if(ancho >= 768){
        
    if(screenTop == 0){
        $("#logouniversidad").css("display","block");
    }else{
        $("#logouniversidad").css("display","none");
    };

    }

    });

}

function mostrar(){
    var filtro = $("#idFiltro").val();

    if(filtro == 'tema'){
        $("#divCodigo").css("display","none");
        $("#divDescripcion").css("display","none");
        $("#divCatalogo").css("display","none");
        $("#divTema").css("display","block");
    }
    else{
    if(filtro == 'codigo'){
        $("#divCodigo").css("display","block");
        $("#divTema").css("display","none");
        $("#divCatalogo").css("display","none");
        $("#divDescripcion").css("display","none");
    }
    else{
    if(filtro == 'catalogo'){
        $("#divCodigo").css("display","none");
        $("#divDescripcion").css("display","none");
        $("#divTema").css("display","none");
        $("#divCatalogo").css("display","block");
    }
    else{
         $("#divCodigo").css("display","none");
        $("#divTema").css("display","none");
        $("#divCatalogo").css("display","none");
        $("#divDescripcion").css("display","block");
        }   
     }
  }
}

function buscarLibro(){

    var idFacultad      = $("#idFacultad").val();
    var idFiltro        = $("#idFiltro").val();
    var descripcion     = $("#txtDescripcion").val();
    var codigo          = $("#txtCodigo").val();
    var idTemas         = $("#idTemas").val();
    var idCatalogo      = $("#idCatalogo").val();
    
    INI_LOADING.show('PROCESANDO...');
    $.post("gestion/modulos/prestamos/prestamos_web.php?cmd=buscarLibro", {
        idFacultad: idFacultad, idFiltro:idFiltro, descripcion: descripcion, codigo:codigo, idTemas:idTemas, idCatalogo:idCatalogo
    }, function(data) {
        $("#divResultado").html(data);
        INI_LOADING.hide();
    });

}

function OpenForm2(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'visualizar') {
        cmd = "loadVer";
        titulo = "Visualizar Libro";
    }
  
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("gestion/modulos/prestamos/prestamos_web.php?cmd=" + cmd, {id: id},
        function(data) {
            $("#childModal1").html(data);
            
        });

        $("#childModal1").modal({
        });
    });
}

function modalDetalleNoticia(id){

    $.post("gestion/modulos/noticias/noticiasweb.php?cmd=detalleNoticia", {
        id: id
    }, function(data) {
        $("#ContenedorModalNoticias").html(data);
        $("#divmodalNoticias").trigger("click");
    });

}



function loginUsuario(){
    
    var $formLogin = $('#login-form');
    var $formLost = $('#lost-form');
    var $formRegister = $('#register-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 300;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;


    var usu = $("#login_username").val();
    var pas = $("#login_password").val();
    $.post("gestion/modulos/login/login.php?cmd=login", {
       pas:pas,usu:usu
    }, function(data) {
        if(data=='passed'){
            msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Bienvenido...");
            setTimeout(function(){ location.reload();   }, 2000);
        }
        else{
        msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Usuario o Contraseña Incorrecta");
        }
    });


                var $lg_username=$('#login_username').val();
                var $lg_password=$('#login_password').val();
                /*if ($lg_username == "ERROR") {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");
                } else {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Login OK");
                }*/
    
    $('#login_register_btn').click( function () { modalAnimate($formLogin, $formRegister) });
    $('#register_login_btn').click( function () { modalAnimate($formRegister, $formLogin); });
    $('#login_lost_btn').click( function () { modalAnimate($formLogin, $formLost); });
    $('#lost_login_btn').click( function () { modalAnimate($formLost, $formLogin); });
    $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); });
    $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); });
    
    function modalAnimate ($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height",$oldH);
        $oldForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $newH}, $modalAnimateTime, function(){
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }
    
    function msgFade ($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function() {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }
    
    function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
        var $msgOld = $divTag.text();
        msgFade($textTag, $msgText);
        $divTag.addClass($divClass);
        $iconTag.removeClass("glyphicon-chevron-right");
        $iconTag.addClass($iconClass + " " + $divClass);
        setTimeout(function() {
            msgFade($textTag, $msgOld);
            $divTag.removeClass($divClass);
            $iconTag.addClass("glyphicon-chevron-right");
            $iconTag.removeClass($iconClass + " " + $divClass);
        }, $msgShowTime);
    }
}

function cerrarSesion(){

    $.post("gestion/modulos/login/login.php?cmd=cerrarSesion", {
    }, function(data) {
        $(location).attr('href','/biblioplax/index.php');
    });
}




function AgregarEjemplar(idLibro,nomLibro,disponible,idFacultad,nomFacultad,ejemplares){

    var flaglogin       = $("#txtFlagLogin").val();

    if(flaglogin == 0){
        alert("Debes iniciar sesión para continuar con la solicitud de préstamo");
        return;
    }

    var idBiblioteca    = $("#txtBiblioteca").val();
    var id              = '';
    var data            = '';
    var flag            = '0';

    if(disponible <= 0){
        alert("No se encuentra ejemplares disponibles del registro seleccionado");
        return;
    }

    if(idBiblioteca != ''){

     if(idBiblioteca != idFacultad){ 
        alert("No puede agregar ejemplares de distintas bibliotecas en la misma solicitud de préstamo");
        return;
     }

    }

     $(".detalles").each(function(i,x){
        id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        if(id == idLibro){ flag = '1'; }
        
    });

    if(flag == '1'){ alert("El ejemplar ya fue agregado a la solicitud"); return; }  

    $("#txtBiblioteca").val(idFacultad);

    $("#divSolicitado").css("display","block");

    var html = '<tr style="color:#333;"><td><input type="hidden" id=txtlibro_'+idLibro+' value= '+idLibro+' class="detalles" >'+nomFacultad+'</td>'+
                '<td>'+ nomLibro +'</td>'+
                '<td><input type="number" id=txtcant_'+idLibro+' value="1" min="1" max="'+disponible+'" size="5" /></td>'+
                '<td align=center><a class="cursor-point" style="cursor:pointer;"  title=Eliminar onclick="quitarEjemplar(this)"><span class="label label-danger">ELIMINAR</span></a></td></tr>';
      $('#detalle').append(html);

        var filas = $('#detalle tr').length;
        $("#txtSeleccionado").html("Ejemplares Solicitados: " + filas);
        $("#btnregistrar").prop("disabled", false);


}        

function quitarEjemplar(objeto){
    $(objeto).parent('td').parent('tr').remove();
    var filas = $('#detalle tr').length;
    if(filas <= 0){
        $("#txtBiblioteca").val('');
        $("#divSolicitado").css("display","none");
    }
    $("#txtSeleccionado").html("Ejemplares Solicitados: " + filas);

}

function registrarPrestamo(){
    var id              = '';
    var data            = '';
    var detLibro        = '';
    var detCant         = '';
    var idBiblioteca    = $("#txtBiblioteca").val();
    var diasPrestamo    = 0;
    var filas           = $('#detalle tr').length;
    var valor           = '';

    if(filas <= 0){
        alert("Debe agregar ejemplares a solicitar");
        return;
    }

     $(".detalles").each(function(i,x){
        id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        valor       = $("#txtlibro_"+id).val();

        if(detLibro == ''){
            detLibro = valor;
        }
        else{
            detLibro = detLibro + '|' + valor;
        }

        valor       = $("#txtcant_"+id).val();

        if(detCant == ''){
            detCant = valor;
        }
        else{
            detCant = detCant + '|' + valor;
        }

    });

        INI_LOADING.show('PROCESANDO...');
    $.post("gestion/modulos/prestamo_ind/prestamo_ind.php?cmd=registrar", {
        idBiblioteca:idBiblioteca, detLibro:detLibro, detCant:detCant,diasPrestamo:diasPrestamo
    }, function(data) {
        if (data>=1) {
            alert("Registro guardado correctamente. Código de solicitud: "+data);
            window.open("gestion/modulos/prestamo_ind/vistas/descarga.php");
            setTimeout(function() { window.open('http://localhost:90/imprimir.php','_blank') },3000);
            setTimeout(function(){ location.href ="index.php";  }, 6000);
            INI_LOADING.hide();
            
        } else {
               if(data == 'failed'){
                    alert('Error al registrar. Intente nuevamente');
                    INI_LOADING.hide();
               }
        else{
               bootbox.alert(data);
               INI_LOADING.hide();
         }
        }
    });             
}

/***********************/
var INI_LOADING = INI_LOADING || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
            '<div class="modal-header"><h3 style="margin:0;color:#333;"></h3></div>' +
            '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
            '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         *                options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *                options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {

            message = 'Procesando...';
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);