// mTipoHabitacion.js
$(document).ready(function() {
    $("#childModal1").html("");
    $("#childModal1").dialog("open");
    $("#childModal1").dialog('destroy').remove();
});

function MainForm(){
	 loadMenu('prestamos');rutaMenu('Solicitudes de Préstamo','Préstamos','Solicitudes');
}

function listado(){
	var idFacultad	= $("#idFacultad").val();
	var idPrestamo 	= $("#txtCodigo").val();
	var codigoU		= $("#txtCodigoU").val();
    var fini 		= $("#fechaInicio").val();
    var ffin 		= $("#fechaFin").val();
	var indActivo	= $("#cboEstado").val();
	var orden 		= $("#orden").attr("value");
	var direccion	= $("#direccion").attr("value");
	var page 		= $("#pag_actual").attr("value");
	
	INI_LOAD();
    $.post("modulos/prestamos/prestamos.php?cmd=listar", {
       idFacultad:idFacultad, idPrestamo:idPrestamo, codigoU:codigoU, fini: fini, ffin:ffin, indActivo: indActivo, orden:orden, direccion:direccion, pagina: page
    }, function(data) {
        $("#ContenedorListado").html(data);
		FIN_LOAD();
    });
}

function OpenForm(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'creacion') {
        cmd = "loadCreacion";
        titulo = "Registrar Solicitud de Préstamo";
    }

    if (vista == 'visualizar') {
        cmd = "loadVisualizar";
        titulo = "Solicitud de Préstamo";
    }

    if (vista == 'atender') {
        cmd = "loadAtender";
        titulo = "Atender Solicitud de Préstamo";
    }

    if (vista == 'finalizar') {
        cmd = "loadFinalizar";
        titulo = "Finalizar Solicitud de Préstamo";
    }
   
  	INI_LOAD();
    $(document).ready(function() {
        //$("#childModal1").html("");
        $.post("modulos/prestamos/prestamos.php?cmd=" + cmd, {id: id},
        function(data) {
			 $("#Contenedorform").html(data);
			//$("#childModal1").html(data);
			$("#titulo").html(titulo);
			FIN_LOAD();
        });

        //$("#childModal1").modal({
        //});
    });
}

function datosUsuario(){
	var codigo = $("#txtCodigoU").val();
	var data1  = '';


	INI_LOAD();
    $(document).ready(function() {
        $.post("modulos/prestamos/prestamos.php?cmd=buscarUsuario", {codigo: codigo},
        function(data) {
        	 if(data=='failed'){
        	 	$("#txtSolicitante").val('No se encontraron datos con el código ingresado');
			 	$("#txtEstado").val('');	
			 	$("#txtidUsuario").val('');
			 	$("#txtCodigoU").val('');
        	 	 $("#txtCodigoU").focus();
        	 	//bootbox.alert('No se encontraron datos con el código ingresado');
        	 }
        	 else{
        	 	data1     = data.split('|');
        	 	$("#txtSolicitante").val(data1[0]);
			 	$("#txtEstado").val(data1[1]);	
			 	$("#txtidUsuario").val(data1[2]);	
        	 }
        	 
			FIN_LOAD();
        });
    });

}

function mostrar(){
	var filtro = $("#idFiltro").val();

	if(filtro == 'tema'){
		$("#divCodigo").css("display","none");
		$("#divDescripcion").css("display","none");
		$("#divTema").css("display","block");
	}
	else{
	if(filtro == 'codigo'){
		$("#divCodigo").css("display","block");
		$("#divTema").css("display","none");
		$("#divDescripcion").css("display","none");
	}
	else{
		$("#divCodigo").css("display","none");
		$("#divTema").css("display","none");
		$("#divDescripcion").css("display","block");
	 	}
	}
}

function OpenForm2(vista, id)
{
    var cmd = "loadCreacion";
    var titulo = "";

    if (vista == 'visualizar') {
        cmd = "loadVer";
        titulo = "Visualizar Libro";
    }
  
  	INI_LOAD();
    $(document).ready(function() {
        $("#childModal1").html("");
        $.post("modulos/prestamos/prestamos.php?cmd=" + cmd, {id: id},
        function(data) {
			$("#childModal1").html(data);
			FIN_LOAD();
        });

        $("#childModal1").modal({
        });
    });
}

function buscarLibro(){

	var idFacultad 		= $("#idFacultad").val();
    var idFiltro 		= $("#idFiltro").val();
	var descripcion		= $("#txtDescripcion").val();
	var codigo 	 		= $("#txtCodigo").val();
	var idTemas 		= $("#idTemas").val();
	var idCatalogo  	= $("#idCatalogo").val();
	
	INI_LOAD();
    $.post("modulos/prestamos/prestamos.php?cmd=buscarLibro", {
        idFacultad: idFacultad, idFiltro:idFiltro, descripcion: descripcion, codigo:codigo, idTemas:idTemas, idCatalogo:idCatalogo
    }, function(data) {
        $("#divTabla").html(data);
		FIN_LOAD();
    });
}

function solicitar(idLibro,nomLibro,disponible,idFacultad,nomFacultad,ejemplares){

	var idBiblioteca 	= $("#txtBiblioteca").val();
	var id 				= '';
	var data 			= '';
	var flag 			= '0';

	if(disponible <= 0){
		bootbox.alert("No se encuentra ejemplares disponibles del registro seleccionado");
		return;
	}

	if(idBiblioteca != ''){

	 if(idBiblioteca != idFacultad){ 
	 	bootbox.alert("No puede agregar ejemplares de distintas bibliotecas en la misma solicitud de préstamo");
		return;
	 }

	}

	 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        if(id == idLibro){ flag = '1'; }
        
	});

	if(flag == '1'){ bootbox.alert("El ejemplar ya fue agregado a la solicitud"); return; }	 

	$("#txtBiblioteca").val(idFacultad);

	$("#divSolicitado").css("display","block");

	var html = '<tr><td><input type="hidden" id=txtlibro_'+idLibro+' value= '+idLibro+' class="detalles" >'+nomFacultad+'</td>'+
				'<td>'+ nomLibro +'</td>'+
				'<td><input type="number" id=txtcant_'+idLibro+' value="1" min="1" max="'+disponible+'" size="5" /></td>'+
				'<td align=center><a class=cursor-point  title=Eliminar onclick="quitarEjemplar(this)"><span class="label label-danger">ELIMINAR</span></a></td></tr>';
      $('#detalle').append(html);

		var filas = $('#detalle tr').length;
		$("#txtSeleccionado").html("Ejemplares Solicitados: " + filas);
		$("#btnregistrar").prop("disabled", false);

}

function quitarEjemplar(objeto){
	$(objeto).parent('td').parent('tr').remove();
	var filas = $('#detalle tr').length;
	if(filas <= 0){
		$("#txtBiblioteca").val('');
		$("#divSolicitado").css("display","none");
		$("#btnregistrar").prop("disabled", true);
	}
	$("#txtSeleccionado").html("Ejemplares Solicitados: " + filas);

}

function registrar(){
	var id 				= '';
	var data 			= '';
	var detLibro		= '';
	var detCant			= '';
	var idBiblioteca 	= $("#txtBiblioteca").val();
	var idUsuario	 	= $("#txtidUsuario").val();
	var diasPrestamo 	= $("#txtDiasPrestamo").val();
	var filas 			= $('#detalle tr').length;
	var valor 			= '';

	if(filas <= 0){
		bootbox.alert("Debe agregar ejemplares a solicitar");
		return;
	}

	 $(".detalles").each(function(i,x){
		id         = $(this).attr('id');
        data       = id.split('_');
        id         = data[1];
        
        valor 		= $("#txtlibro_"+id).val();

        if(detLibro == ''){
        	detLibro = valor;
        }
        else{
        	detLibro = detLibro + '|' + valor;
        }

        valor 		= $("#txtcant_"+id).val();

        if(detCant == ''){
        	detCant = valor;
        }
        else{
        	detCant = detCant + '|' + valor;
        }

	});

		INI_LOAD();
    $.post("modulos/prestamos/prestamos.php?cmd=registrar", {
        idUsuario:idUsuario, idBiblioteca:idBiblioteca, detLibro:detLibro, detCant:detCant,diasPrestamo:diasPrestamo
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
        	   MainForm();
               bootbox.alert('Error al registrar. Intente nuevamente');
			   FIN_LOAD();
        }
    });				
}

function imprimirPrestamo(idPrestamo){
	/*
	if(isset($_GET['cmd'])){
$documento=$_GET['cmd'];
	*/
	setTimeout(function() { window.open('vistas/pdfprestamo.php?cmd='+idPrestamo,'_blank') },3000);
}

function registrarAtender(){
	
	var idPrestamo 		= $("#txtid").val();
	var comentario 		= $("#txtComentario").val();
	var cadena 			= '';
	var campo 			= '';

	
	$(".CkbLibros").each(function(i,x){
	  campo = $(this).attr('id');
	  if($("#"+campo+"").is(':checked')){}
	  else{
	  	if(cadena==''){cadena = campo;}
	    else{cadena = cadena+','+campo;}
	  }
	});

	INI_LOAD();
    $.post("modulos/prestamos/prestamos.php?cmd=registrarAtender", {
        idPrestamo:idPrestamo, cadena:cadena, comentario:comentario
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
        	   MainForm();
               bootbox.alert('Error al registrar. Intente nuevamente');
			   FIN_LOAD();
        }
    });			

}

function registrarFinalizar(){
	
	var idPrestamo 		= $("#txtid").val();
	var comentario 		= $("#txtComentario").val();
	var cadena 			= '';
	var campo 			= '';

	
	$(".CkbLibros").each(function(i,x){
	  campo = $(this).attr('id');
	  if($("#"+campo+"").is(':checked')){}
	  else{
	  	if(cadena==''){cadena = campo;}
	    else{cadena = cadena+','+campo;}
	  }
	});

	INI_LOAD();
    $.post("modulos/prestamos/prestamos.php?cmd=registrarFinalizar", {
        idPrestamo:idPrestamo, cadena:cadena, comentario:comentario
    }, function(data) {
        if (data=="passed") {
			$("#childModal1").modal('hide');
			MainForm();
			bootbox.alert("Registro guardado correctamente");
			FIN_LOAD();
            
        } else {
        	   MainForm();
               bootbox.alert('Error al registrar. Intente nuevamente');
			   FIN_LOAD();
        }
    });			

}