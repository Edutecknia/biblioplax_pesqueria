     <link rel="icon" href="http://pesqueria.lamolina.edu.pe/pesqueria/img/escudo2.png">
       
       <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">&nbsp;</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" id="main-nav-wrap">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <?php if($flagLogedo == 1){ 
                        $nomUsuario    = utf8_encode($usuario['NOM_USUARIO']);
                    ?>

                    <li style="font-color:#fff; color:#fff;top:18px;font-family:Montserrat, Helvetica Neue, Helvetica, Arial, sans-serif" id="divUser">&nbsp;BIENVENIDO(A) <?php echo $nomUsuario;?>&nbsp;&nbsp;</li>
                    <li><a href="gestion/principal.php" class="page-scroll">Modo Avanzado</a></li>
                    <?php } ?>

                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="#catalogo" class="page-scroll">Catálogo</a></li>
                    <li><a href="#portfolio" class="page-scroll">Bases de Datos</a></li>
                    <?php if($flagLogedo == 0){ ?>
                    <li><a role="button" data-toggle="modal" data-target="#login-modal">Ingresar</a></li>
                    <?php } ?>
                   
                    <?php if($flagLogedo == 1){ ?>
                    <li class="menuItem ">
                    <a href="javascript:cerrarSesion();">Salir</a>
                    </li>
                    <?php } ?>

                   
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>