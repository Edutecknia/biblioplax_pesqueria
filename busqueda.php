<?php
require_once "config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

global $conexion;

$valor          = $_POST['txtBusqueda'];
$idFacultad     = 0;

    $modelData      = new Data_sgacarrera();
    $arrayFacultad  = $modelData->fu_listarTodo($conexion,$idFacultad);

    $modelData      = new Data_sgacategoria();
    $arrayTemas     = $modelData->fu_listarTodoCategoria($conexion);

$idFacultad          = 48; 
$modelData           = new Data_sganoticia();
$arrayNoticias       = $modelData->fu_listarTodoNoticia($conexion, $idFacultad);
$arrayEventos        = $modelData->fu_listarTodoEventos($conexion, $idFacultad);

$modelData          = new Data_sgalibro();
$arrayLibroTipo     = $modelData->fu_listarLibroTipo($conexion);


session_start();

$flagLogedo = 1;

if($_SESSION['BTK_USUARIO']==NULL){
session_destroy();
$flagLogedo = 0;
}
else{
    $objUsuario   = $_SESSION['BTK_USUARIO'];
    $idUsuario    = $objUsuario->__get('_sess_usu_id');

    $objsga_usuario = new Data_sgausuario();
    $usuario        = $objsga_usuario->fu_encontrar($conexion,$idUsuario);    
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="img/logo.png">
    <title>Facultad de Pesquería</title>

    <?php include 'css.php' ?>
    <link rel="stylesheet" href="gestion/plugins/select2/select2.min.css">
</head>

<body id="page-top" class="index">

<div class="barraPrincipal" id="colores">
   </div>

   <!--<div class="barraLogoUniversidad" id="logouniversidad">
   </div>-->

   <?php include 'menu.php' ?>

    
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">

        <div class='item active'>
        <img src="img/slider/bienvenida.jpg" alt="UNJFSC">        
        </div>

    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>

        <input type="hidden" name="txtFlagLogin" id="txtFlagLogin" value="<?php echo $flagLogedo; ?>">

        <section id="busqueda">
            <div class="container">
            <div class="row">

                <div class="form-group col-sm-3">
                <h5>Biblioteca:</h5>
                <select class="form-control input-lg " name="idFacultad" id="idFacultad">
                <option value="">TODOS</option>
                <?php foreach ($arrayFacultad as $obj): ?>
                <option value="<?php echo $obj['cod_escuela']; ?>" <?php if($idFacultad == $obj['cod_escuela']){ echo 'selected';}?> >
                <?php echo utf8_encode($obj['nom_escuela']); ?></option>
                <?php endforeach; ?>
                </select>
                </div>


                <div class="form-group col-sm-3">
                <h5>Buscar Por:</h5>
                <select class="form-control input-lg" name="idFiltro" id="idFiltro" onchange="javascript:mostrar();">
                    <option value="titulo">TÍTULO</option>
                    <option value="autor">AUTOR</option>
                    <option value="tema">TEMA</option>
                    <option value="codigo">CÓDIGO</option>
                    <option value="isbn">ISBN</option>
                    <option value="catalogo">CATÁLOGO</option>
                </select>
                </div>

                <div class="form-group col-sm-4" id="divDescripcion">
                <h5>Descripción:</h5>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtDescripcion" name="txtDescripcion" 
                                    class="form-control input-lg" 
                                    placeholder="Descripción de la búsqueda" 
                                    type="text" 
                                    maxlength="100" 
                                    value="<?php echo $valor; ?>" 
                                    >
                                </div>
                </div>

                <div class="form-group col-sm-4" style="display:none" id="divCodigo">
                <h5>Código:</h5>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input id="txtCodigo" name="txtCodigo" 
                                    class="form-control input-lg" 
                                    placeholder="Código de Libro" 
                                    type="number" 
                                    min="1"
                                    maxlength="6" 
                                    >
                                </div>
                </div>


                <div class="form-group col-sm-4" style="display:none" id="divTema">
                <h5>Temas Relacionados:</h5>
                               <select class="form-control input-lg select2" 
                                        name="idTemas" 
                                        id="idTemas"  
                                        data-placeholder="Seleccione" 
                                        style="width: 100%;"
                                >
                                <?php foreach ($arrayTemas as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_CATEGORIA']; ?>"><?php echo utf8_encode($obj['NOM_CATEGORIA']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>

                <div class="form-group col-sm-4" style="display:none" id="divCatalogo">
                <h5>CATÁLOGO:</h5>
                               <select class="form-control input-lg select2" 
                                        name="idCatalogo" 
                                        id="idCatalogo"  
                                        data-placeholder="Seleccione" 
                                        style="width: 100%;"
                                >
                                <?php foreach ($arrayLibroTipo as $obj): 
                                ?>
                                <option value="<?php echo $obj['ID_LIBRO_TIPO']; ?>"><?php echo utf8_encode($obj['DES_LIBRO_TIPO']); ?></option>
                                <?php endforeach; ?>
                                </select>
                </div>
                            <br><br>
                            <span class="input-group-btn">
                            <button class="btn btn-info btn-lg" type="submit" id="btnBusqueda" onclick="javascript:buscarLibro();">
                            <i class="glyphicon glyphicon-search"></i>
                            </button>
                            </span>

                    <input type="hidden" name="txtBiblioteca" id="txtBiblioteca">

                   <div class="col-md-12 table-responsive" id="divResultado">
                   </div>


                <div class="col-md-12"><br><br></div>

                <div class="col-md-12 table-responsive" id="divSolicitado" style="display:none;overflow-x:visible !important;">

                <div class="form-group col-sm-12">
                <h4 class="text-yellow" id="txtSeleccionado">Ejemplares Solicitados:</h4>
                </div>

                    <table id="ejemplares" class="table table-bordered table-hover table-striped dataTable" style="font-size:13px">
                    <thead>
                      <tr style="color:#333">
                        <th>BIBLIOTECA</th>
                        <th>TITULO</th>
                        <th>CANT. SOLICITADA</th>
                        <th class="center">OPCIONES</th>
                      </tr>
                    </thead>
                    <tbody id="detalle">
                    </tbody>  
                    </table>

                <p>&nbsp;</p>
                <center>
                <a class="cursor-point" 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Registrar Préstamo" 
                        style="cursor:pointer; font-size:25px;"
                        onclick="javascript:registrarPrestamo();">
                        <span class='label label-primary'>REGISTRAR</span>
                    </a>
                </center>

                </div>    




            </div>
            </div>
        </section>

                

    <!-- About Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#464646">BASES DE DATOS</h2>
                    <!--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
                </div>
            </div>
             <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="https://www.ebscohost.com/" target="_blank">
                        <img src="img/bases/1.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Ebsco Research Database</h4></center>
                </div>


                <div class="col-md-3 col-sm-6">
                    <a href="http://alicia.concytec.gob.pe/vufind/" target="_blank">
                        <img src="img/bases/2.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Alicia</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://www.scopus.com/sources?zone=&origin=NO%20ORIGIN%20DEFINED" target="_blank">
                        <img src="img/bases/3.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Scopus</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://login.webofknowledge.com/error/Error?Error=IPError&PathInfo=%2F&RouterURL=https%3A%2F%2Fwww.webofknowledge.com%2F&Domain=.webofknowledge.com&Src=IP&Alias=WOK5"
                    target="_blank" >
                        <img src="img/bases/4.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Web of Science</h4></center>
                </div>

                <div class="col-md-12"><br></div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.sciencemag.org/" target="_blank">
                        <img src="img/bases/5.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Science Magazine</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.proquest.com/LATAM-ES/" target="_blank">
                        <img src="img/bases/6.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>ProQuest</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="https://portal.concytec.gob.pe/" target="_blank">
                        <img src="img/bases/concytec.png" class="img-responsive" alt="">
                    </a>
                        <center><h4>Concytec</h4></center>
                </div>

                <div class="col-md-3 col-sm-6">
                    <a href="http://www.sciencedirect.com/" target="_blank">
                        <img src="img/bases/science.jpg" class="img-responsive" alt="">
                    </a>
                        <center><h4>Science Direct</h4></center>
                </div>

               
            </div>
        </div>
    </section>

   

    <img src="img/Linea_dorado.png" class="img-responsive" />

   <?php include 'footer.php' ?>




    <!--MODAL LOGIN-->
    <?php if($flagLogedo == 0){ ?>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <img class="img-circle" id="img_logo" src="img/logo.png">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                </div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <form id="login-form">
                        <div class="modal-body">
                            <div id="div-login-msg">
                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-login-msg" style="color:#000;">Ingrese su usuario y contraseña.</span>
                            </div>
                            <input id="login_username" class="form-control" type="text" placeholder="Usuario" required>
                            <input id="login_password" class="form-control" type="password" placeholder="Contraseña" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="button" onclick="javascript:loginUsuario()" class="btn btn-primary btn-lg btn-block">Ingresar</button>
                            </div>
                            <div>
                            </div>
                        </div>
                    </form>
                    <!-- End # Login Form -->
                    
                    <!-- Begin | Lost Password Form -->
                    <form id="lost-form" style="display:none;">
                        <div class="modal-body">
                            <div id="div-lost-msg">
                                <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-lost-msg">Ingrese su e-mail</span>
                            </div>
                            <input id="lost_email" class="form-control" type="text" placeholder="E-Mail" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
                            </div>
                            <div>
                                <button id="lost_login_btn" type="button" class="btn btn-link">Ingresar</button>
                                <button id="lost_register_btn" type="button" class="btn btn-link">Registrarse</button>
                            </div>
                        </div>
                    </form>
                    <!-- End | Lost Password Form -->
                    
                    <!-- Begin | Register Form -->
                    <form id="register-form" style="display:none;">
                        <div class="modal-body">
                            <div id="div-register-msg">
                                <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-register-msg">Registrarse como evaluador</span>
                            </div>
                            <input id="register_username" class="form-control" type="text" placeholder="Usuario" required>
                            <input id="register_email" class="form-control" type="text" placeholder="E-Mail" required>
                            <input id="register_password" class="form-control" type="password" placeholder="Contraseña" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Registrarse</button>
                            </div>
                            <div>
                                <button id="register_login_btn" type="button" class="btn btn-link">Ingresar</button>
                                <button id="register_lost_btn" type="button" class="btn btn-link">Olvidaste la contraseña?</button>
                            </div>
                        </div>
                    </form>
                    <!-- End | Register Form -->
                    
                </div>
                <!-- End # DIV Form -->
                
            </div>
        </div>
    </div>
    <!-- END # MODAL LOGIN -->
    <?php } ?>
    <!--MODAL LOGIN FIN-->


    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->
            <!--DETALLE DE NOTICIAS-->

<div class="portfolio-modal modal fade" id="ModalNoticias" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--<div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>-->
                <div class="container">

                  <div id="ContenedorModalNoticias"></div>
                    
                </div>
            </div>
        </div>
    </div>
 
    <!-- jQuery -->
    <?php include 'js.php'; ?>
    <script src="gestion/plugins/select2/select2.full.min.js"></script>


 <script>
$(document).ready(function(){

  funcionScroll();

  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });

  $('#CarouselNoticias').bxSlider({
    slideWidth: 500,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 10,
    auto: true  
    });

  $('#CarouselEventos').bxSlider({
    slideWidth: 500,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    slideMargin: 10,
    auto: true /*,
    mode: 'vertical' */
    });

  buscarLibro();
  /*$("#linkemergente").trigger("click");*/

});
</script>

</body>

</html>
