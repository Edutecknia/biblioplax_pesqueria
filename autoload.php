<?php
require_once 'config/web.config.php';

function __autoload($className)
{
    $className = 'class_'.$className;
    $arrayClassName= explode("_", $className);
    $elemUlt = $arrayClassName[(count($arrayClassName)-1)];
    unset($arrayClassName[(count($arrayClassName)-1)]);

    $base = strtolower(implode("/", $arrayClassName));

    $ruta = APP_DIR  . $base . "/class.".$elemUlt . '.php';
    
    if(file_exists($ruta))
        require_once  APP_DIR  . $base ."/class.".$elemUlt . '.php';
}

$_SESSION['pDB'] = array(
    'dbType'=>$dbType,
	'dbHost'=>$dbHost,
	'dbUser'=>$dbUser,
	'dbPass'=>$dbPass,
	'dbName'=>$dbName
	);

/**
* @param PDOStatement $oStm
*/
function closeCursor($oStm) {
    echo "close";
	do $oStm->fetchAll();
	while ($oStm->nextRowSet());
} 
	
$objConexion = new Data_DB();
$conexion = $objConexion->conn();
?>