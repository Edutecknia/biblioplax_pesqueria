<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgalibro.php
 * */
class Data_sgalibro{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listar($conexion,$filtro,$indActivo,$orden,$direccion,$pagina,$idFacultad) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "EXECUTE USP_BTK_LIBRO_LISTAR $filtro,$indActivo,$orden,$direccion,$paginado,$pagina,$idFacultad";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_registrar($conexion, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, 
							$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro,$temasRelacion, $idUsuario, $ip) {
		try {
			
			//$desTitulo = strtoupper($desTitulo);

			$desTitulo = utf8_decode($desTitulo);

			if(trim($idLibroTipo)==""){$idLibroTipo='NULL';}else{ $idLibroTipo= "'" . trim($idLibroTipo) . "'"  ;}
			if(trim($desTitulo)==""){$desTitulo='NULL';}else{ $desTitulo= "'" . trim($desTitulo) . "'"  ;}
			if(trim($idAutor)==""){$idAutor='NULL';}else{ $idAutor= "'" . trim($idAutor) . "'"  ;}
			if(trim($idEditorial)==""){$idEditorial='NULL';}else{ $idEditorial= "'" . trim($idEditorial) . "'"  ;}
			if(trim($anioPublica)==""){$anioPublica='NULL';}else{ $anioPublica= "'" . trim($anioPublica) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($numPagina)==""){$numPagina='NULL';}else{ $numPagina= "'" . trim($numPagina) . "'"  ;}
			if(trim($numVolumen)==""){$numVolumen='NULL';}else{ $numVolumen= "'" . trim($numVolumen) . "'"  ;}
			if(trim($idAdquisicion)==""){$idAdquisicion='NULL';}else{ $idAdquisicion= "'" . trim($idAdquisicion) . "'"  ;}
			if(trim($idProveedor)==""){$idProveedor='NULL';}else{ $idProveedor= "'" . trim($idProveedor) . "'"  ;}
			if(trim($fecAdquisicion)==""){$fecAdquisicion='NULL';}else{ $fecAdquisicion= "'" . trim($fecAdquisicion) . "'"  ;}
			if(trim($preLibro)==""){$preLibro='NULL';}else{ $preLibro= "'" . trim($preLibro) . "'"  ;}
			if(trim($temasRelacion)==""){$temasRelacion='NULL';}else{ $temasRelacion= "'" . trim($temasRelacion) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_LIBRO_REGISTRAR $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, ".
					"$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, $temasRelacion, $idUsuario, $ip";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_registrarEjemplar($conexion, $idLibro, $isbn, $codLibro, $codBarra, $idUsuario, $ip) {
		try {
			
			if($isbn == 'undefined'){ $isbn = '';}

			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($isbn)==""){$isbn='NULL';}else{ $isbn= "'" . trim($isbn) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			if(trim($codBarra)==""){$codBarra='NULL';}else{ $codBarra= "'" . trim($codBarra) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_LIBRO_EJEMPLAR_REGISTRAR $idLibro, $isbn, $codLibro, $codBarra, $idUsuario, $ip ";
		
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrarAutor($conexion, $idLibro, $idAutor, $idUsuario, $ip) {
		try {
					
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_LIBRO_AUTOR_REGISTRAR $idLibro, $idAutor, $idUsuario, $ip ";

			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Encontrar($conexion,$id) {
		try {
						
			$sql = "SELECT ID_LIBRO,ID_CARRERA,ID_LIBRO_TIPO,DES_TITULO,ID_EDITORIAL,NUM_ANIO_PUBLICA,ID_PAIS,NUM_PAGINA,NUM_VOLUMEN,".
					" ID_LIBRO_ADQUISICION,ID_PROVEEDOR,FEC_ADQUISICION,PRE_LIBRO,DES_CATEGORIA_RELACION FROM BTK_LIBRO WHERE ID_LIBRO = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_EncontrarEjemplares($conexion,$id) {
		try {
						
			$sql = "SELECT COD_ISBM,COD_LIBRO,COD_BARRA,DES_ESTADO FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO = ".$id." AND IND_ACTIVO = 1";
			$stm = $conexion->query($sql);
			$result = $stm->fetchAll();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarAutores($conexion,$id) {
		try {
						
			$sql = "SELECT ID_AUTOR FROM BTK_LIBRO_AUTOR WHERE ID_LIBRO = ".$id." AND IND_ACTIVO = 1";
			$stm = $conexion->query($sql);
			$result = $stm->fetchAll();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	

public function fu_editar($conexion, $idLibro, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, 
							$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro,$temasRelacion, $idUsuario, $ip) {
		try {
			
			//$desTitulo = strtoupper($desTitulo);
			$desTitulo = utf8_decode($desTitulo);

			if(trim($idLibroTipo)==""){$idLibroTipo='NULL';}else{ $idLibroTipo= "'" . trim($idLibroTipo) . "'"  ;}
			if(trim($desTitulo)==""){$desTitulo='NULL';}else{ $desTitulo= "'" . trim($desTitulo) . "'"  ;}
			if(trim($idAutor)==""){$idAutor='NULL';}else{ $idAutor= "'" . trim($idAutor) . "'"  ;}
			if(trim($idEditorial)==""){$idEditorial='NULL';}else{ $idEditorial= "'" . trim($idEditorial) . "'"  ;}
			if(trim($anioPublica)==""){$anioPublica='NULL';}else{ $anioPublica= "'" . trim($anioPublica) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($numPagina)==""){$numPagina='NULL';}else{ $numPagina= "'" . trim($numPagina) . "'"  ;}
			if(trim($numVolumen)==""){$numVolumen='NULL';}else{ $numVolumen= "'" . trim($numVolumen) . "'"  ;}
			if(trim($idAdquisicion)==""){$idAdquisicion='NULL';}else{ $idAdquisicion= "'" . trim($idAdquisicion) . "'"  ;}
			if(trim($idProveedor)==""){$idProveedor='NULL';}else{ $idProveedor= "'" . trim($idProveedor) . "'"  ;}
			if(trim($fecAdquisicion)==""){$fecAdquisicion='NULL';}else{ $fecAdquisicion= "'" . trim($fecAdquisicion) . "'"  ;}
			if(trim($preLibro)==""){$preLibro='NULL';}else{ $preLibro= "'" . trim($preLibro) . "'"  ;}
			if(trim($temasRelacion)==""){$temasRelacion='NULL';}else{ $temasRelacion= "'" . trim($temasRelacion) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_LIBRO_EDITAR $idLibro, $idFacultad, $idLibroTipo, $desTitulo, $idEditorial, $anioPublica, $idPais, ".
					"$numPagina, $numVolumen, $idAdquisicion, $idProveedor, $fecAdquisicion, $preLibro, $temasRelacion, $idUsuario, $ip";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrarAdjunto($conexion, $idLibro, $idArchivoTipo, $desArchivo, $idUsuario, $ip) {
		try {
			
			$desArchivo = utf8_decode($desArchivo);

			if(trim($desArchivo)==""){$desArchivo='NULL';}else{ $desArchivo= "'" . trim($desArchivo) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_LIBRO_ARCHIVO_REGISTRAR $idLibro, $idArchivoTipo, $desArchivo, $idUsuario, $ip ";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarAdjunto($conexion,$idLibro) {
		try {
			
			
			$sql 	= "SELECT A.ID_LIBRO_ARCHIVO,A.DES_ARCHIVO,T.NOM_ARCHIVO_TIPO,A.NOM_ARCHIVO,A.ID_ARCHIVO_TIPO ".
					 " FROM BTK_LIBRO_ARCHIVO A INNER JOIN BTK_ARCHIVO_TIPO T ON A.ID_ARCHIVO_TIPO = T.ID_ARCHIVO_TIPO WHERE A.ID_LIBRO = ".$idLibro." AND A.IND_ACTIVO = 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}

			$sql = "EXECUTE USP_BTK_LIBRO_INACTIVAR $id, $idUsuario, $ip";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();

			return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'EXECUTE USP_BTK_LIBRO_ACTIVAR '
              . ':ID,  :ID_USUARIO, :IP '
       		 );
        	
        	$stmt->bindParam(':ID',      			$id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

		public function fu_eliminarAdjunto($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'EXECUTE USP_BTK_LIBRO_ARCHIVO_ELIMINAR '
              . ':ID,  :ID_USUARIO, :IP '
       		 );
        	
        	$stmt->bindParam(':ID',      			$id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_PROVEEDOR,NUM_RUC,NOM_PROVEEDOR,ID_PAIS,DES_DIRECCION,NUM_TELEFONO,DES_MAIL,DES_WEB,DES_CONTACTO FROM BTK_PROVEEDOR WHERE IND_ACTIVO = 1 ORDER BY 3";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarLibroTipo($conexion) {
		try {
			
			$sql 	= "SELECT ID_LIBRO_TIPO,DES_LIBRO_TIPO FROM BTK_LIBRO_TIPO WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarLibroAdquisicion($conexion) {
		try {
			
			$sql 	= "SELECT ID_LIBRO_ADQUISICION,DES_LIBRO_ADQUISICION FROM BTK_LIBRO_ADQUISICION WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarArchivoTipo($conexion) {
		try {
			
			$sql 	= "SELECT ID_ARCHIVO_TIPO,NOM_ARCHIVO_TIPO FROM BTK_ARCHIVO_TIPO WHERE IND_ACTIVO = 1 ORDER BY 2";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarUltimasAdquisiciones($conexion,$idFacultad) {
		try {
			
			$sql 	= "SELECT TOP 6 L.DES_TITULO,".
					  " (SELECT NOM_ARCHIVO FROM BTK_LIBRO_ARCHIVO A WHERE A.ID_LIBRO = L.ID_LIBRO AND A.ID_ARCHIVO_TIPO = 1 AND A.IND_ACTIVO = 1 ) AS CARATULA ".
					  " FROM BTK_LIBRO L WHERE L.ID_CARRERA = ".$idFacultad." AND L.IND_ACTIVO = 1  ORDER BY L.FEC_REGISTRO DESC ";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_cantidadCatalogo($conexion,$idFacultad) {
		try {
			
			$sql 	= "SELECT ".
						" (SELECT COUNT(*) FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO IN( ".
								" SELECT ID_LIBRO FROM BTK_LIBRO WHERE ID_CARRERA = ".$idFacultad." AND ID_LIBRO_TIPO = 6 AND IND_ACTIVO = 1 ".
						" )) AS EJEMPLARES, ".
						" (SELECT COUNT(*) FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO IN( ".
								" SELECT ID_LIBRO FROM BTK_LIBRO WHERE ID_CARRERA = ".$idFacultad." AND ID_LIBRO_TIPO = 14 AND IND_ACTIVO = 1 ".
						" )) AS REVISTAS, ".
						" (SELECT COUNT(*) FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO IN( ".
								" SELECT ID_LIBRO FROM BTK_LIBRO WHERE ID_CARRERA = ".$idFacultad." AND ID_LIBRO_TIPO = 11 AND IND_ACTIVO = 1 ".
						" )) AS VIDEO, ".
						" (SELECT COUNT(*) FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO IN( ".
								" SELECT ID_LIBRO FROM BTK_LIBRO WHERE ID_CARRERA = ".$idFacultad." AND ID_LIBRO_TIPO = 16 AND IND_ACTIVO = 1 ".
						" )) AS AUDIO, ".
						" (SELECT COUNT(*) FROM BTK_LIBRO_EJEMPLAR WHERE ID_LIBRO IN( ".
								" SELECT ID_LIBRO FROM BTK_LIBRO WHERE ID_CARRERA = ".$idFacultad." AND ID_LIBRO_TIPO = 15 AND IND_ACTIVO = 1 ".
						" )) AS CARTA";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetch();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}



}
?>