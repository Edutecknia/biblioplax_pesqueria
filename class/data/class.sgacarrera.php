<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgacarrera.php
 * */
class Data_sgacarrera{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_listarTodo($conexion,$idFacultad) {
		try {
			
			$cadena  = "";

			if($idFacultad > 0 ){
				$cadena = " AND ID_CARRERA = ".$idFacultad;	
				
				$sql 	= "SELECT ID_CARRERA,UPPER(NOM_CARRERA) as NOM_CARRERA FROM CARRERA WHERE 1=1 ".$cadena." AND IND_ACTIVO = 1 ORDER BY 2";
			}
			else{

				$sql 	= "SELECT ID_CARRERA,UPPER(NOM_CARRERA) as NOM_CARRERA FROM CARRERA WHERE 1=1 ".$cadena." AND IND_ACTIVO = 1 AND ID_CARRERA_PADRE IS NULL AND ID_CARRERA NOT IN(19,46,47,65,66)".
						" UNION ".
						" SELECT ID_CARRERA,UPPER(NOM_CARRERA) as NOM_CARRERA FROM CARRERA WHERE 1=1 AND IND_ACTIVO = 1 AND ID_CARRERA IN(44) ORDER BY 2";
			}
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>