<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgalibro.php
 * */
class Data_sgaprestamo{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listarxUsuario($conexion,$fini, $ffin,$indActivo,$orden,$direccion,$pagina,$idUsuario) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$ffin  = $ffin.' 23:59:00';

			$filtro= "'" . trim($filtro) . "'"  ;
			$fini= "'" . trim($fini) . "'"  ;
			$ffin= "'" . trim($ffin) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "EXECUTE USP_BTK_PRESTAMO_USUARIO_LISTAR $filtro,$indActivo,$orden,$direccion,$paginado,$pagina,$idUsuario, $fini, $ffin";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarPrestamos($conexion,$idFacultad, $indActivo,$orden,$direccion,$pagina,$idPrestamo,$codUniversitario,$fini, $ffin) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$ffin  = $ffin.' 23:59:00';

			$fini= "'" . trim($fini) . "'"  ;
			$ffin= "'" . trim($ffin) . "'"  ;
			
			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($idPrestamo)==""){$idPrestamo='NULL';}else{ $idPrestamo= "'" . trim($idPrestamo) . "'"  ;}
			if(trim($codUniversitario)==""){$codUniversitario='NULL';}else{ $codUniversitario= "'" . trim($codUniversitario) . "'"  ;}

			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "EXECUTE USP_BTK_PRESTAMO_LISTAR $idFacultad,$indActivo,$orden,$direccion,$paginado,$pagina,$idPrestamo, $codUniversitario, $fini, $ffin";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_buscarLibro($conexion,$idFacultad,$tipoFiltro,$filtro,$codLibro) {
		try {
			
			$filtro = utf8_decode($filtro);

			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($tipoFiltro)==""){$tipoFiltro='NULL';}else{ $tipoFiltro= "'" . trim($tipoFiltro) . "'"  ;}
			if(trim($filtro)==""){$filtro='NULL';}else{ $filtro= "'" . trim($filtro) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
						
			$sql 	= "EXECUTE USP_BTK_PRESTAMO_LIBRO_LISTAR $idFacultad,$tipoFiltro,$filtro,$codLibro";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


public function fu_registrar($conexion, $idSolicitante, $idFacultad, $diasPrestamo, $idUsuario, $ip) {
		try {
			
			if(trim($idSolicitante)==""){$idSolicitante='NULL';}else{ $idSolicitante= "'" . trim($idSolicitante) . "'"  ;}
			if(trim($idFacultad)==""){$idFacultad='NULL';}else{ $idFacultad= "'" . trim($idFacultad) . "'"  ;}
			if(trim($diasPrestamo)==""){$diasPrestamo='NULL';}else{ $diasPrestamo= "'" . trim($diasPrestamo) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_PRESTAMO_REGISTRAR $idSolicitante, $idFacultad, $diasPrestamo, $idUsuario, $ip ";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrarDetalle($conexion, $idPrestamo, $idLibro, $cantidad, $idUsuario, $ip) {
		try {
			
			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($cantidad)==""){$cantidad='NULL';}else{ $cantidad= "'" . trim($cantidad) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_PRESTAMO_DETALLE_REGISTRAR $idPrestamo, $idLibro, $cantidad, $idUsuario, $ip ";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_Encontrar($conexion,$id) {
		try {
			
			$sql = "SELECT P.ID_PRESTAMO,C.NOM_CARRERA,P.FEC_INICIO,P.FEC_FIN,P.FEC_ENTREGA,DATEDIFF(DD,P.FEC_FIN,P.FEC_ENTREGA) AS ATRASO,E.DES_ESTADO, P.IND_ACTIVO,".
					" U.NOM_USUARIO, U.APE_USUARIO, U.COD_UNIVERSITARIO ".
					" FROM BTK_PRESTAMO P ".
					" INNER JOIN BTK_PRESTAMO_ESTADO E ON P.ID_PRESTAMO_ESTADO = E.ID_PRESTAMO_ESTADO ".
					" INNER JOIN CARRERA C ON P.ID_CARRERA = C.ID_CARRERA ".
					" INNER JOIN BTK_USUARIO U ON P.ID_USUARIO = U.ID_USUARIO ".
					" WHERE P.ID_PRESTAMO = ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_EncontrarDetalle($conexion,$id) {
		try {
			
			$sql = "SELECT L.ID_LIBRO,L.DES_TITULO,E.COD_ISBM,E.COD_LIBRO,E.COD_BARRA".
					" FROM BTK_PRESTAMO_DETALLE D ".
					" INNER JOIN BTK_LIBRO_EJEMPLAR E ON D.ID_LIBRO = E.ID_LIBRO AND D.COD_LIBRO = E.COD_LIBRO ".
					" INNER JOIN BTK_LIBRO L ON L.ID_LIBRO = E.ID_LIBRO ".
					" WHERE D.ID_PRESTAMO = ".$id." AND D.IND_ACTIVO = 1 ORDER BY L.DES_TITULO ";
			$stm = $conexion->query($sql);
			$result = $stm->fetchAll();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarPrestamoEstado($conexion) {
		try {
			
			$sql 	= "SELECT ID_PRESTAMO_ESTADO,DES_ESTADO FROM BTK_PRESTAMO_ESTADO WHERE IND_ACTIVO = 1 ORDER BY NUM_ORDENAMIENTO";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_cambiarEstadoLibro($conexion, $idPrestamo, $idLibro, $codLibro, $idUsuario, $ip) {
		try {
			
			if(trim($idLibro)==""){$idLibro='NULL';}else{ $idLibro= "'" . trim($idLibro) . "'"  ;}
			if(trim($codLibro)==""){$codLibro='NULL';}else{ $codLibro= "'" . trim($codLibro) . "'"  ;}
			
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}


			$sql = "EXECUTE USP_BTK_PRESTAMO_EJEMPLAR_ESTADO_ACTUALIZAR $idPrestamo, $idLibro, $codLibro, $idUsuario, $ip ";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_prestamoAtender($conexion, $idPrestamo, $comentario, $idUsuario, $ip) {
		try {
			
			$comentario = utf8_decode($comentario);

			if(trim($comentario)==""){$comentario='NULL';}else{ $comentario= "'" . trim($comentario) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}

			$sql = "EXECUTE USP_BTK_PRESTAMO_ATENDER_REGISTRAR $idPrestamo, $comentario, $idUsuario, $ip ";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_prestamoFinalizar($conexion, $idPrestamo, $comentario, $idUsuario, $ip) {
		try {
			
			$comentario = utf8_decode($comentario);

			if(trim($comentario)==""){$comentario='NULL';}else{ $comentario= "'" . trim($comentario) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}

			$sql = "EXECUTE USP_BTK_PRESTAMO_FINALIZAR_REGISTRAR $idPrestamo, $comentario, $idUsuario, $ip ";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>