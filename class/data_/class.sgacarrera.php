<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgacarrera.php
 * */
class Data_sgacarrera{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}

public function fu_listarTodo($conexion,$idFacultad) {
		try {
			
			$cadena  = "";

			if($idFacultad > 0 ){
				$cadena = " AND ID_CARRERA = ".$idFacultad;	
				
				$sql 	= "SELECT cod_escuela,UPPER(nom_escuela) as NOM_CARRERA FROM escuela WHERE 1=1 ".$cadena." AND IND_ACTIVO = 1 ORDER BY 2";
			}
			else{

				$sql 	= "SELECT cod_escuela,UPPER(nom_escuela) as NOM_CARRERA FROM escuela WHERE 1=1 ".$cadena." AND IND_ACTIVO = 1 AND ID_CARRERA_PADRE IS NULL AND cod_escuela NOT IN(19,46,47,65,66)".
						" UNION ".
						" SELECT cod_escuela,UPPER(nom_escuela) as NOM_CARRERA FROM escuela WHERE 1=1 AND IND_ACTIVO = 1 AND cod_escuela IN(44) ORDER BY 2";
			}
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>