<?php 
/**
* [GENERADO] :: Generado
* @author   	  -----
* @copyright     2017 © 
* @package       - - -
* @name          class.sgaautor.php
 * */
class Data_sgaautor{
//Atributos
			protected $_rows="";
	
public function __construct() { 
	}
	public function __get($propiedad) {
		$returnValue = (string) "";
		$returnValue = $this->$propiedad;
		return (string) $returnValue;
	}
	public function __set($propiedad, $valor) {
		$this->$propiedad = $valor;
	}


public function fu_listarAutor($conexion,$filtro,$indActivo,$orden,$direccion,$pagina) {
		try {
			
			$paginado = TAM_PAG_LISTADO;

			$filtro = utf8_decode($filtro);
			$filtro= "'" . trim($filtro) . "'"  ;
			if(trim($orden)==""){$orden='NULL';}else{ $orden= "'" . trim($orden) . "'"  ;}
			if(trim($direccion)==""){$direccion='NULL';}else{ $direccion= "'" . trim($direccion) . "'"  ;}
						
			$sql 	= "CALL USP_BTK_AUTOR_LISTAR ($filtro,$indActivo,$orden,$direccion,$paginado,$pagina)";
		
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_registrar($conexion, $nomAutor, $apeAutor, $seudoAutor, $idPais, $idAutorTipo, $idUsuario, $ip) {
		try {
			
			/*$nomAutor = strtoupper($nomAutor);
			$apeAutor = strtoupper($apeAutor);
			$seudoAutor = strtoupper($seudoAutor);*/

			$apeAutor= "'" . trim($apeAutor) . "'"  ;
			$seudoAutor= "'" . trim($seudoAutor) . "'"  ;

			$nomAutor = utf8_decode($nomAutor);
			$apeAutor = utf8_decode($apeAutor);
			$seudoAutor = utf8_decode($seudoAutor);

			if(trim($nomAutor)==""){$nomAutor='NULL';}else{ $nomAutor= "'" . trim($nomAutor) . "'"  ;}
			//if(trim($apeAutor)==""){$apeAutor='NULL';}else{ $apeAutor= "'" . trim($apeAutor) . "'"  ;}
			//if(trim($seudoAutor)==""){$seudoAutor='NULL';}else{ $seudoAutor= "'" . trim($seudoAutor) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_AUTOR_REGISTRAR ($idAutorTipo, $nomAutor,$apeAutor, $seudoAutor, $idPais,$idUsuario,$ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

			/*$stmt = $conexion->prepare(
                'EXECUTE USP_BTK_AUTOR_REGISTRAR '
              . ':NOM_AUTOR, :APE_AUTOR, :DES_SEUDONIMO, :ID_PAIS, :ID_USUARIO, :IP '
       		 );
        	
        	
        	$stmt->bindParam(':NOM_AUTOR',      	$nomAutor,     	PDO::PARAM_STR);
        	$stmt->bindParam(':APE_AUTOR',      	$apeAutor,     	PDO::PARAM_STR);
        	$stmt->bindParam(':DES_SEUDONIMO',      $seudoAutor,    PDO::PARAM_STR);
        	$stmt->bindParam(':ID_PAIS',      		$idPais,     	PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  	PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     		PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
			
		return $var;*/
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_Encontrar($conexion,$id) {
		try {
						
			$sql = "SELECT ID_AUTOR,NOM_AUTOR,APE_AUTOR,DES_SEUDONIMO,ID_PAIS,ID_AUTOR_TIPO FROM BTK_AUTOR WHERE ID_AUTOR =  ".$id;
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
		
		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}	


public function fu_editar($conexion,$id, $nomAutor, $apeAutor, $seudoAutor, $idPais, $idAutorTipo, $idUsuario, $ip) {
		try {
			
			/*$nomAutor = strtoupper($nomAutor);
			$apeAutor = strtoupper($apeAutor);
			$seudoAutor = strtoupper($seudoAutor);*/

			$apeAutor= "'" . trim($apeAutor) . "'"  ;
			$seudoAutor= "'" . trim($seudoAutor) . "'"  ;

			$nomAutor = utf8_decode($nomAutor);
			$apeAutor = utf8_decode($apeAutor);
			$seudoAutor = utf8_decode($seudoAutor);

			if(trim($nomAutor)==""){$nomAutor='NULL';}else{ $nomAutor= "'" . trim($nomAutor) . "'"  ;}
			//if(trim($apeAutor)==""){$apeAutor='NULL';}else{ $apeAutor= "'" . trim($apeAutor) . "'"  ;}
			//if(trim($seudoAutor)==""){$seudoAutor='NULL';}else{ $seudoAutor= "'" . trim($seudoAutor) . "'"  ;}
			if(trim($idPais)==""){$idPais='NULL';}else{ $idPais= "'" . trim($idPais) . "'"  ;}
			if(trim($idUsuario)==""){$idUsuario='NULL';}else{ $idUsuario= "'" . trim($idUsuario) . "'"  ;}
			if(trim($ip)==""){$ip='NULL';}else{ $ip= "'" . trim($ip) . "'"  ;}
						
			$sql = "CALL USP_BTK_AUTOR_EDITAR ($id, $idAutorTipo, $nomAutor,$apeAutor, $seudoAutor, $idPais,$idUsuario,$ip)";
			
			$stm = $conexion->query($sql);
			$result = $stm->fetch();
			return $result;

		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_inactivar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_AUTOR_INACTIVAR '
              . '(:ID_AUTOR,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_AUTOR',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

	public function fu_activar($conexion,$id, $idUsuario, $ip) {
		try {
			 
       $stmt = $conexion->prepare(
                'CALL USP_BTK_AUTOR_ACTIVAR '
              . '(:ID_AUTOR,  :ID_USUARIO, :IP )'
       		 );
        	
        	$stmt->bindParam(':ID_AUTOR',      $id,     PDO::PARAM_INT);
        	$stmt->bindParam(':ID_USUARIO',        	$idUsuario,  PDO::PARAM_INT);
        	$stmt->bindParam(':IP',      			$ip,     PDO::PARAM_STR);

         $passed = $stmt->execute();

            if ($passed)
                $var = 1;
            else
                $var = 0;
      
		return $var;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
	}

public function fu_listarTodo($conexion) {
		try {
			
			$sql 	= "SELECT ID_AUTOR,NOM_AUTOR,APE_AUTOR,DES_SEUDONIMO,ID_PAIS,CONCAT(NOM_AUTOR,' ',APE_AUTOR) AS DATOS FROM BTK_AUTOR WHERE IND_ACTIVO = 1 ORDER BY 2,3";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}

public function fu_listarAutorTipo($conexion) {
		try {
			
			$sql 	= "SELECT ID_AUTOR_TIPO,DES_TIPO FROM BTK_AUTOR_TIPO WHERE IND_ACTIVO = 1 ORDER BY 1";
			
			$stm 	= $conexion->query($sql);
			$result = $stm->fetchAll();

		return $result;
		} catch (PDOException $error) {
			return $error;
			exit();
		 }
}


}
?>