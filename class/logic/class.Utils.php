<?php
/**
 *
 * [Nombre_proyecto_aplicacion] :: Clase Utilitarios "Utils"
 *
 * PHP version 5
 *
 *
 * LICENSE:
 *
 * This library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any
 * later version.
 *
 * @category	ToolsAndUtilities
 * @author		Carlos Zegarra 
 * @copyright	2012 
 * @license		http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @filesource
 * @link		http://www.phpdoc.org
 * @link		http://pear.php.net/PhpDocumentor
 * @todo      	CS cleanup - change package to PhpDocumentor
 *
 *
 * 0. MEtodos en comun para las aplicaciones
 *
 */

class Logic_Utils {

	/**
	 * Retorna la IP del visitante
	 */
	public static function getIP() {

		$ip = "";

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			//check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			//to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	/**
	 * Dar formato a la fecha.
	 * Ejemplo: 2011-01-01 ----- 01/01/2011
	 * Ejemplo: 2011-01-01 08:30:50 ----- 01/01/2011 08:30:50
	 */
	public static function converDateToView($fecha) {
		$resultado = '';

		if($fecha != '' || $fecha != '-'):
		if($fecha != '0000-00-00 00:00:00'):
		$fecha = trim($fecha);

		if($fecha != '' && $fecha != NULL):
		$posicion = strrpos($fecha, " ");
		if($posicion === false) {
			$array_ffecha = explode('-',$fecha);
			$year = $array_ffecha[0];
			$month = $array_ffecha[1];
			$day = $array_ffecha[2];

			$resultado = $day.'-'.$month.'-'.$year;
		} else {
			$array_fecha = explode(' ',$fecha);
			if(count($array_fecha) == 2) {
				$array_ffecha = explode('-',$array_fecha[0]);
				$year = $array_ffecha[0];
				$month = $array_ffecha[1];
				$day = $array_ffecha[2];

				$array_fhora = explode(':',$array_fecha[1]);
				$hour = $array_fhora[0];
				$minute = $array_fhora[1];
				/* Modificado por Carlos Zegarra - 08/02/2012 */
				if(strlen($array_fecha[1])==8){
                                    $second = $array_fhora[2];
                                } else {
                                    $second = "00";
                                }

				$resultado = $day.'-'.$month.'-'.$year.' '.$hour.':'.$minute.':'.$second;
			}
		}
		endif;
		endif;
		endif;

		return $resultado;
	}
	
	public static function zerofill($entero, $largo)
{
    $entero = (int)$entero;   
	$largo = (int)$largo;
	$relleno = '';
	
	    if (strlen($entero) < $largo) 
		{        
		$relleno = str_repeat('0', $largo - strlen($entero));    
		}
		return $relleno . $entero;
}
	public static function converDateFromView($fecha) {
		$resultado = '';

		if($fecha != '' || $fecha != '-'):
			if($fecha != '00-00-0000'):
				$fecha = trim($fecha);
		
				if($fecha != '' && $fecha != NULL):
				$posicion = strrpos($fecha, " ");
				if($posicion === false) {
					$array_ffecha = explode('-',$fecha);
					$year = $array_ffecha[2];
					$month = $array_ffecha[1];
					$day = $array_ffecha[0];
		
					$resultado = $year.'-'.$month.'-'.$day;
				} else {
					$array_fecha = explode(' ',$fecha);
					if(count($array_fecha) == 2) {
						$array_ffecha = explode('-',$array_fecha[0]);
						$year = $array_ffecha[2];
						$month = $array_ffecha[1];
						$day = $array_ffecha[0];
		
						$array_fhora = explode(':',$array_fecha[1]);
						$hour = $array_fhora[0];
						$minute = $array_fhora[1];
						/* Modificado por Carlos Zegarra - 08/02/2012 */
						if(strlen($array_fecha[1])==8){
											$second = $array_fhora[2];
										} else {
											$second = "00";
										}
		
						$resultado = $year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':'.$second;
					}
				}
				endif;
			endif;
		endif;

		return $resultado;
	}
	
	function limpiar_cadena($document){ 
	
	$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript 
				   '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags 
				   '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly 
				   '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA 
	); 
	
	$text = preg_replace($search, '', $document); 
	$text=str_replace("'","´",$text);
	$text = preg_replace("/\r*\n/","\\n",$text);
	$text = preg_replace("/\//","\\\/",$text);
	$text = preg_replace("/\"/","\\\"",$text);
	$text = preg_replace("/'/"," ",$text);
	//$text=addcslashes($text,"\0..\37!@\@\177..\377");
	$text = htmlspecialchars($text);
	$text = preg_replace("/=/", "=\"\"", $text);
	$text = preg_replace("/&quot;/", "&quot;\"", $text);
	$tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
	$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
	$text = preg_replace($tags, $replacement, $text);
	$text = preg_replace("/=\"\"/", "=", $text);
	
	//"+",
	$text = str_replace(
			array("\\","|", "\"",
				 "/",
				 "^", 
				 ">", "<"),
			'', $text);
			
	$text = str_replace(
			array("~","Δ","€","ƒ","…","†","‡",
				  "ˆ","‰","Š","‹","Œ","Ž","'","'","•","–","—","˜","™","š","›","œ","ž","¢","£","¤","¥","¦","§","¨","©",
				  "ª","«","¬","®","¯","°","±","²","³","´","µ","¶","·","¸","¹","º","»","¼","½","¾","Æ","×","Ø","Þ","ß","æ",
				  "ø","þ"),
				   '', $text);		
	
	$no_permitidas= array ("á","é","í","ó","ú","Á","ñ","Ñ","É","Í","Ó","Ú","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
	$si_permitidas= array ("a","e","i","o","u","A","Ñ","Ñ","E","I","O","U","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
	$text = str_replace($no_permitidas, $si_permitidas ,$text);	
	
	return $text; 
	}
	//16/07/2015 inicio
	function limpiar_cadena_($document){ 
	
	$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript 
				   '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags 
				   '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly 
				   '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA 
	); 
	
	$text = preg_replace($search, '', $document); 
	$text=str_replace("'","´",$text);
	$text = preg_replace("/\r*\n/","\\n",$text);
	$text = preg_replace("/\//","\\\/",$text);
	$text = preg_replace("/\"/","\\\"",$text);
	$text = preg_replace("/'/"," ",$text);
	//$text=addcslashes($text,"\0..\37!@\@\177..\377");
	//$text = htmlspecialchars($text);
	$text = preg_replace("/=/", "=\"\"", $text);
	$text = preg_replace("/&quot;/", "&quot;\"", $text);
	$tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
	$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
	$text = preg_replace($tags, $replacement, $text);
	$text = preg_replace("/=\"\"/", "=", $text);
	
	//"+",
	$text = str_replace(
			array("\\","|", "\"",
				 "/",
				 "^", 
				 ">", "<"),
			'', $text);
			
	 /*$text = str_replace(
			array("#","$","%","&","'","(",")","*","+","?","[","]","^","_","`","{","|","}","~","Δ","€","‚","ƒ","…","†","‡",
				  "ˆ","‰","Š","‹","Œ","Ž","'","'","•","–","—","˜","™","š","›","œ","ž","Ÿ","¡","¢","£","¤","¥","¦","§","¨","©",
				  "ª","«","¬","®","¯","°","±","²","³","´","µ","¶","·","¸","¹","º","»","¼","½","¾","¿","Â","Ã","Ä","Å",
				  "Æ","Ç","Ê","Ì","Î","Ï","Ð","Ò","Ô","Õ","Ö","×","Ø","Û","Ý","Þ","ß","â","ã","ä","å","æ",
				  "ç","ê","ë","î","ï","ð","ô","õ","ö","÷","ø","û","þ","ÿ"),
				   '', $text);*/
	
	$text = str_replace(
			array("~","Δ","€","‚","ƒ","…","†","‡",
				  "ˆ","‰","Š","‹","Œ","Ž","'","'","•","—","˜","™","š","›","œ","ž","¢","£","¤","¥","¦","§","¨","©",
				  "ª","«","¬","®","¯","°","±","²","³","´","µ","¶","·","¸","¹","º","»","¼","½","¾","¿","Æ","×","Ø","Þ","ß","æ",
				  "ø","þ"),
				   '', $text);				   
	
	return $text; 
	}
	//16/07/2015 fin
	
	function check_email_address($email) 
{
	// Primero, checamos que solo haya un símbolo @, y que los largos sean correctos
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) 
	{
		// correo inválido por número incorrecto de caracteres en una parte, o número incorrecto de símbolos @
    return false;
  }
  // se divide en partes para hacerlo más sencillo
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) 
	{
    if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) 
		{
      return false;
    }
  } 
  // se revisa si el dominio es una IP. Si no, debe ser un nombre de dominio válido
	if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) 
	{ 
     $domain_array = explode(".", $email_array[1]);
     if (sizeof($domain_array) < 2) 
		 {
        return false; // No son suficientes partes o secciones para se un dominio
     }
     for ($i = 0; $i < sizeof($domain_array); $i++) 
		 {
        if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) 
				{
           return false;
        }
     }
  }
  return true;
}
	
}
?>
